
# EU Exit
defined_text = {
	name = ALB_EU_exit_Loc
	text = { trigger = { ALB = { has_country_flag = Article_50 } }
		localization_key = ALB_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = ARM_EU_exit_Loc
	text = { trigger = { ARM = { has_country_flag = Article_50 } }
		localization_key = ARM_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = AUS_EU_exit_Loc
	text = { trigger = { AUS = { has_country_flag = Article_50 } }
		localization_key = AUS_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = AZE_EU_exit_Loc
	text = { trigger = { AZE = { has_country_flag = Article_50 } }
		localization_key = AZE_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = BEL_EU_exit_Loc
	text = { trigger = { BEL = { has_country_flag = Article_50 } }
		localization_key = BEL_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = BLR_EU_exit_Loc
	text = { trigger = { BLR = { has_country_flag = Article_50 } }
		localization_key = BLR_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = BOS_EU_exit_Loc
	text = { trigger = { BOS = { has_country_flag = Article_50 } }
		localization_key = BOS_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = BUL_EU_exit_Loc
	text = { trigger = { BUL = { has_country_flag = Article_50 } }
		localization_key = BUL_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = CAT_EU_exit_Loc
	text = { trigger = { CAT = { has_country_flag = Article_50 } }
		localization_key = CAT_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = CRE_EU_exit_Loc
	text = { trigger = { CRE = { has_country_flag = Article_50 } }
		localization_key = CRE_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = CRO_EU_exit_Loc
	text = { trigger = { CRO = { has_country_flag = Article_50 } }
		localization_key = CRO_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = CYP_EU_exit_Loc
	text = { trigger = { CYP = { has_country_flag = Article_50 } }
		localization_key = CYP_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = CZE_EU_exit_Loc
	text = { trigger = { CZE = { has_country_flag = Article_50 } }
		localization_key = CZE_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = DEN_EU_exit_Loc
	text = { trigger = { DEN = { has_country_flag = Article_50 } }
		localization_key = DEN_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = ENG_EU_exit_Loc
	text = { trigger = { ENG = { has_country_flag = Article_50 } }
		localization_key = ENG_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = EST_EU_exit_Loc
	text = { trigger = { EST = { has_country_flag = Article_50 } }
		localization_key = EST_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = FIN_EU_exit_Loc
	text = { trigger = { FIN = { has_country_flag = Article_50 } }
		localization_key = FIN_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = FRA_EU_exit_Loc
	text = { trigger = { FRA = { has_country_flag = Article_50 } }
		localization_key = FRA_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = FYR_EU_exit_Loc
	text = { trigger = { FYR = { has_country_flag = Article_50 } }
		localization_key = FYR_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = GEO_EU_exit_Loc
	text = { trigger = { GEO = { has_country_flag = Article_50 } }
		localization_key = GEO_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = GER_EU_exit_Loc
	text = { trigger = { GER = { has_country_flag = Article_50 } }
		localization_key = GER_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = GRE_EU_exit_Loc
	text = { trigger = { GRE = { has_country_flag = Article_50 } }
		localization_key = GRE_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = HOL_EU_exit_Loc
	text = { trigger = { HOL = { has_country_flag = Article_50 } }
		localization_key = HOL_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = HUN_EU_exit_Loc
	text = { trigger = { HUN = { has_country_flag = Article_50 } }
		localization_key = HUN_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = ICE_EU_exit_Loc
	text = { trigger = { ICE = { has_country_flag = Article_50 } }
		localization_key = ICE_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = IRE_EU_exit_Loc
	text = { trigger = { IRE = { has_country_flag = Article_50 } }
		localization_key = IRE_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = ITA_EU_exit_Loc
	text = { trigger = { ITA = { has_country_flag = Article_50 } }
		localization_key = ITA_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = KOS_EU_exit_Loc
	text = { trigger = { KOS = { has_country_flag = Article_50 } }
		localization_key = KOS_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = LAT_EU_exit_Loc
	text = { trigger = { LAT = { has_country_flag = Article_50 } }
		localization_key = LAT_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = LIT_EU_exit_Loc
	text = { trigger = { LIT = { has_country_flag = Article_50 } }
		localization_key = LIT_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = LUX_EU_exit_Loc
	text = { trigger = { LUX = { has_country_flag = Article_50 } }
		localization_key = LUX_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = MLT_EU_exit_Loc
	text = { trigger = { MLT = { has_country_flag = Article_50 } }
		localization_key = MLT_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = MLV_EU_exit_Loc
	text = { trigger = { MLV = { has_country_flag = Article_50 } }
		localization_key = MLV_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = MNT_EU_exit_Loc
	text = { trigger = { MNT = { has_country_flag = Article_50 } }
		localization_key = MNT_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = NOR_EU_exit_Loc
	text = { trigger = { NOR = { has_country_flag = Article_50 } }
		localization_key = NOR_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = POL_EU_exit_Loc
	text = { trigger = { POL = { has_country_flag = Article_50 } }
		localization_key = POL_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = POR_EU_exit_Loc
	text = { trigger = { POR = { has_country_flag = Article_50 } }
		localization_key = POR_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = ROM_EU_exit_Loc
	text = { trigger = { ROM = { has_country_flag = Article_50 } }
		localization_key = ROM_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = SCL_EU_exit_Loc
	text = { trigger = { SCL = { has_country_flag = Article_50 } }
		localization_key = SCL_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = SCO_EU_exit_Loc
	text = { trigger = { SCO = { has_country_flag = Article_50 } }
		localization_key = SCO_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = SER_EU_exit_Loc
	text = { trigger = { SER = { has_country_flag = Article_50 } }
		localization_key = SER_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = SLO_EU_exit_Loc
	text = { trigger = { SLO = { has_country_flag = Article_50 } }
		localization_key = SLO_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = SLV_EU_exit_Loc
	text = { trigger = { SLV = { has_country_flag = Article_50 } }
		localization_key = SLV_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = SPR_EU_exit_Loc
	text = { trigger = { SPR = { has_country_flag = Article_50 } }
		localization_key = SPR_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = SWE_EU_exit_Loc
	text = { trigger = { SWE = { has_country_flag = Article_50 } }
		localization_key = SWE_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = SWI_EU_exit_Loc
	text = { trigger = { SWI = { has_country_flag = Article_50 } }
		localization_key = SWI_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = TUR_EU_exit_Loc
	text = { trigger = { TUR = { has_country_flag = Article_50 } }
		localization_key = TUR_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = UKR_EU_exit_Loc
	text = { trigger = { UKR = { has_country_flag = Article_50 } }
		localization_key = UKR_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = WAS_EU_exit_Loc
	text = { trigger = { WAS = { has_country_flag = Article_50 } }
		localization_key = WAS_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = HLS_EU_exit_Loc
	text = { trigger = { HLS = { has_country_flag = Article_50 } }
		localization_key = HLS_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = SMA_EU_exit_Loc
	text = { trigger = { SMA = { has_country_flag = Article_50 } }
		localization_key = SMA_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = MNC_EU_exit_Loc
	text = { trigger = { MNC = { has_country_flag = Article_50 } }
		localization_key = MNC_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = ADO_EU_exit_Loc
	text = { trigger = { ADO = { has_country_flag = Article_50 } }
		localization_key = ADO_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = LIC_EU_exit_Loc
	text = { trigger = { LIC = { has_country_flag = Article_50 } }
		localization_key = LIC_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = GAL_EU_exit_Loc
	text = { trigger = { GAL = { has_country_flag = Article_50 } }
		localization_key = GAL_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = SIL_EU_exit_Loc
	text = { trigger = { SIL = { has_country_flag = Article_50 } }
		localization_key = SIL_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = SPA_EU_exit_Loc
	text = { trigger = { SPA = { has_country_flag = Article_50 } }
		localization_key = SPA_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = CNR_EU_exit_Loc
	text = { trigger = { CNR = { has_country_flag = Article_50 } }
		localization_key = CNR_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = NAV_EU_exit_Loc
	text = { trigger = { NAV = { has_country_flag = Article_50 } }
		localization_key = NAV_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = BAY_EU_exit_Loc
	text = { trigger = { BAY = { has_country_flag = Article_50 } }
		localization_key = BAY_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = GRL_EU_exit_Loc
	text = { trigger = { GRL = { has_country_flag = Article_50 } }
		localization_key = GRL_Article_50_YES
	}
	text = { localization_key = "" }
}

# ----------- #
##########################
### EU POTEF polls Loc ###
##########################

defined_text = {
	name = POTEF_polls_loc
	text = {
		trigger = {
			has_country_flag = POTEF_polls
		}
		localization_key = show_POTEF_polls_loc_key
	}
	text = {
		trigger = {
			NOT = { has_country_flag = POTEF_polls }
		}
		localization_key = hide_POTEF_polls_loc_key
	}
}

defined_text = {
	name = EU_POTEF_Western_Autocracy
	text = {
		localization_key = EU_POTEF_Western_Autocracy_loc_key
	}
}
defined_text = {
	name = EU_POTEF_conservatism
	text = {
		localization_key = EU_POTEF_conservatism_loc_key
	}
}
defined_text = {
	name = EU_POTEF_liberalism
	text = {
		localization_key = EU_POTEF_liberalism_loc_key
	}
}
defined_text = {
	name = EU_POTEF_socialism
	text = {
		localization_key = EU_POTEF_socialism_loc_key
	}
}
defined_text = {
	name = EU_POTEF_Communist-State
	text = {
		localization_key = EU_POTEF_Communist-State_loc_key
	}
}
defined_text = {
	name = EU_POTEF_anarchist_communism
	text = {
		localization_key = EU_POTEF_anarchist_communism_loc_key
	}
}
defined_text = {
	name = EU_POTEF_Conservative
	text = {
		localization_key = EU_POTEF_Conservative_loc_key
	}
}
defined_text = {
	name = EU_POTEF_Autocracy
	text = {
		localization_key = EU_POTEF_Autocracy_loc_key
	}
}
defined_text = {
	name = EU_POTEF_Mod_Vilayat_e_Faqih
	text = {
		localization_key = EU_POTEF_Mod_Vilayat_e_Faqih_loc_key
	}
}
defined_text = {
	name = EU_POTEF_Vilayat_e_Faqih
	text = {
		localization_key = EU_POTEF_Vilayat_e_Faqih_loc_key
	}
}
defined_text = {
	name = EU_POTEF_Kingdom
	text = {
		localization_key = EU_POTEF_Kingdom_loc_key
	}
}
defined_text = {
	name = EU_POTEF_Caliphate
	text = {
		localization_key = EU_POTEF_Caliphate_loc_key
	}
}
defined_text = {
	name = EU_POTEF_Neutral_Muslim_Brotherhood
	text = {
		localization_key = EU_POTEF_Neutral_Muslim_Brotherhood_loc_key
	}
}
defined_text = {
	name = EU_POTEF_Neutral_Autocracy
	text = {
		localization_key = EU_POTEF_Neutral_Autocracy_loc_key
	}
}
defined_text = {
	name = EU_POTEF_Neutral_conservatism
	text = {
		localization_key = EU_POTEF_Neutral_conservatism_loc_key
	}
}
defined_text = {
	name = EU_POTEF_oligarchism
	text = {
		localization_key = EU_POTEF_oligarchism_loc_key
	}
}
defined_text = {
	name = EU_POTEF_Neutral_Libertarian
	text = {
		localization_key = EU_POTEF_Neutral_Libertarian_loc_key
	}
}
defined_text = {
	name = EU_POTEF_Neutral_green
	text = {
		localization_key = EU_POTEF_Neutral_green_loc_key
	}
}
defined_text = {
	name = EU_POTEF_neutral_Social
	text = {
		localization_key = EU_POTEF_neutral_Social_loc_key
	}
}
defined_text = {
	name = EU_POTEF_Neutral_Communism
	text = {
		localization_key = EU_POTEF_Neutral_Communism_loc_key
	}
}
defined_text = {
	name = EU_POTEF_Nat_Populism
	text = {
		localization_key = EU_POTEF_Nat_Populism_loc_key
	}
}
defined_text = {
	name = EU_POTEF_Nat_Fascism
	text = {
		localization_key = EU_POTEF_Nat_Fascism_loc_key
	}
}
defined_text = {
	name = EU_POTEF_Nat_Autocracy
	text = {
		localization_key = EU_POTEF_Nat_Autocracy_loc_key
	}
}
defined_text = {
	name = EU_POTEF_Monarchist
	text = {
		localization_key = EU_POTEF_Monarchist_loc_key
	}
}

### POTEF nominee ###

defined_text = {
	name = EU_POTEF_Western_Autocracy_nominee
	text = {
		trigger = {
			any_of_scopes = {
				array = global.EU_member
				has_country_flag = POTEF_nominee_0
			}
		}
		localization_key = EU_POTEF_Western_Autocracy_nominee_loc_key
	}
}
defined_text = {
	name = EU_POTEF_conservatism_nominee
	text = {
		trigger = {
			any_of_scopes = {
				array = global.EU_member
				has_country_flag = POTEF_nominee_1
			}
		}
		localization_key = EU_POTEF_conservatism_nominee_loc_key
	}
}
defined_text = {
	name = EU_POTEF_liberalism_nominee
	text = {
		trigger = {
			any_of_scopes = {
				array = global.EU_member
				has_country_flag = POTEF_nominee_2
			}
		}
		localization_key = EU_POTEF_liberalism_nominee_loc_key
	}
}
defined_text = {
	name = EU_POTEF_socialism_nominee
	text = {
		trigger = {
			any_of_scopes = {
				array = global.EU_member
				has_country_flag = POTEF_nominee_3
			}
		}
		localization_key = EU_POTEF_socialism_nominee_loc_key
	}
}
defined_text = {
	name = EU_POTEF_Communist-State_nominee
	text = {
		trigger = {
			any_of_scopes = {
				array = global.EU_member
				has_country_flag = POTEF_nominee_4
			}
		}
		localization_key = EU_POTEF_Communist-State_nominee_loc_key
	}
}
defined_text = {
	name = EU_POTEF_anarchist_communism_nominee
	text = {
		trigger = {
			any_of_scopes = {
				array = global.EU_member
				has_country_flag = POTEF_nominee_5
			}
		}
		localization_key = EU_POTEF_anarchist_communism_nominee_loc_key
	}
}
defined_text = {
	name = EU_POTEF_Conservative_nominee
	text = {
		trigger = {
			any_of_scopes = {
				array = global.EU_member
				has_country_flag = POTEF_nominee_6
			}
		}
		localization_key = EU_POTEF_Conservative_nominee_loc_key
	}
}
defined_text = {
	name = EU_POTEF_Autocracy_nominee
	text = {
		trigger = {
			any_of_scopes = {
				array = global.EU_member
				has_country_flag = POTEF_nominee_7
			}
		}
		localization_key = EU_POTEF_Autocracy_nominee_loc_key
	}
}
defined_text = {
	name = EU_POTEF_Mod_Vilayat_e_Faqih_nominee
	text = {
		trigger = {
			any_of_scopes = {
				array = global.EU_member
				has_country_flag = POTEF_nominee_8
			}
		}
		localization_key = EU_POTEF_Mod_Vilayat_e_Faqih_nominee_loc_key
	}
}
defined_text = {
	name = EU_POTEF_Vilayat_e_Faqih_nominee
	text = {
		trigger = {
			any_of_scopes = {
				array = global.EU_member
				has_country_flag = POTEF_nominee_9
			}
		}
		localization_key = EU_POTEF_Vilayat_e_Faqih_nominee_loc_key
	}
}
defined_text = {
	name = EU_POTEF_Kingdom_nominee
	text = {
		trigger = {
			any_of_scopes = {
				array = global.EU_member
				has_country_flag = POTEF_nominee_10
			}
		}
		localization_key = EU_POTEF_Kingdom_nominee_loc_key
	}
}
defined_text = {
	name = EU_POTEF_Caliphate_nominee
	text = {
		trigger = {
			any_of_scopes = {
				array = global.EU_member
				has_country_flag = POTEF_nominee_11
			}
		}
		localization_key = EU_POTEF_Caliphate_nominee_loc_key
	}
}
defined_text = {
	name = EU_POTEF_Neutral_Muslim_Brotherhood_nominee
	text = {
		trigger = {
			any_of_scopes = {
				array = global.EU_member
				has_country_flag = POTEF_nominee_12
			}
		}
		localization_key = EU_POTEF_Neutral_Muslim_Brotherhood_nominee_loc_key
	}
}
defined_text = {
	name = EU_POTEF_Neutral_Autocracy_nominee
	text = {
		trigger = {
			any_of_scopes = {
				array = global.EU_member
				has_country_flag = POTEF_nominee_13
			}
		}
		localization_key = EU_POTEF_Neutral_Autocracy_nominee_loc_key
	}
}
defined_text = {
	name = EU_POTEF_Neutral_conservatism_nominee
	text = {
		trigger = {
			any_of_scopes = {
				array = global.EU_member
				has_country_flag = POTEF_nominee_14
			}
		}
		localization_key = EU_POTEF_Neutral_conservatism_nominee_loc_key
	}
}
defined_text = {
	name = EU_POTEF_oligarchism_nominee
	text = {
		trigger = {
			any_of_scopes = {
				array = global.EU_member
				has_country_flag = POTEF_nominee_15
			}
		}
		localization_key = EU_POTEF_oligarchism_nominee_loc_key
	}
}
defined_text = {
	name = EU_POTEF_Neutral_Libertarian_nominee
	text = {
		trigger = {
			any_of_scopes = {
				array = global.EU_member
				has_country_flag = POTEF_nominee_16
			}
		}
		localization_key = EU_POTEF_Neutral_Libertarian_nominee_loc_key
	}
}
defined_text = {
	name = EU_POTEF_Neutral_green_nominee
	text = {
		trigger = {
			any_of_scopes = {
				array = global.EU_member
				has_country_flag = POTEF_nominee_17
			}
		}
		localization_key = EU_POTEF_Neutral_green_nominee_loc_key
	}
}
defined_text = {
	name = EU_POTEF_neutral_Social_nominee
	text = {
		trigger = {
			any_of_scopes = {
				array = global.EU_member
				has_country_flag = POTEF_nominee_18
			}
		}
		localization_key = EU_POTEF_neutral_Social_nominee_loc_key
	}
}
defined_text = {
	name = EU_POTEF_Neutral_Communism_nominee
	text = {
		trigger = {
			any_of_scopes = {
				array = global.EU_member
				has_country_flag = POTEF_nominee_19
			}
		}
		localization_key = EU_POTEF_Neutral_Communism_nominee_loc_key
	}
}
defined_text = {
	name = EU_POTEF_Nat_Populism_nominee
	text = {
		trigger = {
			any_of_scopes = {
				array = global.EU_member
				has_country_flag = POTEF_nominee_20
			}
		}
		localization_key = EU_POTEF_Nat_Populism_nominee_loc_key
	}
}
defined_text = {
	name = EU_POTEF_Nat_Fascism_nominee
	text = {
		trigger = {
			any_of_scopes = {
				array = global.EU_member
				has_country_flag = POTEF_nominee_21
			}
		}
		localization_key = EU_POTEF_Nat_Fascism_nominee_loc_key
	}
}
defined_text = {
	name = EU_POTEF_Nat_Autocracy_nominee
	text = {
		trigger = {
			any_of_scopes = {
				array = global.EU_member
				has_country_flag = POTEF_nominee_22
			}
		}
		localization_key = EU_POTEF_Nat_Autocracy_nominee_loc_key
	}
}
defined_text = {
	name = EU_POTEF_Monarchist_nominee
	text = {
		trigger = {
			any_of_scopes = {
				array = global.EU_member
				has_country_flag = POTEF_nominee_23
			}
		}
		localization_key = EU_POTEF_Monarchist_nominee_loc_key
	}
}

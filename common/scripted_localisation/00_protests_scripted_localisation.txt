defined_text = {
	name = adm_spending_protest_TT
	text = {
		trigger = {
			check_variable = { THIS.expected_adm_spending > THIS.bureaucracy }
		}
		localization_key = red_adm_spending_TT
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_adm_spending < THIS.bureaucracy }
		}
		localization_key = green_adm_spending_TT
	}
	text = {
		localization_key = yellow_adm_spending_TT
	}
}

defined_text = {
	name = police_spending_protest_TT
	text = {
		trigger = {
			check_variable = { THIS.expected_police_spending > THIS.crime_fighting }
		}
		localization_key = red_police_spending_TT
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_police_spending < THIS.crime_fighting }
		}
		localization_key = green_police_spending_TT
	}
	text = {
		localization_key = yellow_police_spending_TT
	}
}

defined_text = {
	name = edu_spending_protest_TT
	text = {
		trigger = {
			check_variable = { THIS.expected_edu_spending > THIS.education_budget }
		}
		localization_key = red_edu_spending_TT
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_edu_spending < THIS.education_budget }
		}
		localization_key = green_edu_spending_TT
	}
	text = {
		localization_key = yellow_edu_spending_TT
	}
}

defined_text = {
	name = health_spending_protest_TT
	text = {
		trigger = {
			check_variable = { THIS.expected_health_spending > THIS.health_budget }
		}
		localization_key = red_health_spending_TT
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_health_spending < THIS.health_budget }
		}
		localization_key = green_health_spending_TT
	}
	text = {
		localization_key = yellow_health_spending_TT
	}
}

defined_text = {
	name = social_spending_protest_TT
	text = {
		trigger = {
			check_variable = { THIS.expected_welfare_spending > THIS.social_budget }
		}
		localization_key = red_social_spending_TT
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_welfare_spending < THIS.social_budget }
		}
		localization_key = green_social_spending_TT
	}
	text = {
		localization_key = yellow_social_spending_TT
	}
}

defined_text = {
	name = military_spending_protest_TT
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp > THIS.Military_Spending }
		}
		localization_key = red_military_spending_TT
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp < THIS.Military_Spending }
		}
		localization_key = green_military_spending_TT
	}
	text = {
		localization_key = yellow_military_spending_TT
	}
}

#desired spending as text
defined_text = {
	name = exp_adm_spending_text_TT
	text = {
		trigger = {
			check_variable = { THIS.expected_adm_spending = 1 }
		}
		localization_key = bureau_01
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_adm_spending = 2 }
		}
		localization_key = bureau_02
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_adm_spending = 3 }
		}
		localization_key = bureau_03
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_adm_spending = 4 }
		}
		localization_key = bureau_04
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_adm_spending = 5 }
		}
		localization_key = bureau_05
	}
}

defined_text = {
	name = exp_police_spending_text_TT
	text = {
		trigger = {
			check_variable = { THIS.expected_police_spending = 1 }
		}
		localization_key = police_01
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_police_spending = 2 }
		}
		localization_key = police_02
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_police_spending = 3 }
		}
		localization_key = police_03
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_police_spending = 4 }
		}
		localization_key = police_04
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_police_spending = 5 }
		}
		localization_key = police_05
	}
}

defined_text = {
	name = exp_edu_spending_text_TT
	text = {
		trigger = {
			check_variable = { THIS.expected_edu_spending = 1 }
		}
		localization_key = edu_01
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_edu_spending = 2 }
		}
		localization_key = edu_02
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_edu_spending = 3 }
		}
		localization_key = edu_03
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_edu_spending = 4 }
		}
		localization_key = edu_04
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_edu_spending = 5 }
		}
		localization_key = edu_05
	}
}

defined_text = {
	name = exp_health_spending_text_TT
	text = {
		trigger = {
			check_variable = { THIS.expected_health_spending = 1 }
		}
		localization_key = health_01
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_health_spending = 2 }
		}
		localization_key = health_02
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_health_spending = 3 }
		}
		localization_key = health_03
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_health_spending = 4 }
		}
		localization_key = health_04
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_health_spending = 5 }
		}
		localization_key = health_05
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_health_spending = 6 }
		}
		localization_key = health_06
	}
}

defined_text = {
	name = exp_social_spending_text_TT
	text = {
		trigger = {
			check_variable = { THIS.expected_welfare_spending = 1 }
		}
		localization_key = social_01
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_welfare_spending = 2 }
		}
		localization_key = social_02
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_welfare_spending = 3 }
		}
		localization_key = social_03
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_welfare_spending = 4 }
		}
		localization_key = social_04
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_welfare_spending = 5 }
		}
		localization_key = social_05
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_welfare_spending = 6 }
		}
		localization_key = social_06
	}
}

defined_text = {
	name = exp_military_spending_text_TT
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp = 1 }
		}
		localization_key = defence_00
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp = 2 }
		}
		localization_key = defence_01
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp = 3 }
		}
		localization_key = defence_02
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp = 4 }
		}
		localization_key = defence_03
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp = 5 }
		}
		localization_key = defence_04
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp = 6 }
		}
		localization_key = defence_05
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp = 7 }
		}
		localization_key = defence_06
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp = 8 }
		}
		localization_key = defence_07
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp = 9 }
		}
		localization_key = defence_08
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp = 10 }
		}
		localization_key = defence_09
	}
}

defined_text = {
	name = protest_strength_TT
	text = {
		trigger = {
			check_variable = { THIS.protest_strength < 20 }
		}
		localization_key = green_protest_strength_TT
	}
	text = {
		trigger = {
			check_variable = { THIS.protest_strength < 50 }
		}
		localization_key = yellow_protest_strength_TT
	}
	text = {
		localization_key = red_protest_strength_TT
	}
}

defined_text = {
	name = protest_radicalisation_TT
	text = {
		trigger = {
			check_variable = { THIS.protest_radicalisation < 2 }
		}
		localization_key = green_protest_radicalisation_TT
	}
	text = {
		trigger = {
			check_variable = { THIS.protest_radicalisation < 5 }
		}
		localization_key = yellow_protest_radicalisation_TT
	}
	text = {
		localization_key = red_protest_radicalisation_TT
	}
}

defined_text = {
	name = fuel_protests_TT
	text = {
		trigger = {
			has_fuel < 1
		}
		localization_key = no_fuel_protests_TT
	}
	text = {
		localization_key = fuel_protests_TT
	}
}

defined_text = {
	name = adm_spending_protest_title_1
	text = {
		trigger = {
			check_variable = { THIS.expected_adm_spending = 1 }
		}
		localization_key = bureau_01_expected
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_adm_spending > 1 }
			has_idea = bureau_01
		}
		localization_key = bureau_01_under
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_adm_spending < 1 }
			has_idea = bureau_01
		}
		localization_key = bureau_01_over
	}
	text = {
		localization_key = bureau_01_neutral
	}
}
defined_text = {
	name = adm_spending_protest_title_2
	text = {
		trigger = {
			check_variable = { THIS.expected_adm_spending = 2 }
		}
		localization_key = bureau_02_expected
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_adm_spending > 2 }
			has_idea = bureau_02
		}
		localization_key = bureau_02_under
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_adm_spending < 2 }
			has_idea = bureau_02
		}
		localization_key = bureau_02_over
	}
	text = {
		localization_key = bureau_02_neutral
	}
}
defined_text = {
	name = adm_spending_protest_title_3
	text = {
		trigger = {
			check_variable = { THIS.expected_adm_spending = 3 }
		}
		localization_key = bureau_03_expected
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_adm_spending > 3 }
			has_idea = bureau_03
		}
		localization_key = bureau_03_under
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_adm_spending < 3 }
			has_idea = bureau_03
		}
		localization_key = bureau_03_over
	}
	text = {
		localization_key = bureau_03_neutral
	}
}
defined_text = {
	name = adm_spending_protest_title_4
	text = {
		trigger = {
			check_variable = { THIS.expected_adm_spending = 4 }
		}
		localization_key = bureau_04_expected
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_adm_spending > 4 }
			has_idea = bureau_04
		}
		localization_key = bureau_04_under
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_adm_spending < 4 }
			has_idea = bureau_04
		}
		localization_key = bureau_04_over
	}
	text = {
		localization_key = bureau_04_neutral
	}
}
defined_text = {
	name = adm_spending_protest_title_5
	text = {
		trigger = {
			check_variable = { THIS.expected_adm_spending = 5 }
		}
		localization_key = bureau_05_expected
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_adm_spending > 5 }
			has_idea = bureau_05
		}
		localization_key = bureau_05_under
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_adm_spending < 5 }
			has_idea = bureau_05
		}
		localization_key = bureau_05_over
	}
	text = {
		localization_key = bureau_05_neutral
	}
}

defined_text = {
	name = military_spending_protest_title_0
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp = 1 }
		}
		localization_key = defence_00_expected
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp > 1 }
			has_idea = defence_00
		}
		localization_key = defence_00_under
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp < 1 }
			has_idea = defence_00
		}
		localization_key = defence_00_over
	}
	text = {
		localization_key = defence_00_neutral
	}
}
defined_text = {
	name = military_spending_protest_title_1
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp = 2 }
		}
		localization_key = defence_01_expected
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp > 2 }
			has_idea = defence_01
		}
		localization_key = defence_01_under
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp < 2 }
			has_idea = defence_01
		}
		localization_key = defence_01_over
	}
	text = {
		localization_key = defence_01_neutral
	}
}
defined_text = {
	name = military_spending_protest_title_2
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp = 3 }
		}
		localization_key = defence_02_expected
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp > 3 }
			has_idea = defence_02
		}
		localization_key = defence_02_under
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp < 3 }
			has_idea = defence_02
		}
		localization_key = defence_02_over
	}
	text = {
		localization_key = defence_02_neutral
	}
}
defined_text = {
	name = military_spending_protest_title_3
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp = 4 }
		}
		localization_key = defence_03_expected
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp > 4 }
			has_idea = defence_03
		}
		localization_key = defence_03_under
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp < 4 }
			has_idea = defence_03
		}
		localization_key = defence_03_over
	}
	text = {
		localization_key = defence_03_neutral
	}
}
defined_text = {
	name = military_spending_protest_title_4
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp = 5 }
		}
		localization_key = defence_04_expected
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp > 5 }
			has_idea = defence_04
		}
		localization_key = defence_04_under
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp < 5 }
			has_idea = defence_04
		}
		localization_key = defence_04_over
	}
	text = {
		localization_key = defence_04_neutral
	}
}
defined_text = {
	name = military_spending_protest_title_5
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp = 6 }
		}
		localization_key = defence_05_expected
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp > 6 }
			has_idea = defence_05
		}
		localization_key = defence_05_under
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp < 6 }
			has_idea = defence_05
		}
		localization_key = defence_05_over
	}
	text = {
		localization_key = defence_05_neutral
	}
}
defined_text = {
	name = military_spending_protest_title_6
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp = 7 }
		}
		localization_key = defence_06_expected
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp > 7 }
			has_idea = defence_06
		}
		localization_key = defence_06_under
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp < 7 }
			has_idea = defence_06
		}
		localization_key = defence_06_over
	}
	text = {
		localization_key = defence_06_neutral
	}
}
defined_text = {
	name = military_spending_protest_title_7
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp = 8 }
		}
		localization_key = defence_07_expected
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp > 8 }
			has_idea = defence_07
		}
		localization_key = defence_07_under
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp < 8 }
			has_idea = defence_07
		}
		localization_key = defence_07_over
	}
	text = {
		localization_key = defence_07_neutral
	}
}
defined_text = {
	name = military_spending_protest_title_8
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp = 9 }
		}
		localization_key = defence_08_expected
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp > 9 }
			has_idea = defence_08
		}
		localization_key = defence_08_under
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp < 9 }
			has_idea = defence_08
		}
		localization_key = defence_08_over
	}
	text = {
		localization_key = defence_08_neutral
	}
}
defined_text = {
	name = military_spending_protest_title_9
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp = 10 }
		}
		localization_key = defence_09_expected
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp > 10 }
			has_idea = defence_09
		}
		localization_key = defence_09_under
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp < 10 }
			has_idea = defence_09
		}
		localization_key = defence_09_over
	}
	text = {
		localization_key = defence_09_neutral
	}
}

defined_text = {
	name = police_spending_protest_title_1
	text = {
		trigger = {
			check_variable = { THIS.expected_police_spending = 1 }
		}
		localization_key = police_01_expected
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_police_spending > 1 }
			has_idea = police_01
		}
		localization_key = police_01_under
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_police_spending < 1 }
			has_idea = police_01
		}
		localization_key = police_01_over
	}
	text = {
		localization_key = police_01_neutral
	}
}
defined_text = {
	name = police_spending_protest_title_2
	text = {
		trigger = {
			check_variable = { THIS.expected_police_spending = 2 }
		}
		localization_key = police_02_expected
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_police_spending > 2 }
			has_idea = police_02
		}
		localization_key = police_02_under
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_police_spending < 2 }
			has_idea = police_02
		}
		localization_key = police_02_over
	}
	text = {
		localization_key = police_02_neutral
	}
}
defined_text = {
	name = police_spending_protest_title_3
	text = {
		trigger = {
			check_variable = { THIS.expected_police_spending = 3 }
		}
		localization_key = police_03_expected
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_police_spending > 3 }
			has_idea = police_03
		}
		localization_key = police_03_under
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_police_spending < 3 }
			has_idea = police_03
		}
		localization_key = police_03_over
	}
	text = {
		localization_key = police_03_neutral
	}
}
defined_text = {
	name = police_spending_protest_title_4
	text = {
		trigger = {
			check_variable = { THIS.expected_police_spending = 4 }
		}
		localization_key = police_04_expected
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_police_spending > 4 }
			has_idea = police_04
		}
		localization_key = police_04_under
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_police_spending < 4 }
			has_idea = police_04
		}
		localization_key = police_04_over
	}
	text = {
		localization_key = police_04_neutral
	}
}
defined_text = {
	name = police_spending_protest_title_5
	text = {
		trigger = {
			check_variable = { THIS.expected_police_spending = 5 }
		}
		localization_key = police_05_expected
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_police_spending > 5 }
			has_idea = police_05
		}
		localization_key = police_05_under
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_police_spending < 5 }
			has_idea = police_05
		}
		localization_key = police_05_over
	}
	text = {
		localization_key = police_05_neutral
	}
}

defined_text = {
	name = edu_spending_protest_title_1
	text = {
		trigger = {
			check_variable = { THIS.expected_edu_spending = 1 }
		}
		localization_key = edu_01_expected
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_edu_spending > 1 }
			has_idea = edu_01
		}
		localization_key = edu_01_under
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_edu_spending < 1 }
			has_idea = edu_01
		}
		localization_key = edu_01_over
	}
	text = {
		localization_key = edu_01_neutral
	}
}
defined_text = {
	name = edu_spending_protest_title_2
	text = {
		trigger = {
			check_variable = { THIS.expected_edu_spending = 2 }
		}
		localization_key = edu_02_expected
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_edu_spending > 2 }
			has_idea = edu_02
		}
		localization_key = edu_02_under
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_edu_spending < 2 }
			has_idea = edu_02
		}
		localization_key = edu_02_over
	}
	text = {
		localization_key = edu_02_neutral
	}
}
defined_text = {
	name = edu_spending_protest_title_3
	text = {
		trigger = {
			check_variable = { THIS.expected_edu_spending = 3 }
		}
		localization_key = edu_03_expected
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_edu_spending > 3 }
			has_idea = edu_03
		}
		localization_key = edu_03_under
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_edu_spending < 3 }
			has_idea = edu_03
		}
		localization_key = edu_03_over
	}
	text = {
		localization_key = edu_03_neutral
	}
}
defined_text = {
	name = edu_spending_protest_title_4
	text = {
		trigger = {
			check_variable = { THIS.expected_edu_spending = 4 }
		}
		localization_key = edu_04_expected
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_edu_spending > 4 }
			has_idea = edu_04
		}
		localization_key = edu_04_under
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_edu_spending < 4 }
			has_idea = edu_04
		}
		localization_key = edu_04_over
	}
	text = {
		localization_key = edu_04_neutral
	}
}
defined_text = {
	name = edu_spending_protest_title_5
	text = {
		trigger = {
			check_variable = { THIS.expected_edu_spending = 5 }
		}
		localization_key = edu_05_expected
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_edu_spending > 5 }
			has_idea = edu_05
		}
		localization_key = edu_05_under
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_edu_spending < 5 }
			has_idea = edu_05
		}
		localization_key = edu_05_over
	}
	text = {
		localization_key = edu_05_neutral
	}
}

defined_text = {
	name = health_spending_protest_title_1
	text = {
		trigger = {
			check_variable = { THIS.expected_health_spending = 1 }
		}
		localization_key = health_01_expected
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_health_spending > 1 }
			has_idea = health_01
		}
		localization_key = health_01_under
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_health_spending < 1 }
			has_idea = health_01
		}
		localization_key = health_01_over
	}
	text = {
		localization_key = health_01_neutral
	}
}
defined_text = {
	name = health_spending_protest_title_2
	text = {
		trigger = {
			check_variable = { THIS.expected_health_spending = 2 }
		}
		localization_key = health_02_expected
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_health_spending > 2 }
			has_idea = health_02
		}
		localization_key = health_02_under
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_health_spending < 2 }
			has_idea = health_02
		}
		localization_key = health_02_over
	}
	text = {
		localization_key = health_02_neutral
	}
}
defined_text = {
	name = health_spending_protest_title_3
	text = {
		trigger = {
			check_variable = { THIS.expected_health_spending = 3 }
		}
		localization_key = health_03_expected
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_health_spending > 3 }
			has_idea = health_03
		}
		localization_key = health_03_under
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_health_spending < 3 }
			has_idea = health_03
		}
		localization_key = health_03_over
	}
	text = {
		localization_key = health_03_neutral
	}
}
defined_text = {
	name = health_spending_protest_title_4
	text = {
		trigger = {
			check_variable = { THIS.expected_health_spending = 4 }
		}
		localization_key = health_04_expected
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_health_spending > 4 }
			has_idea = health_04
		}
		localization_key = health_04_under
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_health_spending < 4 }
			has_idea = health_04
		}
		localization_key = health_04_over
	}
	text = {
		localization_key = health_04_neutral
	}
}
defined_text = {
	name = health_spending_protest_title_5
	text = {
		trigger = {
			check_variable = { THIS.expected_health_spending = 5 }
		}
		localization_key = health_05_expected
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_health_spending > 5 }
			has_idea = health_05
		}
		localization_key = health_05_under
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_health_spending < 5 }
			has_idea = health_05
		}
		localization_key = health_05_over
	}
	text = {
		localization_key = health_05_neutral
	}
}
defined_text = {
	name = health_spending_protest_title_6
	text = {
		trigger = {
			check_variable = { THIS.expected_health_spending = 6 }
		}
		localization_key = health_06_expected
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_health_spending > 6 }
			has_idea = health_06
		}
		localization_key = health_06_under
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_health_spending < 6 }
			has_idea = health_06
		}
		localization_key = health_06_over
	}
	text = {
		localization_key = health_06_neutral
	}
}

defined_text = {
	name = social_spending_protest_title_1
	text = {
		trigger = {
			check_variable = { THIS.expected_welfare_spending = 1 }
		}
		localization_key = social_01_expected
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_welfare_spending > 1 }
			has_idea = social_01
		}
		localization_key = social_01_under
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_welfare_spending < 1 }
			has_idea = social_01
		}
		localization_key = social_01_over
	}
	text = {
		localization_key = social_01_neutral
	}
}
defined_text = {
	name = social_spending_protest_title_2
	text = {
		trigger = {
			check_variable = { THIS.expected_welfare_spending = 2 }
		}
		localization_key = social_02_expected
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_welfare_spending > 2 }
			has_idea = social_02
		}
		localization_key = social_02_under
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_welfare_spending < 2 }
			has_idea = social_02
		}
		localization_key = social_02_over
	}
	text = {
		localization_key = social_02_neutral
	}
}
defined_text = {
	name = social_spending_protest_title_3
	text = {
		trigger = {
			check_variable = { THIS.expected_welfare_spending = 3 }
		}
		localization_key = social_03_expected
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_welfare_spending > 3 }
			has_idea = social_03
		}
		localization_key = social_03_under
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_welfare_spending < 3 }
			has_idea = social_03
		}
		localization_key = social_03_over
	}
	text = {
		localization_key = social_03_neutral
	}
}
defined_text = {
	name = social_spending_protest_title_4
	text = {
		trigger = {
			check_variable = { THIS.expected_welfare_spending = 4 }
		}
		localization_key = social_04_expected
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_welfare_spending > 4 }
			has_idea = social_04
		}
		localization_key = social_04_under
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_welfare_spending < 4 }
			has_idea = social_04
		}
		localization_key = social_04_over
	}
	text = {
		localization_key = social_04_neutral
	}
}
defined_text = {
	name = social_spending_protest_title_5
	text = {
		trigger = {
			check_variable = { THIS.expected_welfare_spending = 5 }
		}
		localization_key = social_05_expected
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_welfare_spending > 5 }
			has_idea = social_05
		}
		localization_key = social_05_under
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_welfare_spending < 5 }
			has_idea = social_05
		}
		localization_key = social_05_over
	}
	text = {
		localization_key = social_05_neutral
	}
}
defined_text = {
	name = social_spending_protest_title_6
	text = {
		trigger = {
			check_variable = { THIS.expected_welfare_spending = 6 }
		}
		localization_key = social_06_expected
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_welfare_spending > 6 }
			has_idea = social_06
		}
		localization_key = social_06_under
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_welfare_spending < 6 }
			has_idea = social_06
		}
		localization_key = social_06_over
	}
	text = {
		localization_key = social_06_neutral
	}
}
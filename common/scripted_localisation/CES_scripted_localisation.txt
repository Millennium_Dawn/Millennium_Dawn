defined_text = {
	name = CES_our_beloved_autocrat_kaz_desc

	text = {
		trigger = {
			original_tag = KAZ
		}
		localization_key = KAZ_our_beloved_autocrat_kaz_desc
	}
	text = {
		trigger = {
			original_tag = UZB
		}
		localization_key = UZB_our_beloved_autocrat_kaz_desc
	}
	text = {
		trigger = {
			original_tag = KYR
		}
		localization_key = KYR_our_beloved_autocrat_kaz_desc
	}
}

defined_text = {
	name = CES_strongman_dream_kaz_desc

	text = {
		trigger = {
			original_tag = KAZ
		}
		localization_key = KAZ_strongman_dream_kaz_desc
	}
	text = {
		trigger = {
			original_tag = UZB
		}
		localization_key = UZB_strongman_dream_kaz_desc
	}
	text = {
		trigger = {
			original_tag = KYR
		}
		localization_key = KYR_strongman_dream_kaz_desc
	}
}

defined_text = {
	name = CES_centralasia

	text = {
		trigger = {
			original_tag = KAZ
		}
		localization_key = KAZ_centralasia
	}
	text = {
		trigger = {
			original_tag = UZB
		}
		localization_key = UZB_centralasia
	}
	text = {
		trigger = {
			original_tag = KYR
		}
		localization_key = KYR_centralasia
	}
}
# Allows you to create your own dynamic keys
# to be called in localization.
# defined_text -> this is it, we're defining the text
# text -> a discrete entry that can be picked to display in loc.
# trigger -> determines if a text entry will be picked or not.
# (The triggers need to be valid for the scope the key is called in
# (eg Root or From.From).)
# localization_key -> points to the localization key
# that'll be used if trigger passes

#defined_text = {
#	name = GER_Wind_status
#	text = {
#		trigger = {
#			check_variable = { GER_dark_winds_status < 30 }
#			original_tag = GER
#		}
#		localization_key = GER_dark_winds_status_low
#	}
#	text = {
#		trigger = {
#			check_variable = { GER_dark_winds_status < 60 }
#			original_tag = GER
#		}
#		localization_key = GER_dark_winds_status_moderat
#	}
#	text = {
#		trigger = {
#			check_variable = { GER_dark_winds_status < 90 }
#			original_tag = GER
#		}
#		localization_key = GER_dark_winds_status_high
#	}
#	text = {
#		trigger = {
#			check_variable = { var = GER_dark_winds_status value = 90 compare = greater_than_or_equals }
#			original_tag = GER
#		}
#		localization_key = GER_dark_winds_status_critical
#	}
#}
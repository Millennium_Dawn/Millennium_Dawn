defined_text = {
	text = {
		trigger = {
			SAU = {
				NOT = {
					has_idea = full_isr_boycotte
					has_idea = pol_isr_boycotte
					has_idea = econ_isr_boycotte
				}
			}
		}
		localization_key = saud_no_boycott_tt
	}
	text = {
		trigger = {
			SAU = {
				has_idea = full_isr_boycotte
			}
		}
		localization_key = saud_fully_boycott_tt
	}
	text = {
		trigger = {
			SAU = {
				has_idea = pol_isr_boycotte
			}
		}
		localization_key = saud_polit_boycott_tt
	}
	text = {
		trigger = {
			SAU = {
				has_idea = econ_isr_boycotte
			}
		}
		localization_key = saud_econ_boycott_tt
	}
	name = saud_bloc_loc
}
defined_text = {
	text = {
		trigger = {
			KUW = {
				NOT = {
					has_idea = full_isr_boycotte
					has_idea = pol_isr_boycotte
					has_idea = econ_isr_boycotte
				}
			}
		}
		localization_key = kuw_no_boycott_tt
	}
	text = {
		trigger = {
			KUW = {
				has_idea = full_isr_boycotte
			}
		}
		localization_key = kuw_fully_boycott_tt
	}
	text = {
		trigger = {
			KUW = {
				has_idea = pol_isr_boycotte
			}
		}
		localization_key = kuw_polit_boycott_tt
	}
	text = {
		trigger = {
			KUW = {
				has_idea = econ_isr_boycotte
			}
		}
		localization_key = kuw_econ_boycott_tt
	}
	name = kuw_bloc_loc
}
defined_text = {
	text = {
		trigger = {
			IRQ = {
				NOT = {
					has_idea = full_isr_boycotte
					has_idea = pol_isr_boycotte
					has_idea = econ_isr_boycotte
				}
			}
		}
		localization_key = irq_no_boycott_tt
	}
	text = {
		trigger = {
			IRQ = {
				has_idea = full_isr_boycotte
			}
		}
		localization_key = irq_fully_boycott_tt
	}
	text = {
		trigger = {
			IRQ = {
				has_idea = pol_isr_boycotte
			}
		}
		localization_key = irq_polit_boycott_tt
	}
	text = {
		trigger = {
			IRQ = {
				has_idea = econ_isr_boycotte
			}
		}
		localization_key = irq_econ_boycott_tt
	}
	name = irq_bloc_loc
}

defined_text = {
	text = {
		trigger = {
			PER = {
				NOT = {
					has_idea = full_isr_boycotte
					has_idea = pol_isr_boycotte
					has_idea = econ_isr_boycotte
				}
			}
		}
		localization_key = irq_no_boycott_tt
	}
	text = {
		trigger = {
			PER = {
				has_idea = full_isr_boycotte
			}
		}
		localization_key = per_fully_boycott_tt
	}
	text = {
		trigger = {
			PER = {
				has_idea = pol_isr_boycotte
			}
		}
		localization_key = per_polit_boycott_tt
	}
	text = {
		trigger = {
			PER = {
				has_idea = econ_isr_boycotte
			}
		}
		localization_key = per_econ_boycott_tt
	}
	name = per_bloc_loc
}

defined_text = {
	text = {
		trigger = {
			LEB = {
				NOT = {
					has_idea = full_isr_boycotte
					has_idea = pol_isr_boycotte
					has_idea = econ_isr_boycotte
				}
			}
		}
		localization_key = irq_no_boycott_tt
	}
	text = {
		trigger = {
			LEB = {
				has_idea = full_isr_boycotte
			}
		}
		localization_key = leb_fully_boycott_tt
	}
	text = {
		trigger = {
			LEB = {
				has_idea = pol_isr_boycotte
			}
		}
		localization_key = leb_polit_boycott_tt
	}
	text = {
		trigger = {
			LEB = {
				has_idea = econ_isr_boycotte
			}
		}
		localization_key = leb_econ_boycott_tt
	}
	name = leb_bloc_loc
}

defined_text = {
	text = {
		trigger = {
			SYR = {
				NOT = {
					has_idea = full_isr_boycotte
					has_idea = pol_isr_boycotte
					has_idea = econ_isr_boycotte
				}
			}
		}
		localization_key = syr_no_boycott_tt
	}
	text = {
		trigger = {
			SYR = {
				has_idea = full_isr_boycotte
			}
		}
		localization_key = syr_fully_boycott_tt
	}
	text = {
		trigger = {
			SYR = {
				has_idea = pol_isr_boycotte
			}
		}
		localization_key = syr_polit_boycott_tt
	}
	text = {
		trigger = {
			SYR = {
				has_idea = econ_isr_boycotte
			}
		}
		localization_key = syr_econ_boycott_tt
	}
	name = syr_bloc_loc
}

defined_text = {
	text = {
		trigger = {
			YEM = {
				NOT = {
					has_idea = full_isr_boycotte
					has_idea = pol_isr_boycotte
					has_idea = econ_isr_boycotte
				}
			}
		}
		localization_key = yem_no_boycott_tt
	}
	text = {
		trigger = {
			YEM = {
				has_idea = full_isr_boycotte
			}
		}
		localization_key = yem_fully_boycott_tt
	}
	text = {
		trigger = {
			YEM = {
				has_idea = pol_isr_boycotte
			}
		}
		localization_key = yem_polit_boycott_tt
	}
	text = {
		trigger = {
			YEM = {
				has_idea = econ_isr_boycotte
			}
		}
		localization_key = yem_econ_boycott_tt
	}
	name = yem_bloc_loc
}

defined_text = {
	text = {
		trigger = {
			LBA = {
				NOT = {
					has_idea = full_isr_boycotte
					has_idea = pol_isr_boycotte
					has_idea = econ_isr_boycotte
				}
			}
		}
		localization_key = lba_no_boycott_tt
	}
	text = {
		trigger = {
			LBA = {
				has_idea = full_isr_boycotte
			}
		}
		localization_key = lba_fully_boycott_tt
	}
	text = {
		trigger = {
			LBA = {
				has_idea = pol_isr_boycotte
			}
		}
		localization_key = lba_polit_boycott_tt
	}
	text = {
		trigger = {
			LBA = {
				has_idea = econ_isr_boycotte
			}
		}
		localization_key = lba_econ_boycott_tt
	}
	name = lba_bloc_loc
}

defined_text = {
	text = {
		trigger = {
			ALG = {
				NOT = {
					has_idea = full_isr_boycotte
					has_idea = pol_isr_boycotte
					has_idea = econ_isr_boycotte
				}
			}
		}
		localization_key = agl_no_boycott_tt
	}
	text = {
		trigger = {
			ALG = {
				has_idea = full_isr_boycotte
			}
		}
		localization_key = agl_fully_boycott_tt
	}
	text = {
		trigger = {
			ALG = {
				has_idea = pol_isr_boycotte
			}
		}
		localization_key = agl_polit_boycott_tt
	}
	text = {
		trigger = {
			ALG = {
				has_idea = econ_isr_boycotte
			}
		}
		localization_key = agl_econ_boycott_tt
	}
	name = agl_bloc_loc
}

defined_text = {
	text = {
		trigger = {
			ALG = {
				NOT = {
					has_idea = full_isr_boycotte
					has_idea = pol_isr_boycotte
					has_idea = econ_isr_boycotte
				}
			}
		}
		localization_key = agl_no_boycott_tt
	}
	text = {
		trigger = {
			ALG = {
				has_idea = full_isr_boycotte
			}
		}
		localization_key = agl_fully_boycott_tt
	}
	text = {
		trigger = {
			ALG = {
				has_idea = pol_isr_boycotte
			}
		}
		localization_key = agl_polit_boycott_tt
	}
	text = {
		trigger = {
			ALG = {
				has_idea = econ_isr_boycotte
			}
		}
		localization_key = agl_econ_boycott_tt
	}
	name = agl_bloc_loc
}

defined_text = {
	text = {
		trigger = {
			ALG = {
				NOT = {
					has_idea = full_isr_boycotte
					has_idea = pol_isr_boycotte
					has_idea = econ_isr_boycotte
				}
			}
		}
		localization_key = agl_no_boycott_tt
	}
	text = {
		trigger = {
			ALG = {
				has_idea = full_isr_boycotte
			}
		}
		localization_key = agl_fully_boycott_tt
	}
	text = {
		trigger = {
			ALG = {
				has_idea = pol_isr_boycotte
			}
		}
		localization_key = agl_polit_boycott_tt
	}
	text = {
		trigger = {
			ALG = {
				has_idea = econ_isr_boycotte
			}
		}
		localization_key = agl_econ_boycott_tt
	}
	name = agl_bloc_loc
}

defined_text = {
	text = {
		trigger = {
			SHA = {
				NOT = {
					has_idea = full_isr_boycotte
					has_idea = pol_isr_boycotte
					has_idea = econ_isr_boycotte
				}
			}
		}
		localization_key = sha_no_boycott_tt
	}
	text = {
		trigger = {
			SHA = {
				has_idea = full_isr_boycotte
			}
		}
		localization_key = sha_fully_boycott_tt
	}
	text = {
		trigger = {
			SHA = {
				has_idea = pol_isr_boycotte
			}
		}
		localization_key = sha_polit_boycott_tt
	}
	text = {
		trigger = {
			SHA = {
				has_idea = econ_isr_boycotte
			}
		}
		localization_key = sha_econ_boycott_tt
	}
	name = sha_bloc_loc
}

defined_text = {
	text = {
		trigger = {
			SUD = {
				NOT = {
					has_idea = full_isr_boycotte
					has_idea = pol_isr_boycotte
					has_idea = econ_isr_boycotte
				}
			}
		}
		localization_key = sud_no_boycott_tt
	}
	text = {
		trigger = {
			SUD = {
				has_idea = full_isr_boycotte
			}
		}
		localization_key = sud_fully_boycott_tt
	}
	text = {
		trigger = {
			SUD = {
				has_idea = pol_isr_boycotte
			}
		}
		localization_key = sud_polit_boycott_tt
	}
	text = {
		trigger = {
			SUD = {
				has_idea = econ_isr_boycotte
			}
		}
		localization_key = sud_econ_boycott_tt
	}
	name = sud_bloc_loc
}

defined_text = {
	text = {
		trigger = {
			QAT = {
				NOT = {
					has_idea = full_isr_boycotte
					has_idea = pol_isr_boycotte
					has_idea = econ_isr_boycotte
				}
			}
		}
		localization_key = qat_no_boycott_tt
	}
	text = {
		trigger = {
			QAT = {
				has_idea = full_isr_boycotte
			}
		}
		localization_key = qat_fully_boycott_tt
	}
	text = {
		trigger = {
			QAT = {
				has_idea = pol_isr_boycotte
			}
		}
		localization_key = qat_polit_boycott_tt
	}
	text = {
		trigger = {
			QAT = {
				has_idea = econ_isr_boycotte
			}
		}
		localization_key = qat_econ_boycott_tt
	}
	name = qat_bloc_loc
}

defined_text = {
	text = {
		trigger = {
			UAE = {
				NOT = {
					has_idea = full_isr_boycotte
					has_idea = pol_isr_boycotte
					has_idea = econ_isr_boycotte
				}
			}
		}
		localization_key = uae_no_boycott_tt
	}
	text = {
		trigger = {
			UAE = {
				has_idea = full_isr_boycotte
			}
		}
		localization_key = uae_fully_boycott_tt
	}
	text = {
		trigger = {
			UAE = {
				has_idea = pol_isr_boycotte
			}
		}
		localization_key = uae_polit_boycott_tt
	}
	text = {
		trigger = {
			UAE = {
				has_idea = econ_isr_boycotte
			}
		}
		localization_key = uae_econ_boycott_tt
	}
	name = uae_bloc_loc
}

defined_text = {
	text = {
		trigger = {
			TUN = {
				NOT = {
					has_idea = full_isr_boycotte
					has_idea = pol_isr_boycotte
					has_idea = econ_isr_boycotte
				}
			}
		}
		localization_key = tun_no_boycott_tt
	}
	text = {
		trigger = {
			TUN = {
				has_idea = full_isr_boycotte
			}
		}
		localization_key = tun_fully_boycott_tt
	}
	text = {
		trigger = {
			TUN = {
				has_idea = pol_isr_boycotte
			}
		}
		localization_key = tun_polit_boycott_tt
	}
	text = {
		trigger = {
			TUN = {
				has_idea = econ_isr_boycotte
			}
		}
		localization_key = tun_econ_boycott_tt
	}
	name = tun_bloc_loc
}

defined_text = {
	text = {
		trigger = {
			BHR = {
				NOT = {
					has_idea = full_isr_boycotte
					has_idea = pol_isr_boycotte
					has_idea = econ_isr_boycotte
				}
			}
		}
		localization_key = tun_no_boycott_tt
	}
	text = {
		trigger = {
			BHR = {
				has_idea = full_isr_boycotte
			}
		}
		localization_key = bhr_fully_boycott_tt
	}
	text = {
		trigger = {
			BHR = {
				has_idea = pol_isr_boycotte
			}
		}
		localization_key = bhr_polit_boycott_tt
	}
	text = {
		trigger = {
			BHR = {
				has_idea = econ_isr_boycotte
			}
		}
		localization_key = bhr_econ_boycott_tt
	}
	name = bhr_bloc_loc
}

defined_text = {
	text = {
		trigger = {
			MOR = {
				NOT = {
					has_idea = full_isr_boycotte
					has_idea = pol_isr_boycotte
					has_idea = econ_isr_boycotte
				}
			}
		}
		localization_key = mor_no_boycott_tt
	}
	text = {
		trigger = {
			MOR = {
				has_idea = full_isr_boycotte
			}
		}
		localization_key = mor_fully_boycott_tt
	}
	text = {
		trigger = {
			MOR = {
				has_idea = pol_isr_boycotte
			}
		}
		localization_key = mor_polit_boycott_tt
	}
	text = {
		trigger = {
			MOR = {
				has_idea = econ_isr_boycotte
			}
		}
		localization_key = mor_econ_boycott_tt
	}
	name = mor_bloc_loc
}

defined_text = {
	text = {
		trigger = {
			OMA = {
				NOT = {
					has_idea = full_isr_boycotte
					has_idea = pol_isr_boycotte
					has_idea = econ_isr_boycotte
				}
			}
		}
		localization_key = oma_no_boycott_tt
	}
	text = {
		trigger = {
			OMA = {
				has_idea = full_isr_boycotte
			}
		}
		localization_key = oma_fully_boycott_tt
	}
	text = {
		trigger = {
			OMA = {
				has_idea = pol_isr_boycotte
			}
		}
		localization_key = oma_polit_boycott_tt
	}
	text = {
		trigger = {
			OMA = {
				has_idea = econ_isr_boycotte
			}
		}
		localization_key = oma_econ_boycott_tt
	}
	name = oma_bloc_loc
}

defined_text = {
	text = {
		trigger = {
			MAU = {
				NOT = {
					has_idea = full_isr_boycotte
					has_idea = pol_isr_boycotte
					has_idea = econ_isr_boycotte
				}
			}
		}
		localization_key = mau_no_boycott_tt
	}
	text = {
		trigger = {
			MAU = {
				has_idea = full_isr_boycotte
			}
		}
		localization_key = mau_fully_boycott_tt
	}
	text = {
		trigger = {
			MAU = {
				has_idea = pol_isr_boycotte
			}
		}
		localization_key = mau_polit_boycott_tt
	}
	text = {
		trigger = {
			MAU = {
				has_idea = econ_isr_boycotte
			}
		}
		localization_key = mau_econ_boycott_tt
	}
	name = mau_bloc_loc
}

defined_text = {
	text = {
		trigger = {
			EGY = {
				NOT = {
					has_idea = full_isr_boycotte
					has_idea = pol_isr_boycotte
					has_idea = econ_isr_boycotte
				}
			}
		}
		localization_key = egy_no_boycott_tt
	}
	text = {
		trigger = {
			EGY = {
				has_idea = full_isr_boycotte
			}
		}
		localization_key = egy_fully_boycott_tt
	}
	text = {
		trigger = {
			EGY = {
				has_idea = pol_isr_boycotte
			}
		}
		localization_key = egy_polit_boycott_tt
	}
	text = {
		trigger = {
			EGY = {
				has_idea = econ_isr_boycotte
			}
		}
		localization_key = egy_econ_boycott_tt
	}
	name = egy_bloc_loc
}

defined_text = {
	text = {
		trigger = {
			JOR = {
				NOT = {
					has_idea = full_isr_boycotte
					has_idea = pol_isr_boycotte
					has_idea = econ_isr_boycotte
				}
			}
		}
		localization_key = jor_no_boycott_tt
	}
	text = {
		trigger = {
			JOR = {
				has_idea = full_isr_boycotte
			}
		}
		localization_key = jor_fully_boycott_tt
	}
	text = {
		trigger = {
			JOR = {
				has_idea = pol_isr_boycotte
			}
		}
		localization_key = jor_polit_boycott_tt
	}
	text = {
		trigger = {
			JOR = {
				has_idea = econ_isr_boycotte
			}
		}
		localization_key = jor_econ_boycott_tt
	}
	name = jor_bloc_loc
}

###diamonds stuff
defined_text = {
	text = {
		trigger = {
			OR = {
				AGL = {
					has_idea = angola_israel_diamonds
				}
				UNI = {
					has_idea = angola_israel_diamonds
				}
			}
		}
		localization_key = diamond_angola_loc_yes_tt
	}
	text = {
		trigger = {
			NOT = {
				OR = {
					AGL = {
						has_idea = angola_israel_diamonds
					}
					UNI = {
						has_idea = angola_israel_diamonds
					}
				}
			}

		}
		localization_key = diamond_angola_loc_no_tt
	}
	name = diamond_angola_loc
}
defined_text = {
	text = {
		trigger = {
			OR = {
				DRC = {
					has_idea = congo_israel_diamonds
				}
				RCD = {
					has_idea = congo_israel_diamonds
				}
				MLC = {
					has_idea = congo_israel_diamonds
				}
			}
		}
		localization_key = diamond_congo_loc_yes_tt
	}
	text = {
		trigger = {
			NOT = {
				OR = {
					DRC = {
						has_idea = congo_israel_diamonds
					}
					RCD = {
						has_idea = congo_israel_diamonds
					}
					MLC = {
						has_idea = congo_israel_diamonds
					}
				}
			}

		}
		localization_key = diamond_congo_loc_no_tt
	}
	name = diamond_congo_loc
}
defined_text = {
	text = {
		trigger = {
			NAM = {
				has_idea = namibia_israel_diamonds
			}
		}
		localization_key = diamond_namibia_loc_yes_tt
	}
	text = {
		trigger = {
			NOT = {
				NAM = {
					has_idea = namibia_israel_diamonds
				}
			}
		}
		localization_key = diamond_namibia_loc_no_tt
	}
	name = diamond_namibia_loc
}

defined_text = {
	text = {
		trigger = {
			SOV = {
				has_idea = russia_israel_diamonds
			}
		}
		localization_key = diamond_russia_loc_yes_tt
	}
	text = {
		trigger = {
			NOT = {
				SOV = {
					has_idea = russia_israel_diamonds
				}
			}
		}
		localization_key = diamond_russia_loc_no_tt
	}
	name = diamond_russia_loc
}

defined_text = {
	text = {
		trigger = {
			BOT = {
				has_idea = botswana_israel_diamonds
			}
		}
		localization_key = diamond_botswana_loc_yes_tt
	}
	text = {
		trigger = {
			NOT = {
				BOT = {
					has_idea = botswana_israel_diamonds
				}
			}
		}
		localization_key = diamond_botswana_loc_no_tt
	}
	name = diamond_botswana_loc
}

defined_text = {
	text = {
		trigger = {
			CAN = {
				has_idea = canada_israel_diamonds
			}
		}
		localization_key = diamond_canada_loc_yes_tt
	}
	text = {
		trigger = {
			NOT = {
				CAN = {
					has_idea = canada_israel_diamonds
				}
			}
		}
		localization_key = diamond_canada_loc_no_tt
	}
	name = diamond_canada_loc
}
#####

#################
defined_text = {
	name = afghanistan_influence_per
	text = {
		trigger = {
			OR = {
				AFG = {
					iran_has_more_than_20_influece_trigger = yes
				}
				TAL = {
					iran_has_more_than_20_influece_trigger = yes
				}
			}

		}
		localization_key = GFX_afghanistan_high_influence_per
	}
	text = {
		trigger = {
			OR = {
				TAL = { #need
					iran_has_more_than_10_influence_trigger = yes
				}
				AFG = { #need
					iran_has_more_than_10_influence_trigger = yes
				}
			}

		}
		localization_key = GFX_afghanistan_medium_influence_per
	}
	text = {
		trigger = {
			OR = {
				TAL = {
					iran_has_more_than_1_influence_trigger = yes
				}
				AFG = {
					iran_has_more_than_1_influence_trigger = yes
				}
			}

		}
		localization_key = GFX_afghanistan_low_influence_per
	}
	text = {
		trigger = {
			OR = {
				AFG = {
					iran_has_less_than_1_influence_trigger = yes
				}
				TAL = {
					iran_has_less_than_1_influence_trigger = yes
				}
			}
		}
		localization_key = GFX_afghanistan_no_influence_per
	}
}
defined_text = {
	name = pakistan_influence_per
	text = {
		trigger = {
			PAK = {
				iran_has_more_than_20_influece_trigger = yes
			}
		}
		localization_key = GFX_pakistan_high_influence_per
	}
	text = {
		trigger = {
			PAK = {
				iran_has_more_than_10_influence_trigger = yes
			}
		}
		localization_key = GFX_pakistan_medium_influence_per
	}
	text = {
		trigger = {
			PAK = {
				iran_has_more_than_1_influence_trigger = yes
			}
		}
		localization_key = GFX_pakistan_low_influence_per
	}
	text = {
		trigger = {
			PAK = {
				iran_has_less_than_1_influence_trigger = yes
			}
		}
		localization_key = GFX_pakistan_no_influence_per
	}
}
defined_text = {
	name = turkmenistan_influence_per
	text = {
		trigger = {
			TRK = {
				iran_has_more_than_20_influece_trigger = yes
			}
		}
		localization_key = GFX_turkmenistan_high_influence_per
	}
	text = {
		trigger = {
			TRK = {
				iran_has_more_than_10_influence_trigger = yes
			}
		}
		localization_key = GFX_turkmenistan_medium_influence_per
	}
	text = {
		trigger = {
			TRK = {
				iran_has_more_than_1_influence_trigger = yes
			}
		}
		localization_key = GFX_turkmenistan_low_influence_per
	}
	text = {
		trigger = {
			TRK = {
				iran_has_less_than_1_influence_trigger = yes
			}
		}
		localization_key = GFX_turkmenistan_no_influence_per
	}
}
defined_text = {
	name = azerbaijan_influence_per
	text = {
		trigger = {
			AZE = {
				iran_has_more_than_20_influece_trigger = yes
			}
		}
		localization_key = GFX_azerbaijan_high_influence_per
	}
	text = {
		trigger = {
			AZE = {
				iran_has_more_than_10_influence_trigger = yes
			}
		}
		localization_key = GFX_azerbaijan_medium_influence_per
	}
	text = {
		trigger = {
			AZE = {
				iran_has_more_than_1_influence_trigger = yes
			}
		}
		localization_key = GFX_azerbaijan_low_influence_per
	}
	text = {
		trigger = {
			AZE = {
				iran_has_less_than_1_influence_trigger = yes
			}
		}
		localization_key = GFX_azerbaijan_no_influence_per
	}
}
defined_text = {
	name = armenia_influence_per
	text = {
		trigger = {
			OR = {
				NKR = {
					iran_has_more_than_20_influece_trigger = yes
				}
				ARM = {
					iran_has_more_than_20_influece_trigger = yes
				}
			}
		}
		localization_key = GFX_armenia_high_influence_per
	}
	text = {
		trigger = {
			OR = {
				NKR = {
					iran_has_more_than_10_influence_trigger = yes
				}
				ARM = {
					iran_has_more_than_10_influence_trigger = yes
				}
			}
		}
		localization_key = GFX_armenia_medium_influence_per
	}
	text = {
		trigger = {
			OR = {
				NKR = {
					iran_has_more_than_1_influence_trigger = yes
				}
				ARM = {
					iran_has_more_than_1_influence_trigger = yes
				}
			}
		}
		localization_key = GFX_armenia_low_influence_per
	}
	text = {
		trigger = {
			AZE = {
				iran_has_less_than_1_influence_trigger = yes
			}
		}
		localization_key = GFX_armenia_no_influence_per
	}
}
defined_text = {
	name = turkey_influence_per
	text = {
		trigger = {
			TUR = {
				iran_has_more_than_20_influece_trigger = yes
			}
		}
		localization_key = GFX_turkey_high_influence_per
	}
	text = {
		trigger = {
			TUR = {
				iran_has_more_than_10_influence_trigger = yes
			}
		}
		localization_key = GFX_turkey_medium_influence_per
	}
	text = {
		trigger = {
			TUR = {
				iran_has_more_than_1_influence_trigger = yes
			}
		}
		localization_key = GFX_turkey_low_influence_per
	}
	text = {
		trigger = {
			TUR = {
				iran_has_less_than_1_influence_trigger = yes
			}
		}
		localization_key = GFX_turkey_no_influence_per
	}
}
defined_text = {
	name = syria_influence_per
	text = {
		trigger = {
			SYR = {
				iran_has_more_than_20_influece_trigger = yes
			}
		}
		localization_key = GFX_syria_high_influence_per
	}
	text = {
		trigger = {
			SYR = {
				iran_has_more_than_10_influence_trigger = yes
			}
		}
		localization_key = GFX_syria_medium_influence_per
	}
	text = {
		trigger = {
			SYR = {
				iran_has_more_than_1_influence_trigger = yes
			}
		}
		localization_key = GFX_syria_low_influence_per
	}
	text = {
		trigger = {
			SYR = {
				iran_has_less_than_1_influence_trigger = yes
			}
		}
		localization_key = GFX_syria_no_influence_per
	}
}
defined_text = {
	name = iraq_influence_per
	text = {
		trigger = {
			OR = {
				KUR = {
					iran_has_more_than_20_influece_trigger = yes
				}
				IRQ = {
					iran_has_more_than_20_influece_trigger = yes
				}
			}
		}
		localization_key = GFX_iraq_high_influence_per
	}
	text = {
		trigger = {
			OR = {
				KUR = {
					iran_has_more_than_10_influence_trigger = yes
				}
				IRQ = {
					iran_has_more_than_10_influence_trigger = yes
				}
			}
		}
		localization_key = GFX_iraq_medium_influence_per
	}
	text = {
		trigger = {
			OR = {
				KUR = {
					iran_has_more_than_1_influence_trigger = yes
				}
				IRQ = {
					iran_has_more_than_1_influence_trigger = yes
				}
			}
		}
		localization_key = GFX_iraq_low_influence_per
	}
	text = {
		trigger = {
			OR = {
				KUR = {
					iran_has_less_than_1_influence_trigger = yes
				}
				IRQ = {
					iran_has_less_than_1_influence_trigger = yes
				}
			}
		}
		localization_key = GFX_iraq_no_influence_per
	}
}
defined_text = {
	name = lebonan_influence_per
	text = {
		trigger = {
			OR = {
				HEZ = {
					iran_has_more_than_20_influece_trigger = yes
				}
				LEB = {
					iran_has_more_than_20_influece_trigger = yes
				}
			}

		}
		localization_key = GFX_lebonan_high_influence_per
	}
	text = {
		trigger = {
			OR = {
				HEZ = {
					iran_has_more_than_10_influence_trigger = yes
				}
				LEB = {
					iran_has_more_than_10_influence_trigger = yes
				}
			}

		}
		localization_key = GFX_lebonan_medium_influence_per
	}
	text = {
		trigger = {
			OR = {
				HEZ = {
					iran_has_more_than_1_influence_trigger = yes
				}
				LEB = {
					iran_has_more_than_1_influence_trigger = yes
				}
			}

		}
		localization_key = GFX_lebonan_low_influence_per
	}
	text = {
		trigger = {
			OR = {
				HEZ = {
					iran_has_less_than_1_influence_trigger = yes
				}
				LEB = {
					iran_has_less_than_1_influence_trigger = yes
				}
			}
		}
		localization_key = GFX_lebonan_no_influence_per
	}
}
defined_text = {
	name = iran_influence_per
	text = {
		trigger = {
			PER = {
				original_tag = PER
			}
		}
		localization_key = GFX_iran_high_influence_per
	}
}
defined_text = {
	name = palastine_influence_per
	text = {
		trigger = {
			PAL = {
				iran_has_more_than_20_influece_trigger = yes
			}
		}
		localization_key = GFX_palestine_high_influence_per
	}
	text = {
		trigger = {
			PAL = {
				iran_has_more_than_10_influence_trigger = yes
			}
		}
		localization_key = GFX_palestine_medium_influence_per
	}
	text = {
		trigger = {
			PAL = {
				iran_has_more_than_1_influence_trigger = yes
			}
		}
		localization_key = GFX_palestine_low_influence_per
	}
}
defined_text = {
	name = jordan_influence_per
	text = {
		trigger = {
			JOR = {
				iran_has_more_than_20_influece_trigger = yes
			}
		}
		localization_key = GFX_jordan_high_influence_per
	}
	text = {
		trigger = {
			JOR = {
				iran_has_more_than_10_influence_trigger = yes
			}
		}
		localization_key = GFX_jordan_medium_influence_per
	}
	text = {
		trigger = {
			JOR = {
				iran_has_more_than_1_influence_trigger = yes
			}
		}
		localization_key = GFX_jordan_low_influence_per
	}
	text = {
		trigger = {
			JOR = {
				iran_has_less_than_1_influence_trigger = yes
			}
		}
		localization_key = GFX_jordan_no_influence_per
	}
}
defined_text = {
	name = kuwait_influence_per
	text = {
		trigger = {
			KUW = {
				iran_has_more_than_20_influece_trigger = yes
			}
		}
		localization_key = GFX_kuwait_high_influence_per
	}
	text = {
		trigger = {
			KUW = {
				iran_has_more_than_10_influence_trigger = yes
			}
		}
		localization_key = GFX_kuwait_medium_influence_per
	}
	text = {
		trigger = {
			KUW = {
				iran_has_more_than_1_influence_trigger = yes
			}
		}
		localization_key = GFX_kuwait_low_influence_per
	}
	text = {
		trigger = {
			KUW = {
				iran_has_less_than_1_influence_trigger = yes
			}
		}
		localization_key = GFX_kuwait_no_influence_per
	}
}
defined_text = {
	name = saudi_arabia_influence_per
	text = {
		trigger = {
			SAU = {
				iran_has_more_than_20_influece_trigger = yes
			}
		}
		localization_key = GFX_saudi_arabia_high_influence_per
	}
	text = {
		trigger = {
			SAU = {
				iran_has_more_than_10_influence_trigger = yes
			}
		}
		localization_key = GFX_saudi_arabia_medium_influence_per
	}
	text = {
		trigger = {
			SAU = {
				iran_has_more_than_1_influence_trigger = yes
			}
		}
		localization_key = GFX_saudi_arabia_low_influence_per
	}
	text = {
		trigger = {
			SAU = {
				iran_has_less_than_1_influence_trigger = yes
			}
		}
		localization_key = GFX_saudi_arabia_no_influence_per
	}
}
defined_text = {
	name = yemen_influence_per
	text = {
		trigger = {
			YEM = {
				iran_has_more_than_20_influece_trigger = yes
			}
		}
		localization_key = GFX_yemen_high_influence_per
	}
	text = {
		trigger = {
			YEM = {
				iran_has_more_than_10_influence_trigger = yes
			}
		}
		localization_key = GFX_yemen_medium_influence_per
	}
	text = {
		trigger = {
			YEM = {
				iran_has_more_than_1_influence_trigger = yes
			}
		}
		localization_key = GFX_yemen_low_influence_per
	}
	text = {
		trigger = {
			YEM = {
				iran_has_less_than_1_influence_trigger = yes
			}
		}
		localization_key = GFX_yemen_no_influence_per
	}
}
defined_text = {
	name = oman_influence_per
	text = {
		trigger = {
			OMA = {
				iran_has_more_than_20_influece_trigger = yes
			}
		}
		localization_key = GFX_oman_high_influence_per
	}
	text = {
		trigger = {
			OMA = {
				iran_has_more_than_10_influence_trigger = yes
			}
		}
		localization_key = GFX_oman_medium_influence_per
	}
	text = {
		trigger = {
			OMA = {
				iran_has_more_than_1_influence_trigger = yes
			}
		}
		localization_key = GFX_oman_low_influence_per
	}
	text = {
		trigger = {
			OMA = {
				iran_has_less_than_1_influence_trigger = yes
			}
		}
		localization_key = GFX_oman_no_influence_per
	}
}
defined_text = {
	name = uae_influence_per
	text = {
		trigger = {
			UAE = {
				iran_has_more_than_20_influece_trigger = yes
			}
		}
		localization_key = GFX_uae_high_influence_per
	}
	text = {
		trigger = {
			UAE = {
				iran_has_more_than_10_influence_trigger = yes
			}
		}
		localization_key = GFX_uae_medium_influence_per
	}
	text = {
		trigger = {
			UAE = {
				iran_has_more_than_1_influence_trigger = yes
			}
		}
		localization_key = GFX_uae_low_influence_per
	}
	text = {
		trigger = {
			UAE = {
				iran_has_less_than_1_influence_trigger = yes
			}
		}
		localization_key = GFX_uae_no_influence_per
	}
}
defined_text = {
	name = qatar_influence_per
	text = {
		trigger = {
			QAT = {
				iran_has_more_than_20_influece_trigger = yes
			}
		}
		localization_key = GFX_qatar_high_influence_per
	}
	text = {
		trigger = {
			QAT = {
				iran_has_more_than_10_influence_trigger = yes
			}
		}
		localization_key = GFX_qatar_medium_influence_per
	}
	text = {
		trigger = {
			QAT = {
				iran_has_more_than_1_influence_trigger = yes
			}
		}
		localization_key = GFX_qatar_low_influence_per
	}
	text = {
		trigger = {
			QAT = {
				iran_has_less_than_1_influence_trigger = yes
			}
		}
		localization_key = GFX_qatar_no_influence_per
	}
}
######################


### Fuck Pakman, all my homies hate Pakman
defined_text = {
	name = knes_status
	text = {
		trigger = {
			ISR = {
				has_country_flag = ISR_small_knesset
			}
		}
		localization_key = ISR_knesset_small_TT
	}
	text = {
		trigger = {
			ISR = {
				has_country_flag = ISR_good_size_knesset
			}
		}
		localization_key = ISR_knesset_big_TT
	}
	text = {
		trigger = {
			ISR = {
				has_country_flag = ISR_big_size_knesset
			}
		}
		localization_key = ISR_knesset_big_super_TT
	}
}
defined_text = {
	name = knes_approvio
	text = {
		trigger = {
			ISR = {
				check_variable = { bill_support > 60 }
				NOT = { check_variable = { bill_support = 0 } }
			}
		}
		localization_key = ISR_knesset_approve_TT
	}
	text = {
		trigger = {
			ISR = {
				check_variable = { bill_support < 60 } 
				NOT = { check_variable = { bill_support = 0 } }
			}
		}
		localization_key = ISR_knesset_rejected_TT
	}
	text = {
		trigger = {
			ISR = {
				check_variable = { bill_support = 0 }
			}
		}
		localization_key = ISR_knesset_no_TT
	}
}
#Palestine Israel Stuff

defined_text = {
	name = select_thingie_palisr
	text = {
		trigger = {
			NOT = {
				has_country_flag = gu_westbank_select
				has_country_flag = gu_mainisr_select
				has_country_flag = gu_gaza_select
			}
		}
		localization_key = select_nothing_tt
	}
	text = {
		trigger = {
			has_country_flag = gu_westbank_select
		}
		localization_key = select_westbank_tt
	}
	text = {
		trigger = {
			has_country_flag = gu_mainisr_select
		}
		localization_key = select_mainisr_tt
	}
	text = {
		trigger = {
			has_country_flag = gu_gaza_select
		}
		localization_key = select_gaza_tt
	}
}

defined_text = {
	name = operation_namefi
	text = {
		trigger = {
			PAL = {
				has_country_flag = ISR_start_operation
			}
		}
		localization_key = irr_homa_tt
	}
	text = {
		trigger = {
			ISR = {
				has_country_flag = ISR_start_operation
			}
		}
		localization_key = homat_mahgehn_tt
	}
}
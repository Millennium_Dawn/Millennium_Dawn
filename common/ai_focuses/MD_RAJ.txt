
ai_focus_defense_RAJ = {
	research = {
		defensive = 5.0
		CAT_artillery = 5.0
		CAT_inf_wep = 1.0
	}
}

ai_focus_aggressive_RAJ = {
	research = {
		offensive = 5.0
		CAT_armor = 5.0
	}
}

ai_focus_war_production_RAJ = {
	research = {
		CAT_construction_tech = 10.0
		CAT_fuel_oil = 12.0
		CAT_nfibers = 10.0
		CAT_3d = 10.0
		CAT_ai = 6.0
		CAT_genes = 4.0
		CAT_excavation_tech = 4.0
		CAT_infrastructure = 3.0
	}
}

ai_focus_military_equipment_RAJ = {
	research = {
		CAT_inf = 5.0
		CAT_inf_wep = 25.0
		CAT_nvg = 10.0
		CAT_cnc = 10.0
		CAT_support_weapons = 10.0
		CAT_special_forces = 10.0

		CAT_at = 15.0
		CAT_aa = 15.0

		CAT_artillery = 8.0
		# AI Research SAM Missiles only. It's the only one they use so it's important to keep up
		CAT_sam = 5.0
	}
}

ai_focus_military_advancements_RAJ = {
	research = {
		CAT_eastern = 10.00

		CAT_l_drone = 2.0
		CAT_special_forces = 4.0
		CAT_support_weapons = 20.0
		CAT_nvg = 15.0

		CAT_armor = 20.0
		CAT_mbt = 4.0
		CAT_apc = 6.0
		CAT_ifv = 6.0
		CAT_rec_tank = 2.0

		CAT_util = 10.0

		CAT_heli = 2.5
	}
}

ai_focus_peaceful_RAJ = {
	research = {
		CAT_industry = 10.0
		CAT_ai = 8.0
		CAT_nfibers = 8.0
		CAT_3d = 8.0
		CAT_internet_tech = 14.0
		CAT_computing_tech = 14.0
		CAT_construction_tech = 20.0
		CAT_excavation_tech = 15.0
		CAT_genes = 8.0
		CAT_fuel_oil = 10
		CAT_infrastructure = 3
	}
}

ai_focus_naval_RAJ = {
	research = {
		naval_doctrine = 2.0

		CAT_naval_eqp = 7.0
		CAT_naval_misc = 5.0

		CAT_n_cruiser = 0.0
		CAT_m_cruiser = 2.0
		CAT_destroyer = 4.0
		CAT_frigate = 8.0
		CAT_corvette = 8.0
		Cat_TRANS_SHIP = 6.0

		CAT_n_cv = 0.0
		CAT_cv = 0.0
		CAT_lha = 8.0
		CAT_lpd = 4.0

		CAT_d_sub = 6.0
		CAT_atk_sub = 0.0
		CAT_m_sub = 0.0
	}
}

ai_focus_naval_air_RAJ = {
	research = {
		CAT_naval_air = 25.0
	}
}

ai_focus_aviation_RAJ = {
	research = {
		CAT_air_doctrine = 2.0

		CAT_air_eqp = 15.0
		CAT_air_spc = 8.0
		CAT_air_wpn = 8.0

		CAT_h_air = 5.0
		CAT_str_bomber = 7.5
		CAT_cas = 10.0
		CAT_trans_plane = 5.0

		CAT_heli = 2.0
		CAT_trans_heli = 10.0
		CAT_atk_heli = 5.0

		CAT_fighter = 10.0
		CAT_mr_fighter = 8.0
		CAT_s_fighter = 2.0
		CAT_l_s_fighter = 1.0
		CAT_as_fighter = 10.0
		CAT_a_uav = -10.0
	}
}

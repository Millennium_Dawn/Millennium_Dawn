ideas = {
	air_force_spirit = {

		independent_air_force_spirit = {
			ledger = air
			modifier = {
				air_advisor_cost_factor = -0.75
			}
			ai_will_do = {
				factor = 1
			}
		}

		industrial_destruction_spirit = {
			ledger = air
			research_bonus = {
				CAT_large_plane = 0.15
			}
			modifier = {
				large_plane_airframe_design_cost_factor = -0.50
				large_plane_maritime_patrol_airframe_design_cost_factor = -0.50
			}
			ai_will_do = {
				factor = 1
			}
		}

		ground_element_destruction_spirit = {
			ledger = air
			research_bonus = {
				CAT_medium_plane = 0.15
			}
			modifier = {
				medium_plane_airframe_design_cost_factor = -0.50
				medium_plane_cas_airframe_design_cost_factor = -0.50
			}
			ai_will_do = {
				factor = 1
			}
		}

		clear_the_skies_spirit = {
			ledger = air
			research_bonus = {
				CAT_medium_plane = 0.15
			}
			modifier = {
				medium_plane_fighter_airframe_design_cost_factor = -0.50
				medium_plane_airframe_design_cost_factor = -0.50
			}
			ai_will_do = {
				factor = 1
			}
		}

		cost_efficient_airforce_spirit = {
			ledger = air
			research_bonus = {
				CAT_small_plane = 0.15
			}
			modifier = {
				medium_plane_airframe_design_cost_factor = -0.50
				small_plane_suicide_airframe_design_cost_factor = -0.50
			}
			ai_will_do = {
				factor = 1.5
			}
		}

		naval_support_aviation_spirit = {
			ledger = air
			research_bonus = {
				CAT_small_plane = 0.15
				CAT_medium_plane = 0.15
			}
			modifier = {
				cv_medium_plane_airframe_design_cost_factor = -0.50
				small_plane_airframe_design_cost_factor = -0.50
			}
			ai_will_do = {
				factor = 1
			}
		}

		helicopter_cavalry_spirit = {
			ledger = air
			research_bonus = {
				CAT_heli = 0.15
			}
			modifier = {
				attack_helicopter_hull_design_cost_factor = -0.50
			}
			ai_will_do = {
				factor = 1
			}
		}

		effective_training_programs_spirit = {
			ledger = air
			modifier = {
				air_training_xp_gain_factor = 0.25
			}
			ai_will_do = {
				factor = 1
			}
		}

		air_crew_surveys_spirit = {
			ledger = air
			modifier = {
				air_doctrine_cost_factor = -0.10
				air_accidents_factor = -0.25
			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	air_force_command_spirit = {

		battlefield_air_interdiction_spirit = {
			ledger = air
			modifier = {
				ground_attack_factor = 0.05
				air_escort_efficiency = 0.05
			}
			ai_will_do = {
				factor = 1
			}
		}

		cvw_night_fighting_spirit = {
			ledger = air
			modifier = {
				air_carrier_night_penalty_reduction_factor = 0.33
			}
			ai_will_do = {
				factor = 1
			}
		}

		veteran_air_instructors_spirit = {
			ledger = air
			modifier = {
				air_wing_xp_loss_when_killed_factor = -0.25
			}
			ai_will_do = {
				factor = 1
			}
		}

		centralized_control_spirit = {
			ledger = air
			modifier = {
				air_superiority_detect_factor = 0.1
				air_mission_efficiency = 0.1
			}
			ai_will_do = {
				factor = 1
			}
		}

		steel_wings_steel_hearts_spirit = {
			ledger = air
			modifier = {
				air_untrained_pilots_penalty_factor = -0.33
			}
			ai_will_do = {
				factor = 1
			}
		}

		home_defence_spirit = {
			ledger = air
			modifier = {
				air_home_defence_factor = 0.1
			}
			ai_will_do = {
				factor = 1
			}
		}

		air_power_projection_spirit = {
			ledger = air
			modifier = {
				air_power_projection_factor = 0.1
			}
			ai_will_do = {
				factor = 1
			}
		}

		massed_strike_spirit = {
			ledger = air
			modifier = {
				army_bonus_air_superiority_factor = 0.05
			}
			ai_will_do = {
				factor = 1
			}
		}

		continuous_strike_spirit = {
			ledger = air
			modifier = {
				air_cas_efficiency = 0.25
			}
			ai_will_do = {
				factor = 1
			}
		}

		total_devastation_spirit = {
			ledger = air
			modifier = {
				air_strategic_bomber_bombing_factor = 0.1
			}
			ai_will_do = {
				factor = 1
			}
		}
	}
}
#Made by Lord Bogdanoff
ideas = {
	country = {
		LRP_unproffesional_army = {
			allowed = {	always = no	}
			allowed_civil_war = { always = yes }
			removal_cost = -1
			picture = bad_army_idea
			modifier = {
				army_defence_factor = -0.30
				army_attack_factor = -0.30
				training_time_army_factor = -0.1
			}
		}
		LRP_proffesional_army = {
			allowed = {	always = no	}
			allowed_civil_war = { always = yes }
			removal_cost = -1
			picture = army_good_officer
			modifier = {
				army_defence_factor = 0.05
				army_attack_factor = 0.05
				training_time_army_factor = 0.09
			}
		}
		LRP_proffesional_army2 = {
			allowed = {	always = no	}
			name = LRP_proffesional_army
			allowed_civil_war = { always = yes }
			removal_cost = -1
			picture = army_good_officer
			modifier = {
				army_defence_factor = 0.15
				army_attack_factor = 0.15
				training_time_army_factor = 0.2
			}
		}
		LRP_bivaluta = {
			picture = dpr_bivaluta
			allowed = {	always = no	}
			allowed_civil_war = { always = yes }
			removal_cost = -1
			modifier = {
				stability_factor = -0.25
				tax_gain_multiplier_modifier = -0.2
				economic_cycles_cost_factor = 0.5
			}
		}
		LRP_no_government_idea = {
			allowed = {	always = no	}
			allowed_civil_war = { always = yes }
			removal_cost = -1
			picture = LRP_government_idea
			modifier = {
				political_power_gain = -0.05
				foreign_influence_defense_modifier = -0.15
			}
		}
		LRP_mgb = {
			allowed = {	always = no	}
			allowed_civil_war = { always = yes }
			removal_cost = -1
			picture = roit_police_idea
			modifier = {
				encryption_factor = 0.5
				foreign_influence_defense_modifier = 0.10
			}
		}
		LRP_ter_orobona = {
			allowed = {	always = no	}
			allowed_civil_war = { always = yes }
			removal_cost = -1
			picture = LRP_teroborona_idea
			modifier = {
				mobilization_speed = 0.04
				army_core_defence_factor = 0.03
				war_support_factor = 0.01
				training_time_army_factor = -0.02
			}
		}
		LRP_forts_buildings = {
			allowed = { always = no }
			picture = production_bonus
			modifier = {
				production_speed_bunker_factor = 0.2
			}
		}
		LRP_UDA_idea = {
			picture = UDA_idea
			modifier = {
				army_org_factor = -0.05
				army_attack_factor = 0.05
				army_defence_factor = 0.05
				army_breakthrough_against_major_factor = 0.05
			}
		}
		##########VOLHYNIA CONTENT##########
		VRP_unproffesional_army = {
			allowed = {	always = no	}
			allowed_civil_war = { always = yes }
			removal_cost = -1
			picture = bad_army_idea
			modifier = {
				army_defence_factor = -0.30
				army_attack_factor = -0.30
				training_time_army_factor = -0.1
			}
		}
		VRP_proffesional_army = {
			allowed = {	always = no	}
			allowed_civil_war = { always = yes }
			removal_cost = -1
			picture = army_good_officer
			modifier = {
				army_defence_factor = 0.05
				army_attack_factor = 0.05
				training_time_army_factor = 0.09
			}
		}
		VRP_proffesional_army2 = {
			allowed = {	always = no	}
			name = LRP_proffesional_army
			allowed_civil_war = { always = yes }
			removal_cost = -1
			picture = army_good_officer
			modifier = {
				army_defence_factor = 0.15
				army_attack_factor = 0.15
				training_time_army_factor = 0.2
			}
		}
		VRP_bivaluta = {
			picture = dpr_bivaluta
			allowed = {	always = no	}
			allowed_civil_war = { always = yes }
			removal_cost = -1
			modifier = {
				stability_factor = -0.25
				tax_gain_multiplier_modifier = -0.2
				economic_cycles_cost_factor = 0.5
			}
		}
		VRP_no_government_idea = {
			allowed = {	always = no	}
			allowed_civil_war = { always = yes }
			removal_cost = -1
			picture = vrp_govern_idea
			modifier = {
				political_power_gain = -0.05
				foreign_influence_defense_modifier = -0.15
			}
		}
		VRP_mgb = {
			allowed = {	always = no	}
			allowed_civil_war = { always = yes }
			removal_cost = -1
			picture = roit_police_idea
			modifier = {
				encryption_factor = 0.5
				foreign_influence_defense_modifier = 0.10
			}
		}
		VRP_ter_orobona = {
			allowed = {	always = no	}
			allowed_civil_war = { always = yes }
			removal_cost = -1
			picture = vrp_teroborona_idea
			modifier = {
				mobilization_speed = 0.04
				army_core_defence_factor = 0.03
				war_support_factor = 0.01
				training_time_army_factor = -0.02
			}
		}
		VRP_forts_buildings = {
			allowed = { always = no }
			picture = production_bonus
			modifier = {
				production_speed_bunker_factor = 0.2
			}
		}
		LRP_civil_war = {
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			cancel = {
				OR = {
					POL = { has_war_with = UKR }
					is_subject_of = POL
					is_subject_of = UKR
					NOT = { has_war_with = UKR }
				}
			}
			removal_cost = -1
			picture = lrp_war_galicia
			targeted_modifier = {
				tag = UKR
				attack_bonus_against = -0.10
				defense_bonus_against = 0.50
			}
		}
		UKR_civil_war1 = {
			allowed = {
				original_tag = UKR
			}
			allowed_civil_war = {
				always = yes
			}
			cancel = {
				OR = {
					POL = { has_war_with = UKR }
					is_subject_of = POL
					NOT = { country_exists = LRP }
					NOT = { country_exists = VRP }
					VRP = { is_subject_of = UKR }
					LRP = { is_subject_of = UKR }
				}
			}
			removal_cost = -1
			picture = lrp_war_galicia
			targeted_modifier = {
				tag = LRP
				attack_bonus_against = -0.48
				breakthrough_bonus_against = -0.6
				defense_bonus_against = 0.1
			}
			targeted_modifier = {
				tag = VRP
				attack_bonus_against = -0.48
				breakthrough_bonus_against = -0.6
				defense_bonus_against = 0.1
			}
		}
	}
}
ideas = {
	materiel_manufacturer = {
		designer = yes
		PHI_ferfrans_materiel_manufacturer = {
			allowed = { original_tag = PHI }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PHI_ferfrans_materiel_manufacturer" }
			picture = Ferfrans_logo
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_inf = 0.124
			}

			traits = { CAT_inf_4 }
			ai_will_do = {
				factor = 1
			}
		}
		PHI_floro_international_materiel_manufacturer = {
			allowed = { original_tag = PHI }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PHI_floro_international_materiel_manufacturer" }
			picture = floro-international-corporation
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_inf_wep = 0.155
			}

			traits = { CAT_inf_wep_5 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	aircraft_manufacturer = {
		designer = yes
		PHI_aviation_composite_technology_aircraft_manufacturer = {
			allowed = { original_tag = PHI }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PHI_aviation_composite_technology_aircraft_manufacturer" }
			picture = floro-international-corporation # TODO: Replace with Company Logo
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_l_fighter = 0.124
			}

			traits = { CAT_l_fighter_4 }
			ai_will_do = {
				factor = 1
			}
		}
	}
}

ideas = {
	materiel_manufacturer = {
		designer = yes

		FRA_nexter_materiel_manufacturer = {
			allowed = { original_tag = FRA }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_nexter_materiel_manufacturer" }

			picture = Nexter
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_inf = 0.217
			}

			traits = {
				CAT_inf_7

			}
			ai_will_do = {
				factor = 1
			}

		}
	}

	tank_manufacturer = {
		designer = yes

		FRA_nexter_tank_manufacturer = {
			allowed = { original_tag = FRA }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_nexter_tank_manufacturer" }

			picture = Nexter
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_armor = 0.217
			}

			traits = {
				CAT_armor_7
			}

			ai_will_do = {
				factor = 1
			}
		}

		FRA_airbus_helicopters_tank_manufacturer = {
			allowed = { original_tag = FRA }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_airbus_helicopters_tank_manufacturer" }

			picture = Airbus_helicopters

			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_heli = 0.248
			}

			traits = {
				CAT_heli_8

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	aircraft_manufacturer = {
		designer = yes

		FRA_dassault_aviation_aircraft_manufacturer = {
			allowed = { original_tag = FRA }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_dassault_aviation_aircraft_manufacturer" }

			picture = Dassault_Aviation

			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_fixed_wing = 0.248
			}

			traits = {
				CAT_fighter_8
			}

			ai_will_do = {
				factor = 1
			}
		}

		FRA_airbus_defence_aircraft_manufacturer = {
			allowed = { original_tag = FRA }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_airbus_defence_aircraft_manufacturer" }

			picture = Airbus_Defence

			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_fixed_wing = 0.279
			}

			traits = {
				CAT_fixed_wing_9
			}

			ai_will_do = {
				factor = 1
			}
		}
	}

	naval_manufacturer = {
		designer = yes

		FRA_naval_group_naval_manufacturer = {
			allowed = { original_tag = FRA }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_naval_group_naval_manufacturer" }

			picture = Naval_Group
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_naval_eqp = 0.248
			}

			traits = {
				CAT_naval_eqp_8
			}

			ai_will_do = {
				factor = 1
			}
		}

		FRA_naval_group_naval_manufacturer2 = {
			allowed = { original_tag = FRA }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_naval_group_naval_manufacturer" }

			picture = Naval_Group

			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_sub = 0.248
			}

			traits = {
				CAT_sub_8
			}

			ai_will_do = {
				factor = 1
			}
		}
	}
}
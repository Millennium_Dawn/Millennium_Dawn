ideas = {
	country = {
		#The Reclamation Front Ideas
		KAN_ideas_tribal_resistance_spirit = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea KAN_ideas_tribal_resistance_spirit" }
			picture = masters_of_our_fate
			allowed_civil_war = { always = yes }
			modifier = {
				surrender_limit = 0.50
				river_crossing_factor = -0.20
				terrain_penalty_reduction = 0.25
				pocket_penalty = -0.50
				army_core_attack_factor = 0.10
				army_core_defence_factor = 0.50
			}
		}
	
	
	}
}

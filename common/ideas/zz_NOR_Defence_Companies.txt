ideas = {
	materiel_manufacturer = {
		designer = yes
		NOR_kongsberg_materiel_manufacturer = {
			allowed = { original_tag = NOR }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea NOR_kongsberg_materiel_manufacturer" }
			picture = kongsberg-logo
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_aa = 0.248
			}

			traits = { CAT_aa_8 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	naval_manufacturer = {
		designer = yes
		NOR_stx_europe_naval_manufacturer = {
			allowed = { original_tag = NOR }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea NOR_stx_europe_naval_manufacturer" }
			picture = STX_Europe_logo
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_naval_eqp = 0.217
			}

			traits = { CAT_naval_eqp_7 }
			ai_will_do = {
				factor = 1
			}
		}

		NOR_umoe_mandal_naval_manufacturer = {
			allowed = { original_tag = NOR }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea NOR_umoe_mandal_naval_manufacturer" }
			picture = UMOE_Mandal
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_surface_ship = 0.155
			}

			traits = { CAT_surface_ship_5 }
			ai_will_do = {
				factor = 1
			}
		}
	}
}

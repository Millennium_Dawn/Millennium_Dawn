ideas = {
	country = {

		NATO_ukraine_help = {
			picture = GFX_idea_army_artillery_good
			allowed = {
				has_idea = NATO_member
			}
			modifier = {
				industrial_capacity_factory = -0.2
			}
		}
	}
}

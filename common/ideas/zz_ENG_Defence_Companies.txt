ideas = {

	materiel_manufacturer = {

		designer = yes

		ENG_bae_land_systems_materiel_manufacturer = {
			allowed = { original_tag = ENG }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ENG_bae_land_systems_materiel_manufacturer" }

			picture = BAE_Systems
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_inf = 0.279
			}

			traits = {
				CAT_inf_9

			}
			ai_will_do = {
				factor = 1
			}
		}

		ENG_qinetiq_materiel_manufacturer = {
			allowed = { original_tag = ENG }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ENG_qinetiq_materiel_manufacturer" }

			picture = QinetiQ
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_at = 0.217
			}

			traits = {
				CAT_at_7

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	tank_manufacturer = {

		designer = yes

		ENG_bae_land_systems_tank_manufacturer = {
			allowed = { original_tag = ENG }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ENG_bae_land_systems_tank_manufacturer" }

			picture = BAE_Systems
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_armor = 0.279
			}

			traits = {
				CAT_armor_9

			}
			ai_will_do = {
				factor = 1
			}
		}
		ENG_thales_tank_manufacturer = {
			allowed = { original_tag = ENG }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ENG_thales_tank_manufacturer" }

			picture = Thales

			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_afv = 0.248
			}

			traits = {
				CAT_afv_8

			}
			ai_will_do = {
				factor = 1
			}

		}

		ENG_agustawestland_tank_manufacturer = {
			allowed = { original_tag = ENG }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ENG_agustawestland_tank_manufacturer" }

			picture = AgustaWestland
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_heli = 0.217
			}

			traits = {
				CAT_heli_7

			}
			ai_will_do = {
				factor = 1
			}

		}
	}

	aircraft_manufacturer = {

		designer = yes

		ENG_bae_aerospace_aircraft_manufacturer = {
			allowed = { original_tag = ENG }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ENG_bae_aerospace_aircraft_manufacturer" }

			picture = BAE_Systems
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_fixed_wing = 0.248
			}

			traits = {
				CAT_fixed_wing_8

			}
			ai_will_do = {
				factor = 1
			}

		}
	}
	naval_manufacturer = {

		designer = yes

		ENG_bae_systems_marine_naval_manufacturer = {
			allowed = { original_tag = ENG }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ENG_bae_systems_marine_naval_manufacturer" }

			picture = BAE_Systems

			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_naval_eqp = 0.279
			}

			traits = {
				CAT_naval_eqp_9

			}
			ai_will_do = {
				factor = 1
			}

		}

		ENG_bae_systems_submarine_naval_manufacturer = {
			allowed = { original_tag = ENG }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ENG_bae_systems_submarine_naval_manufacturer" }

			picture = BAE_Systems
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_nuke_sub = 0.217
			}

			traits = {
				CAT_nuke_sub_7

			}
			ai_will_do = {
				factor = 1
			}
		}
	}
}
ideas = {
	country = {
		#The Florida Uprising Ideas
		FLA_ideas_better_dead_than_red = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FLA_ideas_better_dead_than_red" }
			picture = masters_of_our_fate
			allowed_civil_war = { always = yes }
			modifier = {
				surrender_limit = 0.50
				river_crossing_factor = -0.10
				terrain_penalty_reduction = 0.25
				pocket_penalty = -0.25
				army_core_attack_factor = 0.10
				army_core_defence_factor = 0.50
			}
		}


	}
}

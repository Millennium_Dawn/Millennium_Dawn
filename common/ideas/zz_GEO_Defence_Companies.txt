ideas = {

	tank_manufacturer = {

		designer = yes

		GEO_tbilisi_aircraft_manufacturing_tank_manufacturer = {
			allowed = { original_tag = GEO }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea GEO_tbilisi_aircraft_manufacturing_tank_manufacturer" }

			picture = Tbilisi_Aircraft_Manufacturing
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_armor = 0.093
			}

			traits = {
				CAT_armor_3

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	materiel_manufacturer = {

		designer = yes

		GEO_tbilisi_aircraft_manufacturing_materiel_manufacturer = {
			allowed = { original_tag = GEO }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea GEO_tbilisi_aircraft_manufacturing_materiel_manufacturer" }

			picture = Tbilisi_Aircraft_Manufacturing

			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_inf_wep = 0.093
			}

			traits = {
				CAT_inf_wep_3
			}
			ai_will_do = {
				factor = 1
			}
		}
	}
}

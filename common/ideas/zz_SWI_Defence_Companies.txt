ideas = {

	tank_manufacturer = {

		designer = yes

		SWI_mowag_tank_manufacturer = {
			allowed = { original_tag = SWI }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SWI_mowag_tank_manufacturer" }

			picture = mowag_logo
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_afv = 0.248
			}

			traits = {
				CAT_afv_8

			}
			ai_will_do = {
				factor = 1
			}

		}
	}

	materiel_manufacturer = {

		designer = yes

		SWI_ruag_materiel_manufacturer = {
			allowed = { original_tag = SWI }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SWI_ruag_materiel_manufacturer" }

			picture = RUAG
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_inf_wep = 0.217
			}

			traits = {
				CAT_inf_wep_7

			}
			ai_will_do = {
				factor = 1
			}

		}
	}

	aircraft_manufacturer = {

		designer = yes

		SWI_ruag_aircraft_manufacturer = {
			allowed = { original_tag = SWI }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SWI_ruag_aircraft_manufacturer" }

			picture = RUAG
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_h_air = 0.217
			}

			traits = {
				CAT_h_air_7

			}
			ai_will_do = {
				factor = 1
			}
		}
		SWI_pilatus_aircraft_aircraft_manufacturer = {
			allowed = { original_tag = SWI }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SWI_pilatus_aircraft_aircraft_manufacturer" }

			picture = Pilatus_Aircraft_logo

			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_l_fighter = 0.186
			}

			traits = {
				CAT_l_fighter_6

			}
			ai_will_do = {
				factor = 1
			}
		}
	}
}
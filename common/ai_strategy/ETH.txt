ETH_eritrea_border_conflicts = {
	allowed = { original_tag = ETH }
	enable = {
		country_exists = ERI
		has_opinion_modifier = ERI_Border_Disputes
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = antagonize id = "ERI" value = 100 }
	ai_strategy = { type = contain id = "ERI" value = 100 }
	ai_strategy = { type = conquer id = "ERI" value = 100 }
}

ETH_support_somalia_against_SHB = {
	allowed = { original_tag = ETH }
	enable = {
		country_exists = SHB
		SHB = { has_war_with = SOM }
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = befriend id = "SOM" value = 75 }
	ai_strategy = { type = support id = "SOM" value = 75 }
	ai_strategy = { type = send_volunteers_desire id = "SOM" value = 100 }
}

#If Mengistu comes to power, he will support Sudanese rebels against Sudan
ETH_support_SSU = {
	allowed = { original_tag = ERI }

	enable = {
		original_tag = ERI
		NOT = { has_war_with = SSU }
		emerging_communist_state_are_in_power = yes
	}

	abort_when_not_enabled = yes

	ai_strategy = { type = support id = "SSU" value = 50 }
	ai_strategy = { type = befriend id = "SSU" value = 50 }
	ai_strategy = { type = support id = "SUD" value = -50 }
	ai_strategy = { type = befriend id = "SUD" value = -50 }
}
GER_we_want_the_pumaifv = { #tell GER AI to research the 2015 IFV hull ahead of time
	allowed = { original_tag = GER }
	enable = {
		country_exists = GER
		NOT = { has_tech = afv_tech_3 }
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = research_tech id = afv_tech_3 value = 100 }
}
GER_support_afghanistan = {
	allowed = { original_tag = GER }
	enable = {
		country_exists = AFG
		NOT = { has_war_with = AFG }
		AFG = { NOT = { has_government = fascism } }
	}
	abort = {
		OR = {
			NOT = { country_exists = AFG }
			has_war_with = AFG
			AFG = { has_government = fascism }
		}
	}

	ai_strategy = { type = befriend id = "AFG" value = 50 }
	ai_strategy = { type = support id = "AFG" value = 150 }
	ai_strategy = { type = send_volunteers_desire id = "AFG" value = 50 }
}

##Germany unique AI for combat units
GER_default_naval_breakdown = {
	allowed = { original_tag = GER }
	enable = { original_tag = GER }
	abort_when_not_enabled = yes

	ai_strategy = { type = role_ratio id = naval_corvettes value = 25 }
	ai_strategy = { type = role_ratio id = naval_frigate value = 30 }
	ai_strategy = { type = role_ratio id = naval_destroyer value = 15 }
	ai_strategy = { type = role_ratio id = naval_stealth_destroyer value = 2 }
	ai_strategy = { type = role_ratio id = naval_attack_submarine value = 20 }
	ai_strategy = { type = role_ratio id = naval_missile_submarine value = 2 }
	ai_strategy = { type = role_ratio id = naval_helicopter_operator value = 4 }
	ai_strategy = { type = role_ratio id = naval_carrier value = 1 }
	ai_strategy = { type = role_ratio id = naval_cruiser value = 1 }
}

GER_support_bosnian_against_rsk = {
	allowed = { original_tag = GER }
	enable = {
		BOS = {
			has_country_flag = defeat_all_serbs
			OR = {
				has_idea = BOS_EUFOR
				has_idea = BOS_SFOR
			}
		}
		RSK = { has_war_with = BOS }
		has_idea = bos_sfor_vol_troops
	}
	abort_when_not_enabled = yes

	ai_strategy = {
		type = send_volunteers_desire
		id = BOS
		value = 200
	}
	ai_strategy = {
		type = befriend
		id = BOS
		value = 200
	}
	ai_strategy = {
		type = support
		id = BOS
		value = 200
	}
}
#CIVIL WAR IN AMERICA - GERMANY
GER_texas_help = {
	allowed = { original_tag = GER }
	enable = {
		original_tag = GER
		country_exists = TEX
		GER = {
			has_war = no
			has_government = nationalist
		}
	}
	abort_when_not_enabled = yes
	ai_strategy = { type = send_volunteers_desire id = "TEX" value = 1000 }
	ai_strategy = { type = send_lend_lease_desire id = "TEX" value = 400 }
}
GER_lakota_help = {
	allowed = { original_tag = GER }
	enable = {
		original_tag = GER
		country_exists = CAS
		GER = {
			has_war = no
			has_government = nationalist
		}
	}
	abort_when_not_enabled = yes
	ai_strategy = { type = send_volunteers_desire id = "CAS" value = 1000 }
	ai_strategy = { type = send_lend_lease_desire id = "CAS" value = 400 }
}
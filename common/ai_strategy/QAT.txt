#Libya befriendeed Qatar
QAT_befriend_libya = {
	allowed = { original_tag = QAT }

	enable = {
		LBA = { has_completed_focus = LBA_contact_qatar }
		NOT = { has_war_with = LBA }
	}

	abort_when_not_enabled = yes

	ai_strategy = { type = befriend id = "LBA" value = 25 }
}
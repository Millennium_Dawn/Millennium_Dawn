china_friend = {
   set_temp_variable = { treasury_change = 5 }
   modify_treasury_effect = yes
		set_temp_variable = { percent_change = 8 }
		set_temp_variable = { tag_index = CHI }
		set_temp_variable = { influence_target = ABK }
		change_influence_percentage = yes
		add_opinion_modifier = {
			target = CHI
			modifier = diplomatic_support
		}
		reverse_add_opinion_modifier = {
			target = CHI
			modifier = diplomatic_support
		}
}			
	

modify_soviet_support = {
	add_to_variable = { soviet_support = modify_soviet_support }

	custom_effect_tooltip = soviet_support_TT

	clamp_variable = {
		var = soviet_support
		min = 0
		max = 100
	}
}

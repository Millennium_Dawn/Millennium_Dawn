TAJ_check_radicalism = {
	if = {
		limit = {
			check_variable = {
				TAJ_radical_influence > 99
			}
		}
		country_event = {
			id = tajik_radicalism.1
			days = 2
		}
	}
	random_list = {
		50 = {
			modifier = {
				check_variable = {
					TAJ_radical_influence > 50
				}
				factor = 1.25
			}
			if = { #Intense attacks
				limit = {
					check_variable = {
						TAJ_radical_influence > 74
					}
				}
				random_list = {
					25 = {
						country_event = {
							id = tajik_radicalism.2
						}
					}
					25 = {
						country_event = {
							id = tajik_radicalism.3
						}
					}
					25 = {
						country_event = {
							id = tajik_radicalism.4
						}
					}
					25 = {
						country_event = {
							id = tajik_radicalism.5
						}
					}
				}
			}
			else_if = { #Moderate attacks
				limit = {
					check_variable = {
						TAJ_radical_influence > 49
					}
				}
				random_list = {
					25 = {
						country_event = {
							id = tajik_radicalism.6
						}
					}
					25 = {
						country_event = {
							id = tajik_radicalism.7
						}
					}
					25 = {
						country_event = {
							id = tajik_radicalism.8
						}
					}
					25 = {
						country_event = {
							id = tajik_radicalism.9
						}
					}
				}
			}
			else_if = { #Light attacks
				limit = {
					check_variable = {
						TAJ_radical_influence > 15
					}
				}
				random_list = {
					25 = {
						country_event = {
							id = tajik_radicalism.10
						}
					}
					25 = {
						country_event = {
							id = tajik_radicalism.11
						}
					}
					25 = {
						country_event = {
							id = tajik_radicalism.12
						}
					}
					25 = {
						country_event = {
							id = tajik_radicalism.14
						}
					}
				}
			}
		}
		50 = {
			# Nothing
		}
	}
}

IRN_vote = {
	if = {
		limit = {
			TAJ = {
				check_variable = {
					TAJ.iranic_states_vote > 1
				}
			}
		}
		custom_effect_tooltip = TAJ_taj_TT
		TAJ = {
			country_event = {
				id = iranic_confederation.5
				days = 5
			}
		}
	}
	if = {
		limit = {
			PER = {
				check_variable = {
					PER.iranic_states_vote > 1
				}
			}
		}
		custom_effect_tooltip = TAJ_per_TT
		PER = {
			country_event = {
				id = iranic_confederation.5
				days = 5
			}
		}
	}
	if = {
		limit = {
			AFG = {
				check_variable = {
					AFG.iranic_states_vote > 1
				}
			}
		}
		custom_effect_tooltip = TAJ_afg_TT
		AFG = {
			country_event = {
				id = iranic_confederation.5
				days = 5
			}
		}
	}
	if = {
		limit = {
			KUR = {
				check_variable = {
					KUR.iranic_states_vote > 1
				}
			}
		}
		custom_effect_tooltip = TAJ_kur_TT
		KUR = {
			country_event = {
				id = iranic_confederation.5
				days = 5
			}
		}
	}
	if = {
		limit = {
			SOO = {
				check_variable = {
					SOO.iranic_states_vote > 1
				}
			}
		}
		custom_effect_tooltip = TAJ_soo_TT
		SOO = {
			country_event = {
				id = iranic_confederation.5
				days = 5
			}
		}
	}
}

IRN_event = {
	hidden_effect = {
		if = {
			limit = {
				ROOT = {
					original_tag = TAJ
				}
			}
			TAJ = {
				country_event = {
					id = iranic_confederation.8
					days = 5
				}
			}
		}
		if = {
			limit = {
				ROOT = {
					original_tag = AFG
				}
			}
			AFG = {
				country_event = {
					id = iranic_confederation.8
					days = 5
				}
			}
		}
		if = {
			limit = {
				ROOT = {
					original_tag = PER
				}
			}
			PER = {
				country_event = {
					id = iranic_confederation.8
					days = 5
				}
			}
		}
		if = {
			limit = {
				ROOT = {
					original_tag = KUR
				}
			}
			KUR = {
				country_event = {
					id = iranic_confederation.8
					days = 5
				}
			}
		}
		if = {
			limit = {
				ROOT = {
					original_tag = SOO
				}
			}
			SOO = {
				country_event = {
					id = iranic_confederation.8
					days = 5
				}
			}
		}
	}
}

# Random bad pop-ups for the black market
TAJ_random_pop_up_bad_high = {
	random_list = {
		50 = {
			add_to_variable = { TAJ_event_counter_1_bad_event = 1 }
		}
		50 = {
		}
	}
	if = {
		limit = { check_variable = { TAJ_event_counter_1_bad_event > 7 } }
		set_variable = { TAJ_event_counter_1_bad_event = 0 }
		random_list = {
			10 = {
				country_event = tajik_corruption.2
			}
			11 = {
				country_event = tajik_corruption.3
			}
			11 = {
				country_event = tajik_corruption.4
			}
			11 = {
				country_event = tajik_corruption.5
			}
			11 = {
				country_event = tajik_corruption.6
			}
			11 = {
				country_event = tajik_corruption.7
			}
			11 = {
				country_event = tajik_corruption.8
			}
			12 = {
				country_event = tajik_corruption.9
			}
			12 = {
				country_event = tajik_corruption.10
			}
		}
	}
}

TAJ_random_pop_up_bad_medium = {
	random_list = {
		50 = {
			add_to_variable = { TAJ_event_counter_1_bad_event = 1 }
		}
		50 = {
		}
	}
	if = {
		limit = { check_variable = { TAJ_event_counter_1_bad_event > 5 } }
		set_variable = { TAJ_event_counter_1_bad_event = 0 }
		random_list = {
			10 = {
				country_event = tajik_corruption.2
			}
			11 = {
				country_event = tajik_corruption.3
			}
			11 = {
				country_event = tajik_corruption.4
			}
			11 = {
				country_event = tajik_corruption.5
			}
			11 = {
				country_event = tajik_corruption.6
			}
			11 = {
				country_event = tajik_corruption.7
			}
			11 = {
				country_event = tajik_corruption.8
			}
			12 = {
				country_event = tajik_corruption.9
			}
			12 = {
				country_event = tajik_corruption.10
			}
		}
	}
}

TAJ_random_pop_up_bad_low = {
	random_list = {
		60 = {
			add_to_variable = { TAJ_event_counter_1_bad_event = 1 }
		}
		40 = {
		}
	}
	if = {
		limit = { check_variable = { TAJ_event_counter_1_bad_event > 4 } }
		set_variable = { TAJ_event_counter_1_bad_event = 0 }
		random_list = {
			10 = {
				country_event = tajik_corruption.2
			}
			11 = {
				country_event = tajik_corruption.3
			}
			11 = {
				country_event = tajik_corruption.4
			}
			11 = {
				country_event = tajik_corruption.5
			}
			11 = {
				country_event = tajik_corruption.6
			}
			11 = {
				country_event = tajik_corruption.7
			}
			11 = {
				country_event = tajik_corruption.8
			}
			12 = {
				country_event = tajik_corruption.9
			}
			12 = {
				country_event = tajik_corruption.10
			}
		}
	}
}

# Good Events

TAJ_random_pop_up_good_high = {
	random_list = {
		30 = {
			add_to_variable = { TAJ_event_counter_1_good_event = 1 }
		}
		70 = {
		}
	}
	if = {
		limit = { check_variable = { TAJ_event_counter_1_good_event > 6 } }
		set_variable = { TAJ_event_counter_1_good_event = 0 }
		random_list = {
			10 = {
				country_event = tajik_corruption.11
			}
			11 = {
				country_event = tajik_corruption.12
			}
			11 = {
				country_event = tajik_corruption.13
			}
			11 = {
				country_event = tajik_corruption.14
			}
			11 = {
				country_event = tajik_corruption.15
			}
			11 = {
				country_event = tajik_corruption.16
			}
			11 = {
				country_event = tajik_corruption.17
			}
			12 = {
				country_event = tajik_corruption.18
			}
			12 = {
				country_event = tajik_corruption.19
			}
		}
	}
}

TAJ_random_pop_up_good_medium = {
	random_list = {
		40 = {
			add_to_variable = { TAJ_event_counter_1_good_event = 1 }
		}
		60 = {
		}
	}
	if = {
		limit = { check_variable = { TAJ_event_counter_1_good_event > 5 } }
		set_variable = { TAJ_event_counter_1_good_event = 0 }
		random_list = {
			10 = {
				country_event = tajik_corruption.11
			}
			11 = {
				country_event = tajik_corruption.12
			}
			11 = {
				country_event = tajik_corruption.13
			}
			11 = {
				country_event = tajik_corruption.14
			}
			11 = {
				country_event = tajik_corruption.15
			}
			11 = {
				country_event = tajik_corruption.16
			}
			11 = {
				country_event = tajik_corruption.17
			}
			12 = {
				country_event = tajik_corruption.18
			}
			12 = {
				country_event = tajik_corruption.19
			}
		}
	}
}


TAJ_random_pop_up_good_low = {
	random_list = {
		50 = {
			add_to_variable = { TAJ_event_counter_1_good_event = 1 }
		}
		50 = {
		}
	}
	if = {
		limit = { check_variable = { TAJ_event_counter_1_good_event > 3 } }
		set_variable = { TAJ_event_counter_1_good_event = 0 }
		random_list = {
			10 = {
				country_event = tajik_corruption.11
			}
			11 = {
				country_event = tajik_corruption.12
			}
			11 = {
				country_event = tajik_corruption.13
			}
			11 = {
				country_event = tajik_corruption.14
			}
			11 = {
				country_event = tajik_corruption.15
			}
			11 = {
				country_event = tajik_corruption.16
			}
			11 = {
				country_event = tajik_corruption.17
			}
			12 = {
				country_event = tajik_corruption.18
			}
			12 = {
				country_event = tajik_corruption.19
			}
		}
	}
}
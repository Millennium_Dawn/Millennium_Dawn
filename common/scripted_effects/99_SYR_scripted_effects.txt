# Author(s): AngriestBird, Viruce
SYR_decrease_divided_syria = {
	if = { limit = { has_idea = divided_syria_3 }
		remove_ideas = divided_syria_3
	}
	else_if = { limit = { has_idea = divided_syria_2 }
		swap_ideas = {
			add_idea = divided_syria_3
			remove_idea = divided_syria_2
		}
	}
	else_if = { limit = { has_idea = divided_syria_1 }
		swap_ideas = {
			add_idea = divided_syria_2
			remove_idea = divided_syria_1
		}
	}
	else_if = { limit = { has_idea = divided_syria }
		swap_ideas = {
			add_idea = divided_syria_1
			remove_idea = divided_syria
		}
	}
}

SYR_alawite_political_activity_modifier = {
	set_variable = { alawite_status = 0 }
	if = {
		limit = { has_idea = crippling_corruption }
		add_to_variable = { alawite_status = 2.5 }
	}
	else_if = {
		limit = { has_idea = rampant_corruption }
		add_to_variable = { alawite_status = 5 }
	}
	else_if = {
		limit = { has_idea = unrestrained_corruption }
		add_to_variable = { alawite_status = 7.5 }
	}
	else_if = {
		limit = { has_idea = systematic_corruption }
		add_to_variable = { alawite_status = 10 }
	}
	else_if = {
		limit = { has_idea = widespread_corruption }
		add_to_variable = { alawite_status = 12.5 }
	}
	else_if = {
		limit = { has_idea = medium_corruption }
		add_to_variable = { alawite_status = 15 }
	}
	else_if = {
		limit = { has_idea = modest_corruption }
		add_to_variable = { alawite_status = 17.5 }
	}
	else_if = {
		limit = { has_idea = slight_corruption }
		add_to_variable = { alawite_status = 20 }
	}
	else_if = {
		limit = { has_idea = negligible_corruption }
		add_to_variable = { alawite_status = 22.5 }
	}

	#calculating social liberalism effect
	set_variable = { social_liberalism = 100 }
	subtract_from_variable = { social_liberalism = social_conservatism_society }
	divide_variable = { social_liberalism = 4 }
	add_to_variable = { alawite_status = social_liberalism }

	#effect of islamist renaissance
	if = { limit = { has_completed_focus = SYR_islamist_renaissance }
		subtract_from_variable = { alawite_status = 10 }
	}

	#effect of religious values
	if = { limit = { has_completed_focus = SYR_religious_values }
		subtract_from_variable = { alawite_status = 20 }
	}

	#effect of empowered sharia courts
	if = { limit = { has_completed_focus = SYR_empower_sharia_courts }
		subtract_from_variable = { alawite_status = 20 }
	}

	#calculating infrastructure effect
	set_variable = { alawite_infrastructure_effect = 0 }
	if = {
		limit = { 189 = { is_owned_by = SYR } }
		add_to_variable = { alawite_infrastructure_effect = 189.infrastructure_level }
	}
	if = {
		limit = { 158 = { is_owned_by = SYR } }
		add_to_variable = { alawite_infrastructure_effect = 158.infrastructure_level }
	}
	multiply_variable = { alawite_infrastructure_effect = 2.5 }
	add_to_variable = { alawite_status = alawite_infrastructure_effect }

	#calculating infrastructure effect
	set_variable = { alawite_int_infrastructure_effect = 0 }
	if = {
		limit = { 189 = { is_owned_by = SYR } }
		add_to_variable = { alawite_int_infrastructure_effect = 189.building_level@internet_station }
	}
	if = {
		limit = { 158 = { is_owned_by = SYR } }
		add_to_variable = { alawite_int_infrastructure_effect = 158.building_level@internet_station }
	}
	multiply_variable = { alawite_int_infrastructure_effect = 1.5 }
	add_to_variable = { alawite_status = alawite_int_infrastructure_effect }

	#calculating difference in gdps per capita to determine the effect
	set_variable = { latakia_gdp = 0 }
	if = {
		limit = { 189 = { is_owned_by = SYR } }
		set_variable = { latakia_industrial_complex_gdp = 189.industrial_complex_level }
		multiply_variable = { latakia_industrial_complex_gdp = 248 }
		add_to_variable = { latakia_gdp = latakia_industrial_complex_gdp }

		set_variable = { latakia_arms_factory_gdp = 189.building_level@arms_factory }
		multiply_variable = { latakia_arms_factory_gdp = 155 }
		add_to_variable = { latakia_gdp = latakia_arms_factory_gdp }

		set_variable = { latakia_dockyard_gdp = 189.building_level@dockyard }
		multiply_variable = { latakia_dockyard_gdp = 155 }
		add_to_variable = { latakia_gdp = latakia_dockyard_gdp }

		set_variable = { latakia_offices_gdp = 189.building_level@offices }
		multiply_variable = { latakia_offices_gdp = 388 }
		add_to_variable = { latakia_gdp = latakia_offices_gdp }
	}
	set_variable = { hatay_gdp = 0 }
	if = {
		limit = { 158 = { is_owned_by = SYR } }
		set_variable = { hatay_industrial_complex_gdp = 158.industrial_complex_level }
		multiply_variable = { hatay_industrial_complex_gdp = 248 }
		add_to_variable = { hatay_gdp = hatay_industrial_complex_gdp }

		set_variable = { hatay_arms_factory_gdp = 158.building_level@arms_factory }
		multiply_variable = { hatay_arms_factory_gdp = 155 }
		add_to_variable = { hatay_gdp = hatay_arms_factory_gdp }

		set_variable = { hatay_dockyard_gdp = 158.building_level@dockyard }
		multiply_variable = { hatay_dockyard_gdp = 155 }
		add_to_variable = { hatay_gdp = hatay_dockyard_gdp }

		set_variable = { hatay_offices_gdp = 158.building_level@offices }
		multiply_variable = { hatay_offices_gdp = 388 }
		add_to_variable = { hatay_gdp = hatay_offices_gdp }
	}

	set_variable = { alawite_gdp_per_capita = hatay_gdp }
	add_to_variable = { alawite_gdp_per_capita = latakia_gdp }
	set_variable = { alawite_population = 0 }
	if = {
		limit = { 158 = { is_owned_by = SYR } }
		add_to_variable = { alawite_population = 158.state_population }
	}
	if = {
		limit = { 189 = { is_owned_by = SYR } }
		add_to_variable = { alawite_population = 189.state_population }
	}
	divide_variable = { alawite_gdp_per_capita = alawite_population }
	divide_variable = { alawite_gdp_per_capita = gdp_per_capita }
	multiply_variable = { alawite_gdp_per_capita = 45 }
	add_to_variable = { alawite_status = alawite_gdp_per_capita }

	#assads in power effect
	if = {
		limit = { NOT = { has_completed_focus = SYR_kick_out_al_assads } }
		add_to_variable = { alawite_status = 40 }
	}

	#reserved parliamentary seats effect
	if = {
		limit = { has_country_flag = alawite_reserved_parliamentary_seats }
		add_to_variable = { alawite_status = 30 }
	}

	#autonomous_region_event
	if = {
		limit = { has_country_flag = alawite_semi_autonomy }
		add_to_variable = { alawite_status = 30 }
	}

	#discontent effects
	if = {
		limit = { has_idea = alawite_discontent }
		add_to_variable = { alawite_status = -20 }
	}

	clamp_variable = { var = alawite_status min = 0 max = 100 }

	#law change cost calculations for federal path
	if = {
		limit = { has_country_flag = alawite_semi_autonomy }
		set_variable = { alawite_gov_change_cost = alawite_status }
		subtract_from_variable = { alawite_gov_change_cost = 50 }
		multiply_variable = { alawite_gov_change_cost = -0.003 }
		clamp_variable = { var = alawite_gov_change_cost min = 0 max = 1 }
		add_to_variable = { alawite_gov_change_cost = 0.05 }
	}

#stability effect calculations
	set_variable = { alawite_status_stability_modifier = alawite_status }
	subtract_from_variable = { alawite_status_stability_modifier = 50 }
	divide_variable = { alawite_status_stability_modifier = 1000 }

#drift defense calculations
	set_variable = { alawite_status_drift_defense = alawite_status }
	subtract_from_variable = { alawite_status_drift_defense = 50 }
	multiply_variable = { alawite_status_drift_defense = -2 }
	divide_variable = { alawite_status_drift_defense = 1000 }

	force_update_dynamic_modifier = yes
}

SYR_kurdish_political_activity_modifier = {

	set_variable = { kurdish_status = 0 }
	if = {			#calculating corruption effect
		limit = { has_idea = crippling_corruption }
		add_to_variable = { kurdish_status = 4 }
	}
	else_if = {
		limit = { has_idea = rampant_corruption }
		add_to_variable = { kurdish_status = 8 }
	}
	else_if = {
		limit = { has_idea = unrestrained_corruption }
		add_to_variable = { kurdish_status = 12 }
	}
	else_if = {
		limit = { has_idea = systematic_corruption }
		add_to_variable = { kurdish_status = 16 }
	}
	else_if = {
		limit = { has_idea = widespread_corruption }
		add_to_variable = { kurdish_status = 20 }
	}
	else_if = {
		limit = { has_idea = medium_corruption }
		add_to_variable = { kurdish_status = 24 }
	}
	else_if = {
		limit = { has_idea = modest_corruption }
		add_to_variable = { kurdish_status = 28 }
	}
	else_if = {
		limit = { has_idea = slight_corruption }
		add_to_variable = { kurdish_status = 32 }
	}
	else_if = {
		limit = { has_idea = negligible_corruption }
		add_to_variable = { kurdish_status = 36 }
	}

#calculating infrastructure effect
	set_variable = { kurdish_infrastructure_effect = 193.infrastructure_level } #calculating infrastructure effect
	multiply_variable = { kurdish_infrastructure_effect = 2.5 }
	add_to_variable = { kurdish_status = kurdish_infrastructure_effect }

#calculating internet infrastructure effect:
	set_variable = { kurdish_int_infrastructure_effect = 193.building_level@internet_station }
	multiply_variable = { kurdish_int_infrastructure_effect = 1.5 }
	add_to_variable = { kurdish_status = kurdish_int_infrastructure_effect }

#calculating difference in gdps per capita to determine the effect
	set_variable = { kurdish_industrial_complex_gdp = 193.industrial_complex_level }
	multiply_variable = { kurdish_industrial_complex_gdp = 248 }
	add_to_variable = { kurdish_gdp_per_capita = kurdish_industrial_complex_gdp }

	set_variable = { kurdish_arms_factory_gdp = 193.building_level@arms_factory }
	multiply_variable = { kurdish_arms_factory_gdp = 155 }
	add_to_variable = { kurdish_gdp_per_capita = kurdish_arms_factory_gdp }

	set_variable = { kurdish_dockyard_gdp = 193.building_level@dockyard }
	multiply_variable = { kurdish_dockyard_gdp = 155 }
	add_to_variable = { kurdish_gdp_per_capita = kurdish_dockyard_gdp }

	set_variable = { kurdish_offices_gdp = 193.building_level@offices }
	multiply_variable = { kurdish_offices_gdp = 388 }
	add_to_variable = { kurdish_gdp_per_capita = kurdish_offices_gdp }

	divide_variable = { kurdish_gdp_per_capita = 193.state_population }
	divide_variable = { kurdish_gdp_per_capita = gdp_per_capita }
	multiply_variable = { kurdish_gdp_per_capita = 45 }
	add_to_variable = { kurdish_status = kurdish_gdp_per_capita }

#kurds citizenship denied effect
	if = {
		limit = { has_idea = idea_syrian_kurds_without_citizenships }
		subtract_from_variable = { kurdish_status = 40 }
	}

#reserved parliamentary seats effect
	if = {
		limit = { has_country_flag = rojavan_reserved_parliamentary_seats }
		add_to_variable = { kurdish_status = 30 }
	}

#autonomous_region_event
	if = {
		limit = { has_country_flag = rojava_semi_autonomy }
		add_to_variable = { kurdish_status = 30 }
	}

#discontent effects
	if = {
		limit = { has_idea = kurdish_discontent }
		add_to_variable = { kurdish_status = -20 }
	}

#clamping the variable
	clamp_variable = {
		var = kurdish_status
		min = 0
		max = 100
	}


#law change cost calculations for federal path
	if = {
		limit = { has_country_flag = rojava_semi_autonomy }
		set_variable = { kurdish_gov_change_cost = kurdish_status }
		subtract_from_variable = { kurdish_gov_change_cost = 50 }
		multiply_variable = { kurdish_gov_change_cost = -0.003 }
		clamp_variable = {
			var = kurdish_gov_change_cost
			min = 0
			max = 1
		}
		add_to_variable = { kurdish_gov_change_cost = 0.05 }
	}

#stability effect calculations
	set_variable = { kurdish_status_stability_modifier = kurdish_status }
	subtract_from_variable = { kurdish_status_stability_modifier = 50 }
	multiply_variable = { kurdish_status_stability_modifier = 2 }
	divide_variable = { kurdish_status_stability_modifier = 1000 }

#drift defense calculations
	set_variable = { kurdish_status_drift_defense = kurdish_status }
	subtract_from_variable = { kurdish_status_drift_defense = 50 }
	multiply_variable = { kurdish_status_drift_defense = -3 }
	divide_variable = { kurdish_status_drift_defense = 1000 }


	force_update_dynamic_modifier = yes
}

SYR_druze_political_activity_modifier = {

	set_variable = { druze_status = 0 }
	#calculating corruption effect
	if = {
		limit = { has_idea = crippling_corruption }
		add_to_variable = { druze_status = 2.5 }
	}
	else_if = {
		limit = { has_idea = rampant_corruption }
		add_to_variable = { druze_status = 5 }
	}
	else_if = {
		limit = { has_idea = unrestrained_corruption }
		add_to_variable = { druze_status = 7.5 }
	}
	else_if = {
		limit = { has_idea = systematic_corruption }
		add_to_variable = { druze_status = 10 }
	}
	else_if = {
		limit = { has_idea = widespread_corruption }
		add_to_variable = { druze_status = 12.5 }
	}
	else_if = {
		limit = { has_idea = medium_corruption }
		add_to_variable = { druze_status = 15 }
	}
	else_if = {
		limit = { has_idea = modest_corruption }
		add_to_variable = { druze_status = 17.5 }
	}
	else_if = {
		limit = { has_idea = slight_corruption }
		add_to_variable = { druze_status = 20 }
	}
	else_if = {
		limit = { has_idea = negligible_corruption }
		add_to_variable = { druze_status = 22.5 }
	}

#calculating social liberalism effect
	set_variable = { social_liberalism = 100 }
	subtract_from_variable = { social_liberalism = social_conservatism_society }
	divide_variable = { social_liberalism = 4 }
	add_to_variable = { druze_status = social_liberalism }

#effect of islamist renaissance
	if = {
		limit = { has_completed_focus = SYR_islamist_renaissance }
		subtract_from_variable = { druze_status = 10 }
	}

#effect of religious values
	if = { limit = { has_completed_focus = SYR_religious_values }
		subtract_from_variable = { druze_status = 20 }
	}

#effect of empowered sharia courts
	if = { limit = { has_completed_focus = SYR_empower_sharia_courts }
		subtract_from_variable = { druze_status = 20 }
	}

#calculating infrastructure effect
	set_variable = { druze_infrastructure_effect = 0 }
	if = {
		limit = { 185 = { is_owned_by = SYR } }
		add_to_variable = { druze_infrastructure_effect = 185.infrastructure_level }
	}
	if = {
		limit = { 184 = { is_owned_by = SYR } }
		add_to_variable = { druze_infrastructure_effect = 184.infrastructure_level }
	}
	if = {
		limit = { 587 = { is_owned_by = SYR } }
		add_to_variable = { druze_infrastructure_effect = 587.infrastructure_level }
	}
	multiply_variable = { druze_infrastructure_effect = 2.5 }
	add_to_variable = { druze_status = druze_infrastructure_effect }

	#calculating infrastructure effect
	set_variable = { druze_int_infrastructure_effect = 0 }
	if = {
		limit = { 185 = { is_owned_by = SYR } }
		add_to_variable = { druze_int_infrastructure_effect = 185.building_level@internet_station }
	}
	if = {
		limit = { 184 = { is_owned_by = SYR } }
		add_to_variable = { druze_int_infrastructure_effect = 184.building_level@internet_station }
	}
	if = {
		limit = { 587 = { is_owned_by = SYR } }
		add_to_variable = { druze_int_infrastructure_effect = 587.building_level@internet_station }
	}
	multiply_variable = { druze_int_infrastructure_effect = 1.5 }
	add_to_variable = { druze_status = druze_int_infrastructure_effect }

#calculating difference in gdps per capita to determine the effect
	set_variable = { suwayda_gdp = 0 }
	if = {
		limit = { 185 = { is_owned_by = SYR } }
		set_variable = { suwayda_industrial_complex_gdp = 185.industrial_complex_level }
		multiply_variable = { suwayda_industrial_complex_gdp = 248 }
		add_to_variable = { suwayda_gdp = suwayda_industrial_complex_gdp }

		set_variable = { suwayda_arms_factory_gdp = 185.building_level@arms_factory }
		multiply_variable = { suwayda_arms_factory_gdp = 155 }
		add_to_variable = { suwayda_gdp = suwayda_arms_factory_gdp }

		set_variable = { suwayda_dockyard_gdp = 185.building_level@dockyard }
		multiply_variable = { suwayda_dockyard_gdp = 155 }
		add_to_variable = { suwayda_gdp = suwayda_dockyard_gdp }

		set_variable = { suwayda_offices_gdp = 185.building_level@offices }
		multiply_variable = { suwayda_offices_gdp = 388 }
		add_to_variable = { suwayda_gdp = suwayda_offices_gdp }
	}
	set_variable = { daraa_gdp = 0 }
	if = {
		limit = { 184 = { is_owned_by = SYR } }
		set_variable = { daraa_industrial_complex_gdp = 184.industrial_complex_level }
		multiply_variable = { daraa_industrial_complex_gdp = 248 }
		add_to_variable = { daraa_gdp = daraa_industrial_complex_gdp }

		set_variable = { daraa_arms_factory_gdp = 184.building_level@arms_factory }
		multiply_variable = { daraa_arms_factory_gdp = 155 }
		add_to_variable = { daraa_gdp = daraa_arms_factory_gdp }

		set_variable = { daraa_dockyard_gdp = 184.building_level@dockyard }
		multiply_variable = { daraa_dockyard_gdp = 155 }
		add_to_variable = { daraa_gdp = daraa_dockyard_gdp }

		set_variable = { daraa_offices_gdp = 184.building_level@offices }
		multiply_variable = { daraa_offices_gdp = 388 }
		add_to_variable = { daraa_gdp = daraa_offices_gdp }
	}
	set_variable = { golan_gdp = 0 }
	if = {
		limit = { 587 = { is_owned_by = SYR } }
		set_variable = { golan_industrial_complex_gdp = 587.industrial_complex_level }
		multiply_variable = { golan_industrial_complex_gdp = 248 }
		add_to_variable = { golan_gdp = golan_industrial_complex_gdp }

		set_variable = { golan_arms_factory_gdp = 587.building_level@arms_factory }
		multiply_variable = { golan_arms_factory_gdp = 155 }
		add_to_variable = { golan_gdp = golan_arms_factory_gdp }

		set_variable = { golan_dockyard_gdp = 587.building_level@dockyard }
		multiply_variable = { golan_dockyard_gdp = 155 }
		add_to_variable = { golan_gdp = golan_dockyard_gdp }

		set_variable = { golan_offices_gdp = 587.building_level@offices }
		multiply_variable = { golan_offices_gdp = 388 }
		add_to_variable = { golan_gdp = golan_offices_gdp }
	}

	set_variable = { druze_gdp_per_capita = daraa_gdp }
	add_to_variable = { druze_gdp_per_capita = suwayda_gdp }
	add_to_variable = { druze_gdp_per_capita = golan_gdp }
	set_variable = { druze_population = 0 }
	if = {
		limit = { 184 = { is_owned_by = SYR } }
		add_to_variable = { druze_population = 184.state_population }
	}
	if = {
		limit = { 185 = { is_owned_by = SYR } }
		add_to_variable = { druze_population = 185.state_population }
	}
	if = {
		limit = { 587 = { is_owned_by = SYR } }
		add_to_variable = { druze_population = 587.state_population }
	}
	divide_variable = { druze_gdp_per_capita = druze_population }
	divide_variable = { druze_gdp_per_capita = gdp_per_capita }
	multiply_variable = { druze_gdp_per_capita = 45 }
	add_to_variable = { druze_status = druze_gdp_per_capita }

#reserved parliamentary seats effect
	if = {
		limit = { has_country_flag = druze_reserved_parliamentary_seats }
		add_to_variable = { druze_status = 30 }
	}

#autonomous_region_event
	if = {
		limit = { has_country_flag = druze_semi_autonomy }
		add_to_variable = { druze_status = 30 }
	}

#discontent effects
	if = {
		limit = { has_idea = druze_discontent }
		add_to_variable = { druze_status = -20 }
	}
	clamp_variable = { var = druze_status min = 0 max = 100 }


#law change cost calculations for federal path
	if = {
		limit = { has_country_flag = druze_semi_autonomy }
		set_variable = { druze_gov_change_cost = druze_status }
		subtract_from_variable = { druze_gov_change_cost = 50 }
		multiply_variable = { druze_gov_change_cost = -0.003 }
		clamp_variable = {
			var = druze_gov_change_cost
			min = 0
			max = 1
		}
		add_to_variable = { druze_gov_change_cost = 0.025 }
	}

#stability effect calculations
	set_variable = { druze_status_stability_modifier = druze_status }
	subtract_from_variable = { druze_status_stability_modifier = 50 }
	divide_variable = { druze_status_stability_modifier = 1500 }

#drift defense calculations
	set_variable = { druze_status_drift_defense = druze_status }
	subtract_from_variable = { druze_status_drift_defense = 50 }
	multiply_variable = { druze_status_drift_defense = -2 }
	divide_variable = { druze_status_drift_defense = 2000 }

	force_update_dynamic_modifier = yes
}

syrian_industry_effect1 = {
	if = {
		limit = { has_idea = paralyzing_corruption }
		set_temp_variable = { treasury_change = -56 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = crippling_corruption }
		set_temp_variable = { treasury_change = -50.5 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = rampant_corruption }
		set_temp_variable = { treasury_change = -45 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = unrestrained_corruption }
		set_temp_variable = { treasury_change = -39.5 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = systematic_corruption }
		set_temp_variable = { treasury_change = -34 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = widespread_corruption }
		set_temp_variable = { treasury_change = -28.5 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = medium_corruption }
		set_temp_variable = { treasury_change = -23 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = modest_corruption }
		set_temp_variable = { treasury_change = -17.5 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = slight_corruption }
		set_temp_variable = { treasury_change = -12 }
		modify_treasury_effect = yes
	}
}

syrian_industry_effect2 = {
	if = {
		limit = { has_idea = paralyzing_corruption }
		set_temp_variable = { treasury_change = -81 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = crippling_corruption }
		set_temp_variable = { treasury_change = -73 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = rampant_corruption }
		set_temp_variable = { treasury_change = -65 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = unrestrained_corruption }
		set_temp_variable = { treasury_change = -57 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = systematic_corruption }
		set_temp_variable = { treasury_change = -49 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = widespread_corruption }
		set_temp_variable = { treasury_change = -41 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = medium_corruption }
		set_temp_variable = { treasury_change = -33 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = modest_corruption }
		set_temp_variable = { treasury_change = -25 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = slight_corruption }
		set_temp_variable = { treasury_change = -17 }
		modify_treasury_effect = yes
	}
}

syrian_renewable_effect = {
	if = {
		limit = { has_idea = paralyzing_corruption }
		set_temp_variable = { treasury_change = -49 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = crippling_corruption }
		set_temp_variable = { treasury_change = -45 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = rampant_corruption }
		set_temp_variable = { treasury_change = -41 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = unrestrained_corruption }
		set_temp_variable = { treasury_change = -37 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = systematic_corruption }
		set_temp_variable = { treasury_change = -33 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = widespread_corruption }
		set_temp_variable = { treasury_change = -29 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = medium_corruption }
		set_temp_variable = { treasury_change = -25 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = modest_corruption }
		set_temp_variable = { treasury_change = -21 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = slight_corruption }
		set_temp_variable = { treasury_change = -17 }
		modify_treasury_effect = yes
	}
}

syrian_energy_effect = {
	if = {
		limit = { has_idea = paralyzing_corruption }
		set_temp_variable = { treasury_change = -30 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = crippling_corruption }
		set_temp_variable = { treasury_change = -27 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = rampant_corruption }
		set_temp_variable = { treasury_change = -24 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = unrestrained_corruption }
		set_temp_variable = { treasury_change = -21 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = systematic_corruption }
		set_temp_variable = { treasury_change = -18 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = widespread_corruption }
		set_temp_variable = { treasury_change = -15 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = medium_corruption }
		set_temp_variable = { treasury_change = -12 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = modest_corruption }
		set_temp_variable = { treasury_change = -9 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = slight_corruption }
		set_temp_variable = { treasury_change = -6 }
		modify_treasury_effect = yes
	}
}

syrian_corrcost = {
	if = {
		limit = { has_idea = paralyzing_corruption }
		set_temp_variable = { treasury_change = -42 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = crippling_corruption }
		set_temp_variable = { treasury_change = -36 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = rampant_corruption }
		set_temp_variable = { treasury_change = -30 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = unrestrained_corruption }
		set_temp_variable = { treasury_change = -24 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = systematic_corruption }
		set_temp_variable = { treasury_change = -18 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = widespread_corruption }
		set_temp_variable = { treasury_change = -12 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = medium_corruption }
		set_temp_variable = { treasury_change = -6 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = modest_corruption }
		set_temp_variable = { treasury_change = -3 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = slight_corruption }
		set_temp_variable = { treasury_change = 0 }
		modify_treasury_effect = yes
	}
}

syrian_corrcost_agri = {
	if = {
		limit = { has_idea = paralyzing_corruption }
		set_temp_variable = { treasury_change = -48.75 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = crippling_corruption }
		set_temp_variable = { treasury_change = -42.75 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = rampant_corruption }
		set_temp_variable = { treasury_change = -36.75 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = unrestrained_corruption }
		set_temp_variable = { treasury_change = -30.75 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = systematic_corruption }
		set_temp_variable = { treasury_change = -24.75 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = widespread_corruption }
		set_temp_variable = { treasury_change = -18.75 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = medium_corruption }
		set_temp_variable = { treasury_change = -12.75 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = modest_corruption }
		set_temp_variable = { treasury_change = -6.75 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = slight_corruption }
		set_temp_variable = { treasury_change = -3.75 }
		modify_treasury_effect = yes
	}
}
syrian_corrcost_civ = {
	if = {
		limit = { has_idea = paralyzing_corruption }
		set_temp_variable = { treasury_change = -31.5 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = crippling_corruption }
		set_temp_variable = { treasury_change = -28.5 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = rampant_corruption }
		set_temp_variable = { treasury_change = -25.5 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = unrestrained_corruption }
		set_temp_variable = { treasury_change = -22.5 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = systematic_corruption }
		set_temp_variable = { treasury_change = -19.5 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = widespread_corruption }
		set_temp_variable = { treasury_change = -16.5 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = medium_corruption }
		set_temp_variable = { treasury_change = -13.5 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = modest_corruption }
		set_temp_variable = { treasury_change = -10.5 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = slight_corruption }
		set_temp_variable = { treasury_change = -7.5 }
		modify_treasury_effect = yes
	}
}
syrian_corrcost_civtwo = {
	if = {
		limit = { has_idea = paralyzing_corruption }
		set_temp_variable = { treasury_change = -31.5 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = crippling_corruption }
		set_temp_variable = { treasury_change = -57 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = rampant_corruption }
		set_temp_variable = { treasury_change = -51 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = unrestrained_corruption }
		set_temp_variable = { treasury_change = -45 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = systematic_corruption }
		set_temp_variable = { treasury_change = -39 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = widespread_corruption }
		set_temp_variable = { treasury_change = -33 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = medium_corruption }
		set_temp_variable = { treasury_change = -27 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = modest_corruption }
		set_temp_variable = { treasury_change = -21 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = slight_corruption }
		set_temp_variable = { treasury_change = -15 }
		modify_treasury_effect = yes
	}
}
syrian_corrcost_off = {
	if = {
		limit = { has_idea = paralyzing_corruption }
		set_temp_variable = { treasury_change = -36 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = crippling_corruption }
		set_temp_variable = { treasury_change = -33 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = rampant_corruption }
		set_temp_variable = { treasury_change = -30 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = unrestrained_corruption }
		set_temp_variable = { treasury_change = -27 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = systematic_corruption }
		set_temp_variable = { treasury_change = -24 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = widespread_corruption }
		set_temp_variable = { treasury_change = -21 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = medium_corruption }
		set_temp_variable = { treasury_change = -18 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = modest_corruption }
		set_temp_variable = { treasury_change = -15 }
		modify_treasury_effect = yes
	}
	else_if = {
		limit = { has_idea = slight_corruption }
		set_temp_variable = { treasury_change = -12 }
		modify_treasury_effect = yes
	}
}

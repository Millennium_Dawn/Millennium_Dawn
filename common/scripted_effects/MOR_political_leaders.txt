# Author(s): AngriestBird
set_leader_MOR = {
	if = { limit = { has_country_flag = set_Western_Autocracy }
		if = { limit = { check_variable = { Western_Autocracy_leader = 0 } }
			add_to_variable = { Western_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Mahmoud Arshan"
				picture = "Mahmoud_Arshan.dds"
				ideology = Western_Autocracy
				traits = {
					western_Western_Autocracy
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Western_Autocracy_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_conservatism }
		if = { limit = { check_variable = { conservatism_leader = 0 } }
			add_to_variable = { conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Ahmed Osman"
				picture = "ahmed_osman.dds"
				ideology = conservatism
				traits = {
					western_conservatism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { conservatism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { conservatism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Salaheddine Mezouar"
				picture = "Salaheddine_Mezouar.dds"
				ideology = conservatism
				traits = {
					western_conservatism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { conservatism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { conservatism_leader = 2 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Aziz Akhannouch"
				picture = "aziz_akhannouch.dds"
				ideology = conservatism
				traits = {
					western_conservatism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { conservatism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = {
		limit = { has_country_flag = set_liberalism }
		if = { limit = { check_variable = { liberalism_leader = 0 } }
			add_to_variable = { liberalism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Moulay Ismaïl Alaoui"
				picture = "ismail_alaoui.dds"
				ideology = liberalism
				traits = {
					western_liberalism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { liberalism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { liberalism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { liberalism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Nabil Benabdallah"
				picture = "nabil_benabdallah.dds"
				ideology = liberalism
				traits = {
					western_liberalism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { liberalism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = {
		limit = { has_country_flag = set_socialism }
		if = { limit = { check_variable = { socialism_leader = 0 } }
			add_to_variable = { socialism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Abderrahmane Youssoufi"
				picture = "abderrahmane_youssoufi.dds"
				ideology = socialism
				traits = {
					western_socialism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { socialism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { socialism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { socialism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Mohamed El Yazghi"
				picture = "Mohamed_El_Yazghi.dds"
				ideology = socialism
				traits = {
					western_socialism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { socialism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { socialism_leader = 2 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { socialism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Abdelwahed Radi"
				picture = "Abdelwahed_Radi.dds"
				ideology = socialism
				traits = {
					western_socialism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { socialism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { socialism_leader = 3 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { socialism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Driss Lachgar"
				picture = "driss_lachgar.dds"
				ideology = socialism
				traits = {
					western_socialism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { socialism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Communist-State }
		if = { limit = { check_variable = { Communist-State_leader = 0 } }
			add_to_variable = { Communist-State_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Ali Boutouala"
				picture = "Ali_Boutouala.dds"
				ideology = Communist-State
				traits = {
					emerging_Communist-State
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Communist-State_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_anarchist_communism }
		if = { limit = { check_variable = { anarchist_communism_leader = 0 } }
			add_to_variable = { anarchist_communism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Mohamed Bensaid Ait Idde"
				picture = "Mohamed_Bensaid_Ait_Idder.dds"
				ideology = anarchist_communism
				traits = {
					emerging_anarchist_communism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { anarchist_communism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { anarchist_communism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { anarchist_communism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Nabila Mounib"
				picture = "Nabila_Mounib.dds"
				ideology = anarchist_communism
				traits = {
					emerging_anarchist_communism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { anarchist_communism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { anarchist_communism_leader = 2 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { anarchist_communism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Jamal El Asri"
				picture = "Jamal_El_Asri.dds"
				ideology = anarchist_communism
				traits = {
					emerging_anarchist_communism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { anarchist_communism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = {
		limit = { has_country_flag = set_Kingdom }
		if = { limit = { check_variable = { Kingdom_leader = 0 } }
			add_to_variable = { Kingdom_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Mohamed Khalidi"
				picture = "Mohamed_Khalidi.dds"
				ideology = Kingdom
				traits = {
					salafist_Kingdom
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Kingdom_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { Kingdom_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Kingdom_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Abdelbarii Zemzami"
				picture = "Abdel_Bari_Zamzami.dds"
				ideology = Kingdom
				traits = {
					salafist_Kingdom
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Kingdom_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Neutral_conservatism }
		if = { limit = { check_variable = { Neutral_conservatism_leader = 0 } }
			add_to_variable = { Neutral_conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Abbas El Fassi"
				picture = "abbas_el_fassi.dds"
				ideology = Neutral_conservatism
				traits = {
					neutrality_Neutral_conservatism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_conservatism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { Neutral_conservatism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Neutral_conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Abdelhamid Chabat"
				picture = "Abdelhamid_Chabat.dds"
				ideology = Neutral_conservatism
				traits = {
					neutrality_Neutral_conservatism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_conservatism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { Neutral_conservatism_leader = 2 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Neutral_conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Nizar Baraka"
				picture = "Nizar_Baraka.dds"
				ideology = Neutral_conservatism
				traits = {
					neutrality_Neutral_conservatism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_conservatism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Neutral_Muslim_Brotherhood }
		if = { limit = { check_variable = { Neutral_Muslim_Brotherhood_leader = 0 } }
			add_to_variable = { Neutral_Muslim_Brotherhood_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Abdelkrim al-Khatib"
				picture = "abddelkrim_al_khatib.dds"
				ideology = Neutral_Muslim_Brotherhood
				traits = {
					neutrality_Neutral_Muslim_Brotherhood
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Muslim_Brotherhood_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { Neutral_Muslim_Brotherhood_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Neutral_Muslim_Brotherhood_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Saadeddine Othmani"
				picture = "saadeddine_othmani.dds"
				ideology = Neutral_Muslim_Brotherhood
				traits = {
					neutrality_Neutral_Muslim_Brotherhood
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Muslim_Brotherhood_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { Neutral_Muslim_Brotherhood_leader = 2 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Neutral_Muslim_Brotherhood_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Abdelilah Benkirane"
				picture = "Abdel_Ilah_Benkiran.dds"
				ideology = Neutral_Muslim_Brotherhood
				traits = {
					neutrality_Neutral_Muslim_Brotherhood
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Muslim_Brotherhood_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Neutral_Autocracy }
		if = { limit = { check_variable = { Neutral_Autocracy_leader = 0 } }
			add_to_variable = { Neutral_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Mohammed al-Guerbouzi"
				picture = "mohand_laenser.dds"
				ideology = Neutral_Autocracy
				traits = {
					neutrality_Neutral_Autocracy
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Autocracy_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Neutral_green }
		if = { limit = { check_variable = { Neutral_Green_leader = 0 } }
			add_to_variable = { Neutral_Green_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Mohammed al-Guerbouzi"
				picture = "Mohamed_Fares.dds"
				ideology = Neutral_green
				traits = {
					neutrality_Neutral_green
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Green_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Neutral_libertarian }
		if = { limit = { check_variable = { Neutral_libertarian_leader = 0 } }
			add_to_variable = { Neutral_libertarian_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Mohamed Abied"
				picture = "Mohamed_Abied.dds"
				ideology = Neutral_Libertarian
				traits = {
					neutrality_Neutral_Libertarian
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_libertarian_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { Neutral_libertarian_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Neutral_libertarian_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Mohammed Sajid"
				picture = "Mohammed_Sajid.dds"
				ideology = Neutral_Libertarian
				traits = {
					neutrality_Neutral_Libertarian
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_libertarian_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Neutral_Communism }
		if = { limit = { check_variable = { Neutral_Communism_leader = 0 } }
			add_to_variable = { Neutral_Communism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Mustapha Brahma"
				picture = "Mustapha_Brahma.dds"
				ideology = Neutral_Communism
				traits = {
					neutrality_Neutral_Communism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Communism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { Neutral_Communism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Neutral_Communism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Abdallah El Harif"
				picture = "Abdallah_El_Harif.dds"
				ideology = Neutral_Communism
				traits = {
					neutrality_Neutral_Communism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Communism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Neutral_social }
		if = { limit = { check_variable = { Neutral_social_leader = 0 } }
			add_to_variable = { Neutral_social_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Thami El Khyari"
				picture = "Thami_El_Khyari.dds"
				ideology = Neutral_social
				traits = {
					neutrality_Neutral_social
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_social_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { Neutral_social_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Neutral_social_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Mustapha Benali"
				picture = "Mustapha_Benali.dds"
				ideology = Neutral_social
				traits = {
					neutrality_Neutral_social
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_social_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_oligarchism }
		if = { limit = { check_variable = { oligarchism_leader = 0 } }
			add_to_variable = { oligarchism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Mohamed Cheikh Biadillah"
				picture = "Mohamed_Cheikh_Biadillah.dds"
				ideology = oligarchism
				traits = {
					neutrality_oligarchism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { oligarchism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { oligarchism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { oligarchism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Ilyas El Omari"
				picture = "ilyas_el_omari.dds"
				ideology = oligarchism
				traits = {
					neutrality_oligarchism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { oligarchism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { oligarchism_leader = 2 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { oligarchism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Abdellatif Ouahbi"
				picture = "Abdellatif_Ouahbi.dds"
				ideology = oligarchism
				traits = {
					neutrality_oligarchism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { oligarchism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Nat_Populism }
		if = { limit = { check_variable = { Nat_Populism_leader = 0 } }
			add_to_variable = { Nat_Populism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Thami El Ouazzani"
				picture = "Thami_El_Ouazzani.dds"
				ideology = Nat_Populism
				traits = {
					nationalist_Nat_Populism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Populism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
			set_temp_variable = { b = 1 }
		if = { limit = { check_variable = { Nat_Populism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Nat_Populism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Ahmed Belghazi"
				picture = "hmed_Belghazi.dds"
				ideology = Nat_Populism
				traits = {
					nationalist_Nat_Populism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Populism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Nat_Autocracy }
		if = { limit = { check_variable = { Nat_Autocracy_leader = 0 } }
			add_to_variable = { Nat_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Abderrahmane Sbai"
				picture = "Abderrahmane_Sbai.dds"
				ideology = Nat_Autocracy
				traits = {
					nationalist_Nat_Autocracy
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Autocracy_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { Nat_Autocracy_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Nat_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Abdellatif Loudiyi"
				picture = "Loudiyi.dds"
				ideology = Nat_Autocracy
				traits = {
					nationalist_Nat_Autocracy
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Autocracy_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Monarchist }
		if = { limit = { check_variable = { Monarchist_leader = 0 } }
			add_to_variable = { Monarchist_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Mohammed VI"
				picture = "Mohammed_VI.dds"
				ideology = Monarchist
				traits = {
					nationalist_Monarchist
					king
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Monarchist_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
}

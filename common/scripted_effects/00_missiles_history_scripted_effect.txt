set_up_starting_missile_configurations = {
	# New Custom Missile System is DLC Only
	if = { limit = { has_dlc = "Gotterdammerung" }

		CHI = {
			531 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
			534 = {
				add_building_construction = {
					type = rocket_site
					level = 1
					instant_build = yes
				}
			}
			537 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
			538 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
			541 = {
				add_building_construction = {
					type = rocket_site
					level = 1
					instant_build = yes
				}
			}
			548 = {
				add_building_construction = {
					type = rocket_site
					level = 1
					instant_build = yes
				}
			}
			570 = {
				add_building_construction = {
					type = rocket_site
					level = 1
					instant_build = yes
				}
			}
			588 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
			589 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
		}
		ISR = {
			207 = {
				add_building_construction = {
					type = rocket_site
					level = 3
					instant_build = yes
				}
			}
			204 = {
				add_building_construction = {
					type = rocket_site
					level = 3
					instant_build = yes
				}
			}
		}
		ITA = {

			78 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
		}
		NKO = {
			602 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
		}
		PAK = {
			422 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
		}
		PER = {
			400 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
			404 = {
				add_building_construction = {
					type = rocket_site
					level = 1
					instant_build = yes
				}
			}
			405 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
			406 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
			416 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
			976 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
			977 = {
				add_building_construction = {
					type = rocket_site
					level = 1
					instant_build = yes
				}
			}
			978 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
		}
		RAJ = {
			432 = {
				add_building_construction = {
					type = rocket_site
					level = 3
					instant_build = yes
				}
			}
			438 = {
				add_building_construction = {
					type = rocket_site
					level = 3
					instant_build = yes
				}
			}
			451 = {
				add_building_construction = {
					type = rocket_site
					level = 1
					instant_build = yes
				}
			}
			469 = {
				add_building_construction = {
					type = rocket_site
					level = 1
					instant_build = yes
				}
			}
		}
		TAI = {
			1157 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
			1156 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
			1155 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
			600 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
		}
		ENG = {
			14 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
			1193 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
			437 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
			8 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
		}
		SPR = {
			970 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
			971 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
		}
		SPR = {
			58 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
			64 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
			61 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
		}
		UKR = {
			693 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
			696 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
			698 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
			700 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
			1075 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
			1089 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
			1090 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
			1086 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
		}
		SOV = {
			642 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
			643 = {
				add_building_construction = {
					type = rocket_site
					level = 4
					instant_build = yes
				}
			}
			644 = {
				add_building_construction = {
					type = rocket_site
					level = 3
					instant_build = yes
				}
			}
			649 = {
				add_building_construction = {
					type = rocket_site
					level = 3
					instant_build = yes
				}
			}
			652 = {
				add_building_construction = {
					type = rocket_site
					level = 1
					instant_build = yes
				}
			}
			667 = {
				add_building_construction = {
					type = rocket_site
					level = 3
					instant_build = yes
				}
			}
			670 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
			676 = {
				add_building_construction = {
					type = rocket_site
					level = 3
					instant_build = yes
				}
			}
			681 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
			687 = {
				add_building_construction = {
					type = rocket_site
					level = 3
					instant_build = yes
				}
			}
			692 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
			1070 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
			1189 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
		}
		POL = {
			114 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
			111 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
		}
		GER = {
			539 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
			37 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
			43 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
		}
		USA = {
			488 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
			772 = {
				add_building_construction = {
					type = rocket_site
					level = 1
					instant_build = yes
				}
			}
			787 = {
				add_building_construction = {
					type = rocket_site
					level = 4
					instant_build = yes
				}
			}
			788 = {
				add_building_construction = {
					type = rocket_site
					level = 4
					instant_build = yes
				}
			}
			789 = {
				add_building_construction = {
					type = rocket_site
					level = 4
					instant_build = yes
				}
			}
			804 = {
				add_building_construction = {
					type = rocket_site
					level = 4
					instant_build = yes
				}
			}
			811 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
			814 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
			817 = {
				add_building_construction = {
					type = rocket_site
					level = 2
					instant_build = yes
				}
			}
		}
	}
	LBA = {
		391 = {
			add_building_construction = {
				type = rocket_site
				level = 1
				instant_build = yes
			}
		}
		919 = {
			add_building_construction = {
				type = rocket_site
				level = 1
				instant_build = yes
			}
		}
		395 = {
			add_building_construction = {
				type = rocket_site
				level = 1
				instant_build = yes
			}
		}
		396 = {
			add_building_construction = {
				type = rocket_site
				level = 1
				instant_build = yes
			}
		}
		392 = {
			add_building_construction = {
				type = rocket_site
				level = 1
				instant_build = yes
			}
		}
		1100 = {
			add_building_construction = {
				type = rocket_site
				level = 1
				instant_build = yes
			}
		}
	}
}

us_missile_add_test = {
	add_equipment_to_stockpile = {
			type = sam_missile_equipment_1
			amount = 1350
		}
		add_equipment_to_stockpile = {
			type = sam_missile_equipment_2
			amount = 800
		}
		add_equipment_to_stockpile = {
			type = sam_missile_equipment_3
			amount = 600
		}
		add_equipment_to_stockpile = {
			type = sam_missile_equipment_4
			amount = 450
		}
		add_equipment_to_stockpile = {
			type = nuclear_missile_equipment_1
			amount = 700
		}
		add_equipment_to_stockpile = {
			type = nuclear_missile_equipment_3
			amount = 480
		}
		add_equipment_to_stockpile = {
			type = nuclear_missile_equipment_4
			amount = 900
		}
		add_equipment_to_stockpile = {
			type = guided_missile_equipment_1
			amount = 1050
		}
		add_equipment_to_stockpile = {
			type = guided_missile_equipment_2
			amount = 3575
		}
		add_equipment_to_stockpile = {
			type = guided_missile_equipment_3
			amount = 2025
		}
}
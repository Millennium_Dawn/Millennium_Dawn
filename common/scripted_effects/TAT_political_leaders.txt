set_leader_TAT = {
	if = { limit = { has_country_flag = set_conservatism }
		if = {
			limit = {
				check_variable = { conservatism_leader = 0 }
			}
			create_country_leader = {
				name = "Vil Mirzayanov"
				picture = "gfx/leaders/SOV/TAT/Vil_Mirzayanov.dds"
				ideology = conservatism
				traits = {
					western_conservatism
				}
			}
		}
	}
	else_if = { limit = { has_country_flag = set_liberalism }
		if = { limit = { check_variable = { liberalism_leader = 0 } }
			create_country_leader = {
				name = "Danis Safargali"
				picture = "gfx/leaders/SOV/TAT/Danis_Safargali.dds"
				ideology = liberalism
				traits = {
					western_liberalism
				}
			}
		}
		if = { limit = { check_variable = { liberalism_leader = 1 } }
			create_country_leader = {
				name = "Batyrkhan Agzamov"
				picture = "gfx/leaders/SOV/TAT/Batyrkhan_Agzamov.dds"
				ideology = liberalism
				traits = {
					western_liberalism
				}
			}
		}
	}
	else_if = { limit = { has_country_flag = set_Communist-State }
		if = {
			limit = {
				check_variable = { Communist-State_leader = 0 }
			}
			add_to_variable = { Communist-State_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Vladimir Komoedov"
				picture = "gfx/leaders/SOV/TAT/Vladimir_Komoedov.dds"
				ideology = Communist-State
				traits = {
					emerging_Communist-State
				}
			}
			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Communist-State_leader = 1 } }
			if = { limit = { has_start_date < 2016.1.2 } set_temp_variable = { b = 1 } }
		}
		if = {
			limit = {
				check_variable = { Communist-State_leader = 1 } NOT = { check_variable = { b = 1 } }
			}

			create_country_leader = {
				name = "Alfred Valiyev"
				picture = "gfx/leaders/SOV/TAT/Alfred_Valiyev.dds"
				ideology = Communist-State
				traits = {
					emerging_Communist-State
				}
			}
		}
	}
	else_if = { limit = { has_country_flag = set_anarchist_communism }
		if = {
			limit = {
				check_variable = { anarchist_communism_leader = 0 }
			}
			#add_to_variable = { anarchist_communism_leader = 1 }
			#hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Ravil Khusnulin"
				picture = "gfx/leaders/SOV/TAT/Ravil_Khusnulin.dds"
				ideology = anarchist_communism
				traits = {
					emerging_anarchist_communism
				}
			}
			#if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { anarchist_communism_leader = 1 } }
			#if = { limit = { has_start_date < 2016.1.2 } set_temp_variable = { b = 1 } }
		}
		if = {
			limit = {
				check_variable = { anarchist_communism_leader = 1 } NOT = { check_variable = { b = 1 } }
			}

			create_country_leader = {
				name = "Askar Sabirov"
				picture = "gfx/leaders/SOV/TAT/Askar_Sabirov.dds"
				ideology = anarchist_communism
				traits = {
					emerging_anarchist_communism
				}
			}
		}
	}
	else_if = { limit = { has_country_flag = set_Conservative }
		if = { limit = { check_variable = { Conservative_leader = 0 } }
			add_to_variable = { Conservative_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Mintimer Shaimiev"
				picture = "gfx/leaders/SOV/TAT/Mintimer_Shaimiev.dds"
				ideology = Conservative
				traits = {
					emerging_Conservative pro_russia
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Conservative_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { Conservative_leader = 1 } NOT = { check_variable = { b = 1 } } }
		add_to_variable = { Conservative_leader = 1 }
		hidden_effect = { kill_country_leader = yes }

		create_country_leader = {
			name = "Rustam Minnikhanov"
			picture = "gfx/leaders/SOV/TAT/Rustam_Minnikhanov.dds"
			ideology = Conservative
			traits = {
				emerging_Conservative pro_russia
			}
		}

		if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Conservative_leader = 1 } }
		set_temp_variable = { b = 1 }
	}
	}
	else_if = { limit = { has_country_flag = set_Neutral_Muslim_Brotherhood }
		if = { limit = { check_variable = { Neutral_Muslim_Brotherhood_leader = 0 } }

			create_country_leader = {
				name = "Rafis Kashapov"
				picture = "gfx/leaders/SOV/TAT/Rafis_Kashapov.dds"
				ideology = Neutral_Muslim_Brotherhood
				traits = {
					neutrality_Neutral_Muslim_Brotherhood
				}
			}
		}
		if = { limit = { check_variable = { Neutral_Muslim_Brotherhood_leader = 1 } }
			#add_to_variable = { Neutral_Muslim_Brotherhood_leader = 1 }
			#hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Farit Zakiev"
				picture = "gfx/leaders/SOV/TAT/Farit_Zakiev.dds"
				ideology = Neutral_Muslim_Brotherhood
				traits = {
					neutrality_Neutral_Muslim_Brotherhood
				}
			}
		}
	}
	else_if = { limit = { has_country_flag = set_Neutral_conservatism }
		if = {
			limit = {
				check_variable = { Neutral_conservatism_leader = 0 }
			}

			create_country_leader = {
				name = "Oleg Korobchenko"
				picture = "gfx/leaders/SOV/TAT/Oleg_Korobchenko.dds"
				ideology = Neutral_conservatism
				traits = {
					neutrality_Neutral_conservatism
				}
			}
		}
	}
	else_if = { limit = { has_country_flag = set_Neutral_green }
		if = {
			limit = {
				check_variable = { Neutral_green_leader = 0 }
			}

			create_country_leader = {
				name = "Rinat Zakirov"
				picture = "gfx/leaders/SOV/TAT/Rinat_Zakirov.dds"
				ideology = Neutral_green
				traits = {
					neutrality_Neutral_green
				}
			}
		}
	}
	else_if = { limit = { has_country_flag = set_neutral_Social }
		if = {
			limit = {
				check_variable = { neutral_Social_leader = 0 }
			}
			add_to_variable = { neutral_Social_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Ivan Grachev"
				picture = "gfx/leaders/SOV/TAT/Ivan_Grachev.dds"
				ideology = neutral_Social
				traits = {
					neutrality_neutral_Social
				}
			}
			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { neutral_Social_leader = 1 } }
			if = { limit = { has_start_date < 2016.1.2 } set_temp_variable = { b = 1 } }
		}
		if = {
			limit = {
				check_variable = { neutral_Social_leader = 1 } NOT = { check_variable = { b = 1 } }
			}

			create_country_leader = {
				name = "Almir Mikheyev"
				picture = "gfx/leaders/SOV/TAT/Almir_Mikheyev.dds"
				ideology = neutral_Social
				traits = {
					neutrality_neutral_Social
				}
			}
		}
	}
	else_if = { limit = { has_country_flag = set_Nat_Populism }
		if = {
			limit = {
				check_variable = { Nat_Populism_leader = 0 }
			}

			create_country_leader = {
				name = "Fauziya Bayramova"
				picture = "gfx/leaders/SOV/TAT/Fauziya_Bayramova.dds"
				ideology = Nat_Populism
				traits = {
					nationalist_Nat_Populism
					writer
				}
			}
		}
	}
}

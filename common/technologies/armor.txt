#Written by Hiddengearz
technologies = {
	#X axis
	@1945 = -4
	@1965 = 0
	@1975 = 4
	@1985 = 8
	@1995 = 12
	@2005 = 16
	@2015 = 20
	@2025 = 24
	@2035 = 28

	#Y Axis
	@row1 = -2
	@row2 = 0
	@row3 = 2
	@row4 = 4
	@row5 = 6
	@row6 = 8
	@row7 = 10

	#1945
	Early_APC = {

		enable_equipments = {
			APC_Equipment
			MBT_Equipment
			IFV_Equipment
			light_tank_hull
		}
		enable_subunits = {
			Mech_Inf_Bat
			Arm_Inf_Bat
			Mech_Air_Inf_Bat
			Arm_Air_Inf_Bat
			Mech_Marine_Bat
			Arm_Marine_Bat
			armor_Bat
			armor_Comp
			Mech_Recce_Comp
			Arm_Recce_Comp
			H_Engi_Comp
			Arm_Air_assault_Bat
		}
		path = {
			leads_to_tech = APC_1
			research_cost_coeff = 1
		}

		path = {
			leads_to_tech = IFV_1
			research_cost_coeff = 1
		}

		path = {
			leads_to_tech = MBT_1
			research_cost_coeff = 1
		}

		research_cost = 1
		start_year = 1945
		folder = {
			name = armour_folder
			position = { x = @row2 y = @1945 }
		}

		ai_will_do = {
			factor = 15
		}

		categories = {
			CAT_armor
			CAT_afv
			CAT_apc
			CAT_Military
		}
	}

	fake_CAT_armor = {
		research_cost = 1
		categories = {
			CAT_armor
		}

		#fake tech
		ai_will_do = {
			factor = -100
		}
	}

	#1965
	MBT_1 = {

		enable_equipments = {
			MBT_1
		}

		path = {
			leads_to_tech = MBT_2
			research_cost_coeff = 1
		}

		research_cost = 2.1
		start_year = 1965
		folder = {
			name = armour_folder
			position = { x = @row1 y = @1965 }
		}

		ai_will_do = {
			factor = 1
		}

		categories = {
			CAT_armor
			CAT_tanks
			CAT_mbt
			CAT_Military
		}
	}


	#1975
	MBT_2 = {

		enable_equipments = {
			MBT_2
		}

		path = {
			leads_to_tech = MBT_3
			research_cost_coeff = 1
		}

		research_cost = 2.2
		start_year = 1975
		folder = {
			name = armour_folder
			position = { x = @row1 y = @1975 }
		}

		ai_will_do = {
			factor = 1
		}

		categories = {
			CAT_armor
			CAT_tanks
			CAT_mbt
			CAT_Military
		}
	}

	#1985
	MBT_3 = {

		enable_equipments = {
			MBT_3
		}

		path = {
			leads_to_tech = MBT_4
			research_cost_coeff = 1
		}

		research_cost = 2.3
		start_year = 1985
		folder = {
			name = armour_folder
			position = { x = @row1 y = @1985 }
		}

		ai_will_do = {
			factor = 1
		}

		categories = {
			CAT_armor
			CAT_tanks
			CAT_mbt
			CAT_Military
		}
	}

	#1995
	MBT_4 = {

		enable_equipments = {
			MBT_4
		}

		path = {
			leads_to_tech = MBT_5
			research_cost_coeff = 1
		}

		research_cost = 2.4
		start_year = 1995
		folder = {
			name = armour_folder
			position = { x = @row1 y = @1995 }
		}


		ai_will_do = {
			factor = 1
		}

		categories = {
			CAT_armor
			CAT_tanks
			CAT_mbt
			CAT_Military
		}
	}

	#2015
	MBT_5 = {

		enable_equipments = {
			MBT_5
		}

		path = {
			leads_to_tech = MBT_7
			research_cost_coeff = 1
		}

		research_cost = 2.6
		start_year = 2015
		folder = {
			name = armour_folder
			position = { x = @row1 y = @2015 }
		}


		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.5
				date < 2015.01.01
			}
			modifier = {
				factor = 0.2
				date < 2005.01.01
			}
		}

		categories = {
			CAT_armor
			CAT_tanks
			CAT_mbt
			CAT_Military
		}
	}

	#2025
	MBT_7 = {

		enable_equipments = {
			MBT_7
		}

		path = {
			leads_to_tech = MBT_8
			research_cost_coeff = 1
		}

		research_cost = 2.7
		start_year = 2025
		folder = {
			name = armour_folder
			position = { x = @row1 y = @2025 }
		}

		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.5
				date < 2025.01.01
			}
			modifier = {
				factor = 0.2
				date < 2020.01.01
			}
		}

		categories = {
			CAT_armor
			CAT_tanks
			CAT_mbt
			CAT_Military
		}
	}

	#2035
	MBT_8 = {

		enable_equipments = {
			MBT_8
		}

		research_cost = 2.8
		start_year = 2035
		folder = {
			name = armour_folder
			position = { x = @row1 y = @2035 }
		}

		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.5
				date < 2035.01.01
			}
			modifier = {
				factor = 0.2
				date < 2030.01.01
			}
		}

		categories = {
			CAT_armor
			CAT_tanks
			CAT_mbt
			CAT_Military
		}
	}

	####APC####
	#1965
	APC_1 = {

		enable_equipments = {
			APC_1
		}

		path = {
			leads_to_tech = APC_2
			research_cost_coeff = 1
		}

		research_cost = 1.4
		start_year = 1965
		folder = {
			name = armour_folder
			position = { x = @row2 y = @1965 }
		}

		ai_will_do = {
			factor = 1
		}

		categories = {
			CAT_armor
			CAT_afv
			CAT_apc
			CAT_Military
		}
	}

	#1975
	APC_2 = {

		enable_equipments = {
			APC_2
		}

		path = {
			leads_to_tech = APC_3
			research_cost_coeff = 1
		}

		research_cost = 1.5
		start_year = 1975
		folder = {
			name = armour_folder
			position = { x = @row2 y = @1975 }
		}

		ai_will_do = {
			factor = 1
		}

		categories = {
			CAT_armor
			CAT_afv
			CAT_apc
			CAT_Military
		}
	}

	#1985
	APC_3 = {

		enable_equipments = {
			APC_3
		}

		path = {
			leads_to_tech = APC_4
			research_cost_coeff = 1
		}

		research_cost = 1.6
		start_year = 1985
		folder = {
			name = armour_folder
			position = { x = @row2 y = @1985 }
		}

		ai_will_do = {
			factor = 1
		}

		categories = {
			CAT_armor
			CAT_afv
			CAT_apc
			CAT_Military
		}
	}

	#1995
	APC_4 = {

		enable_equipments = {
			APC_4
		}

		path = {
			leads_to_tech = APC_5
			research_cost_coeff = 1
		}

		research_cost = 1.7
		start_year = 1995
		folder = {
			name = armour_folder
			position = { x = @row2 y = @1995 }
		}

		ai_will_do = {
			factor = 1
		}

		categories = {
			CAT_armor
			CAT_afv
			CAT_apc
			CAT_Military
		}
	}

	#2005
	APC_5 = {

		enable_equipments = {
			APC_5
		}

		path = {
			leads_to_tech = APC_6
			research_cost_coeff = 1
		}

		research_cost = 1.8
		start_year = 2005
		folder = {
			name = armour_folder
			position = { x = @row2 y = @2005 }
		}

		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.5
				date < 2005.01.01
			}
			modifier = {
				factor = 0.2
				date < 2002.01.01
			}
		}

		categories = {
			CAT_armor
			CAT_afv
			CAT_apc
			CAT_Military
		}
	}

	#2015
	APC_6 = {

		enable_equipments = {
			APC_6
		}

		path = {
			leads_to_tech = APC_7
			research_cost_coeff = 1
		}

		research_cost = 1.9
		start_year = 2015
		folder = {
			name = armour_folder
			position = { x = @row2 y = @2015 }
		}

		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.5
				date < 2015.01.01
			}
			modifier = {
				factor = 0.2
				date < 2010.01.01
			}
		}

		categories = {
			CAT_armor
			CAT_afv
			CAT_apc
			CAT_Military
		}
	}
	#2025
	APC_7 = {

		enable_equipments = {
			APC_7
		}

		path = {
			leads_to_tech = APC_8
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 2025
		folder = {
			name = armour_folder
			position = { x = @row2 y = @2025 }
		}

		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.5
				date < 2025.01.01
			}
			modifier = {
				factor = 0.2
				date < 2020.01.01
			}
		}

		categories = {
			CAT_armor
			CAT_afv
			CAT_apc
			CAT_Military
		}
	}

	#2035
	APC_8 = {

		enable_equipments = {
			APC_8
		}

		research_cost = 2.1
		start_year = 2035

		folder = {
			name = armour_folder
			position = { x = @row2 y = @2035 }
		}

		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.5
				date < 2035.01.01
			}
			modifier = {
				factor = 0.2
				date < 2030.01.01
			}
		}

		categories = {
			CAT_armor
			CAT_afv
			CAT_apc
			CAT_Military
		}
	}

	###IFV###

	#1965
	IFV_1 = {

		enable_equipments = {
			IFV_1
		}

		path = {
			leads_to_tech = IFV_2
			research_cost_coeff = 1
		}

		research_cost = 1.7
		start_year = 1965
		folder = {
			name = armour_folder
			position = { x = @row3 y = @1965 }
		}

		ai_will_do = {
			factor = 1
		}

		categories = {
			CAT_armor
			CAT_afv
			CAT_ifv
			CAT_Military
		}
	}

	#1975
	IFV_2 = {

		enable_equipments = {
			IFV_2
		}

		path = {
			leads_to_tech = IFV_3
			research_cost_coeff = 1
		}

		research_cost = 1.8
		start_year = 1975
		folder = {
			name = armour_folder
			position = { x = @row3 y = @1975 }
		}

		ai_will_do = {
			factor = 1
		}

		categories = {
			CAT_armor
			CAT_afv
			CAT_ifv
			CAT_Military
		}
	}

	#1985
	IFV_3 = {

		enable_equipments = {
			IFV_3
		}

		path = {
			leads_to_tech = IFV_4
			research_cost_coeff = 1
		}

		research_cost = 1.9
		start_year = 1985
		folder = {
			name = armour_folder
			position = { x = @row3 y = @1985 }
		}

		ai_will_do = {
			factor = 1
		}

		categories = {
			CAT_armor
			CAT_afv
			CAT_ifv
			CAT_Military
		}
	}

	#1995
	IFV_4 = {

		enable_equipments = {
			IFV_4
		}

		path = {
			leads_to_tech = IFV_5
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1995
		folder = {
			name = armour_folder
			position = { x = @row3 y = @1995 }
		}

		ai_will_do = {
			factor = 1
		}

		categories = {
			CAT_armor
			CAT_afv
			CAT_ifv
			CAT_Military
		}
	}

	#2005
	IFV_5 = {

		enable_equipments = {
			IFV_5
		}

		path = {
			leads_to_tech = IFV_6
			research_cost_coeff = 1
		}

		research_cost = 2.1
		start_year = 2005
		folder = {
			name = armour_folder
			position = { x = @row3 y = @2005 }
		}

		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.5
				date < 2005.01.01
			}
			modifier = {
				factor = 0.2
				date < 2002.01.01
			}
		}

		categories = {
			CAT_armor
			CAT_afv
			CAT_ifv
			CAT_Military
		}
	}

	#2015
	IFV_6 = {

		enable_equipments = {
			IFV_6
		}

		path = {
			leads_to_tech = IFV_7
			research_cost_coeff = 1
		}

		research_cost = 2.2
		start_year = 2015
		folder = {
			name = armour_folder
			position = { x = @row3 y = @2015 }
		}

		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.5
				date < 2015.01.01
			}
			modifier = {
				factor = 0.2
				date < 2010.01.01
			}
		}

		categories = {
			CAT_armor
			CAT_afv
			CAT_ifv
			CAT_Military
		}
	}

	#2025
	IFV_7 = {

		enable_equipments = {
			IFV_7
		}

		path = {
			leads_to_tech = IFV_8
			research_cost_coeff = 1
		}

		research_cost = 2.3
		start_year = 2025
		folder = {
			name = armour_folder
			position = { x = @row3 y = @2025 }
		}

		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.5
				date < 2025.01.01
			}
			modifier = {
				factor = 0.2
				date < 2020.01.01
			}
		}

		categories = {
			CAT_armor
			CAT_afv
			CAT_ifv
			CAT_Military
		}
	}

	#2035
	IFV_8 = {

		enable_equipments = {
			IFV_8
		}

		research_cost = 2.4
		start_year = 2035
		folder = {
			name = armour_folder
			position = { x = @row3 y = @2035 }
		}

		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.5
				date < 2035.01.01
			}
			modifier = {
				factor = 0.2
				date < 2030.01.01
			}
		}

		categories = {
			CAT_armor
			CAT_afv
			CAT_ifv
			CAT_Military
		}
	}

	#1965
	Rec_tank_0 = {

		enable_equipments = {
			Rec_tank_0
		}
		enable_subunits = {
			L_arm_Bat
			armor_Recce_Comp
		}
		path = {
			leads_to_tech = Rec_tank_1
			research_cost_coeff = 1
		}

		research_cost = 1.4
		start_year = 1965
		folder = {
			name = armour_folder
			position = { x = @row4 y = @1965 }
		}

		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				OR = {
					has_tech = Rec_tank_1
					has_tech = Rec_tank_2
					has_tech = Rec_tank_3
					has_tech = Rec_tank_4
					has_tech = Rec_tank_5
					can_research = Rec_tank_1
					can_research = Rec_tank_2
					can_research = Rec_tank_3
					can_research = Rec_tank_4
					can_research = Rec_tank_5
					is_researching_technology = Rec_tank_1
					is_researching_technology = Rec_tank_2
					is_researching_technology = Rec_tank_3
					is_researching_technology = Rec_tank_4
					is_researching_technology = Rec_tank_5
				}
			}
		}

		categories = {
			CAT_armor
			CAT_tanks
			CAT_rec_tank
			CAT_Military
		}
	}
	#1985
	Rec_tank_1 = {

		enable_equipments = {
			Rec_tank_1
		}

		path = {
			leads_to_tech = Rec_tank_2
			research_cost_coeff = 1
		}

		research_cost = 1.5
		start_year = 1985
		folder = {
			name = armour_folder
			position = { x = @row4 y = @1985 }
		}

		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				OR = {
					has_tech = Rec_tank_2
					has_tech = Rec_tank_3
					has_tech = Rec_tank_4
					has_tech = Rec_tank_5
					can_research = Rec_tank_2
					can_research = Rec_tank_3
					can_research = Rec_tank_4
					can_research = Rec_tank_5
					is_researching_technology = Rec_tank_2
					is_researching_technology = Rec_tank_3
					is_researching_technology = Rec_tank_4
					is_researching_technology = Rec_tank_5
				}
			}
		}

		categories = {
			CAT_armor
			CAT_tanks
			CAT_rec_tank
			CAT_Military
		}
	}
	#2005
	Rec_tank_2 = {

		enable_equipments = {
			Rec_tank_2
		}

		path = {
			leads_to_tech = Rec_tank_3
			research_cost_coeff = 1
		}

		research_cost = 1.6
		start_year = 2005
		folder = {
			name = armour_folder
			position = { x = @row4 y = @2005 }
		}

		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.5
				date < 2005.01.01
			}
			modifier = {
				factor = 0.2
				date < 2002.01.01
			}
			modifier = {
				factor = 0
				OR = {
					has_tech = Rec_tank_3
					has_tech = Rec_tank_4
					has_tech = Rec_tank_5
					can_research = Rec_tank_3
					can_research = Rec_tank_4
					can_research = Rec_tank_5
					is_researching_technology = Rec_tank_3
					is_researching_technology = Rec_tank_4
					is_researching_technology = Rec_tank_5
				}
			}
		}

		categories = {
			CAT_armor
			CAT_tanks
			CAT_rec_tank
			CAT_Military
		}
	}
	#2015
	Rec_tank_3 = {

		enable_equipments = {
			Rec_tank_3
		}

		path = {
			leads_to_tech = Rec_tank_4
			research_cost_coeff = 1
		}

		research_cost = 1.7
		start_year = 2015
		folder = {
			name = armour_folder
			position = { x = @row4 y = @2015 }
		}

		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.5
				date < 2015.01.01
			}
			modifier = {
				factor = 0.2
				date < 2010.01.01
			}
			modifier = {
				factor = 0
				OR = {
					has_tech = Rec_tank_4
					has_tech = Rec_tank_5
					can_research = Rec_tank_4
					can_research = Rec_tank_5
					is_researching_technology = Rec_tank_4
					is_researching_technology = Rec_tank_5
				}
			}
		}

		categories = {
			CAT_armor
			CAT_tanks
			CAT_rec_tank
			CAT_Military
		}
	}
	#2025
	Rec_tank_4 = {

		enable_equipments = {
			Rec_tank_4
		}

		path = {
			leads_to_tech = Rec_tank_5
			research_cost_coeff = 1
		}

		research_cost = 1.8
		start_year = 2025
		folder = {
			name = armour_folder
			position = { x = @row4 y = @2025 }
		}

		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.5
				date < 2025.01.01
			}
			modifier = {
				factor = 0.2
				date < 2020.01.01
			}
			modifier = {
				factor = 0
				OR = {
					has_tech = Rec_tank_5
					can_research = Rec_tank_5
					is_researching_technology = Rec_tank_5
				}
			}
		}

		categories = {
			CAT_armor
			CAT_tanks
			CAT_rec_tank
			CAT_Military
		}
	}
	#2035
	Rec_tank_5 = {

		enable_equipments = {
			Rec_tank_5
		}

		research_cost = 1.9
		start_year = 2035
		folder = {
			name = armour_folder
			position = { x = @row4 y = @2035 }
		}

		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.5
				date < 2035.01.01
			}
			modifier = {
				factor = 0.2
				date < 2031.01.01
			}
		}

		categories = {
			CAT_armor
			CAT_tanks
			CAT_rec_tank
			CAT_Military
		}
	}

}

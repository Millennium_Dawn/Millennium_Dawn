characters = {

	#################
	### GENERALS ####
	#################

	HWI_Austin_S_Miller = {
		name = "Austin S. Miller"
		portraits = {
			army = { small = "gfx/leaders/HWI/small/Zgen_Austin_S_Miller_small.dds" }
			army = { large = "gfx/leaders/HWI/Zgeneral_Austin_S_Miller.dds" }
		}
		
		corps_commander = {
			traits = {  infantry_officer infantry_leader organizer ranger desert_fox air_cavalry_leader }
			skill = 5
			attack_skill = 7
			defense_skill = 5
			planning_skill = 3
			logistics_skill = 1
		}
	}

}

characters = {

	#################
	### GENERALS ####
	#################

	ASK_Julie_A_Small = {
		name = "Julie A. Small"
		portraits = {
			army = { small = "gfx/leaders/ASK/small/Zgen_Julie_A_Small_small.dds" }
			army = { large = "gfx/leaders/ASK/Zgeneral_Julie_A_Small.dds" }
		}
		
		corps_commander = {
			traits = { kind_leader hill_fighter winter_specialist }
			skill = 2
			attack_skill = 2
			defense_skill = 3
			planning_skill = 1
			logistics_skill = 1
		}
	}

}

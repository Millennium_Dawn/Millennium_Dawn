characters = {
	# Zaddy Rahmon
	TAJ_emomali_rahom = {
		name = "Emomali Rahmonov"
		portraits = {
			civilian = {
				large = "gfx/leaders/TAJ/emomali_rahmon_young.dds"
			}
		}
		country_leader = {
			desc = "TAJ_emomali_rahom_desc"
			ideology = Conservative
			traits = {
				emerging_Conservative
				his_excellency
				military_career
				political_dancer
			}
		}
	}
	TAJ_emomali_rahom_old = {
		name = "Emomali Rahmonov"
		portraits = {
			civilian = {
				large = "gfx/leaders/TAJ/Emomali_Rahmon.dds"
			}
		}
		country_leader = {
			desc = "TAJ_emomali_rahom_desc"
			ideology = Conservative
			traits = {
				emerging_Conservative
				his_excellency
				military_career
				capable
				political_dancer
			}
		}
	}

	TAJ_emomali_sobirzoda = {
		name = "Emamoli Sobirzoda"
		portraits = {
			army = {
				small = "gfx/leaders/TAJ/small/Portrait_Emomali_Sobirzoda_small.dds"
				large = "gfx/leaders/TAJ/Portrait_Emomali_Sobirzoda.dds"
			}
		}
		advisor = {
			slot = army_chief
			idea_token = emomali_sobirzoda
			ledger = army
			allowed = {
				original_tag = TAJ
			}
			traits = {
				army_chief_entrenchment_2
			}
			cost = 100
			ai_will_do = {
				factor = 1
			}
		}
		corps_commander = {
			traits = { trait_cautious trait_engineer }
			skill = 2
			attack_skill = 2
			defense_skill = 1
			planning_skill = 2
			logistics_skill = 2
			visible = {
				has_completed_focus = TAJ_expand_joint_chiefs_of_staff
			}
		}
	}

	TAJ_zarif_sharifzoda = {
		name = "Zarif Sharifzoda"
		portraits = {
			army = {
				small = "gfx/leaders/TAJ/small/zarif_sharifzoda_small.dds"
				large = "gfx/leaders/TAJ/zarif_sharifzoda.dds"
			}
		}
		advisor = {
			slot = army_chief
			idea_token = emomali_sobirzoda
			ledger = army
			allowed = {
				original_tag = TAJ
			}
			traits = {
				army_chief_morale_3
			}
			cost = 100
			ai_will_do = {
				factor = 1
			}
		}
		field_marshal = {
			traits = { old_guard inspirational_leader }
			skill = 3
			attack_skill = 2
			defense_skill = 2
			planning_skill = 2
			logistics_skill = 4
		}
	}

	TAJ_rajabali_rahmonali = {
		name = "Rajabali Rahmonali"
		portraits = {
			army = {
				small = "gfx/leaders/TAJ/small/rajabali_rahmonali_small.dds"
				large = "gfx/leaders/TAJ/rajabali_rahmonali.dds"
			}
		}
		advisor = {
			slot = army_chief
			idea_token = emomali_sobirzoda
			ledger = army
			allowed = {
				original_tag = TAJ
			}
			traits = {
				army_chief_entrenchment_2
			}
			cost = 100
			ai_will_do = {
				factor = 1
			}
		}
		corps_commander = {
			traits = { trait_mountaineer }
			skill = 2
			attack_skill = 1
			defense_skill = 2
			planning_skill = 1
			logistics_skill = 2
			visible = {
				has_completed_focus = TAJ_ensure_regime_loyalty
			}
		}
	}

	TAJ_ghaffor_mirzoyev = {
		name = "Ghaffor Mirzoyev"
		portraits = {
			army = {
				small = "gfx/leaders/TAJ/small/ghaffor_mirzoyev_small.dds"
				large = "gfx/leaders/TAJ/ghaffor_mirzoyev.dds"
			}
		}
		advisor = {
			slot = high_command
			idea_token = ghaffor_mirzoyev
			ledger = army
			allowed = {
				original_tag = TAJ
			}
			traits = {
				army_regrouping_2
			}
			cost = 100
			ai_will_do = {
				factor = 1
			}
		}
		corps_commander = {
			traits = { combined_arms_expert }
			skill = 2
			attack_skill = 1
			defense_skill = 3
			planning_skill = 1
			logistics_skill = 2
		}
	}
	TAJ_sherali_khayrulloyev = {
		name = "Sherali Khairulloyev"
		portraits = {
			army = {
				small = "gfx/leaders/TAJ/small/sherali_khayrulloyev_small.dds"
				large = "gfx/leaders/TAJ/sherali_khayrulloyev.dds"
			}
		}
		advisor = {
			slot = air_chief
			idea_token = sherali_khayrulloyev
			ledger = air
			allowed = {
				original_tag = TAJ
			}
			traits = {
				air_chief_safety_2
			}
			cost = 100
			ai_will_do = {
				factor = 1
			}
		}
	}
	TAJ_sherali_mirzo = {
		name = "Sherali Mirzo"
		portraits = {
			army = {
				small = "gfx/leaders/TAJ/small/Portrait_Sherali_Mirzo_small.dds"
				large = "gfx/leaders/TAJ/Portrait_Sherali_Mirzo.dds"
			}
		}
		advisor = {
			slot = high_command
			idea_token = sherali_mirzo
			ledger = army
			allowed = {
				original_tag = TAJ
			}
			traits = {
				army_chief_organizational_2
			}
			cost = 100
			ai_will_do = {
				factor = 1
			}
		}
		corps_commander = {
			traits = { trait_mountaineer }
			skill = 2
			attack_skill = 3
			defense_skill = 1
			planning_skill = 1
			logistics_skill = 3
		}
	}
}
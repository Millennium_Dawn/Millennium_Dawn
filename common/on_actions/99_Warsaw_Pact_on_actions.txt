on_actions = {
	#
		on_daily_POL = {
			effect = {
				if = {
					limit = { 
						POL = {
							has_country_flag = warsaw_member_check
							SOV = {
								has_civil_war = yes
							}
						} 
					}
					POL = { country_event = sov_warsaw_pact.15 }
				}
			}
		}
		on_daily_HUN = {
			effect = {
				if = {
					limit = { 
						HUN = {
							has_country_flag = warsaw_member_check
							SOV = {
								has_civil_war = yes
							}
						} 
					}
					HUN = { country_event = sov_warsaw_pact.15 }
				}
			}
		}
		on_daily_CZE = {
			effect = {
				if = {
					limit = { 
						CZE = {
							has_country_flag = warsaw_member_check
							SOV = {
								has_civil_war = yes
							}
						} 
					}
					CZE = { country_event = sov_warsaw_pact.15 }
				}
			}
		}
		on_daily_SLO = {
			effect = {
				if = {
					limit = { 
						SLO = {
							has_country_flag = warsaw_member_check
							SOV = {
								has_civil_war = yes
							}
						} 
					}
					SLO = { country_event = sov_warsaw_pact.15 }
				}
			}
		}
		on_daily_ROM = {
			effect = {
				if = {
					limit = { 
						ROM = {
							has_country_flag = warsaw_member_check
							SOV = {
								has_civil_war = yes
							}
						} 
					}
					ROM = { country_event = sov_warsaw_pact.15 }
				}
			}
		}
		on_daily_ALB = {
			effect = {
				if = {
					limit = { 
						ALB = {
							has_country_flag = warsaw_member_check
							SOV = {
								has_civil_war = yes
							}
						} 
					}
					ALB = { country_event = sov_warsaw_pact.15 }
				}
			}
		}
		on_daily_BUL = {
			effect = {
				if = {
					limit = { 
						BUL = {
							has_country_flag = warsaw_member_check
							SOV = {
								has_civil_war = yes
							}
						} 
					}
					BUL = { country_event = sov_warsaw_pact.15 }
				}
			}
		}
		on_daily_SOV = {
			effect = {
				if = {
					limit = {
						GER = {
							has_country_flag = raskol
						  }
						  any_country = {
							NOT = {
								has_country_flag = WP_escaped
							}
							has_autonomy_state = autonomy_warsaw
							has_country_flag = GDR_test
							has_war_with = GER
						  }
					NOT = { has_country_flag = reistablish_frg_raskol }
					SOV = {
						has_idea = SOV_warsaw_pact_idea
					}
				}
				set_country_flag = reistablish_frg_raskol
				SOV = { country_event = sov_warsaw_pact.17 }
			}
		}
	}
}
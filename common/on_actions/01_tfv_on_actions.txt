on_actions = {
	on_capitulation = { }

	#ROOT is subject FROM is previous overlord
	on_subject_free = {
		effect = {
			ROOT = {
				calculate_influence_percentage = yes #set alignment drifts from influence
				startup_politics = yes
				ingame_update_setup = yes 	#money system
				setup_init_factions = yes 	#flags for int. factions
				set_law_vars = yes 	#vars for change laws secondary effects
				update_neighbors_effects = yes # Updates Neighbor Effects
				economic_cycle_drift_popularity = yes # Actually impacts the economic cycle drifts
				if = { limit = { is_ai = yes }
					ai_update_build_units = yes
				}

				country_event = { id = brotherhood.0 } #Sets muslim_brotherhood relations
				if = {
					limit = {
						NOT = { has_variable = domestic_influence_amount }
					}
					set_variable = { domestic_influence_amount = 100 }
				}
				recalculate_influence = yes

				# Union state flag
				if = {
					limit = { has_country_flag = BLR_unionstate_agree }
					clr_country_flag = BLR_unionstate_agree
				}
			}
		}
	}

	#ROOT is subject FROM is overlord
	on_subject_annexed = {
		effect = { ##Grants Cores on Annexation
			ROOT = {
				for_each_scope_loop = {
					array = core_states
					add_core_of = FROM
				}
				every_unit_leader = {
					set_nationality = FROM
				}
				if = {
					limit = { has_country_flag = overlord_subsidies }
					clr_country_flag = overlord_subsidies
					clr_country_flag = overlord_is_subsidizing_us_@FROM
				}
				if = {
					limit = {
						has_country_flag = SCO_joint_exercises_offered
					}
					clr_country_flag = SCO_joint_exercises_offered
				}
				FROM = {
					if = {
						limit = { ROOT = { has_country_flag = overlord_subsidies } }
						clr_country_flag = overlord_subsidies
						clr_country_flag = overlord_is_subsidizing_us_@ROOT
					}
					if = {
						limit = {
							has_country_flag = SCO_joint_exercises_offered
						}
						clr_country_flag = SCO_joint_exercises_offered
					}
					add_to_variable = { treasury = ROOT.treasury }
					add_to_variable = { debt = ROOT.debt }
					add_to_variable = { int_investments = ROOT.int_investments }

					country_event = { id = overlord.1 }
					if = { limit = { original_tag = USA }
						add_to_array = {
							USA_collapse_releases = ROOT.id
						}
					}
				}
			}
		}
	}

	#used when puppeting in a peace conference
	#ROOT = nation being puppeted, FROM = overlord
	on_puppet = {
		effect = {
			ROOT = {
				effect_puppet_match_ruling_party_to_overlord = yes #Just to ensure a correct thing
				research_slot_init_for_country = yes
				calculate_expected_spending = yes
				effect_puppet_match_ruling_party_to_overlord = yes
				set_temp_variable = { percent_change = 30 }
				set_temp_variable = { tag_index = FROM.id }
				set_temp_variable = { influence_target = THIS.id }
				change_influence_percentage = yes
			}
			if = {
				limit = { FROM = { has_idea = faction_resistance_axis_member } }
				ROOT = { add_ideas = faction_resistance_axis_member }
			}
		}
	}

	#ROOT = nation being liberated, FROM = leader of the liberators
	on_liberate = {
		effect = {
			log = "[GetDateText]: [This.GetName]: country [ROOT.GetName]|[THIS.GetName] is liberated by [FROM.GetName]"
			# if basic setup failed for some reason - country file not loaded
			if = { limit = { NOT = { country_exists = ROOT } }
				if = {
					limit = {
						NOT = {
							has_idea = sunni
							has_idea = shia
							has_idea = sufi_islam
							has_idea = ibadi
							has_idea = christian
							has_idea = orthodox_christian
							has_idea = pluralist
							has_idea = buddism
							has_idea = hindu
							has_idea = judaism
							has_idea = shinto
							has_idea = cheondo
							has_idea = uniate_church
							has_idea = zoroastrian
						}
					}

					log = "[GetDateText]: [This.GetName]: country [ROOT.GetName] history file is not loaded"

					set_variable = { parent_tag = FROM }
					# Russian shards
					if = {
						limit = {
							OR = {
								tag = CSB
								tag = ESB
								tag = DON
								tag = KAE
							}
						}

						set_variable = { parent_tag = SOV }
					}

					log = "[GetDateText]: [This.GetName]: country [ROOT.GetName] parent tag is setup as [?parent_tag.GetName]"

					ROOT = {
						if = {
							limit = {
								NOT = { has_variable = domestic_influence_amount }
							}
							set_variable = { domestic_influence_amount = 100 }
						}
						if = {
							limit = { NOT = { has_variable = corporate_tax_rate } }
							set_variable = { var = corporate_tax_rate value = 20 }
						}
						if = {
							limit = { NOT = { has_variable = population_tax_rate } }
							set_variable = { var = population_tax_rate value = 20 }
						}
						ingame_init_investment_system = yes
						init_auto_influence_array = yes

						calculate_influence_percentage = yes #set alignment drifts from influence
						startup_politics = yes
						ingame_update_setup = yes 	#money system
						setup_init_factions = yes 	#flags for int. factions
						set_law_vars = yes 	#vars for change laws secondary effects
						update_neighbors_effects = yes # Updates Neighbor Effects
						economic_cycle_drift_popularity = yes #drift from neighbor
						ai_update_build_units = yes
						calculate_expected_spending = yes

						set_temp_variable = { temp_democratic = FROM.party_popularity@democratic }
						multiply_temp_variable = { temp_democratic = 100 }
						set_temp_variable = { temp_communism = FROM.party_popularity@communism }
						multiply_temp_variable = { temp_communism = 100 }
						set_temp_variable = { temp_neutrality = FROM.party_popularity@neutrality }
						multiply_temp_variable = { temp_neutrality = 100 }
						set_temp_variable = { temp_fascism = FROM.party_popularity@fascism }
						multiply_temp_variable = { temp_fascism = 100 }
						set_temp_variable = { temp_nationalist = FROM.party_popularity@nationalist }
						multiply_temp_variable = { temp_nationalist = 100 }

						if = {
							limit = {
								has_government = nationalist
							}
							multiply_temp_variable = { temp_neutrality = 0.5 }
							add_to_temp_variable = { temp_nationalist = temp_neutrality }
							add_to_array = { ruling_party = 20 }
						}
						else_if = {
							limit = {
								has_government = communism
							}
							multiply_temp_variable = { temp_democratic = 0.5 }
							add_to_temp_variable = { temp_communism = temp_democratic }
							add_to_array = { ruling_party = 6 }
						}
						else_if = {
							limit = {
								has_government = democratic
							}
							multiply_temp_variable = { temp_communism = 0.5 }
							add_to_temp_variable = { temp_democratic = temp_communism }
							add_to_array = { ruling_party = 1 }
						}
						else_if = {
							limit = {
								has_government = fascism
							}
							multiply_temp_variable = { temp_neutrality = 0.5 }
							add_to_temp_variable = { temp_fascism = temp_neutrality }
							add_to_array = { ruling_party = 14 }
						}
						else_if = {
							limit = {
								has_government = neutrality
							}
							add_to_array = { ruling_party = 11 }
						}
						# neutrals fight eachother

						set_popularities = {
							democratic = temp_democratic
							communism = temp_communism
							neutrality = temp_neutrality
							fascism = temp_fascism
							nationalist = temp_nationalist
						}
						start_politics_input = yes
						#copy politics
						for_loop_effect = {
							end = party_pop_array^num
							value = v
							set_variable = { party_pop_array^v = FROM.party_pop_array^v }
						}

						add_up_party_totals = yes
						set_party_positions = yes
						startup_politics = yes

						#VARIABLES
						set_variable = { var = corporate_tax_rate value = FROM.corporate_tax_rate }
						set_variable = { var = population_tax_rate value = FROM.population_tax_rate }
						set_variable = { treasury = 0 }

						#don't get values of original yet - REMINDER -> add it on_civil_war_end
						set_variable = { debt = 0 }
						set_variable = { int_investments = 0 }

						#influence
						set_variable = { domestic_influence_amount = FROM.domestic_influence_amount }
						#just copy all
						for_loop_effect = {
							end = FROM.influence_array^num
							value = v

							add_to_array = { influence_array = FROM.influence_array^v }
							add_to_array = { influence_array_val = FROM.influence_array_val^v }
						}
						recalculate_influence = yes

						#GAME VARIABLES
						add_political_power = FROM.political_power
						add_political_power = 400 #bonus

						#LAWS
						set_variable = { gdp_per_capita = FROM.gdp_per_capita }
						update_budget_laws_for_new_nation = yes

						set_variable = { economic_cycles = FROM.economic_cycles }
						if = { limit = { check_variable = { economic_cycles = 6 } }
							add_ideas = depression
						}
						else_if = { limit = { check_variable = { economic_cycles = 5 } }
							add_ideas = recession
						}
						else_if = { limit = { check_variable = { economic_cycles = 4 } }
							add_ideas = stagnation
						}
						else_if = { limit = { check_variable = { economic_cycles = 3 } }
							add_ideas = stable_growth
						}
						else_if = { limit = { check_variable = { economic_cycles = 2 } }
							add_ideas = fast_growth
						}
						else_if = { limit = { check_variable = { economic_cycles = 1 } }
							add_ideas = economic_boom
						}

						if = { limit = { FROM = { has_idea = paralyzing_corruption } }
							add_ideas = paralyzing_corruption
						}
						else_if = { limit = { FROM = { has_idea = crippling_corruption } }
							add_ideas = crippling_corruption
						}
						else_if = { limit = { FROM = { has_idea = rampant_corruption } }
							add_ideas = rampant_corruption
						}
						else_if = { limit = { FROM = { has_idea = unrestrained_corruption } }
							add_ideas = unrestrained_corruption
						}
						else_if = { limit = { FROM = { has_idea = systematic_corruption } }
							add_ideas = systematic_corruption
						}
						else_if = { limit = { FROM = { has_idea = widespread_corruption } }
							add_ideas = widespread_corruption
						}
						else_if = { limit = { FROM = { has_idea = medium_corruption } }
							add_ideas = medium_corruption
						}
						else_if = { limit = { FROM = { has_idea = modest_corruption } }
							add_ideas = modest_corruption
						}
						else_if = { limit = { FROM = { has_idea = slight_corruption } }
							add_ideas = slight_corruption
						}
						else_if = { limit = { FROM = { has_idea = negligible_corruption } }
							add_ideas = negligible_corruption
						}

						if = { limit = { FROM = { has_idea = consumption_economy } }
							add_ideas = consumption_economy
						}
						else_if = { limit = { FROM = { has_idea = mixed_economy } }
							add_ideas = mixed_economy
						}
						else_if = { limit = { FROM = { has_idea = export_economy } }
							add_ideas = export_economy
						}

						if = { limit = { FROM = { has_idea = no_military } }
							add_ideas = no_military
						}
						else_if = { limit = { FROM = { has_idea = volunteer_army } }
							add_ideas = volunteer_army
						}
						else_if = { limit = { FROM = { has_idea = partial_draft_army } }
							add_ideas = partial_draft_army
						}
						else_if = { limit = { FROM = { has_idea = draft_army } }
							add_ideas = draft_army
						}

						if = { limit = { FROM = { has_idea = no_women_in_military } }
							add_ideas = no_women_in_military
						}
						else_if = { limit = { FROM = { has_idea = volunteer_women } }
							add_ideas = volunteer_women
						}
						else_if = { limit = { FROM = { has_idea = drafted_women } }
							add_ideas = drafted_women
						}

						if = { limit = { FROM = { has_idea = intervention_isolation } }
							add_ideas = intervention_isolation
						}
						else_if = { limit = { FROM = { has_idea = intervention_local_security } }
							add_ideas = intervention_local_security
						}
						else_if = { limit = { FROM = { has_idea = intervention_limited_interventionism } }
							add_ideas = intervention_limited_interventionism
						}
						else_if = { limit = { FROM = { has_idea = intervention_regional_interventionism } }
							add_ideas = intervention_regional_interventionism
						}
						else_if = { limit = { FROM = { has_idea = intervention_global_interventionism } }
							add_ideas = intervention_global_interventionism
						}
						else_if = { limit = { FROM = { has_idea = intervention_neo_imperialism } }
							add_ideas = intervention_neo_imperialism
						}

						if = { limit = { FROM = { has_idea = officer_baptism_by_fire } }
							add_ideas = officer_baptism_by_fire
						}
						else_if = { limit = { FROM = { has_idea = officer_basic_training } }
							add_ideas = officer_basic_training
						}
						else_if = { limit = { FROM = { has_idea = officer_advanced_training } }
							add_ideas = officer_advanced_training
						}
						else_if = { limit = { FROM = { has_idea = officer_military_school } }
							add_ideas = officer_military_school
						}
						else_if = { limit = { FROM = { has_idea = officer_military_academy } }
							add_ideas = officer_military_academy
						}
						else_if = { limit = { FROM = { has_idea = officer_international_education } }
							add_ideas = officer_international_education
						}

						### Internal factions
						if = { limit = { FROM = { has_idea = small_medium_business_owners } }
							add_ideas = small_medium_business_owners
						}
						if = { limit = { FROM = { has_idea = international_bankers } }
							add_ideas = international_bankers
						}
						if = { limit = { FROM = { has_idea = fossil_fuel_industry } }
							add_ideas = fossil_fuel_industry
						}
						if = { limit = { FROM = { has_idea = industrial_conglomerates } }
							add_ideas = industrial_conglomerates
						}
						if = { limit = { FROM = { has_idea = oligarchs } }
							add_ideas = oligarchs
						}
						if = { limit = { FROM = { has_idea = maritime_industry } }
							add_ideas = maritime_industry
						}
						if = { limit = { FROM = { has_idea = defense_industry } }
							add_ideas = defense_industry
						}
						if = { limit = { FROM = { has_idea = the_military } }
							add_ideas = the_military
						}
						if = { limit = { FROM = { has_idea = intelligence_community } }
							add_ideas = intelligence_community
						}
						if = { limit = { FROM = { has_idea = labour_unions } }
							add_ideas = labour_unions
						}
						if = { limit = { FROM = { has_idea = landowners } }
							add_ideas = landowners
						}
						if = { limit = { FROM = { has_idea = farmers } }
							add_ideas = farmers
						}
						if = {
							limit = {
								FROM = {
									has_idea = communist_cadres
									FROM = {
										OR = {
											is_in_array = { ruling_party = 4 }
											is_in_array = { ruling_party = 19 }
										}
									}
								}
							}
							add_ideas = communist_cadres
						}
						if = { limit = { FROM = { has_idea = the_priesthood } }
							add_ideas = the_priesthood
						}
						if = { limit = { FROM = { has_idea = the_ulema } }
							add_ideas = the_ulema
						}
						if = { limit = { FROM = { has_idea = the_clergy } }
							add_ideas = the_clergy
						}
						if = { limit = { FROM = { has_idea = wahabi_ulema } }
							add_ideas = wahabi_ulema
						}
						if = { limit = { FROM = { has_idea = the_donju } }
							add_ideas = the_donju
						}
						if = { limit = { FROM = { has_idea = the_bazaar } }
							add_ideas = the_bazaar
						}
						if = {
							limit = {
								FROM = {
									has_idea = saudi_royal_family
									FROM = {
										OR = {
											is_in_array = { ruling_party = 10 }
											is_in_array = { ruling_party = 23 }
											is_in_array = { ruling_party = 0 }
										}
									}
								}
							}
							add_ideas = saudi_royal_family
						}
						if = {
							limit = {
								FROM = {
									has_idea = irgc
									FROM = {
										NOT = {
											has_government = democratic
											has_government = fascism
										}
									}
								}
							}
							add_ideas = irgc
						}
						if = {
							limit = {
								FROM = {
									has_idea = iranian_quds_force
									FROM = {
										NOT = {
											has_government = democratic
											has_government = fascism
										}
									}
								}
							}
							add_ideas = iranian_quds_force
						}
						if = {
							limit = {
								FROM = {
									has_idea = vevak
									FROM = {
										NOT = {
											has_government = democratic
											has_government = fascism
										}
									}
								}
							}
							add_ideas = vevak
						}
						if = {
							limit = {
								FROM = {
									has_idea = foreign_jihadis
									FROM = {
										OR = {
											is_in_array = { ruling_party = 11 }
										}
									}
								}
							}
							add_ideas = foreign_jihadis
						}
						if = { limit = { FROM = { has_idea = wall_street } }
							add_ideas = wall_street
						}
						if = { limit = { FROM = { has_idea = chaebols } }
							add_ideas = chaebols
						}
						#copy leader throughput
						set_variable = { Western_Autocracy_leader = FROM.Western_Autocracy_leader?0 }
						set_variable = { conservatism_leader = FROM.conservatism_leader?0 }
						set_variable = { liberalism_leader = FROM.liberalism_leader?0 }
						set_variable = { socialism_leader = FROM.socialism_leader?0 }

						set_variable = { Communist-State_leader = FROM.Communist-State_leader?0 }
						set_variable = { anarchist_communism_leader = FROM.anarchist_communism_leader?0 }
						set_variable = { Conservative_leader = FROM.Conservative_leader?0 }
						set_variable = { Autocracy_leader = FROM.Autocracy_leader?0 }
						set_variable = { Mod_Vilayat_e_Faqih_leader = FROM.Mod_Vilayat_e_Faqih_leader?0 }
						set_variable = { Vilayat_e_Faqih_leader = FROM.Vilayat_e_Faqih_leader?0 }

						set_variable = { Kingdom_leader = FROM.Kingdom_leader?0 }
						set_variable = { Caliphate_leader = FROM.Caliphate_leader?0 }

						set_variable = { Neutral_Muslim_Brotherhood_leader = FROM.Neutral_Muslim_Brotherhood_leader?0 }
						set_variable = { Neutral_Autocracy_leader = FROM.Neutral_Autocracy_leader?0 }
						set_variable = { Neutral_conservatism_leader = FROM.Neutral_conservatism_leader?0 }
						set_variable = { oligarchism_leader = FROM.oligarchism_leader?0 }
						set_variable = { Neutral_Libertarian_leader = FROM.Neutral_Libertarian_leader?0 }
						set_variable = { Neutral_green_leader = FROM.Neutral_green_leader?0 }
						set_variable = { neutral_Social_leader = FROM.neutral_Social_leader?0 }
						set_variable = { Neutral_Communism_leader = FROM.Neutral_Communism_leader?0 }
						set_variable = { Nat_Populism_leader = FROM.Nat_Populism_leader?0 }
						set_variable = { Nat_Fascism_leader = FROM.Nat_Fascism_leader?0 }
						set_variable = { Nat_Autocracy_leader = FROM.Nat_Autocracy_leader?0 }
						set_variable = { Monarchist_leader = FROM.Monarchist_leader?0 }

						calculate_influence_percentage = yes #set alignment drifts from influence
						ingame_update_setup = yes 	#money system
						setup_init_factions = yes 	#flags for int. factions
						update_neighbors_effects = yes # Updates Neighbor Effects
						economic_cycle_drift_popularity = yes #drift from neighbor

						set_variable = { civilian_factory_employment_var = FROM.civilian_factory_employment_var }
						set_variable = { naval_factory_employment_var = FROM.naval_factory_employment_var }
						set_variable = { military_factory_employment_var = FROM.military_factory_employment_var }
						set_variable = { office_employment_var = FROM.office_employment_var }
						set_variable = { agriculture_district_employment_var = FROM.agriculture_district_employment_var }


						# set religion
						if = { limit = { FROM = { has_idea = sunni } }
							add_ideas = sunni
						}
						else_if = {
							limit = { FROM = { has_idea = shia } }
							add_ideas = shia
						}
						else_if = {
							limit = { FROM = { has_idea = sufi_islam } }
							add_ideas = sufi_islam
						}
						else_if = {
							limit = { FROM = { has_idea = ibadi } }
							add_ideas = ibadi
						}
						else_if = {
							limit = { FROM = { has_idea = christian } }
							add_ideas = christian
						}
						else_if = {
							limit = { FROM = { has_idea = orthodox_christian } }
							add_ideas = orthodox_christian
						}
						else_if = {
							limit = { FROM = { has_idea = buddism } }
							add_ideas = buddism
						}
						else_if = {
							limit = { FROM = { has_idea = hindu } }
							add_ideas = hindu
						}
						else_if = {
							limit = { FROM = { has_idea = judaism } }
							add_ideas = judaism
						}
						else_if = {
							limit = { FROM = { has_idea = shinto } }
							add_ideas = shinto
						}
						else_if = {
							limit = { FROM = { has_idea = cheondo } }
							add_ideas = cheondo
						}
						else_if = {
							limit = { FROM = { has_idea = uniate_church } }
							add_ideas = uniate_church
						}
						else_if = {
							limit = { FROM = { has_idea = zoroastrian } }
							add_ideas = zoroastrian
						}
						else = {
							add_ideas = pluralist
						}

						country_event = { id = brotherhood.0 } #Sets muslim_brotherhood relations

						inherit_technology = FROM

						add_ideas = { police_02 }
						add_ideas = { health_02 }
						add_ideas = { bureau_02 }
						add_ideas = { edu_02 }
						add_ideas = { social_02 }
						add_ideas = { defence_02 }
					}
				}
				else = {
					ROOT = {
						set_temp_variable = { percent_change = 15 }
						set_temp_variable = { tag_index = FROM.id }
						set_temp_variable = { influence_target = ROOT.id }

						if = {
							limit = { NOT = { has_variable = corporate_tax_rate } }
							set_variable = { var = corporate_tax_rate value = 20 }
						}
						if = {
							limit = { NOT = { has_variable = population_tax_rate } }
							set_variable = { var = population_tax_rate value = 20 }
						}
						ingame_init_investment_system = yes
						init_auto_influence_array = yes

						calculate_influence_percentage = yes #set alignment drifts from influence
						startup_politics = yes
						setup_init_factions = yes 	#flags for int. factions
						set_law_vars = yes 	#vars for change laws secondary effects
						update_neighbors_effects = yes # Updates Neighbor Effects
						economic_cycle_drift_popularity = yes #drift from neighbor
						ai_update_build_units = yes
						calculate_expected_spending = yes

						set_variable = { civilian_factory_employment_var = FROM.civilian_factory_employment_var }
						set_variable = { naval_factory_employment_var = FROM.naval_factory_employment_var }
						set_variable = { military_factory_employment_var = FROM.military_factory_employment_var }
						set_variable = { office_employment_var = FROM.office_employment_var }
						set_variable = { agriculture_district_employment_var = FROM.agriculture_district_employment_var }

						ingame_update_setup = yes 	#money system
						research_slot_init_for_country = yes
					}
				}
			}
		}
	}

	#used when puppeting through the occupied territories menu during peace time (or when releasing from non-core but owned territory, f.e. Britain releasing Egypt)
	#ROOT = nation being released, FROM = overlord
	on_release_as_puppet = {
		effect = {
			ROOT = {
				set_variable = { civilian_factory_employment_var = 1 }
				set_variable = { naval_factory_employment_var = 1 }
				set_variable = { military_factory_employment_var = 1 }
				set_variable = { office_employment_var = 1 }
				set_variable = { agriculture_district_employment_var = 1 }
				set_variable = { gdpc_converging_var = FROM.gdpc_converging_var }
				country_event = { id = brotherhood.0 } #Sets muslim_brotherhood relations
				if = {
					limit = {
						NOT = { has_variable = domestic_influence_amount }
					}
					set_variable = { domestic_influence_amount = 100 }
				}
				if = {
					limit = { FROM = { has_government = democratic } }
					set_politics = {
						ruling_party = democratic
						last_election = "1999.10.3"
						election_frequency = 60
						elections_allowed = yes
					}
					set_popularities = {
						democratic = 70.0 #Western
						communism = 10.0 #Emerging
						fascism = 0.0 #Salafist
						neutrality = 10.0 #Non Aligned
						nationalist = 10.0 #Nationalist
					}
				}
				if = {
					limit = { FROM = { has_government = communism } }
					set_politics = {
						ruling_party = communism
						last_election = "1999.10.3"
						election_frequency = 60
						elections_allowed = yes
					}
					set_popularities = {
						democratic = 10.0 #Western
						communism = 70.0 #Emerging
						fascism = 0.0 #Salafist
						neutrality = 10.0 #Non Aligned
						nationalist = 10.0 #Nationalist
					}
				}
				if = {
					limit = { FROM = { has_government = neutrality } }
					set_politics = {
						ruling_party = neutrality
						last_election = "1999.10.3"
						election_frequency = 60
						elections_allowed = yes
					}
					set_popularities = {
						democratic = 10.0 #Western
						communism = 10.0 #Emerging
						fascism = 0.0 #Salafist
						neutrality = 70.0 #Non Aligned
						nationalist = 10.0 #Nationalist
					}
				}
				if = {
					limit = { FROM = { has_government = nationalist } }
					set_politics = {
						ruling_party = nationalist
						last_election = "1999.10.3"
						election_frequency = 60
						elections_allowed = yes
					}
					set_popularities = {
						democratic = 10.0 #Western
						communism = 10.0 #Emerging
						fascism = 0.0 #Salafist
						neutrality = 10.0 #Non Aligned
						nationalist = 70.0 #Nationalist
					}
				}
				if = {
					limit = { FROM = { has_government = fascism } }
					set_politics = {
						ruling_party = fascism
						last_election = "1999.10.3"
						election_frequency = 60
						elections_allowed = no
					}
					set_popularities = {
						democratic = 10.0 #Western
						communism = 10.0 #Emerging
						fascism = 70.0 #Salafist
						neutrality = 10.0 #Non Aligned
						nationalist = 0.0 #Nationalist
					}
				}
				if = {
					limit = { FROM = { NOT = { has_government = fascism } } }
					set_temp_variable = { year_change = 3 }
					set_election_year_60_months = yes
				}
				ingame_init_investment_system = yes
				init_auto_influence_array = yes
				inherit_technology = FROM
				if = {
					limit = {
						OR = {
							NOT = { has_variable = corporate_tax_rate }
							NOT = { has_variable = population_tax_rate }
						}
					}
					set_variable = { var = corporate_tax_rate value = 20 }
					set_variable = { var = population_tax_rate value = 20 }
				}

				calculate_influence_percentage = yes #set alignment drifts from influence
				startup_politics = yes
				ingame_update_setup = yes 	#money system
				setup_init_factions = yes 	#flags for int. factions
				set_law_vars = yes 	#vars for change laws secondary effects
				update_neighbors_effects = yes # Updates Neighbor Effects
				economic_cycle_drift_popularity = yes #drift from neighbor
				ai_update_build_units = yes
				effect_puppet_match_ruling_party_to_overlord = yes #Just to ensure a correct thing
				research_slot_init_for_country = yes
				calculate_expected_spending = yes

				setup_missile_configuration_effect = yes
			}
		}
	}

	#ROOT is free nation FROM is releaser
	on_release_as_free = {
		effect = {
			ROOT = {
				set_variable = { civilian_factory_employment_var = 1 }
				set_variable = { naval_factory_employment_var = 1 }
				set_variable = { military_factory_employment_var = 1 }
				set_variable = { office_employment_var = 1 }
				set_variable = { agriculture_district_employment_var = 1 }
				set_variable = { gdpc_converging_var = FROM.gdpc_converging_var }
				country_event = { id = brotherhood.0 } #Sets muslim_brotherhood relations
				if = {
					limit = {
						NOT = { has_variable = domestic_influence_amount }
					}
					set_variable = { domestic_influence_amount = 100 }
				}
				if = {
					limit = { NOT = { has_variable = corporate_tax_rate } }
					set_variable = { var = corporate_tax_rate value = 20 }
				}
				if = {
					limit = { NOT = { has_variable = population_tax_rate } }
					set_variable = { var = population_tax_rate value = 20 }
				}
				ingame_init_investment_system = yes
				init_auto_influence_array = yes

				calculate_influence_percentage = yes #set alignment drifts from influence
				startup_politics = yes
				ingame_update_setup = yes 	#money system
				setup_init_factions = yes 	#flags for int. factions
				set_law_vars = yes 	#vars for change laws secondary effects
				update_neighbors_effects = yes # Updates Neighbor Effects
				economic_cycle_drift_popularity = yes #drift from neighbor
				ai_update_build_units = yes
				calculate_expected_spending = yes

				setup_missile_configuration_effect = yes
			}
			set_temp_variable = { percent_change = 5 }
			set_temp_variable = { tag_index = FROM.id }
			set_temp_variable = { influence_target = ROOT.id }
			change_influence_percentage = yes
			research_slot_init_for_country = yes
		}
	}
}

# Valid unit modifiers are not exhaustive
# Currently supported:
# army_morale
# army_morale_factor
# army_org
# army_org_factor
# supply_consumption_factor
# equipment_capture
# equipment_capture_factor
# army_fuel_capacity_factor
# army_fuel_consumption_factor
# recon_factor
# recon_factor_while_entrenched
# transport_capacity (?)
# breakthrough_factor
# armor_factor
# army_strength_factor
# experience_loss_factor
# leader_modifier applies unit modifiers to all units under them as a general, similarly to traits.
# army_attack_factor
# army_defence_factor
# max_dig_in
# max_dig_in_factor

@cost = 30

unit_medals = {

	### GER
	deployment_medal = {
		available = { MD_should_have_german_medals_trigger = yes }
		frame = 1
		icon = "GFX_medal_icon_ger_md"

		cost = @cost

		unit_modifiers = {
			army_org_factor = 0.05
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	combat_action = {
		available = { MD_should_have_german_medals_trigger = yes }
		frame = 2
		icon = "GFX_medal_icon_ger_md"

		cost = @cost

		unit_modifiers = {
			army_attack_factor = 0.03
			army_defence_factor = 0.03
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	badge_of_honor = {
		available = { MD_should_have_german_medals_trigger = yes }
		frame = 3
		icon = "GFX_medal_icon_ger_md"

		cost = @cost

		unit_modifiers = {
			army_morale_factor = 0.05
			supply_consumption_factor = -0.05
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	cross_of_valor = {
		available = { MD_should_have_german_medals_trigger = yes }
		frame = 4
		icon = "GFX_medal_icon_ger_md"

		cost = @cost

		unit_modifiers = {
			breakthrough_factor = 0.03
			army_strength_factor = 0.03
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	### USA
	bronze_star = {
		available = { MD_should_have_usa_medals_trigger = yes }
		frame = 1
		icon = "GFX_medal_icon_usa"

		cost = @cost

		unit_modifiers = {
			army_morale_factor = 0.05
			army_org_factor = 0.05
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	silver_star = {
		available = { MD_should_have_usa_medals_trigger = yes }
		frame = 2
		icon = "GFX_medal_icon_usa"

		cost = @cost

		unit_modifiers = {
			supply_consumption_factor = -0.1
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	distinguished_service_cross = {
		available = { MD_should_have_usa_medals_trigger = yes }
		frame = 3
		icon = "GFX_medal_icon_usa"

		cost = @cost

		unit_modifiers = {
			army_attack_factor = 0.05
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	medal_of_honor = {
		available = { MD_should_have_usa_medals_trigger = yes }
		frame = 4
		icon = "GFX_medal_icon_usa"

		cost = @cost

		unit_modifiers = {
			experience_loss_factor = -0.2
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	### SOV

	battle_merit_medal = {
		available = { MD_should_have_russian_medals_trigger = yes }
		frame = 1
		icon = "GFX_medal_icon_sov_md"

		cost = @cost

		unit_modifiers = {
			army_morale_factor = 0.1
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	order_of_kutuzov = {
		available = { MD_should_have_russian_medals_trigger = yes }
		frame = 2
		icon = "GFX_medal_icon_sov_md"

		cost = @cost

		unit_modifiers = {
			army_defence_factor = 0.1
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	order_of_saint_george = {
		available = { MD_should_have_russian_medals_trigger = yes }
		frame = 3
		icon = "GFX_medal_icon_sov_md"

		cost = @cost

		unit_modifiers = {
			army_attack_factor = 0.05
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	order_of_the_butcher = {
		available = { MD_should_have_russian_medals_trigger = yes }
		frame = 4
		icon = "GFX_medal_icon_sov_md"

		cost = @cost

		unit_modifiers = {
			breakthrough_factor = 0.05
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	hero_of_the_russia = {
		available = {
			MD_should_have_russian_medals_trigger = yes
			emerging_communist_state_are_not_in_power = yes
		}
		frame = 5
		icon = "GFX_medal_icon_sov_md"

		cost = @cost

		unit_modifiers = {
			army_org_factor = 0.05
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	hero_of_the_soviet_union = {
		available = { MD_should_have_soviet_medals_trigger = yes }
		frame = 6
		icon = "GFX_medal_icon_sov_md"

		cost = @cost

		unit_modifiers = {
			army_org_factor = 0.05
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	### WAG

	za_otvagy = {
		available = { MD_should_have_wagner_medals_trigger = yes }
		frame = 1
		icon = "GFX_medal_icon_wag_md"

		cost = @cost

		unit_modifiers = {
			army_morale_factor = 0.1
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	za_myzestvo = {
		available = { MD_should_have_wagner_medals_trigger = yes }
		frame = 2
		icon = "GFX_medal_icon_wag_md"

		cost = @cost

		unit_modifiers = {
			breakthrough_factor = 0.05
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	za_crov_i_hrabrost = {
		available = { MD_should_have_wagner_medals_trigger = yes }
		frame = 3
		icon = "GFX_medal_icon_wag_md"

		cost = @cost

		unit_modifiers = {
			army_defence_factor = 0.1
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	krov_chest_rodina_otvaga = {
		available = { MD_should_have_wagner_medals_trigger = yes }
		frame = 4
		icon = "GFX_medal_icon_wag_md"

		cost = @cost

		unit_modifiers = {
			army_attack_factor = 0.05
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	hero_of_the_pmc_wagner = {
		available = { MD_should_have_wagner_medals_trigger = yes }
		frame = 5
		icon = "GFX_medal_icon_wag_md"

		cost = @cost

		unit_modifiers = {
			army_org_factor = 0.05
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	### ENG
	dispatches_oak_leaves = {
		available = { MD_should_have_english_medals_trigger = yes }
		frame = 1
		icon = "GFX_medal_icon_eng"

		cost = @cost

		unit_modifiers = {
			recon_factor_while_entrenched = 0.25
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	order_british_empire = {
		available = { MD_should_have_english_medals_trigger = yes }
		frame = 2
		icon = "GFX_medal_icon_eng"

		cost = @cost

		unit_modifiers = {
			army_strength_factor = 0.1
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	distinguished_service_order = {
		available = { MD_should_have_english_medals_trigger = yes }
		frame = 3
		icon = "GFX_medal_icon_eng"

		cost = @cost

		unit_modifiers = {
			army_defence_factor = 0.075
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	victoria_cross = {
		available = { MD_should_have_english_medals_trigger = yes }
		frame = 4
		icon = "GFX_medal_icon_eng"

		cost = @cost

		unit_modifiers = {
			army_morale_factor = 0.1
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	### FRA
	combatants_cross = {
		available = { MD_should_have_french_medals_trigger = yes }
		frame = 1
		icon = "GFX_medal_icon_fra"

		cost = @cost

		unit_modifiers = {
			max_dig_in_factor = 0.1
			recon_factor_while_entrenched = 0.1
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	war_cross = {
		available = { MD_should_have_french_medals_trigger = yes }
		frame = 2
		icon = "GFX_medal_icon_fra"

		cost = @cost

		unit_modifiers = {
			army_org_factor = 0.05
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	order_of_liberation = {
		available = { MD_should_have_french_medals_trigger = yes }
		frame = 3
		icon = "GFX_medal_icon_fra"

		cost = @cost

		unit_modifiers = {
			army_fuel_consumption_factor = -0.1
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	legion_of_honor = {
		available = { MD_should_have_french_medals_trigger = yes }
		frame = 4
		icon = "GFX_medal_icon_fra"

		cost = @cost

		unit_modifiers = {
			supply_consumption_factor = -0.05
			army_defence_factor = 0.05
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	### ITA

	war_merit_cross = {
		available = { MD_should_have_italian_medals_trigger = yes }
		frame = 1
		icon = "GFX_medal_icon_ita"

		cost = @cost

		unit_modifiers = {
			equipment_capture_factor = 0.05
			army_morale_factor = 0.05
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	cross_of_military_valor = {
		available = { MD_should_have_italian_medals_trigger = yes }
		frame = 2
		icon = "GFX_medal_icon_ita"

		cost = @cost

		unit_modifiers = {
			army_strength_factor = 0.05
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	order_of_savoy_fifth = {
		available = { MD_should_have_italian_medals_trigger = yes }
		frame = 3
		icon = "GFX_medal_icon_ita"

		cost = @cost

		unit_modifiers = {
			supply_consumption_factor = -0.1
			army_defence_factor = -0.05
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	order_of_savoy_first = {
		available = { MD_should_have_italian_medals_trigger = yes }
		frame = 4
		icon = "GFX_medal_icon_ita"

		cost = @cost

		unit_modifiers = {
			army_attack_factor = 0.05
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	### JAP
	military_medal_of_honor = {
		available = { MD_should_have_japanese_medals_trigger = yes }
		frame = 1
		icon = "GFX_medal_icon_jap"

		cost = @cost

		unit_modifiers = {
			army_morale_factor = 0.075
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	sacred_treasure = {
		available = { MD_should_have_japanese_medals_trigger = yes }
		frame = 2
		icon = "GFX_medal_icon_jap"

		cost = @cost

		unit_modifiers = {
			army_morale_factor = 0.05
			army_strength_factor = 0.05
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	golden_kite = {
		available = { MD_should_have_japanese_medals_trigger = yes }
		frame = 3
		icon = "GFX_medal_icon_jap"

		cost = @cost

		unit_modifiers = {
			supply_consumption_factor = -0.05
			army_attack_factor = 0.05
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	order_of_chrysanthemum = {
		available = { MD_should_have_japanese_medals_trigger = yes }
		frame = 4
		icon = "GFX_medal_icon_jap"

		cost = @cost

		unit_modifiers = {
			breakthrough_factor = 0.05
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	### CHI
	national_defense_service = {
		available = { MD_should_have_chinese_medals_trigger = yes }
		frame = 1
		icon = "GFX_medal_icon_chi_md"

		cost = @cost

		unit_modifiers = {
			army_morale_factor = 0.1
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	august_first_medal = {
		available = { MD_should_have_chinese_medals_trigger = yes }
		frame = 2
		icon = "GFX_medal_icon_chi_md"

		cost = @cost

		unit_modifiers = {
			army_org_factor = 0.05
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	republic_medal = {
		available = { MD_should_have_chinese_medals_trigger = yes }
		frame = 3
		icon = "GFX_medal_icon_chi_md"

		cost = @cost

		unit_modifiers = {
			army_attack_factor = 0.03
			army_defence_factor = 0.03
			breakthrough_factor = 0.03
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	### Generic Democratic

	democratic_gallantry = {
		available = {
			has_government = democratic
			MD_should_have_any_unique_medals_trigger = no
		}
		frame = 1
		icon = "GFX_medal_icon_democratic"

		cost = @cost

		unit_modifiers = {
			army_morale_factor = 0.1
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	democratic_honor = {
		available = {
			has_government = democratic
			MD_should_have_any_unique_medals_trigger = no
		}
		frame = 2
		icon = "GFX_medal_icon_democratic"

		cost = @cost

		unit_modifiers = {
			supply_consumption_factor = -0.1
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	democratic_war_cross = {
		available = {
			has_government = democratic
			MD_should_have_any_unique_medals_trigger = no
		}
		frame = 3
		icon = "GFX_medal_icon_democratic"

		cost = @cost

		unit_modifiers = {
			army_defence_factor = 0.05
			recon_factor = 0.1
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	democratic_lion = {
		available = {
			has_government = democratic
			MD_should_have_any_unique_medals_trigger = no
		}
		frame = 4
		icon = "GFX_medal_icon_democratic"

		cost = @cost

		unit_modifiers = {
			experience_loss_factor = -0.15
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	### Generic Communist
	communist_service_medal = {
		available = {
			has_government = communism
			MD_should_have_any_unique_medals_trigger = no
		}
		frame = 1
		icon = "GFX_medal_icon_communism"

		cost = @cost

		unit_modifiers = {
			supply_consumption_factor = -0.05
			army_fuel_consumption_factor = -0.05
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	communist_red_star = {
		available = {
			has_government = communism
			MD_should_have_any_unique_medals_trigger = no
		}
		frame = 2
		icon = "GFX_medal_icon_communism"

		cost = @cost

		unit_modifiers = {
			army_morale_factor = 0.05
			army_defence_factor = 0.05
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	communist_merit_medal = {
		available = {
			has_government = communism
			MD_should_have_any_unique_medals_trigger = no
		}
		frame = 3
		icon = "GFX_medal_icon_communism"

		cost = @cost

		unit_modifiers = {
			army_org_factor = 0.05
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	communist_hero_of_revolution = {
		available = {
			has_government = communism
			MD_should_have_any_unique_medals_trigger = no
		}
		frame = 4
		icon = "GFX_medal_icon_communism"

		cost = @cost

		unit_modifiers = {
			army_strength_factor = 0.05
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	### Generic Nationalism

	nat_bravery = {
		available = {
			has_government = nationalist
			MD_should_have_any_unique_medals_trigger = no
		}
		frame = 1
		icon = "GFX_medal_icon_fascism"

		cost = @cost

		unit_modifiers = {
			army_org_factor = 0.05
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	nat_merit = {
		available = {
			has_government = nationalist
			MD_should_have_any_unique_medals_trigger = no
		}
		frame = 2
		icon = "GFX_medal_icon_fascism"

		cost = @cost

		unit_modifiers = {
			army_attack_factor = 0.05
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	nat_order_brave = {
		available = {
			has_government = nationalist
			MD_should_have_any_unique_medals_trigger = no
		}
		frame = 3
		icon = "GFX_medal_icon_fascism"

		cost = @cost

		unit_modifiers = {
			armor_factor = 0.1
			recon_factor = 0.1
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	nat_heroism = {
		available = {
			has_government = nationalist
			MD_should_have_any_unique_medals_trigger = no
		}
		frame = 4
		icon = "GFX_medal_icon_fascism"

		cost = @cost

		unit_modifiers = {
			breakthrough_factor = 0.05
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	### Generic Salafism
	fascism_bravery = {
		available = {
			has_government = fascism
			MD_should_have_any_unique_medals_trigger = no
		}
		frame = 1
		icon = "GFX_medal_icon_fascism"

		cost = @cost

		unit_modifiers = {
			army_org_factor = 0.05
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}

	}

	fascism_merit = {
		available = {
			has_government = fascism
			MD_should_have_any_unique_medals_trigger = no
		}
		frame = 2
		icon = "GFX_medal_icon_fascism"

		cost = @cost

		unit_modifiers = {
			army_attack_factor = 0.05
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	fascism_order_brave = {
		available = {
			has_government = fascism
			MD_should_have_any_unique_medals_trigger = no
		}
		frame = 3
		icon = "GFX_medal_icon_fascism"

		cost = @cost

		unit_modifiers = {
			armor_factor = 0.1
			recon_factor = 0.1
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	fascism_heroism = {
		available = {
			has_government = fascism
			MD_should_have_any_unique_medals_trigger = no
		}
		frame = 4
		icon = "GFX_medal_icon_fascism"

		cost = @cost

		unit_modifiers = {
			breakthrough_factor = 0.05
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	### Generic Neutrality

	neutral_bravery = {
		available = {
			has_government = neutrality
			MD_should_have_any_unique_medals_trigger = no
		}
		frame = 1
		icon = "GFX_medal_icon_default"

		cost = @cost

		unit_modifiers = {
			army_defence_factor = 0.05
			max_dig_in_factor = 0.1
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	neutral_military_order = {
		available = {
			has_government = neutrality
			MD_should_have_any_unique_medals_trigger = no
		}
		frame = 2
		icon = "GFX_medal_icon_default"

		cost = @cost

		unit_modifiers = {
			supply_consumption_factor = -0.1
			army_morale_factor = 0.05
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	neutral_sword = {
		available = {
			has_government = neutrality
			MD_should_have_any_unique_medals_trigger = no
		}
		frame = 3
		icon = "GFX_medal_icon_default"

		cost = @cost

		unit_modifiers = {
			army_attack_factor = 0.05
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	neutral_virtue = {
		available = {
			has_government = neutrality
			MD_should_have_any_unique_medals_trigger = no
		}
		frame = 4
		icon = "GFX_medal_icon_default"

		cost = @cost

		unit_modifiers = {
			experience_loss_factor = -0.1
			army_morale_factor = 0.05
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	per_medal_1 = {
		available = {
			MD_should_have_iranian_medals_trigger = yes
		}
		frame = 1
		icon = "GFX_medal_icon_per_md"

		cost = @cost

		unit_modifiers = {
			army_morale_factor = 0.1
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	per_medal_2 = {
		available = {
			MD_should_have_iranian_medals_trigger = yes
		}
		frame = 2
		icon = "GFX_medal_icon_per_md"

		cost = @cost

		unit_modifiers = {
			army_defence_factor = 0.05
			max_dig_in_factor = 0.1
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	per_medal_3 = {
		available = {
			MD_should_have_iranian_medals_trigger = yes
		}
		frame = 3
		icon = "GFX_medal_icon_per_md"

		cost = @cost

		unit_modifiers = {
			army_attack_factor = 0.05
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	per_medal_4 = {
		available = {
			MD_should_have_iranian_medals_trigger = yes
		}
		frame = 4
		icon = "GFX_medal_icon_per_md"

		cost = @cost

		unit_modifiers = {
			recon_factor = 0.1
			experience_loss_factor = -0.05
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	per_medal_5 = {
		available = {
			MD_should_have_iranian_medals_trigger = yes
		}
		frame = 5
		icon = "GFX_medal_icon_per_md"

		cost = @cost

		unit_modifiers = {
			supply_consumption_factor = 0.05
			equipment_capture_factor = 0.05
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}

	per_medal_6 = {
		available = {
			MD_should_have_iranian_medals_trigger = yes
		}
		frame = 6
		icon = "GFX_medal_icon_per_md"

		cost = @cost

		unit_modifiers = {
			army_org_factor = 0.05
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}
	#afg
	AFG_test = {
		available = {
			MD_should_have_iranian_medals_trigger = yes
		}
		frame = 6
		icon = "GFX_medal_icon_per_md"

		cost = @cost

		unit_modifiers = {
			army_org_factor = 0.05
		}

		one_time_effect = {
			add_divisional_commander_xp = 100
		}
	}
}
#example_dynamic_modifier = {
#		icon = "GFX_idea_unknown" # optional, will show up in guis if icon is specified
#		enable = { always = yes } #optional, the modifier won't apply if not enabled
#		remove_trigger = { always = no } #optional, will remove the modifier if true
#
#		# list of modifiers
#		fuel_cost = 321
#		max_fuel = var_max_fuel # will be taken from a variable
#	}


african_union_reforms_modifier = {
	icon = "GFX_idea_African_union2"

	enable = { always = yes }

	return_on_investment_modifier = african_union_return_on_investment_modifier
	interest_rate_multiplier_modifier = african_union_interest_rate_multiplier_modifier
	productivity_growth_modifier = african_union_productivity_growth_modifier
	research_speed_factor = african_union_research_speed_factor
	production_speed_buildings_factor = african_union_production_speed_buildings_factor
	production_speed_dockyard_factor = african_union_production_speed_dockyard_factor
	production_speed_arms_factory_factor = african_union_production_speed_arms_factory_factor
	production_speed_fossil_powerplant_factor = african_union_production_speed_fossil_powerplant_factor
	oil_export_multiplier_modifier = african_union_oil_export_multiplier_modifier
	fossil_pp_fuel_consumption_modifier = african_union_fossil_pp_fuel_consumption_modifier
	resource_export_multiplier_modifier = african_union_resource_export_multiplier_modifier
	foreign_influence_defense_modifier = african_union_foreign_influence_defense_modifier
	foreign_influence_modifier = african_union_foreign_influence_modifier
	local_resources_factor = african_union_local_resources_factor
	tax_gain_multiplier_modifier = african_union_tax_gain_multiplier_modifier
	health_cost_multiplier_modifier = african_union_health_cost_multiplier_modifier
	education_cost_multiplier_modifier = african_union_education_cost_multiplier_modifier
	political_power_factor = african_union_political_power_factor
	migration_rate_value_factor = african_union_migration_rate_value_factor
	army_defence_factor = african_union_army_defence_factor
	army_attack_factor = african_union_army_attack_factor
	corruption_cost_factor = african_union_corruption_cost_factor
	stability_factor = african_union_stability_factor
	drift_defence_factor = african_union_drift_defence_factor
	democratic_drift = african_union_democratic_drift
	communism_drift = african_union_communism_drift
	fascism_drift = african_union_fascism_drift
	neutrality_drift = african_union_neutrality_drift
	nationalist_drift = african_union_nationalist_drift
	send_volunteers_tension = african_union_send_volunteers_tension
	send_volunteer_size = african_union_send_volunteer_size
	olv_production_speed_modifier = african_union_satellite_speed_modifier
	gnss_production_speed_modifier = african_union_satellite_speed_modifier
	comsat_production_speed_modifier = african_union_satellite_speed_modifier
	spysat_production_speed_modifier = african_union_satellite_speed_modifier

}

african_union_headquarters = {
	icon = "GFX_idea_AU_member"

	enable = { always = yes }

	state_productivity_growth_modifier = 0.25
}
#example_dynamic_modifier = {
#		icon = "GFX_idea_unknown" # optional, will show up in guis if icon is specified
#		enable = { always = yes } #optional, the modifier won't apply if not enabled
#		remove_trigger = { always = no } #optional, will remove the modifier if true
#
#		# list of modifiers
#		fuel_cost = 321
#		max_fuel = var_max_fuel # will be taken from a variable
#	}

us_economic_policies = {
	icon = "GFX_idea_new_deal"
	enable = {
		original_tag = USA
		NOT = {
			has_country_flag = collapsed_nation
		}
	}

	consumer_goods_factor = econ_consumer_goods
	industrial_capacity_factory = econ_factory_output
	industrial_capacity_dockyard = econ_dockyard_output
	production_factory_max_efficiency_factor = econ_production_efficiency_cap
	production_factory_start_efficiency_factor = econ_starting_factory_efficiency
	civ_facs_worker_requirement_modifier = econ_civ_factory_workforce
	mil_facs_worker_requirement_modifier = econ_mil_factory_workforce
	offices_worker_requirement_modifier = econ_office_workforce
	productivity_growth_modifier = econ_productivity
}

USA_dynamic_modifier_department_of_the_interior = {
	icon = "GFX_idea_USA_interiori"
	enable = {
		original_tag = USA
		NOT = {
			has_country_flag = collapsed_nation
		}
	}

	production_speed_buildings_factor = doti_construction_speed
	production_speed_industrial_complex_factor = doti_civ_construction_speed
	production_speed_offices_factor = doti_office_construction_speed
	production_speed_arms_factory_factor = doti_mil_construction_speed
	production_speed_dockyard_factor = doti_dockyard_construction_speed
	production_speed_nuclear_reactor_factor = doti_nuclear_reactor_construction_speed
	local_resources_factor = doti_resource_extraction
}

USA_dynamic_modifier_department_of_energy = {
	icon = "GFX_idea_USA_energyi"
	enable = {
		original_tag = USA
		NOT = {
			has_country_flag = collapsed_nation
		}
	}

	energy_use_multiplier = doe_energy_consumption
	pop_energy_use_multiplier = doe_population_energy_consumption
	renewable_energy_gain_multiplier = doe_renewable_energy_production
	energy_gain_multiplier = doe_energy_production
	fossil_pp_energy_generation_modifier = doe_fossil_fuel_energy_production
	nuclear_energy_generation_modifier = doe_nuclear_energy_production
}

USA_domestic_policies = {
	icon = "GFX_idea_oligarchs"
	enable = {
		original_tag = USA
		NOT = {
			has_country_flag = collapsed_nation
		}
	}

	political_power_factor = domestic_political_power_gain
	drift_defence_factor = domestic_drift_defense
	encryption_factor = domestic_encryption_factor
	decryption_factor = domestic_decryption_factor
	monthly_population = domestic_monthly_pop
	research_speed_factor = domestic_research_speed
	high_unemployment_threshold_modifier = domestic_unemployment_tolerance
	foreign_influence_modifier = domestic_offensive_foreign_influence
	foreign_influence_defense_modifier = domestic_foreign_influence_defense
	civilian_intel_factor = domestic_civ_intel
	democratic_drift = domestic_democratic_drift
	communism_drift = domestic_communist_drift
	neutrality_drift = domestic_neutrality_drift
	nationalist_drift = domestic_nationalist_drift
}

USA_dynamic_modifier_us_general_staff = {
	icon = "GFX_idea_department_of_defense"
	enable = {
		original_tag = USA
		NOT = {
			has_country_flag = collapsed_nation
		}
	}

	experience_gain_factor = genstaff_leader_xp_gain
	max_planning_factor = genstaff_max_planning_factor
	supply_consumption_factor = genstaff_supply_consumption_factor
	conscription_factor = genstaff_recruitable_population
	conscription = genstaff_conscription
	experience_gain_army = genstaff_daily_army_xp
	experience_gain_air = genstaff_daily_air_xp
	experience_gain_navy = genstaff_daily_navy_xp
}

USA_dynamic_modifier_us_army = {
	icon = "GFX_idea_USA_armyi"
	enable = {
		original_tag = USA
		NOT = {
			has_country_flag = collapsed_nation
		}
	}

	recon_factor = us_army_recon_factor
	army_org_regain = us_army_army_org_regain
	army_org_factor = us_army_army_org
	army_attack_factor = us_army_army_attack
	army_defence_factor = us_army_army_defence
	breakthrough_factor = us_army_breakthrough_factor
	training_time_army_factor = us_army_army_training_time
	experience_gain_army_unit_factor = us_army_army_unit_xp_gain
	coordination_bonus = us_army_coordination
}

USA_dynamic_modifier_us_navy = {
	icon = "GFX_idea_USA_navyi"
	enable = {
		original_tag = USA
		NOT = {
			has_country_flag = collapsed_nation
		}
	}

	repair_speed_factor = us_navy_repair_speed_factor
	naval_morale_factor = us_navy_naval_morale_factor
	naval_speed_factor = us_navy_naval_speed
	naval_damage_factor = us_navy_naval_damage
	naval_defense_factor = us_navy_naval_defense
}

USA_dynamic_modifier_us_airforce = {
	icon = "GFX_idea_USA_airi"
	enable = {
		original_tag = USA
		NOT = {
			has_country_flag = collapsed_nation
		}
	}

	air_mission_efficiency = us_airforce_air_mission_efficiency
	air_attack_factor = us_airforce_air_attack
	air_defence_factor = us_airforce_air_defence
	air_agility_factor = us_airforce_air_agility
}

USA_federal_budgets = {
	icon = "GFX_idea_USA_capitol_building"
	enable = {
		original_tag = USA
		NOT = {
			has_country_flag = collapsed_nation
		}
	}

	bureaucracy_cost_multiplier_modifier = federal_budget_bureaucracy_cost
	personnel_cost_multiplier_modifier = federal_budget_personnel_cost
	police_cost_multiplier_modifier = federal_budget_police_cost
	education_cost_multiplier_modifier = federal_budget_education_cost
	health_cost_multiplier_modifier = federal_budget_healthcare_cost
	social_cost_multiplier_modifier = federal_budget_welfare_cost
	min_export = federal_budget_market_resources
	return_on_investment_modifier = federal_budget_investment_return
	resource_export_multiplier_modifier = federal_budget_export_money_return
}

USA_american_tax_code = {
	icon = "GFX_idea_irs_logo"
	enable = {
		original_tag = USA
		NOT = {
			has_country_flag = collapsed_nation
		}
	}

	interest_rate_multiplier_modifier = tax_code_interest_rate
	tax_gain_multiplier_modifier = tax_code_tax_collection_efficiency
	population_tax_income_multiplier_modifier = tax_code_population_tax_collection_efficiency
	corporate_tax_income_multiplier_modifier = tax_code_corporate_tax_collection_efficiency
	office_park_income_tax_modifier = tax_code_office_park_tax_collection_efficiency
	dockyard_income_tax_modifier = tax_code_dockyard_tax_collection_efficiency
	military_industry_tax_modifier = tax_code_military_tax_collection_efficiency
	civilian_industry_tax_modifier = tax_code_civilian_tax_collection_efficiency
}

USA_f35_supply_chain = {
	icon = GFX_idea_factory_strikes
	enable = {
		original_tag = USA
		NOT = {
			has_country_flag = collapsed_nation
		}
	}

	production_factory_max_efficiency_factor = f35_production_debuff
}
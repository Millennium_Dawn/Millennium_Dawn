#example_dynamic_modifier = {
#		icon = "GFX_idea_unknown" # optional, will show up in guis if icon is specified
#		enable = { always = yes } #optional, the modifier won't apply if not enabled
#		remove_trigger = { always = no } #optional, will remove the modifier if true
#
#		# list of modifiers
#		fuel_cost = 321
#		max_fuel = var_max_fuel # will be taken from a variable
#	}

###################
# Post United Era #
###################

southern_republic_divided_we_fell = {
	icon = "GFX_idea_cracked_usa"
	enable = {
		original_tag = CSA
	}

	political_power_factor = southern_republic_political_power_factor 
	drift_defence_factor = southern_republic_drift_defence_factor 
	monthly_population = southern_republic_monthly_population 
	conscription_factor = southern_republic_recruitable_population 
	foreign_influence_modifier = southern_republic_foreign_influence_offense 
	foreign_influence_defense_modifier = southern_republic_foriegn_influence_defense 
}

southern_republic_domestic_policies = {
	icon = "GFX_idea_political_reform"
	enable = {
		original_tag = CSA
	}

	political_power_factor = SRA_domestic_political_power_factor 
	drift_defence_factor = SRA_domestic_drift_defence_factor 
	monthly_population = SRA_domestic_monthly_pop
	research_speed_factor = SRA_domestic_research_speed
	high_unemployment_threshold_modifier = SRA_domestic_unemployment_tolerance
	foreign_influence_modifier = SRA_domestic_offensive_foreign_influence
	foreign_influence_defense_modifier = SRA_domestic_foreign_influence_defense
	democratic_drift = SRA_domestic_democratic_drift
	communism_drift = SRA_domestic_communist_drift
	neutrality_drift = SRA_domestic_neutrality_drift
	nationalist_drift = SRA_domestic_nationalist_drift
}

southern_republic_federal_budgets = {
	icon = "GFX_idea_USA_capitol_building"
	enable = {
		original_tag = CSA
	}

	bureaucracy_cost_multiplier_modifier = SRA_budget_bureaucracy_cost
	personnel_cost_multiplier_modifier = SRA_budget_personnel_cost
	police_cost_multiplier_modifier = SRA_budget_police_cost
	education_cost_multiplier_modifier = SRA_budget_education_cost
	health_cost_multiplier_modifier = SRA_budget_healthcare_cost
	social_cost_multiplier_modifier = SRA_budget_welfare_cost 
}

southern_republic_industrial_policies = {
	icon = "GFX_idea_resource_production"
	enable = {
		original_tag = CSA
	}

	consumer_goods_factor = SRA_industrial_consumer_goods
	industrial_capacity_factory = SRA_industrial_factory_output
	industrial_capacity_dockyard = SRA_industrial_dockyard_output
	production_factory_max_efficiency_factor = SRA_industrial_production_efficiency_cap
	production_speed_buildings_factor = SRA_industrial_construction_speed
	energy_use_multiplier = SRA_industrial_energy_consumption
	energy_gain_multiplier = SRA_industrial_energy_production
}

southern_republic_resource_management_policies = {
	icon = "GFX_idea_worker_reforms"
	enable = {
		original_tag = CSA
	}

	civ_facs_worker_requirement_modifier = SRA_resource_management_civ_factory_workforce
	mil_facs_worker_requirement_modifier = SRA_resource_management_mil_factory_workforce
	offices_worker_requirement_modifier = SRA_resource_management_office_workforce
	resource_sector_workers_modifier = SRA_resource_management_resource_extraction_workforce
	agriculture_workers_modifier = SRA_resource_management_agriculture_workforce
	productivity_growth_modifier = SRA_resource_management_productivity
	local_resources_factor = SRA_resource_management_resource_extraction
}

southern_republic_tax_policies = {
	icon = "GFX_idea_consumer_goods"
	enable = {
		original_tag = CSA
	}

	interest_rate_multiplier_modifier = SRA_tax_interest_rate
	population_tax_income_multiplier_modifier = SRA_tax_population_tax_collection_efficiency
	corporate_tax_income_multiplier_modifier = SRA_tax_corporate_tax_collection_efficiency
	office_park_income_tax_modifier = SRA_tax_office_park_tax_collection_efficiency
	dockyard_income_tax_modifier = SRA_tax_dockyard_tax_collection_efficiency
	military_industry_tax_modifier = SRA_tax_military_tax_collection_efficiency
	civilian_industry_tax_modifier = SRA_tax_civilian_tax_collection_efficiency
}

southern_republic_army_service_branch = {
	icon = "GFX_idea_generic_smallarms"
	enable = {
		original_tag = CSA
	}

	max_planning_factor = SRA_army_max_planning_factor
	supply_consumption_factor = SRA_army_supply_consumption_factor
	conscription = SRA_army_conscription
	breakthrough_factor = SRA_army_breakthrough_factor
	army_org_factor = SRA_army_army_org
	army_attack_factor = SRA_army_army_attack
	army_defence_factor = SRA_army_army_defence
	training_time_army_factor = SRA_army_army_training_time
}

southern_republic_air_force_service_branch = {
	icon = "GFX_idea_Generic_Aircraft_Company_Green"
	enable = {
		original_tag = CSA
	}

	air_mission_efficiency = SRA_airforce_air_mission_efficiency
	air_attack_factor = SRA_airforce_air_attack
	air_defence_factor = SRA_airforce_air_defence
	air_agility_factor = SRA_airforce_air_agility
	ground_attack_factor = SRA_airforce_ground_attack_factor
}

southern_republic_navy_service_branch = {
	icon = "GFX_idea_Generic_Ship_Company"
	enable = {
		original_tag = CSA
	}

	navy_anti_air_attack_factor = SRA_navy_anti_air_attack_factor
	repair_speed_factor = SRA_navy_repair_speed_factor
	naval_morale_factor = SRA_navy_naval_morale_factor
	naval_speed_factor = SRA_navy_naval_speed
	naval_damage_factor = SRA_navy_naval_damage
	naval_defense_factor = SRA_navy_naval_defense
}
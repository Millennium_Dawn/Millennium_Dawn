DEN_army_dynamic_modifier = { #NATIONAL SPIRIT
	enable = { always = yes }

	icon = GFX_idea_Demark_armed_forces_dynamic

	personnel_cost_multiplier_modifier = DEN_army_personnel_cost_multiplier_modifier
	army_org_factor = DEN_army_org_factor
	conscription_factor = DEN_army_conscription_factor
	army_morale = DEN_army_morale
	army_leader_start_level = DEN_army_army_leader_start_level
	army_leader_cost_factor = DEN_army_army_leader_cost_factor
	command_power_gain = DEN_army_command_power_gain
	max_planning = DEN_army_max_planning
	planning_speed = DEN_army_planning_speed
	supply_factor = DEN_army_supply_factor
	army_speed_factor = DEN_army_army_speed_factor
	winter_attrition_factor = DEN_army_winter_attrition_factor
	max_dig_in = DEN_army_max_dig_in
	dig_in_speed_factor = DEN_army_dig_in_speed_factor
	training_time_factor = DEN_training_time_factor
	no_supply_grace = DEN_no_supply_grace
	army_attack_factor = DEN_Army_attack
	army_defence_factor = DEN_Army_defence
	recon_factor = DEN_recon_factor
	breakthrough_factor = DEN_breakthrough_factor
	army_artillery_attack_factor = DEN_army_artillery_attack_factor
	experience_gain_factor = DEN_leader_xp_gain
	org_loss_when_moving = DEN_org_loss_when_moving
	experience_gain_army = DEN_experience_gain_army
	army_org_regain = DEN_army_org_regain
	terrain_penalty_reduction = DEN_terrain_penalty_reduction
	army_personnel_cost_multiplier_modifier = DEN_army_personnel_cost_multiplier_modifier

	#CIR
	foreign_influence_defense_modifier = DEN_foreign_influence_defense_modifier
	foreign_influence_modifier = DEN_foreign_influence_modifier

	#Luftwaffe
	air_attack_factor = DEN_air_attack
	air_defence_factor = DEN_air_defence
	air_agility_factor = DEN_air_agility
	air_mission_efficiency = DEN_air_mission_efficiency

	#Marine
	navy_personnel_cost_multiplier_modifier = DEN_navy_personnel_cost_multiplier_modifier
	positioning = DEN_positioning
	navy_max_range_factor = DEN_navy_max_range_factor
	navy_fuel_consumption_factor = DEN_navy_fuel_consumption_factor

	#BWB
	equipment_cost_multiplier_modifier = DEN_equipment_cost_multiplier_modifier
	military_factories_productivity = DEN_military_factories_productivity
	dockyard_productivity = DEN_dockyard_productivity
}

DEN_local_construction_initiatives_modifier = {
	enable = { always = yes }

	icon = GFX_idea_construction

	state_production_speed_buildings_factor = 0.15
}

DEN_local_construction_renewable_initiatives_modifier = {
	enable = { always = yes }

	icon = GFX_idea_construction

	state_production_speed_synthetic_refinery_factor = 0.15
}

DEN_greenlandic_separatism_modifier = {
	enable = { always = yes }
	icon = GFX_idea_low_popular_support3

	recruitable_population_factor = -0.15
	state_resources_factor = -0.25
	army_speed_factor_for_controller = -0.15
	state_production_speed_buildings_factor = -0.25
}

DEN_faroese_separatism_modifier = {
	enable = { always = yes }
	icon = GFX_idea_low_popular_support3

	recruitable_population_factor = -0.15
	state_resources_factor = -0.25
	army_speed_factor_for_controller = -0.15
	state_production_speed_buildings_factor = -0.25
}

DEN_colonial_holding_modifier = {
	enable = { always = yes }
	icon = GFX_idea_central_management

	remove_trigger = {
		NOT = { is_subject_of = DEN }
	}

	production_speed_buildings_factor = DEN_colonial_holding_modifier_1
	political_power_factor = DEN_colonial_holding_modifier_2
	consumer_goods_factor = DEN_colonial_holding_modifier_3
	local_resources_factor = DEN_colonial_holding_modifier_4
	conscription_factor = DEN_colonial_holding_modifier_5
	stability_factor = DEN_colonial_holding_modifier_6
	custom_modifier_tooltip = DEN_colonial_holding_tt
}

DEN_colonial_holding_master_modifier = {
	enable = { always = yes }
	icon = GFX_idea_central_management

	conscription_factor = DEN_colonial_holding_master_modifier_1
	stability_factor = DEN_colonial_holding_master_modifier_2
	custom_modifier_tooltip = DEN_colonial_holding_master_tt
}

DEN_colonial_investment_modifier = {
	enable = { always = yes }
	icon = GFX_idea_central_management

	state_production_speed_buildings_factor = 0.15
	state_resources_factor = 0.15
}
#their is a hiden treasure in my Pants, you want to see it?
DEN_the_State = {
	enable = { always = yes }
	icon = GFX_idea_central_management

	bureaucracy_cost_multiplier_modifier = DEN_bureaucracy_cost_multiplier_modifier_var
	police_cost_multiplier_modifier = DEN_police_cost_multiplier_modifier_var
	education_cost_multiplier_modifier = DEN_education_cost_multiplier_modifier_var
	health_cost_multiplier_modifier = DEN_health_cost_multiplier_modifier_var
	social_cost_multiplier_modifier = DEN_social_cost_multiplier_modifier_var
	production_speed_offices_factor = DEN_offices_contruction_speed
	production_speed_arms_factory_factor = DEN_arms_factory_contruction_speed
	production_speed_industrial_complex_factor = DEN_civ_construction_speed
	pop_energy_use_multiplier = DEN_population_energy_consumption
	civ_facs_worker_requirement_modifier = DEN_civ_factory_workforce
	mil_facs_worker_requirement_modifier = DEN_mil_factory_workforce
	offices_worker_requirement_modifier = DEN_office_workforce
	stability_factor = DEN_stability_factor
	monthly_population = DEN_monthly_population

}

DEN_Industrial_modifier = {
	enable = { always = yes }
	icon = GFX_idea_resource_production

	production_speed_buildings_factor = DEN_production_speed_buildings_factor
	office_park_income_tax_modifier = DEN_office_park_income_tax_modifier
	receiving_investment_cost_modifier = DEN_receiving_investment_cost_modifier
	return_on_investment_modifier = DEN_return_on_investment_modifier
	local_resources_factor = DEN_local_resources_factor
	production_factory_efficiency_gain_factor = DEN_production_factory_efficiency_gain_factor
	research_speed_factor = DEN_research_speed_factor
	tax_gain_multiplier_modifier = DEN_tax_gain_multiplier_modifier
	state_renewable_energy_generation_modifier = 0.25
}

DEN_Culture_modifier = {
	enable = { always = yes }
	icon = GFX_idea_monarchy

	drift_defence_factor = DEN_Culture_drift_defence_factor
	conscription_factor = DEN_Culture_conscription_factor
	stability_factor = DEN_Culture_stability_factor
	war_support_factor = DEN_Culture_war_support_factor
	monthly_population = DEN_Culture_monthly_population
	global_building_slots_factor = DEN_Culture_global_building_slots_factor
	enemy_operative_detection_chance_factor = DEN_Culture_enemy_operative_detection_chance_factor
	resistance_damage_to_garrison = DEN_Culture_resistance_damage_to_garrison
	resistance_growth = DEN_Culture_resistance_growth
	army_leader_cost_factor = DEN_Culture_army_leader_cost_factor
	conversion_cost_civ_to_mil_factor = DEN_Culture_conversion_cost_civ_to_mil_factor
	army_core_defence_factor = DEN_Culture_army_core_defence_factor
	nationalist_drift = DEN_Culture_nationalist_drift
	justify_war_goal_time = DEN_Culture_justify_war_goal_time
	country_productivity_growth_modifier = DEN_Culture_country_productivity_growth_modifier
}
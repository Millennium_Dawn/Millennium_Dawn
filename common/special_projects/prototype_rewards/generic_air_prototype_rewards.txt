@base_reward_weight = 2 #Base Reward weight value
@reward_weight_factor_theme = 0.25 # Applied to factor - Used to reduce chances of repeating the same reward theme within the same project (ie. Failure 1 & Failure 2)

#Low Scientist XP gain
sp_air_generic_reward_scientist_xp_1 = {
	fire_only_once = yes

	threshold = {
		min = 0
		max = 100
	}

	weight = {
		base = @base_reward_weight
		modifier = {
			factor = @reward_weight_factor_theme
			has_project_flag = sp_air_generic_reward_scientist_xp_flag
		}
		modifier = {
			factor = 0
			var:scientist = {
				has_scientist_level = {
					level > 4
					specialization = specialization_air
				}
			}
		}
	}

	option = {
		token = sp_air_generic_reward_option_scientist_xp

		iteration_output = {
			scientist_effects = {
				add_scientist_xp = {
					experience = constant:sp_scientist_xp_gain.low
					specialization = specialization_air
				}
				FROM = { set_project_flag = sp_air_generic_reward_scientist_xp_flag }
			}
		}
	}
}

#Medium Scientist XP gain
sp_air_generic_reward_scientist_xp_2 = {
	fire_only_once = yes

	threshold = {
		min = 0
		max = 100
	}

	weight = {
		base = @base_reward_weight
		modifier = {
			factor = @reward_weight_factor_theme
			has_project_flag = sp_air_generic_reward_scientist_xp_flag
		}
		modifier = {
			factor = 0
			var:scientist = {
				has_scientist_level = {
					level > 4
					specialization = specialization_air
				}
			}
		}
	}

	option = {
		token = sp_air_generic_reward_option_scientist_xp

		iteration_output = {
			scientist_effects = {
				if = {
					limit = {
						has_scientist_level = {
							level < 5
							specialization = specialization_air
						}
					}
					add_scientist_xp = {
						experience = constant:sp_scientist_xp_gain.medium
						specialization = specialization_air
					}
				}
				FROM = { set_project_flag = sp_air_generic_reward_scientist_xp_flag }
			}
		}
	}
}

#High Scientist XP gain
sp_air_generic_reward_scientist_xp_3 = {
	fire_only_once = yes

	threshold = {
		min = 0
		max = 100
	}

	weight = {
		base = @base_reward_weight
		modifier = {
			factor = @reward_weight_factor_theme
			has_project_flag = sp_air_generic_reward_scientist_xp_flag
		}
		modifier = {
			factor = 0
			var:scientist = {
				has_scientist_level = {
					level > 4
					specialization = specialization_air
				}
			}
		}
	}

	option = {
		token = sp_air_generic_reward_option_scientist_xp

		iteration_output = {
			scientist_effects = {
				if = {
					limit = {
						has_scientist_level = {
							level < 5
							specialization = specialization_air
						}
					}
					add_scientist_xp = {
						experience = constant:sp_scientist_xp_gain.high
						specialization = specialization_air
					}
				}
				FROM = { set_project_flag = sp_air_generic_reward_scientist_xp_flag }
			}
			country_effects = {
				add_breakthrough_progress = {
					specialization = specialization_air
					value = sp_breakthrough_progress.medium
				}
			}
		}
	}
}

#Low Army XP
sp_air_generic_reward_army_xp_1 = {
	fire_only_once = yes

	threshold = {
		min = 50
		max = 100
	}

	weight = {
		base = @base_reward_weight
		modifier = {
			factor = @reward_weight_factor_theme
			has_project_flag = sp_air_generic_reward_army_xp_flag
		}
	}

	option = {
		token = sp_air_generic_reward_option_army_xp

		iteration_output = {
			country_effects = {
				air_experience = constant:sp_military_xp_gain.low
				FROM = { set_project_flag = sp_air_generic_reward_army_xp_flag }
			}
		}
	}
}

#medium Army XP
sp_air_generic_reward_army_xp_2 = {
	fire_only_once = yes

	threshold = {
		min = 50
		max = 100
	}

	weight = {
		base = @base_reward_weight
		modifier = {
			factor = @reward_weight_factor_theme
			has_project_flag = sp_air_generic_reward_army_xp_flag
		}
	}

	option = {
		token = sp_air_generic_reward_option_army_xp

		iteration_output = {
			country_effects = {
				air_experience = constant:sp_military_xp_gain.medium
				FROM = { set_project_flag = sp_air_generic_reward_army_xp_flag }
			}
		}
	}
}

#High Army XP
sp_air_generic_reward_army_xp_3 = {
	fire_only_once = yes

	threshold = {
		min = 50
		max = 100
	}

	weight = {
		base = @base_reward_weight
		modifier = {
			factor = @reward_weight_factor_theme
			has_project_flag = sp_air_generic_reward_army_xp_flag
		}
	}

	option = {
		token = sp_air_generic_reward_option_army_xp

		iteration_output = {
			country_effects = {
				air_experience = constant:sp_military_xp_gain.high
				add_breakthrough_progress = {
					specialization = specialization_air
					value = sp_breakthrough_progress.medium
				}
				FROM = { set_project_flag = sp_air_generic_reward_army_xp_flag }
			}
		}
	}
}

#Low Extra Progress
sp_air_generic_reward_major_progress_1 = {
	fire_only_once = yes

	threshold = {
		min = 0
		max = 100
	}

	weight = {
		base = @base_reward_weight
		modifier = {
			factor = @reward_weight_factor_theme
			has_project_flag = sp_air_generic_reward_major_progress_flag
		}
	}

	option = {
		token = sp_air_generic_reward_option_major_breakthrough

		iteration_output = {
			country_effects = {
				FROM = {
					add_project_progress_ratio = constant:sp_progress.gain.low
					set_project_flag = sp_air_generic_reward_major_progress_flag
				}
			}
			scientist_effects = {
				if = {
					limit = {
						has_scientist_level = {
							level < 5
							specialization = specialization_air
						}
					}
					add_scientist_xp = {
						experience = constant:sp_scientist_xp_gain.low
						specialization = specialization_air
					}
				}
			}
		}
	}
}

#Medium Extra Progress
sp_air_generic_reward_major_progress_2 = {
	fire_only_once = yes

	threshold = {
		min = 0
		max = 100
	}

	weight = {
		base = @base_reward_weight
		modifier = {
			factor = @reward_weight_factor_theme
			has_project_flag = sp_air_generic_reward_major_progress_flag
		}
	}

	option = {
		token = sp_air_generic_reward_option_major_breakthrough

		iteration_output = {
			country_effects = {
				FROM = {
					add_project_progress_ratio = constant:sp_progress.gain.medium
					set_project_flag = sp_air_generic_reward_major_progress_flag
				}
			}
			scientist_effects = {
				if = {
					limit = {
						has_scientist_level = {
							level < 5
							specialization = specialization_air
						}
					}
					add_scientist_xp = {
						experience = constant:sp_scientist_xp_gain.low
						specialization = specialization_air
					}
				}
			}
		}
	}
}

#High Extra Progress
sp_air_generic_reward_major_progress_3 = {
	fire_only_once = yes

	threshold = {
		min = 0
		max = 100
	}

	weight = {
		base = @base_reward_weight
		modifier = {
			factor = @reward_weight_factor_theme
			has_project_flag = sp_air_generic_reward_major_progress_flag
		}
	}

	option = {
		token = sp_air_generic_reward_option_major_breakthrough

		iteration_output = {
			country_effects = {
				FROM = {
					add_project_progress_ratio = constant:sp_progress.gain.high
					set_project_flag = sp_air_generic_reward_major_progress_flag
				}

				add_breakthrough_progress = {
					specialization = specialization_air
					value = sp_breakthrough_progress.medium
				}
			}

			scientist_effects = {
				if = {
					limit = {
						has_scientist_level = {
							level < 5
							specialization = specialization_air
						}
					}
					add_scientist_xp = {
						experience = constant:sp_scientist_xp_gain.low
						specialization = specialization_air
					}
				}
			}
		}
	}
}

#Low Progress Loss
sp_air_generic_reward_test_failure_1 = {
	fire_only_once = yes

	threshold = {
		min = 15 #Loss is 5
		max = 100
	}

	weight = {
		base = @base_reward_weight
		modifier = {
			factor = @reward_weight_factor_theme
			has_project_flag = sp_air_generic_reward_test_failure_flag
		}
	}

	option = {
		token = sp_air_generic_reward_option_test_failure

		iteration_output = {
			country_effects = {
				FROM = {
					add_project_progress_ratio = constant:sp_progress.loss.low

					set_project_flag = sp_air_generic_reward_test_failure_flag
				}
			}
			scientist_effects = {
				if = {
					limit = {
						has_scientist_level = {
							level < 5
							specialization = specialization_air
						}
					}
					add_scientist_xp = {
						experience = constant:sp_scientist_xp_gain.low
						specialization = specialization_air
					}
				}
			}
		}
	}
}

#Medium Progress Loss
sp_air_generic_reward_test_failure_2 = {
	fire_only_once = yes

	threshold = {
		min = 30 #Loss is 10
		max = 100
	}

	weight = {
		base = @base_reward_weight
		modifier = {
			factor = @reward_weight_factor_theme
			has_project_flag = sp_air_generic_reward_test_failure_flag
		}
	}

	option = {
		token = sp_air_generic_reward_option_test_failure

		iteration_output = {
			country_effects = {
				FROM = {
					add_project_progress_ratio = constant:sp_progress.loss.medium

					set_project_flag = sp_air_generic_reward_test_failure_flag
				}
			}
			scientist_effects = {
				if = {
					limit = {
						has_scientist_level = {
							level < 5
							specialization = specialization_air
						}
					}
					add_scientist_xp = {
						experience = constant:sp_scientist_xp_gain.low
						specialization = specialization_air
					}
				}
			}
		}
	}
}

#High Progress Loss
sp_air_generic_reward_test_failure_3 = {
	fire_only_once = yes

	threshold = {
		min = 50 #Loss is 15
		max = 100
	}

	weight = {
		base = @base_reward_weight
		modifier = {
			factor = @reward_weight_factor_theme
			has_project_flag = sp_air_generic_reward_test_failure_flag
		}
	}

	option = {
		token = sp_air_generic_reward_option_test_failure

		iteration_output = {
			country_effects = {
				FROM = {
					add_project_progress_ratio = constant:sp_progress.loss.high

					set_project_flag = sp_air_generic_reward_test_failure_flag
				}

				add_breakthrough_progress = {
					specialization = specialization_air
					value = sp_breakthrough_progress.medium
				}
			}
			scientist_effects = {
				if = {
					limit = {
						has_scientist_level = {
							level < 5
							specialization = specialization_air
						}
					}
					add_scientist_xp = {
						experience = constant:sp_scientist_xp_gain.low
						specialization = specialization_air
					}
				}
			}
		}
	}
}

sp_air_generic_reward_resource_scarcity = {
	fire_only_once = yes

	threshold = {
		min = 25
		max = 80
	}

	weight = {
		base = 0.5
	}

	option = {
		token = sp_air_generic_reward_option_resource_scarcity

		iteration_output = {
			country_effects = {
				FROM = { add_project_progress_ratio = constant:sp_progress.loss.low }
			}

			scientist_effects = {
				add_scientist_xp = {
					experience = constant:sp_scientist_xp_gain.low
					specialization = specialization_air
				}
			}
		}
	}
}

sp_air_generic_reward_critical_failure = {
	fire_only_once = yes

	threshold = {
		min = 25
		max = 80
	}

	weight = {
		base = 0.25
	}

	option = {
		token = sp_air_generic_reward_option_critical_failure

		iteration_output = {
			country_effects = {
				FROM = { add_project_progress_ratio = constant:sp_progress.loss.medium }
			}
			scientist_effects = {
				add_scientist_xp = {
					experience = constant:sp_scientist_xp_gain.low
					specialization = specialization_air
				}
				injure_scientist_for_days = 30
			}
			facility_state_effects = {
				damage_building = {
					tags = facility
					damage = 0.25
					province = var:facility_province_id
				}
			}
		}
	}
}
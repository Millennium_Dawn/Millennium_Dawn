@base_reward_weight = 2 #Base Reward weight value
@reward_weight_factor_theme = 0.25 # Applied to factor - Used to reduce chances of repeating the same reward theme within the same project (ie. Failure 1 & Failure 2)

#Unique rewards for when Libya does its GMMR special project
#Mainly to impact tribal relations

#Increase Tripolitania opinion
sp_libya_reward_improve_tripolitania_opinion = {
	fire_only_once = yes

	threshold = {
		min = 0
		max = 100
	}

	weight = {
		base = @base_reward_weight
		modifier = {
			factor = @reward_weight_factor_theme
			has_project_flag = sp_libya_reward_improve_tripolitania_opinion_flag
		}
		modifier = {
			factor = 0
			FROM = {
				check_variable = { ctripolitania_loyalty > 0.9 }
			}
		}
		modifier = {
			factor = 0
			FROM = {
				has_country_flag = LBA_tribalism_disabled
			}
		}
		modifier = {
			factor = 0
			has_project_flag = sp_libya_reward_reduce_tripolitania_opinion_flag
		}
	}

	option = {
		token = sp_libya_reward_improve_tripolitania_opinion_option

		iteration_output = {
			country_effects = {
				set_temp_variable = { loyalty_change = 0.10 }
				libya_change_tripolitania_loyalty = yes
				FROM = { set_project_flag = sp_libya_reward_improve_tripolitania_opinion_flag }
			}
		}
	}
}
#Decrease Tripolitania opinion
sp_libya_reward_reduce_tripolitania_opinion = {
	fire_only_once = yes

	threshold = {
		min = 0
		max = 100
	}

	weight = {
		base = @base_reward_weight
		modifier = {
			factor = @reward_weight_factor_theme
			has_project_flag = sp_libya_reward_reduce_tripolitania_opinion_flag
		}
		modifier = {
			factor = 0
			FROM = {
				check_variable = { tripolitania_loyalty < 0.1 }
			}
		}
		modifier = {
			factor = 0
			FROM = {
				has_country_flag = LBA_tribalism_disabled
			}
		}
		modifier = {
			factor = 0
			has_project_flag = sp_libya_reward_improve_tripolitania_opinion_flag
		}
	}

	option = {
		token = sp_libya_reward_reduce_tripolitania_opinion_option

		iteration_output = {
			country_effects = {
				set_temp_variable = { loyalty_change = -0.10 }
				libya_change_tripolitania_loyalty = yes
				FROM = { set_project_flag = sp_libya_reward_reduce_tripolitania_opinion_flag }
			}
		}
	}
}

#Increase Cyrenaica opinion
sp_libya_reward_improve_cyrenaica_opinion = {
	fire_only_once = yes

	threshold = {
		min = 0
		max = 100
	}

	weight = {
		base = @base_reward_weight
		modifier = {
			factor = @reward_weight_factor_theme
			has_project_flag = sp_libya_reward_improve_cyrenaica_opinion_flag
		}
		modifier = {
			factor = 0
			FROM = {
				check_variable = { cyrenaica_loyalty > 0.9 }
			}
		}
		modifier = {
			factor = 0
			FROM = {
				has_country_flag = LBA_tribalism_disabled
			}
		}
		modifier = {
			factor = 0
			has_project_flag = sp_libya_reward_reduce_cyrenaica_opinion_flag
		}
	}

	option = {
		token = sp_libya_reward_improve_cyrenaica_opinion_option

		iteration_output = {
			country_effects = {
				set_temp_variable = { loyalty_change = 0.10 }
				libya_change_cyrenaica_loyalty = yes
				FROM = { set_project_flag = sp_libya_reward_improve_cyrenaica_opinion_flag }
			}
		}
	}
}
#Decrease Cyrenaica opinion
sp_libya_reward_reduce_cyrenaica_opinion = {
	fire_only_once = yes

	threshold = {
		min = 0
		max = 100
	}

	weight = {
		base = @base_reward_weight
		modifier = {
			factor = @reward_weight_factor_theme
			has_project_flag = sp_libya_reward_reduce_cyrenaica_opinion_flag
		}
		modifier = {
			factor = 0
			FROM = {
				check_variable = { cyrenaica_loyalty < 0.1 }
			}
		}
		modifier = {
			factor = 0
			FROM = {
				has_country_flag = LBA_tribalism_disabled
			}
		}
		modifier = {
			factor = 0
			has_project_flag = sp_libya_reward_improve_cyrenaica_opinion_flag
		}
	}

	option = {
		token = sp_libya_reward_reduce_cyrenaica_opinion_option

		iteration_output = {
			country_effects = {
				set_temp_variable = { loyalty_change = -0.10 }
				libya_change_cyrenaica_loyalty = yes
				FROM = { set_project_flag = sp_libya_reward_reduce_cyrenaica_opinion_flag }
			}
		}
	}
}

#Increase Fezzan opinion
sp_libya_reward_improve_fezzan_opinion = {
	fire_only_once = yes

	threshold = {
		min = 0
		max = 100
	}

	weight = {
		base = @base_reward_weight
		modifier = {
			factor = @reward_weight_factor_theme
			has_project_flag = sp_libya_reward_improve_fezzan_opinion_flag
		}
		modifier = {
			factor = 0
			FROM = {
				check_variable = { fezzan_loyalty > 0.9 }
			}
		}
		modifier = {
			factor = 0
			FROM = {
				has_country_flag = LBA_tribalism_disabled
			}
		}
		modifier = {
			factor = 0
			has_project_flag = sp_libya_reward_reduce_fezzan_opinion_flag
		}
	}

	option = {
		token = sp_libya_reward_improve_fezzan_opinion_option

		iteration_output = {
			country_effects = {
				set_temp_variable = { loyalty_change = 0.10 }
				libya_change_fezzan_loyalty = yes
				FROM = { set_project_flag = sp_libya_reward_improve_fezzan_opinion_flag }
			}
		}
	}
}
#Decrease Fezzan opinion
sp_libya_reward_reduce_fezzan_opinion = {
	fire_only_once = yes

	threshold = {
		min = 0
		max = 100
	}

	weight = {
		base = @base_reward_weight
		modifier = {
			factor = @reward_weight_factor_theme
			has_project_flag = sp_libya_reward_reduce_fezzan_opinion_flag
		}
		modifier = {
			factor = 0
			FROM = {
				check_variable = { fezzan_loyalty < 0.1 }
			}
		}
		modifier = {
			factor = 0
			FROM = {
				has_country_flag = LBA_tribalism_disabled
			}
		}
		modifier = {
			factor = 0
			has_project_flag = sp_libya_reward_improve_fezzan_opinion_flag
		}
	}

	option = {
		token = sp_libya_reward_reduce_fezzan_opinion_option

		iteration_output = {
			country_effects = {
				set_temp_variable = { loyalty_change = -0.10 }
				libya_change_fezzan_loyalty = yes
				FROM = { set_project_flag = sp_libya_reward_reduce_fezzan_opinion_flag }
			}
		}
	}
}
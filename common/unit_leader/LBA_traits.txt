
leader_traits = {

	LBA_unit_leader_trait_tripolitania = {
		type = all
		trait_type = personality_trait
		gain_xp = {
			always = no # not gainable
		}
		allowed = {
			FROM = {
				tag = LBA
			}
		}
		modifier = {

		}

		ai_will_do = {
			factor = 1
		}
		new_commander_weight = {
			factor = 0
		}
	}
	LBA_unit_leader_trait_cyrenaica = {
		type = all
		trait_type = personality_trait
		gain_xp = {
			always = no # not gainable
		}
		allowed = {
			FROM = {
				tag = LBA
			}
		}
		modifier = {

		}

		ai_will_do = {
			factor = 1
		}
		new_commander_weight = {
			factor = 0
		}
	}
	LBA_unit_leader_trait_fezzan = {
		type = all
		trait_type = personality_trait
		gain_xp = {
			always = no # not gainable
		}
		allowed = {
			FROM = {
				tag = LBA
			}
		}
		modifier = {

		}

		ai_will_do = {
			factor = 1
		}
		new_commander_weight = {
			factor = 0
		}
	}
	LBA_unit_leader_trait_tuareg = {
		type = all
		trait_type = personality_trait
		gain_xp = {
			always = no # not gainable
		}
		allowed = {
			FROM = {
				tag = LBA
			}
		}
		modifier = {

		}

		ai_will_do = {
			factor = 1
		}
		new_commander_weight = {
			factor = 0
		}
	}
	LBA_unit_leader_trait_toubou = {
		type = all
		trait_type = personality_trait
		gain_xp = {
			always = no # not gainable
		}
		allowed = {
			FROM = {
				tag = LBA
			}
		}
		modifier = {

		}

		ai_will_do = {
			factor = 1
		}
		new_commander_weight = {
			factor = 0
		}
	}
}
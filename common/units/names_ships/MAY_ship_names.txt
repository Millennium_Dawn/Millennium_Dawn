﻿##### Malaysia NAME LISTS #####

### REGULAR DESTROYER NAMES###
MAY_DD_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_DESTROYERS

	for_countries = { MAY }

	type = ship
	ship_types = { stealth_destroyer destroyer }

	prefix = "KD "
	fallback_name = "DDG-%d"

	unique = {
		"Harimau Malaya" "Jelajah Samudra" "Rajawali Laut" "Satria Perkasa" "Bintang Timur" "Langkasuka" "Maharaja Lela" "Pahlawan Sakti" "Perwira Negara" "Garuda Emas" "Sang Nila Utama" "Hang Tuah" "Taming Sari" "Seri Paduka" "Laksamana Sunan" "Gunung Ledang" "Naga Tasik" "Bayu Sentosa" "Keris Berapi" "Wira Ombak"
	}
}

MAY_FRIGATES_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_FRIGATES
	for_countries = { MAY }
	type = ship
	ship_types = {
		frigate
	}
	prefix = "KD "
	fallback_name = "FFG-%d"

	unique = {
		"Cakra Dunia" "Jalur Gemilang" "Sinar Bahari" "Gelombang Mutiara" "Angkasa Jaya" "Dewa Langit" "Laut Kebangsaan" "Panji-Panji" "Rimba Laut" "Majapahit" "Pendekar Laut" "Saga Pusaka" "Teratai Biru" "Gagah Perkasa" "Rimau Selatan" "Bukit Tahan" "Segara Indah" "Mutiara Timur" "Baruna Raya" "Seri Pahlawan" "Kinabalu" "Perisai Diraja" "Badang Kekuatan" "Merak Sakti" "Legenda Malim" "Galang Kemajuan" "Wira Kencana" "Satria Awan" "Harapan Baru" "Sang Suria"
	}
}

### CORVETTE NAMES ###
MAY_CORVETTE_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_CORVETTE

	for_countries = { MAY }
	prefix = "KD "
	type = ship
	ship_types = { corvette }
	fallback_name = "KKG-%d"

	unique = {
		"Dayang Senandung" "Pelangi Samudera" "Bintang Selatan" "Jentayu Sakti" "Kasturi Maju" "Laksar Kedamaian" "Puteri Gunung" "Seroja Lestari" "Maharaja Sari" "Langit Biru" "Sakti Alam" "Garis Pantai" "Nakhoda Ragam" "Samudera Kencana" "Perdana Wira" "Layar Emas" "Anugerah Laut" "Bumi Kenyalang" "Cendana Mutiara" "Duta Selam" "Empayar Laut" "Gelora Bakti" "Hikmah Ombak" "Indah Samudra" "Juara Sebrang" "Kebanggaan Bangsa" "Laut Merdeka" "Mega Jaya" "Nusantara Berjaya" "Ombak Rindu" "Pantai Impian" "Qilin Laut" "Rajawali Sakti" "Senja Bahari" "Tiara Oceania" "Utama Jasa" "Vista Marina" "Wawasan Nusantara" "Xiphias Gladi" "Yudistira Laut"
	}
}

MAY_SUBMARINES_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_SUBMARINES
	for_countries = { MAY }
	prefix = "KD "
	type = ship
	ship_types = {
		attack_submarine missile_submarine
	}
	fallback_name = "SS-%d"
}
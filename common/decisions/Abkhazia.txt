ABK_political_decisions = {

	ABK_need_russian_money = {

		cost = 65
		days_remove = 300
		icon = GFX_decision_russian_money

		visible = {
			emerging_communist_state_are_in_power = yes
		}

		available = {
			has_completed_focus = ABK_president_illness
		}

		remove_effect = {
			SOV = {
				set_temp_variable = { treasury_change = -3.5 }
				modify_treasury_effect = yes
				set_temp_variable = { int_investment_change = 3.5 }
				modify_international_investment_effect = yes
			}
			set_temp_variable = { treasury_change = 3.5 }
			modify_treasury_effect = yes
		}
	}

	ABK_exersises_with_russia = {

		fire_only_once = yes
		cost = 50
		days_remove = 60
		icon = GFX_decision_abkhaz_button

		visible = {
			original_tag = ABK
			emerging_communist_state_are_in_power = yes
		}

		available = {
			has_completed_focus = ABK_training_in_russia
		}

		remove_effect = {
			army_experience = 15
			air_experience = 15
		}
	}

	ABK_fight_with_mafia = {

		fire_only_once = yes
		cost = 130
		days_remove = 280
		icon = GFX_decision_abkhaz_button

		visible = {
			original_tag = ABK
		}

		remove_effect = {
			remove_ideas = ABK_mafia_idea
			add_ideas = ABK_mafia2_idea
			}
		}

	ABK_fight_with_mafia_again = {

		fire_only_once = yes
		cost = 100
		days_remove = 256
		icon = GFX_decision_abkhaz_button

		visible = {
			original_tag = ABK
			has_idea = ABK_mafia2_idea
		}

		remove_effect = {
			remove_ideas = ABK_mafia2_idea
			add_ideas = ABK_mafia3_idea
			}
		}


	ABK_fight_with_mafia_again2 = {

		fire_only_once = yes
		cost = 100
		days_remove = 234
		icon = GFX_decision_abkhaz_button

		visible = {
			original_tag = ABK
			has_idea = ABK_mafia3_idea
		}

		remove_effect = {
			remove_ideas = ABK_mafia3_idea
			add_ideas = ABK_mafia4_idea

		}
	}

	ABK_fight_with_mafia_again3 = {

		fire_only_once = yes
		cost = 80
		days_remove = 210
		icon = GFX_decision_abkhaz_button

		visible = {
			original_tag = ABK
			has_idea = ABK_mafia4_idea
		}

		remove_effect = {
			remove_ideas = ABK_mafia4_idea
			}
		}

	ABK_fight_narco = {

		fire_only_once = yes
		cost = 120
		days_remove = 370
		icon = GFX_decision_abkhaz_button

		visible = {
			OR = {
				has_completed_focus = ABK_president_illness
				has_completed_focus = ABK_president_illness2
				has_completed_focus = ABK_president_illness3
			}
			original_tag = ABK
		}

		remove_effect = {
			remove_ideas = ABK_narcos_idea
			add_ideas = ABK_narcos2_idea
		}


		complete_effect = {
			log = "[GetDateText]: [This.GetName]: decision ABK_fight_narco executed"
			increase_healthcare_budget = yes
		}
	}

	ABK_fight_narco2 = {

		fire_only_once = yes
		cost = 100
		days_remove = 345
		icon = GFX_decision_abkhaz_button

		visible = {
			has_idea = ABK_narcos2_idea
			original_tag = ABK
		}

		remove_effect = {
			remove_ideas = ABK_narcos2_idea
		}
	}

	ABK_belarus_weapon = {

		cost = 70
		days_remove = 1
		icon = GFX_decision_decree8
		fire_only_once = yes

		visible = {
			original_tag = ABK
			country_exists = BLR
		}

		available = {
			BLR = {
				has_opinion = { target = ABK value > 60 }
			}
		}

		complete_effect = {
			log = "[GetDateText]: [This.GetName]: decision ABK_belarus_weapon executed"
			country_event = { id = belarus_export.2 days = 1 }
		}
	}

	ABK_pyatnashka = {

		cost = 55
		days_remove = 30
		icon = GFX_decision_abkhaz_button

		visible = { original_tag = ABK }

		available = {
			OR = {
				emerging_autocracy_are_in_power = yes
				is_subject_of = SOV
			}
			OR = {
				country_exists = NOV
				# country_exists = DPR
			}
		}

		remove_effect = {
			if = {
				limit = {
					country_exists = NOV
				}
				add_opinion_modifier = {
				target = NOV
				modifier = diplomatic_support
				}
				reverse_add_opinion_modifier = {
				target = NOV
				modifier = diplomatic_support
				}
				NOV = {
				country_event = { id = abkh.3 days = 1 } }
			}

			# if = {
			# 	limit = {
			# 		country_exists = DPR
			# 	}
			# 	add_opinion_modifier = {
			# 		target = DPR
			# 		modifier = diplomatic_support
			# 	}
			# 	reverse_add_opinion_modifier = {
			# 		target = DPR
			# 		modifier = diplomatic_support
			# 	}
			# 	DPR = {
			# 		country_event = { id = abkh.4 days = 1 }
			# 	}

			# }

		}

	}

}

ABK_georgia_nationalise = {

	ABK_adjara_nationalise = {

		fire_only_once = yes
		cost = 80
		days_remove = 365
		icon = GFX_decision_georgia

		visible = {
			original_tag = ABK
			owns_state = 466
		}

		available = {
			nationalist_monarchists_are_in_power = yes
		}

		remove_effect = {
			ABK = {
				add_state_core = 466
			}
		}
	}

	ABK_west_geo_nationalise = {

		fire_only_once = yes
		cost = 80
		days_remove = 370
		icon = GFX_decision_georgia

		visible = {
			original_tag = ABK
			owns_state = 707
		}

		available = {
			nationalist_monarchists_are_in_power = yes
		}

		remove_effect = {
			ABK = {
				add_state_core = 707
			}
		}
	}

	ABK_djava_nationalise = {

		fire_only_once = yes
		cost = 80
		days_remove = 360
		icon = GFX_decision_georgia

		visible = {
			original_tag = ABK
			owns_state = 1033
		}

		available = {
			nationalist_monarchists_are_in_power = yes
		}

		remove_effect = {
			ABK = {
				add_state_core = 1033
			}
		}
	}

	ABK_east_geo_nationalise = {

		fire_only_once = yes
		cost = 80
		days_remove = 360
		icon = GFX_decision_georgia

		visible = {
			original_tag = ABK
			owns_state = 708
		}

		available = {
			nationalist_monarchists_are_in_power = yes
		}

		remove_effect = {
			ABK = {
				add_state_core = 708
			}
		}
	}
}


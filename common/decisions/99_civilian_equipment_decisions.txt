civilian_equipment_purchase_decisions = {
	utility_trucks_purchase = {
		icon = GFX_decision_money_dec

		custom_cost_trigger = {
			check_variable = { treasury > 0.5 }
		}
		custom_cost_text = cost_0_5

		available = {
			custom_trigger_tooltip = {
				tooltip = does_not_have_equipment_purchased_flag
				NOT = {
					has_country_flag = equipment_purchased_flag
				}
			}
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision utility_trucks_purchase"
			set_temp_variable = { treasury_change = -0.5 }
			modify_treasury_effect = yes
		}

		days_remove = 25

		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision utility_trucks_purchase"
			ROOT = {
				set_country_flag = { flag = equipment_purchased_flag value = 1 days = 180 }
				add_equipment_to_stockpile = {
					type = util_vehicle_0
					amount = 120
					producer = C01
				}
			}
		}

		ai_will_do = {
			base = 10
			modifier = {
				factor = 0.50
				has_war = no
			}
			modifier = {
				factor = 0
				has_equipment = { util_vehicle_equipment < 750 }
			}
		}
	}
}
NOR_norge_mining_decision_cat = {
	NOR_mines_in_oslo = {
		days_remove = 75

		fire_only_once = yes

		available = {
			NOT = {
				has_country_flag = NOR_taking_decision
			}
			32 = {
				is_owned_and_controlled_by = NOR
			}
		}
		complete_effect = {
			set_country_flag = NOR_taking_decision
		}

		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision NOR_mines_in_oslo"
			clr_country_flag = NOR_taking_decision
			if = {
				limit = {
					has_tech = excavation1
				}
				add_resource = {
					type = steel
					amount = 8
					state = 32
				}
			}
			else_if = {
				limit = {
					has_tech = excavation2
				}
				add_resource = {
					type = steel
					amount = 12
					state = 32
				}
			}
			else_if = {
				limit = {
					has_tech = excavation3
				}
				add_resource = {
					type = steel
					amount = 16
					state = 32
				}
			}
			else_if = {
				limit = {
					has_tech = excavation6
				}
				add_resource = {
					type = steel
					amount = 20
					state = 32
				}
			}
			else_if = {
				limit = {
					has_tech = excavation7
				}
				add_resource = {
					type = steel
					amount = 25
					state = 32
				}
			}
			else_if = {
				limit = {
					has_tech = excavation8
				}
				add_resource = {
					type = steel
					amount = 32
					state = 32
				}
			}
			else_if = {
				limit = {
					has_tech = excavation9
				}
				add_resource = {
					type = steel
					amount = 37
					state = 32
				}
			}
		}

		ai_will_do = {
			factor = 180
		}
	}

	NOR_mines_in_vestlandet = {
		days_remove = 75

		fire_only_once = yes

		available = {
			NOT = {
				has_country_flag = NOR_taking_decision
			}
			30 = {
				is_owned_and_controlled_by = NOR
			}
		}
		complete_effect = {
			set_country_flag = NOR_taking_decision
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision NOR_mines_in_vestlandet"
			clr_country_flag = NOR_taking_decision
			if = {
				limit = {
					has_tech = excavation1
				}
				add_resource = {
					type = tungsten
					amount = 8
					state = 30
				}
			}
			else_if = {
				limit = {
					has_tech = excavation2
				}
				add_resource = {
					type = tungsten
					amount = 12
					state = 30
				}
			}
			else_if = {
				limit = {
					has_tech = excavation3
				}
				add_resource = {
					type = tungsten
					amount = 16
					state = 30
				}
			}
			else_if = {
				limit = {
					has_tech = excavation6
				}
				add_resource = {
					type = tungsten
					amount = 20
					state = 30
				}
			}
			else_if = {
				limit = {
					has_tech = excavation7
				}
				add_resource = {
					type = tungsten
					amount = 25
					state = 30
				}
			}
			else_if = {
				limit = {
					has_tech = excavation8
				}
				add_resource = {
					type = tungsten
					amount = 32
					state = 30
				}
			}
			else_if = {
				limit = {
					has_tech = excavation9
				}
				add_resource = {
					type = tungsten
					amount = 37
					state = 30
				}
			}
		}

		ai_will_do = {
			factor = 180
		}
	}

	NOR_mines_in_norde_norge = {
		days_remove = 75

		fire_only_once = yes

		available = {
			NOT = {
				has_country_flag = NOR_taking_decision
			}
			31 = {
				is_owned_and_controlled_by = NOR
			}
		}
		complete_effect = {
			set_country_flag = NOR_taking_decision
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision NOR_mines_in_vestlandet"
			clr_country_flag = NOR_taking_decision
			if = {
				limit = {
					has_tech = excavation1
				}
				add_resource = {
					type = aluminium
					amount = 8
					state = 31
				}
			}
			else_if = {
				limit = {
					has_tech = excavation2
				}
				add_resource = {
					type = aluminium
					amount = 12
					state = 31
				}
			}
			else_if = {
				limit = {
					has_tech = excavation3
				}
				add_resource = {
					type = aluminium
					amount = 16
					state = 31
				}
			}
			else_if = {
				limit = {
					has_tech = excavation6
				}
				add_resource = {
					type = aluminium
					amount = 20
					state = 31
				}
			}
			else_if = {
				limit = {
					has_tech = excavation7
				}
				add_resource = {
					type = aluminium
					amount = 25
					state = 31
				}
			}
			else_if = {
				limit = {
					has_tech = excavation8
				}
				add_resource = {
					type = aluminium
					amount = 32
					state = 31
				}
			}
			else_if = {
				limit = {
					has_tech = excavation9
				}
				add_resource = {
					type = aluminium
					amount = 37
					state = 31
				}
			}
		}

		ai_will_do = {
			factor = 180
		}
	}

	# Unlocks w/Deep Sea Mining Focus
	NOR_deep_sea_mining_in_trondelag = {
		days_remove = 75

		fire_only_once = yes

		available = {
			NOT = {
				has_country_flag = NOR_taking_decision
			}
			542 = {
				is_owned_and_controlled_by = NOR
			}
		}
		visible = {
			has_completed_focus = NOR_deep_sea_expansion
		}
		complete_effect = {
			set_country_flag = NOR_taking_decision
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision NOR_mines_in_vestlandet"
			clr_country_flag = NOR_taking_decision
			if = {
				limit = {
					has_tech = excavation1
				}
				add_resource = {
					type = oil
					amount = 8
					state = 542
				}
			}
			else_if = {
				limit = {
					has_tech = excavation2
				}
				add_resource = {
					type = oil
					amount = 12
					state = 542
				}
			}
			else_if = {
				limit = {
					has_tech = excavation3
				}
				add_resource = {
					type = oil
					amount = 16
					state = 542
				}
			}
			else_if = {
				limit = {
					has_tech = excavation6
				}
				add_resource = {
					type = oil
					amount = 20
					state = 542
				}
			}
			else_if = {
				limit = {
					has_tech = excavation7
				}
				add_resource = {
					type = oil
					amount = 25
					state = 542
				}
			}
			else_if = {
				limit = {
					has_tech = excavation8
				}
				add_resource = {
					type = oil
					amount = 32
					state = 542
				}
			}
			else_if = {
				limit = {
					has_tech = excavation9
				}
				add_resource = {
					type = oil
					amount = 37
					state = 542
				}
			}
		}

		ai_will_do = {
			factor = 180
		}
	}

	NOR_deep_sea_in_oslo = {
		days_remove = 75

		fire_only_once = yes

		available = {
			NOT = {
				has_country_flag = NOR_taking_decision
			}
			32 = {
				is_owned_and_controlled_by = NOR
			}
		}
		visible = {
			has_completed_focus = NOR_deep_sea_expansion
		}
		complete_effect = {
			set_country_flag = NOR_taking_decision
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision NOR_mines_in_oslo"
			clr_country_flag = NOR_taking_decision
			if = {
				limit = {
					has_tech = excavation1
				}
				add_resource = {
					type = chromium
					amount = 8
					state = 32
				}
			}
			else_if = {
				limit = {
					has_tech = excavation2
				}
				add_resource = {
					type = chromium
					amount = 12
					state = 32
				}
			}
			else_if = {
				limit = {
					has_tech = excavation3
				}
				add_resource = {
					type = chromium
					amount = 16
					state = 32
				}
			}
			else_if = {
				limit = {
					has_tech = excavation6
				}
				add_resource = {
					type = chromium
					amount = 20
					state = 32
				}
			}
			else_if = {
				limit = {
					has_tech = excavation7
				}
				add_resource = {
					type = chromium
					amount = 25
					state = 32
				}
			}
			else_if = {
				limit = {
					has_tech = excavation8
				}
				add_resource = {
					type = chromium
					amount = 32
					state = 32
				}
			}
			else_if = {
				limit = {
					has_tech = excavation9
				}
				add_resource = {
					type = chromium
					amount = 37
					state = 32
				}
			}
		}

		ai_will_do = {
			factor = 180
		}
	}
}
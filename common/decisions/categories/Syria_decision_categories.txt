Syria_decisions = {

	priority = 100

	allowed = {
		OR = {
			original_tag = SYR
			original_tag = NUS
			original_tag = FSA
			original_tag = ALA
			original_tag = ISI
			original_tag = DRU
			original_tag = ROJ
		}
	}

}

Syria_foreign_policy = {

	priority = 100

	allowed = {
		OR = {
			original_tag = SYR
			original_tag = FSA
			original_tag = NUS
		}
	}

}

Syria_external_countries = {

	priority = 100

	allowed = {
		always = yes
	}

}

Syria_hafez_succession = {

	priority = 100

	allowed = {
		original_tag = SYR
	}

	visible = {
		NOT = { has_country_flag = Syria_Hafez_dead }
		NOT = { has_start_date > 2016.1.1 }
	}

}

Syria_greater_syria_decisions = {

	priority = 100

	allowed = {
		OR = {
			original_tag = SYR
			original_tag = FSA
			original_tag = NUS
		}
	}

	visible = {
		has_completed_focus = SYR_greater_syria
		has_government = nationalist
	}
}

Syria_damascus_spring_organization = {

	priority = 100

	allowed = {
		original_tag = SYR
	}

	visible = {
		NOT = { has_country_flag = Syria_Hafez_dead }
	}
}

Syria_economic_investments = {
	icon = GFX_generic_construction_decision_category
	
	priority = 100

	allowed = {
		original_tag = SYR
	}

	visible = {
		has_completed_focus = SYR_economic_revitalization
	}
}

Syria_agricultural_subsidies_decisions = {

	priority = 100

	allowed = {
		original_tag = SYR
	}

	visible = {
		always = yes
	}
}

Syria_united_arab_republic_decisions = {

	priority = 100

	icon = political_actions

	allowed = {
		is_arabic_nation = yes
	}

	visible = {
		has_global_flag = arab_union_formed
		NOT = { has_country_flag = leader_of_arab_union }
	}

	available = {
		has_government = communism
		custom_trigger_tooltip = {
			tooltip = TT_SYR_BAATHIST_IN_POWER
			is_in_array = {
				array = ruling_party
				value = 7
			}
		}
		any_country = {
			has_country_flag = leader_of_arab_union
			NOT = { has_war_with = ROOT }
		}
		OR = {
			any_country = {
				has_country_flag = leader_of_arab_union
				ROOT = { is_subject_of = PREV }
			}
			is_subject = no
		}
	}
}
Syria_digital_investments_decisions = {

	priority = 100

	allowed = {
		has_completed_focus = SYR_digitalisation_campaign
	}

	visible = {
		has_completed_focus = SYR_digitalisation_campaign
	}
}
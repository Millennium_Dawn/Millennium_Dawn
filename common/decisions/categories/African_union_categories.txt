african_union_decisions_category = {

	allowed = {
		is_african_nation = yes
	}

	visible_when_empty = yes

	priority = 100

	visible = {
		NOT = { has_global_flag = African_Union_united }
		has_global_flag = African_Union_formed
	}

}
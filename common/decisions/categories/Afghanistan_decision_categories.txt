Afghanistan_pashtunistan_decisions = {

	allowed = {
		OR = {
			original_tag = AFG
			original_tag = TAL
		}
	}

}
Afghanistan_domestic_decisions = {
	icon = GFX_decision_category_generic_economy
	priority = 190
	allowed = {
		OR = {
			original_tag = AFG
			original_tag = TAL
		}
	}

}
AFG_concessions_village_councils = {
	priority = 400
	allowed = {
		#has_completed_focus = AFG_concessions_village_councils
		OR = {
			original_tag = AFG
			original_tag = TAL
		}
	}

}
AFG_abdul_rashid_rebel = {
	priority = 400
	allowed = {
		original_tag = AFG
	}

}
AFG_Northern_alliance_taloqan_siege = {
	priority = 500
	icon = GFX_decision_category_generic_mountain_fortification
	allowed = {
		OR = {
			original_tag = AFG
		}
	}

}
AFG_State_Map_Recovery = {

	allowed = {
		original_tag = AFG
	}

}

AFG_Agro_mech = {
	allowed = { original_tag = AFG }
	priority = 400
}
AFG_Afghan_northern_alliance = {
	allowed = { original_tag = AFG }
	priority = 400
	#scripted_gui = AFG_internal_factions_decision_ui
}
Taliban_insurgency_category = {
	icon = GFX_decision_category_taliban_insurgency
	# visible_when_empty = yes

	allowed = {
		OR = {
			tag = AFG
			tag = USA
			tag = CAN
			tag = AST
			tag = NZL
			is_asian_nation = yes
			is_middle_eastern_nation = yes
			is_european_nation = yes
		}
	}

	visible = {
		NOT = {
			country_exists = TAL
		}
		AFG = {
			AND = {
				no_jihadist_government = yes
				check_variable = {
					var = taliban_strength
					value = 0
					compare = greater_than_or_equals
				}
				has_country_flag = afg_active_taliban_insurgency
			}
		}
	}
}
sco_category = {

	icon = GFX_decisions_category_sco
	visible_when_empty = yes

	allowed = {
		OR = {
			is_asian_nation = yes
			original_tag = PER
			original_tag = TUR
			original_tag = ABK
			original_tag = ARM
			original_tag = AZE
			original_tag = GEO
			original_tag = SOO
			original_tag = NKR
			original_tag = ALA
			original_tag = AQY
			original_tag = BHR
			original_tag = DRU
			original_tag = FSA
			original_tag = HAM
			original_tag = HEZ
			original_tag = HOU
			original_tag = IRQ
			original_tag = ISI
			original_tag = ISR
			original_tag = JOR
			original_tag = KUR
			original_tag = KUW
			original_tag = LEB
			original_tag = NUS
			original_tag = OMA
			original_tag = PAL
			original_tag = PKK
			original_tag = QAT
			original_tag = ROJ
			original_tag = SAU
			original_tag = SYR
			original_tag = UAE
			original_tag = YEM
			original_tag = SOV
			original_tag = BLR
			AND = {
				original_tag = ITA
				has_completed_focus = ITA_attempt_join_sco
			}
		}
		NOT = { original_tag = TRK }
	}
}

south_china_sea_category = {

	icon = GFX_decision_category_border_conflicts
	allowed = {
		OR = {
			original_tag = CHI
			original_tag = TAI
			original_tag = VIE
			original_tag = PHI
			original_tag = MAY
		}
	}
	visible = {
		OR = {
			controls_state = 813
			controls_state = 801
			controls_state = 526
			controls_state = 802
			controls_state = 491
			controls_state = 816
			controls_state = 943
		}
	}
}

one_china_category = {
	icon = GFX_decision_category_taiwan
	allowed = {
		OR = {
			original_tag = CHI
			original_tag = TAI
		}
	}
	visible = {
		country_exists = TAI
		country_exists = CHI
		TAI = {
			is_subject = no
		}
	}
}

hongkong_category = {
	icon = GFX_decision_category_hong_kong
	allowed = {
		OR = {
			original_tag = CHI
			original_tag = HKG
		}
	}
}

invigorate_country_through_STE_category = {
	icon = GFX_decision_generic_china
	allowed = {
		original_tag = CHI
	}
}

high_speed_railway_dev_category = {
	icon = GFX_decision_category_generic_industry
	allowed = {
		original_tag = CHI
	}
	visible = {
		has_completed_focus = CHI_Chinese_High_Speed_Rail_System
	}
}

southwest_hydroelectricity_dev_category = {
	icon = GFX_decision_generic_construction
	allowed = {
		original_tag = CHI
	}
	visible = {
		has_completed_focus = CHI_Renewable_Strategy
	}
}

xinjiang_category = {
	icon = GFX_decision_category_jap_intervene_in_china
	allowed = {
		original_tag = CHI
	}
	visible = {
		controls_state = 592
		NOT = { has_completed_focus = CHI_Prosperous_Borderland }
	}
}

outer_mongolia_category = {
	icon = GFX_decision_category_jap_intervene_in_china
	allowed = {
		original_tag = CHI
	}
	visible = {
		has_completed_focus = CHI_Aid_Outer_Mongolia
		country_exists = MON
	}
}

taiwan_coup_category = {
	icon = GFX_decision_category_ger_military_buildup
	allowed = {
		OR = {
			original_tag = TAI
			original_tag = USA
			original_tag = JAP
			original_tag = CAN
			original_tag = AST
			original_tag = NZL
			original_tag = SIN
			original_tag = KOR
		}
	}
	visible = {
		TAI = { has_idea = taiwanese_coup }
	}
}

arunachal_war_decisions_category = {
	icon = GFX_decision_generic_china
	picture = GFX_decision_indian_conflict
	priority = 100

	allowed = {
		OR = {
			original_tag = RAJ
			original_tag = CHI
		}
	}

	visible = {
		country_exists = RAJ
		country_exists = CHI
		475 = { is_claimed_by = CHI
				is_controlled_by = RAJ }
		CHI = { has_completed_focus = CHI_Strike_Across_the_Himalayas }
		NOT = {
			OR = {
				CHI = { is_subject_of = RAJ }
				RAJ = { is_subject_of = CHI }
				RAJ = { is_in_faction_with = CHI }
			}
		}
	}
}
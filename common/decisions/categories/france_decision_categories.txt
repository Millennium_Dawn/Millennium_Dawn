FRA_french_space_decision_category = {
	icon = GFX_decision_French_Space_Program
	allowed = { original_tag = FRA }

	priority = 150

	visible = {
		has_country_flag = FRA_new_frontiers
		has_completed_focus = FRA_the_space_program
		is_subject = no
		has_capitulated = no
	}
}

FRA_invest_LEB_decision_category = {
	allowed = { original_tag = FRA }

	priority = 150

	visible = {
		is_subject = no
		has_capitulated = no
		has_completed_focus = FRA_agriculture_works_in_the_levant
	}
}

FRA_french_africa_decision_category = {
	allowed = { original_tag = FRA }

	priority = 150

	visible = {
		is_subject = no
		has_completed_focus = FRA_francafrique
	}
}

CHE_nationalise_caucas_category = {
 	icon = GFX_decision_category_military_operatio
	visible = { original_tag = CHE }
 	allowed = { original_tag = CHE }
}
CHE_caucasian_revol_category = {
	icon = GFX_decision_kavkaz_revol

	picture = GFX_decision_kavkazrevol_econplakat

	allowed = {
		original_tag = CHE
	}
	visible = {
		has_completed_focus = CHE_KSSR
	}
}
CHE_caucasians_revol_category = {
	icon = GFX_decision_kavkaz_revol

	picture = GFX_decision_kavkazrevol_econplakat

	allowed = {
		original_tag = CHE
	}
	visible = {
		has_completed_focus = CHE_caucas_commies_federation
	}
}

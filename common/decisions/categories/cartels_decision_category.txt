# Author(s): CeruleanEyes, AngriestBird
cartels_decision_category = {
	icon = GFX_decision_category_cartel
	visible_when_empty = yes
	priority = 150

	# Check to ensure that nation who defeat it no longer see it
	visible = {
		has_cartels_penalties = yes
		NOT = { has_country_flag = CARTEL_defeated_the_cartels }
	}

	allowed = {
		is_cartel_nation = yes
	}
}
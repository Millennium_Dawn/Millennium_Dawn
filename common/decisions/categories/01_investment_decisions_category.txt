# Author(s): AngriestBird, Hiddengearz
AC_DECISIONS = {
	priority = 9999

	icon = generic_industry

	allowed = { always = yes }
	visible = { always = yes }
}

AC_target_country_DECISIONS = {
	priority = 9998

	icon = generic_industry

	allowed = { always = yes }
	visible = { always = yes }
}
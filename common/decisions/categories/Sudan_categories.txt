sudan_second_civil_war_decisions_category = {

	allowed = {
		OR = {
			original_tag = SUD
			original_tag = SSU
		}
	}

	visible = {
		NOT = { has_global_flag = second_sudanese_civil_war_over }
	}

	priority = 200

}

sudan_war_in_darfur_decisions_category = {

	allowed = {
		OR = {
			original_tag = SUD
			original_tag = SSU
			original_tag = DAR
		}
	}

	visible_when_empty = yes

	visible = {
		has_global_flag = war_in_darfur
		NOT = { has_global_flag = war_in_darfur_over }
	}

	priority = 200

}
scripted_gui = {
	HEZ_international_hezbollah = {
		window_name = "international_hezbollah_gui_window"
		context_type = player_context

		visible = { has_country_flag = international_hezbollah_tab_open }

		properties = {

			mission_progressbar_container = { frame = hez_days_remaining_loc }

			africa_continent_icon = {
				image = "[africa_continent]"
			}
			south_america_continent_icon = {
				image = "[south_america_continent]"
			}
			north_america_continent_icon = {
				image = "[north_america_continent]"
			}
			west_asia_continent_icon = {
				image = "[west_asia_continent]"
			}

			first_flag_icon = {
				image = "[first_clickable_flag]"
			}
			second_flag_icon = {
				image = "[second_clickable_flag]"
			}
			third_flag_icon = {
				image = "[third_clickable_flag]"
			}
			fourth_flag_icon = {
				image = "[fourth_clickable_flag]"
			}
			fifth_flag_icon = {
				image = "[fifth_clickable_flag]"
			}

			recruit_militia_icon = {
				image = "GFX_hezbollah_militia_button"
			}
			sell_weapons_icon = {
				image = "GFX_hezbollah_weapons_button"
			}
			influencing_icon = {
				image = "GFX_hezbollah_influence_button"
			}
			attack_israel_icon = {
				image = "GFX_hezbollah_israel_button"
			}
			hezbollah_first_special_mission_icon = {
				image = "GFX_hezbollah_special_mission_button"
			}
			hezbollah_second_special_mission_icon = {
				image = "GFX_hezbollah_special_mission_button"
			}
			hezbollah_third_special_mission_icon = {
				image = "GFX_hezbollah_special_mission_button"
			}
			hezbollah_fourth_special_mission_icon = {
				image = "GFX_hezbollah_special_mission_button"
			}

			ongoing_mission_pic_icon = {
				image = "[HEZ_ongoing_mission_picture]"
			}
		}

		effects = {
			#Buttons' effects
			close_button_click = { clr_country_flag = international_hezbollah_tab_open }

			first_flag_icon_click = {
				if = {
					limit = { has_country_flag = HEZ_selected_africa }
					set_variable = { hezbollah_selected_country_for_operation = NIG }
				}
				else_if = {
					limit = { has_country_flag = HEZ_selected_north_america }
					set_variable = { hezbollah_selected_country_for_operation = USA }
				}
				else_if = {
					limit = { has_country_flag = HEZ_selected_south_america }
					set_variable = { hezbollah_selected_country_for_operation = ARG }
				}
				else_if = {
					limit = { has_country_flag = HEZ_selected_west_asia }
					set_variable = { hezbollah_selected_country_for_operation = SYR }
				}
			}
			second_flag_icon_click = {
				if = {
					limit = { has_country_flag = HEZ_selected_africa }
					set_variable = { hezbollah_selected_country_for_operation = LBA }
				}
				else_if = {
					limit = { has_country_flag = HEZ_selected_north_america }
					set_variable = { hezbollah_selected_country_for_operation = CAN }
				}
				else_if = {
					limit = { has_country_flag = HEZ_selected_south_america }
					set_variable = { hezbollah_selected_country_for_operation = BRA }
				}
				else_if = {
					limit = { has_country_flag = HEZ_selected_west_asia }
					set_variable = { hezbollah_selected_country_for_operation = IRQ }
				}
			}
			third_flag_icon_click = {
				if = {
					limit = { has_country_flag = HEZ_selected_africa }
					set_variable = { hezbollah_selected_country_for_operation = EGY }
				}
				else_if = {
					limit = { has_country_flag = HEZ_selected_south_america }
					set_variable = { hezbollah_selected_country_for_operation = VEN }
				}
				else_if = {
					limit = { has_country_flag = HEZ_selected_west_asia }
					set_variable = { hezbollah_selected_country_for_operation = PAL }
				}
			}
			fourth_flag_icon_click = {
				if = {
					limit = { has_country_flag = HEZ_selected_africa }
					set_variable = { hezbollah_selected_country_for_operation = SUD }
				}
				else_if = {
					limit = { has_country_flag = HEZ_selected_south_america }
					set_variable = { hezbollah_selected_country_for_operation = COL }
				}
				else_if = {
					limit = { has_country_flag = HEZ_selected_west_asia }
					set_variable = { hezbollah_selected_country_for_operation = YEM }
				}
			}
			fifth_flag_icon_click = {
				if = {
					limit = { has_country_flag = HEZ_selected_africa }
					set_variable = { hezbollah_selected_country_for_operation = SSU }
				}
				else_if = {
					limit = { has_country_flag = HEZ_selected_south_america }
					set_variable = { hezbollah_selected_country_for_operation = MEX }
				}
				else_if = {
					limit = { has_country_flag = HEZ_selected_west_asia }
					set_variable = { hezbollah_selected_country_for_operation = SAU }
				}
			}

			#Africa
			africa_button_1_click = {
				if = {
					limit = {
						OR = {
							NOT = { has_country_flag = HEZ_selected_africa }
							OR = {
								has_country_flag = HEZ_selected_south_america
								has_country_flag = HEZ_selected_north_america
								has_country_flag = HEZ_selected_west_asia
							}
						}
					}
					#Deselect everything else
					clr_country_flag = HEZ_selected_south_america
					clr_country_flag = HEZ_selected_north_america
					clr_country_flag = HEZ_selected_west_asia

					#Select Africa
					set_country_flag = HEZ_selected_africa
				}
				#else = { clr_country_flag = HEZ_selected_africa }
			}
			africa_button_2_click = {
				if = {
					limit = {
						OR = {
							NOT = { has_country_flag = HEZ_selected_africa }
							OR = {
								has_country_flag = HEZ_selected_south_america
								has_country_flag = HEZ_selected_north_america
								has_country_flag = HEZ_selected_west_asia
							}
						}
					}
					#Deselect everything else
					clr_country_flag = HEZ_selected_south_america
					clr_country_flag = HEZ_selected_north_america
					clr_country_flag = HEZ_selected_west_asia

					#Select Africa
					set_country_flag = HEZ_selected_africa
				}
				#else = { clr_country_flag = HEZ_selected_africa }
			}

			#South America
			south_america_button_1_click = {
				if = {
					limit = {
						OR = {
							NOT = { has_country_flag = HEZ_selected_south_america }
							OR = {
								has_country_flag = HEZ_selected_africa
								has_country_flag = HEZ_selected_north_america
								has_country_flag = HEZ_selected_west_asia
							}
						}
					}
					#Deselect everything else
					clr_country_flag = HEZ_selected_africa
					clr_country_flag = HEZ_selected_north_america
					clr_country_flag = HEZ_selected_west_asia

					#Select south_america
					set_country_flag = HEZ_selected_south_america
				}
				#else = { clr_country_flag = HEZ_selected_south_america }
			}
			south_america_button_2_click = {
				if = {
					limit = {
						OR = {
							NOT = { has_country_flag = HEZ_selected_south_america }
							OR = {
								has_country_flag = HEZ_selected_africa
								has_country_flag = HEZ_selected_north_america
								has_country_flag = HEZ_selected_west_asia
							}
						}
					}
					#Deselect everything else
					clr_country_flag = HEZ_selected_africa
					clr_country_flag = HEZ_selected_north_america
					clr_country_flag = HEZ_selected_west_asia

					#Select south_america
					set_country_flag = HEZ_selected_south_america
				}
				#else = { clr_country_flag = HEZ_selected_south_america }
			}

			#North America
			north_america_button_1_click = {
				if = {
					limit = {
						OR = {
							NOT = { has_country_flag = HEZ_selected_north_america }
							OR = {
								has_country_flag = HEZ_selected_africa
								has_country_flag = HEZ_selected_south_america
								has_country_flag = HEZ_selected_west_asia
							}
						}
					}
					#Deselect everything else
					clr_country_flag = HEZ_selected_africa
					clr_country_flag = HEZ_selected_south_america
					clr_country_flag = HEZ_selected_west_asia

					#Select north_america
					set_country_flag = HEZ_selected_north_america
				}
				#else = { clr_country_flag = HEZ_selected_north_america }
			}
			north_america_button_2_click = {
				if = {
					limit = {
						OR = {
							NOT = { has_country_flag = HEZ_selected_north_america }
							OR = {
								has_country_flag = HEZ_selected_africa
								has_country_flag = HEZ_selected_south_america
								has_country_flag = HEZ_selected_west_asia
							}
						}
					}
					#Deselect everything else
					clr_country_flag = HEZ_selected_africa
					clr_country_flag = HEZ_selected_south_america
					clr_country_flag = HEZ_selected_west_asia

					#Select north_america
					set_country_flag = HEZ_selected_north_america
				}
				#else = { clr_country_flag = HEZ_selected_north_america }
			}

			#West Asia
			west_asia_button_1_click = {
				if = {
					limit = {
						OR = {
							NOT = { has_country_flag = HEZ_selected_west_asia }
							OR = {
								has_country_flag = HEZ_selected_africa
								has_country_flag = HEZ_selected_south_america
								has_country_flag = HEZ_selected_north_america
							}
						}
					}
					#Deselect everything else
					clr_country_flag = HEZ_selected_africa
					clr_country_flag = HEZ_selected_south_america
					clr_country_flag = HEZ_selected_north_america

					#Select west_asia
					set_country_flag = HEZ_selected_west_asia
				}
				#else = { clr_country_flag = HEZ_selected_west_asia }
			}
			west_asia_button_2_click = {
				if = {
					limit = {
						OR = {
							NOT = { has_country_flag = HEZ_selected_west_asia }
							OR = {
								has_country_flag = HEZ_selected_africa
								has_country_flag = HEZ_selected_south_america
								has_country_flag = HEZ_selected_north_america
							}
						}
					}
					#Deselect everything else
					clr_country_flag = HEZ_selected_africa
					clr_country_flag = HEZ_selected_south_america
					clr_country_flag = HEZ_selected_north_america

					#Select west_asia
					set_country_flag = HEZ_selected_west_asia
				}
				#else = { clr_country_flag = HEZ_selected_west_asia }
			}
			west_asia_button_3_click = {
				if = {
					limit = {
						OR = {
							NOT = { has_country_flag = HEZ_selected_west_asia }
							OR = {
								has_country_flag = HEZ_selected_africa
								has_country_flag = HEZ_selected_south_america
								has_country_flag = HEZ_selected_north_america
							}
						}
					}
					#Deselect everything else
					clr_country_flag = HEZ_selected_africa
					clr_country_flag = HEZ_selected_south_america
					clr_country_flag = HEZ_selected_north_america

					#Select west_asia
					set_country_flag = HEZ_selected_west_asia
				}
				#else = { clr_country_flag = HEZ_selected_west_asia }
			}

			#Missions Button's Effects
				#Shared Misisons
			recruit_militia_icon_click = {
				set_country_flag = { flag = shared_mission_in_progress value = 1 days = 60 }
				set_country_flag = { flag = has_selected_recruit_militia value = 1 days = 60 }
				#Costs:
				set_temp_variable = { treasury_change = -1 }
				modify_treasury_effect = yes

				#Effects:
				country_event = { id = international_hezbollah.1 days = 60 }

				#Save country as a variable
				set_variable = { the_country_that_hezbollah_is_currently_doing_a_mission_in = hezbollah_selected_country_for_operation }
			}
			sell_weapons_icon_click = {
				set_country_flag = { flag = shared_mission_in_progress value = 1 days = 60 }
				set_country_flag = { flag = has_selected_small_arms value = 1 days = 60 }
				#Costs:
				add_equipment_to_stockpile = {
					type = Inf_equipment
					amount = -200
				}

				#Effects:
				country_event = { id = international_hezbollah.2 days = 60 }

				#Save country as a variable
				set_variable = { the_country_that_hezbollah_is_currently_doing_a_mission_in = hezbollah_selected_country_for_operation }
			}
			influencing_icon_click = {
				set_country_flag = { flag = shared_mission_in_progress value = 1 days = 60 }
				set_country_flag = { flag = has_selected_influence_nation value = 1 days = 60 }
				#Costs:
				add_political_power = -50

				#Effects:
				set_variable = { HEZ_country_to_influence_with_button = hezbollah_selected_country_for_operation }
				country_event = { id = international_hezbollah.3 days = 60 }

				#Save country as a variable
				set_variable = { the_country_that_hezbollah_is_currently_doing_a_mission_in = hezbollah_selected_country_for_operation }
			}
			attack_israel_icon_click = {
				set_country_flag = { flag = shared_mission_in_progress value = 1 days = 60 }
				set_country_flag = { flag = has_selected_attack_zionists value = 1 days = 60 }

				#Effects:
				set_variable = { HEZ_country_to_attack_zionists_with_button = hezbollah_selected_country_for_operation }
				country_event = { id = international_hezbollah.4 days = 60 }

				#Save country as a variable
				set_variable = { the_country_that_hezbollah_is_currently_doing_a_mission_in = hezbollah_selected_country_for_operation }
			}
		}
		triggers = {
			third_flag_icon_visible = { NOT = { OR = { has_country_flag = HEZ_selected_north_america } } }
			fourth_flag_icon_visible = { NOT = { OR = { has_country_flag = HEZ_selected_north_america } } }
			fifth_flag_icon_visible = { NOT = { OR = { has_country_flag = HEZ_selected_north_america } } }
			attack_israel_icon_visible = { NOT = { has_country_flag = HEZ_selected_north_america } }
			attack_israel_title_visible = { NOT = { has_country_flag = HEZ_selected_north_america } }

			country_flag_1_selected_button_visible = {
				OR = {
					AND = {
						has_country_flag = HEZ_selected_north_america
						check_variable = { var:hezbollah_selected_country_for_operation = USA }
					}
					AND = {
						has_country_flag = HEZ_selected_africa
						check_variable = { var:hezbollah_selected_country_for_operation = NIG }
					}
					AND = {
						has_country_flag = HEZ_selected_south_america
						check_variable = { var:hezbollah_selected_country_for_operation = ARG }
					}
					AND = {
						has_country_flag = HEZ_selected_west_asia
						check_variable = { var:hezbollah_selected_country_for_operation = SYR }
					}
				}
			}
			country_flag_2_selected_button_visible = {
				OR = {
					AND = {
						has_country_flag = HEZ_selected_north_america
						check_variable = { var:hezbollah_selected_country_for_operation = CAN }
					}
					AND = {
						has_country_flag = HEZ_selected_africa
						check_variable = { var:hezbollah_selected_country_for_operation = LBA }
					}
					AND = {
						has_country_flag = HEZ_selected_south_america
						check_variable = { var:hezbollah_selected_country_for_operation = BRA }
					}
					AND = {
						has_country_flag = HEZ_selected_west_asia
						check_variable = { var:hezbollah_selected_country_for_operation = IRQ }
					}
				}
			}
			country_flag_3_selected_button_visible = {
				NOT = {
					has_country_flag = HEZ_selected_north_america
				}
				OR = {
					AND = {
						has_country_flag = HEZ_selected_africa
						check_variable = { var:hezbollah_selected_country_for_operation = EGY }
					}
					AND = {
						has_country_flag = HEZ_selected_south_america
						check_variable = { var:hezbollah_selected_country_for_operation = VEN }
					}
					AND = {
						has_country_flag = HEZ_selected_west_asia
						check_variable = { var:hezbollah_selected_country_for_operation = PAL }
					}
				}
			}
			country_flag_4_selected_button_visible = {
				NOT = {
					has_country_flag = HEZ_selected_north_america
				}
				OR = {
					AND = {
						has_country_flag = HEZ_selected_africa
						check_variable = { var:hezbollah_selected_country_for_operation = SUD }
					}
					AND = {
						has_country_flag = HEZ_selected_south_america
						check_variable = { var:hezbollah_selected_country_for_operation = COL }
					}
					AND = {
						has_country_flag = HEZ_selected_west_asia
						check_variable = { var:hezbollah_selected_country_for_operation = YEM }
					}
				}
			}
			country_flag_5_selected_button_visible = {
				NOT = {
					has_country_flag = HEZ_selected_north_america
				}
				OR = {
					AND = {
						has_country_flag = HEZ_selected_africa
						check_variable = { var:hezbollah_selected_country_for_operation = SSU }
					}
					AND = {
						has_country_flag = HEZ_selected_south_america
						check_variable = { var:hezbollah_selected_country_for_operation = MEX }
					}
					AND = {
						has_country_flag = HEZ_selected_west_asia
						check_variable = { var:hezbollah_selected_country_for_operation = SAU }
					}
				}
			}

			#Region Availability
			africa_button_1_click_enabled = {
				custom_trigger_tooltip = {
					tooltip = region_selection_button_africa_TT
					has_completed_focus = HEZ_African_Hezbollah
				}
			}
			africa_button_2_click_enabled = {
				custom_trigger_tooltip = {
					tooltip = region_selection_button_africa_TT
					has_completed_focus = HEZ_African_Hezbollah
				}
			}
			south_america_button_1_click_enabled = {
				custom_trigger_tooltip = {
					tooltip = region_selection_button_south_america_TT
					has_completed_focus = HEZ_Latin_American_Hezbollah
				}
			}
			south_america_button_2_click_enabled = {
				custom_trigger_tooltip = {
					tooltip = region_selection_button_south_america_TT
					has_completed_focus = HEZ_Latin_American_Hezbollah
				}
			}
			north_america_button_1_click_enabled = {
				custom_trigger_tooltip = {
					tooltip = region_selection_button_north_america_TT
					has_completed_focus = HEZ_North_American_Hezbollah
				}
			}
			north_america_button_2_click_enabled = {
				custom_trigger_tooltip = {
					tooltip = region_selection_button_north_america_TT
					has_completed_focus = HEZ_North_American_Hezbollah
				}
			}
			west_asia_button_1_click_enabled = {
				custom_trigger_tooltip = {
					tooltip = region_selection_button_west_asia_TT
					has_completed_focus = HEZ_Hezbollah_in_the_Middle_east
				}
			}
			west_asia_button_2_click_enabled = {
				custom_trigger_tooltip = {
					tooltip = region_selection_button_west_asia_TT
					has_completed_focus = HEZ_Hezbollah_in_the_Middle_east
				}
			}
			west_asia_button_3_click_enabled = {
				custom_trigger_tooltip = {
					tooltip = region_selection_button_west_asia_TT
					has_completed_focus = HEZ_Hezbollah_in_the_Middle_east
				}
			}

			#Mission Availability
			recruit_militia_icon_click_enabled = {
				OR = {
					check_variable = { var:hezbollah_selected_country_for_operation = NIG }
					check_variable = { var:hezbollah_selected_country_for_operation = USA }
					check_variable = { var:hezbollah_selected_country_for_operation = ARG }
					check_variable = { var:hezbollah_selected_country_for_operation = SYR }
					check_variable = { var:hezbollah_selected_country_for_operation = LBA }
					check_variable = { var:hezbollah_selected_country_for_operation = CAN }
					check_variable = { var:hezbollah_selected_country_for_operation = BRA }
					check_variable = { var:hezbollah_selected_country_for_operation = IRQ }
					check_variable = { var:hezbollah_selected_country_for_operation = EGY }
					check_variable = { var:hezbollah_selected_country_for_operation = VEN }
					check_variable = { var:hezbollah_selected_country_for_operation = YEM }
					check_variable = { var:hezbollah_selected_country_for_operation = SUD }
					check_variable = { var:hezbollah_selected_country_for_operation = COL }
					check_variable = { var:hezbollah_selected_country_for_operation = PAL }
					check_variable = { var:hezbollah_selected_country_for_operation = SSU }
					check_variable = { var:hezbollah_selected_country_for_operation = MEX }
					check_variable = { var:hezbollah_selected_country_for_operation = SAU }
				}
				custom_trigger_tooltip = {
					tooltip = mission_in_progress_TT
					NOT = { has_country_flag = shared_mission_in_progress }
				}
				custom_trigger_tooltip = {
					tooltip = at_least_5_percent_shia_popularity_TT
					var:hezbollah_selected_country_for_operation = {
						OR = {
							check_variable = { party_pop_array^8 > 0.049 }
							check_variable = { party_pop_array^9 > 0.049 }
						}
					}
				}
			}
			sell_weapons_icon_click_enabled = {
				OR = {
					check_variable = { var:hezbollah_selected_country_for_operation = NIG }
					check_variable = { var:hezbollah_selected_country_for_operation = USA }
					check_variable = { var:hezbollah_selected_country_for_operation = ARG }
					check_variable = { var:hezbollah_selected_country_for_operation = SYR }
					check_variable = { var:hezbollah_selected_country_for_operation = LBA }
					check_variable = { var:hezbollah_selected_country_for_operation = CAN }
					check_variable = { var:hezbollah_selected_country_for_operation = BRA }
					check_variable = { var:hezbollah_selected_country_for_operation = IRQ }
					check_variable = { var:hezbollah_selected_country_for_operation = EGY }
					check_variable = { var:hezbollah_selected_country_for_operation = VEN }
					check_variable = { var:hezbollah_selected_country_for_operation = YEM }
					check_variable = { var:hezbollah_selected_country_for_operation = SUD }
					check_variable = { var:hezbollah_selected_country_for_operation = COL }
					check_variable = { var:hezbollah_selected_country_for_operation = PAL }
					check_variable = { var:hezbollah_selected_country_for_operation = SSU }
					check_variable = { var:hezbollah_selected_country_for_operation = MEX }
					check_variable = { var:hezbollah_selected_country_for_operation = SAU }
				}
				custom_trigger_tooltip = {
					tooltip = mission_in_progress_TT
					NOT = { has_country_flag = shared_mission_in_progress }
				}
				custom_trigger_tooltip = {
					tooltip = at_least_1_percent_shia_popularity_TT
					var:hezbollah_selected_country_for_operation = {
						OR = {
							check_variable = { party_pop_array^8 > 0.009 }
							check_variable = { party_pop_array^9 > 0.009 }
						}
					}
				}
				custom_trigger_tooltip = {
					tooltip = has_200_small_arms_stockpiled_TT
					has_equipment = {
						Inf_equipment > 199
					}
				}
			}
			influencing_icon_click_enabled = {
				OR = {
					check_variable = { var:hezbollah_selected_country_for_operation = NIG }
					check_variable = { var:hezbollah_selected_country_for_operation = USA }
					check_variable = { var:hezbollah_selected_country_for_operation = ARG }
					check_variable = { var:hezbollah_selected_country_for_operation = SYR }
					check_variable = { var:hezbollah_selected_country_for_operation = LBA }
					check_variable = { var:hezbollah_selected_country_for_operation = CAN }
					check_variable = { var:hezbollah_selected_country_for_operation = BRA }
					check_variable = { var:hezbollah_selected_country_for_operation = IRQ }
					check_variable = { var:hezbollah_selected_country_for_operation = EGY }
					check_variable = { var:hezbollah_selected_country_for_operation = VEN }
					check_variable = { var:hezbollah_selected_country_for_operation = YEM }
					check_variable = { var:hezbollah_selected_country_for_operation = SUD }
					check_variable = { var:hezbollah_selected_country_for_operation = COL }
					check_variable = { var:hezbollah_selected_country_for_operation = PAL }
					check_variable = { var:hezbollah_selected_country_for_operation = SSU }
					check_variable = { var:hezbollah_selected_country_for_operation = MEX }
					check_variable = { var:hezbollah_selected_country_for_operation = SAU }
				}
				custom_trigger_tooltip = {
					tooltip = mission_in_progress_TT
					NOT = { has_country_flag = shared_mission_in_progress }
				}
				var:hezbollah_selected_country_for_operation = { is_influencer = yes }
 				has_political_power > 50
			}
			attack_israel_icon_click_enabled = {
				OR = {
					check_variable = { var:hezbollah_selected_country_for_operation = NIG }
					check_variable = { var:hezbollah_selected_country_for_operation = USA }
					check_variable = { var:hezbollah_selected_country_for_operation = ARG }
					check_variable = { var:hezbollah_selected_country_for_operation = SYR }
					check_variable = { var:hezbollah_selected_country_for_operation = LBA }
					check_variable = { var:hezbollah_selected_country_for_operation = CAN }
					check_variable = { var:hezbollah_selected_country_for_operation = BRA }
					check_variable = { var:hezbollah_selected_country_for_operation = IRQ }
					check_variable = { var:hezbollah_selected_country_for_operation = EGY }
					check_variable = { var:hezbollah_selected_country_for_operation = VEN }
					check_variable = { var:hezbollah_selected_country_for_operation = YEM }
					check_variable = { var:hezbollah_selected_country_for_operation = SUD }
					check_variable = { var:hezbollah_selected_country_for_operation = COL }
					check_variable = { var:hezbollah_selected_country_for_operation = PAL }
					check_variable = { var:hezbollah_selected_country_for_operation = SSU }
					check_variable = { var:hezbollah_selected_country_for_operation = MEX }
					check_variable = { var:hezbollah_selected_country_for_operation = SAU }
				}
				custom_trigger_tooltip = {
					tooltip = mission_in_progress_TT
					NOT = { has_country_flag = shared_mission_in_progress }
				}
				has_manpower > 2000
				has_equipment = {
					Inf_equipment > 300
				}
				has_equipment = {
					cnc_equipment > 100
				}
				has_equipment = {
					AA_Equipment > 60
				}
				has_equipment = {
					L_AT_Equipment > 60
				}
				has_equipment = {
					util_vehicle_equipment > 30
				}
				var:hezbollah_selected_country_for_operation = {
					influence_higher_10 = yes
					OR = {
						gives_military_access_to = USA
						is_subject_of = USA
						gives_military_access_to = ISR
						is_subject_of = ISR
					}
				}
			}
		}
	}
	open_international_hezbollah_gui = {
		context_type = player_context
		window_name = "politics_open_international_hezbollah_gui"
		parent_window_token = politics_tab

		visible = { original_tag = HEZ }

		triggers = {
			open_international_hezbollah_gui_click_enabled = { has_completed_focus = HEZ_International_Hezbollah }
		}

		effects = {
			open_international_hezbollah_gui_click = {
				if = {
					limit = { NOT = { has_country_flag = international_hezbollah_tab_open } }
					set_country_flag = international_hezbollah_tab_open
				}
				else = { clr_country_flag = international_hezbollah_tab_open }
			}
		}
	}
}
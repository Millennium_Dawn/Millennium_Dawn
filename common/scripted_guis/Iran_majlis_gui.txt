scripted_gui = {
	iran_majlis_gui = {
		context_type = decision_category
		window_name = "Iran_majlis_in_power_decision_ui_window"
	}
	#
	PER_revolutionary_recovery = {
		window_name = "PER_revolutionary_recovery_container"
		context_type = decision_category

		properties = {
			revolutionary_recovery_progress = { frame = PER.revolutionary_recovery_status }
		}
	}
	#
	iran_majlis_gui = {
		context_type = decision_category
		window_name = "Iran_iranic_unity_ui_window"
	}
}
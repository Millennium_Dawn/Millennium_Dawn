#Events Written by Luigi IV
add_namespace = cascadia_event



##### Cascadia's TAG is CAS
country_event = {
	id = cascadia_event.1
	title = cascadia_event.1.t
	desc = cascadia_event.1.d
	picture = GFX_politics_far_right
	is_triggered_only = yes
	trigger = {
		original_tag = CAS
	}

	option = {
		name = cascadia_event.1.o1
		log = "[GetDateText]: [This.GetName]: cascadia_event.1.o1 executed"
		add_stability = -0.03
		add_war_support = -0.03
		country_event = {
			id = cascadia_event.2
			days = 7
		}
		ai_chance = { base = 30 }
	}
}

country_event = {
	id = cascadia_event.2
	title = cascadia_event.2.t
	desc = cascadia_event.2.d
	picture = GFX_politics_far_right
	is_triggered_only = yes
	trigger = {
		original_tag = CAS
	}
	immediate = {
		hidden_effect = {
			IDH = { add_state_core = 808 }
		}
	}
	option = {
		name = cascadia_event.2.o1
		log = "[GetDateText]: [This.GetName]: cascadia_event.2.o1 executed"
		add_stability = -0.05
		add_war_support = -0.05
		country_event = {
			id = cascadia_event.3
			days = 7
		}
		ai_chance = { 
			base = 30 
			modifier = {
				has_global_flag = CAS_ROCKY_MOUNTAIN_RESISTANCE_PATH
				factor = 10000
			}
			modifier = {
				has_global_flag = CAS_PEACE_IN_OUR_TIME_PATH
				factor = 1
			}
		}
	}
	option = {
		name = cascadia_event.2.o2
		log = "[GetDateText]: [This.GetName]: cascadia_event.2.o2 executed"
		add_stability = -0.10
		add_war_support = -0.10
		country_event = {
			id = cascadia_event.300
			days = 7
		}
		ai_chance = { 
			base = 30 
			modifier = {
				has_global_flag = CAS_PEACE_IN_OUR_TIME_PATH
				factor = 10000
			}
			modifier = {
				has_global_flag = CAS_ROCKY_MOUNTAIN_RESISTANCE_PATH
				factor = 1
			}
		}
	}
}

country_event = {
	id = cascadia_event.3
	title = cascadia_event.3.t
	desc = cascadia_event.3.d
	picture = GFX_politics_far_right
	is_triggered_only = yes
	trigger = {
		original_tag = CAS
	}
	option = {
		name = cascadia_event.3.o1
		log = "[GetDateText]: [This.GetName]: cascadia_event.3.o1 executed"
		add_stability = -0.07
		add_war_support = -0.07
		country_event = {
			id = cascadia_event.4
			days = 7
		}
		809 = {
			add_building_construction = {
				type = arms_factory
				level = 1
				instant_build = yes
			}
		}
		810 = {
			add_building_construction = {
				type = arms_factory
				level = 1
				instant_build = yes
			}
		}
		ai_chance = { base = 30 }
	}
}

country_event = {
	id = cascadia_event.4
	title = cascadia_event.4.t
	desc = cascadia_event.4.d
	picture = GFX_politics_demands
	is_triggered_only = yes
	trigger = {
		original_tag = CAS
	}
	immediate = {
		hidden_effect = {
			IDH = {
				transfer_state = 808
			}
			IDH = {
				set_variable = { civilian_factory_employment_var = 1 }
				set_variable = { naval_factory_employment_var = 1 }
				set_variable = { military_factory_employment_var = 1 }
				set_variable = { office_employment_var = 1 }
				set_variable = { agriculture_district_employment_var = 1 }
				set_variable = { gdpc_converging_var = FROM.gdpc_converging_var }
				calculate_influence_percentage = yes #set alignment drifts from influence
				startup_politics = yes
				ingame_update_setup = yes 	#money system
				setup_init_factions = yes 	#flags for int. factions
				set_law_vars = yes 	#vars for change laws secondary effects
				update_neighbors_effects = yes # Updates Neighbor Effects
				economic_cycle_drift_popularity = yes #drift from neighbor
				ai_update_build_units = yes
				init_auto_influence_array = yes
				ingame_init_investment_system = yes
				research_slot_init_for_country = yes
				calculate_expected_spending = yes
			}
		}
	}
	option = {
		name = cascadia_event.4.o1
		log = "[GetDateText]: [This.GetName]: cascadia_event.4.o1 executed"
		add_stability = -0.10
		add_war_support = -0.10
		IDH = {
			country_event = {
				id = idaho_event.1
				days = 2
			}
		}
		country_event = {
			id = cascadia_event.5
			days = 7
		}
		809 = {
			add_building_construction = {
				type = arms_factory
				level = 1
				instant_build = yes
			}
		}
		810 = {
			add_building_construction = {
				type = arms_factory
				level = 1
				instant_build = yes
			}
		}
		ai_chance = { base = 30 }
	}
}

country_event = {
	id = cascadia_event.5
	title = cascadia_event.5.t
	desc = cascadia_event.5.d
	picture = GFX_politics_demands
	is_triggered_only = yes
	trigger = {
		original_tag = CAS
	}
	option = {
		name = cascadia_event.5.o1
		log = "[GetDateText]: [This.GetName]: cascadia_event.5.o1 executed"
		declare_war_on = {
			target = IDH
			type = annex_everything
		}
		remove_ideas = CAS_ideas_rocky_mountain_resistance
		hidden_effect = {
			swap_ideas = {
				remove_idea = no_military
				add_idea = volunteer_army
			}
		}
		ai_chance = { base = 30 }
	}
}

country_event = {
	id = cascadia_event.300
	title = cascadia_event.300.t
	desc = cascadia_event.300.d
	picture = GFX_politics_negotiations
	is_triggered_only = yes
	trigger = {
		original_tag = CAS
	}
	option = {
		name = cascadia_event.300.o1
		log = "[GetDateText]: [This.GetName]: cascadia_event.300.o1 executed"
		add_stability = 0.03
		add_war_support = 0.03
		country_event = {
			id = cascadia_event.301
			days = 7
		}
		swap_ideas = {
			remove_idea = CAS_ideas_rocky_mountain_resistance
			add_idea = CAS_ideas_rocky_mountain_crackdowns
		}
		ai_chance = { base = 30 }
	}
}

country_event = {
	id = cascadia_event.301
	title = cascadia_event.301.t
	desc = cascadia_event.301.d
	picture = GFX_politics_negotiations
	is_triggered_only = yes
	trigger = {
		original_tag = CAS
	}
	option = {
		name = cascadia_event.301.o1
		log = "[GetDateText]: [This.GetName]: cascadia_event.301.o1 executed"
		add_stability = 0.05
		add_war_support = 0.05
		country_event = {
			id = cascadia_event.302
			days = 7
		}
		ai_chance = { base = 30 }
	}
}

country_event = {
	id = cascadia_event.302
	title = cascadia_event.302.t
	desc = cascadia_event.302.d
	picture = GFX_politics_negotiations
	is_triggered_only = yes
	trigger = {
		original_tag = CAS
	}
	option = {
		name = cascadia_event.302.o1
		log = "[GetDateText]: [This.GetName]: cascadia_event.302.o1 executed"
		add_stability = 0.07
		add_war_support = 0.07
		country_event = {
			id = cascadia_event.303
			days = 7
		}
		swap_ideas = {
			remove_idea = CAS_ideas_rocky_mountain_crackdowns
			add_idea = CAS_ideas_rocky_mountain_rescue
		}
		ai_chance = { base = 30 }
	}
}

country_event = {
	id = cascadia_event.303
	title = cascadia_event.303.t
	desc = cascadia_event.303.d
	picture = GFX_politics_negotiations
	is_triggered_only = yes
	trigger = {
		original_tag = CAS
	}
	option = {
		name = cascadia_event.303.o1
		log = "[GetDateText]: [This.GetName]: cascadia_event.303.o1 executed"
		add_stability = 0.10
		add_war_support = 0.10
		swap_ideas = {
			remove_idea = CAS_ideas_rocky_mountain_rescue
			add_idea = CAS_ideas_cascadian_union
		}
		808 = {
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = industrial_complex
				level = 2
				instant_build = yes
			}
		}
		ai_chance = { base = 30 }
	}
}
add_namespace = bulg
add_namespace = bul_ACDI
add_namespace = bul_mech
add_namespace = bulg_export

country_event = {
	id = bulg.1
	title = bulg.1.t
	desc = bulg.1.d
	is_triggered_only = yes
	fire_only_once = yes
	picture = GFX_paris_convention
	option = {
		name = bulg.1.a
		log = "[GetDateText]: [This.GetName]: bulg.1.a executed"
		ai_chance = { factor = 1 }
		set_temp_variable = { treasury_change = -3.5 }
		modify_treasury_effect = yes
		set_temp_variable = { int_investment_change = 3.5 }
		modify_international_investment_effect = yes
		BUL = {
			hidden_effect = {
				one_random_infrastructure = yes
				set_temp_variable = { treasury_change = 3.5 }
				modify_treasury_effect = yes
			}
			country_event = bulg.3
		}
	}
	option = {
		name = bulg.1.b
		log = "[GetDateText]: [This.GetName]: bulg.1.b executed"
		ai_chance = { factor = 1 }
		hidden_effect = {
			BUL = {
				country_event = bulg.2
			}
		}
	}
}

country_event = {
	id = bulg.2
	title = bulg.2.t
	desc = bulg.2.d
	is_triggered_only = yes
	fire_only_once = yes
	picture = GFX_paris_convention
	option = {
		name = bulg.2.a
		log = "[GetDateText]: [This.GetName]: bulg.2.a executed"
	}

}

country_event = {
	id = bulg.3
	title = bulg.3.t
	desc = bulg.3.d
	is_triggered_only = yes
	fire_only_once = yes
	picture = GFX_paris_convention
	option = {
		name = bulg.3.a
		log = "[GetDateText]: [This.GetName]: bulg.3.a executed"
		set_country_flag = BUL_russia_agree_flag
		set_temp_variable = { percent_change = 2 }
		set_temp_variable = { tag_index = SOV }
		set_temp_variable = { influence_target = BUL }
		change_influence_percentage = yes
	}

}

country_event = {
	id = bul_ACDI.1
	title = bul_ACDI.1.t
	desc = bul_ACDI.1.d
	picture = GFX_special_forces
	is_triggered_only = yes
	#fire_only_once = yes
	option = { # Accept
		name = bul_ACDI.1.a
		log = "[GetDateText]: [This.GetName]: bul_ACDI.1.a executed"
		ai_chance = {
			base = 1
			modifier = {
				add = 100
				original_tag = SER
			}
			modifier = {
				add = 100
				original_tag = ARM
				NOT = { has_government = democratic }
			}
		}
		if = {
			limit = {
				ROOT = {
					has_idea = NATO_member
				}
			}
			ROOT = {
				remove_ideas = NATO_member
			}
		}
		if = {
			limit = {
				original_tag = GRE
			}
			GRE = {
				add_state_claim = 156
				add_state_claim = 934
			}
		}
		if = {
			limit = {
				original_tag = ARM
			}
			ARM = {
				add_state_claim = 935
				add_state_claim = 163
			}
		}
		if = {
			limit = {
				original_tag = SER
			}
			SER = {
				add_state_claim = 129
				add_state_claim = 1054
			}
		}
		if = {
			limit = {
				original_tag = TUR
			}
			TUR = {
				add_state_claim = 143
				add_state_claim = 144
			}
		}
		ARM = {
			add_to_faction = ROOT
		}
		hidden_effect = {
			BUL = {
				country_event = { id = bul_ACDI.2 }
			}
		}
	}

	option = { # Decline
		name = bul_ACDI.1.b
		log = "[GetDateText]: [This.GetName]: bul_ACDI.1.b executed"
		ai_chance = {
			base = 30
		}
		add_opinion_modifier = {
			target = FROM
			modifier = drama
		}
		custom_effect_tooltip = bul_ACDI_Decline_tt
		hidden_effect = {
			BUL = {
				country_event = { id = bul_ACDI.3 }
			}
		}
	}

	option = { # Alternat.
		name = bul_ACDI.1.c
		log = "[GetDateText]: [This.GetName]: bul_ACDI.1.c executed"
		ai_chance = {
			base = 1
			modifier = {
				add = 100
				OR = {
					original_tag = GRE
					original_tag = ARM
				}
				has_idea = NATO_member
			}
		}
		if = {
			limit = {
				has_equipment = { Inf_equipment > 4999 }
			}
			send_equipment = {
				type = Inf_equipment
				amount = 5000
				target = FROM
			}
		}
		if = {
			limit = {
				has_equipment = { MBT_Equipment > 499 }
			}
			send_equipment = {
				type = MBT_Equipment
				amount = 500
				target = FROM
			}
		}
		if = {
			limit = {
				is_major = yes
				has_equipment = { Inf_equipment < 4999 }
				has_equipment = { MBT_Equipment < 499 }
			}
			ROOT = {
				add_political_power = -200
			}
			FROM = {
				add_political_power = 200
			}
		}
		else_if = {
			limit = {
				has_equipment = { Inf_equipment < 4999 }
				has_equipment = { MBT_Equipment < 499 }
			}
			ROOT = {
				add_political_power = -100
			}
			FROM = {
				add_political_power = 100
			}
		}
		hidden_effect = {
			ARM = {
				country_event = { id = bul_ACDI.4 }
			}
		}
	}
}
country_event = {
	id = bul_ACDI.2
	title = bul_ACDI.2.t
	desc = bul_ACDI.2.d
	picture = GFX_special_forces
	fire_only_once = yes
	is_triggered_only = yes
	option = {
		name = bul_ACDI.2.a
		log = "[GetDateText]: [This.GetName]: bul_ACDI.2.a executed"
	}
}
country_event = {
	id = bul_ACDI.3
	title = bul_ACDI.3.t
	desc = bul_ACDI.3.d
	picture = GFX_special_forces
	fire_only_once = yes
	is_triggered_only = yes
	option = {
		name = bul_ACDI.3.a
		log = "[GetDateText]: [This.GetName]: bul_ACDI.3.a executed"
	}
}
country_event = {
	id = bul_ACDI.4
	title = bul_ACDI.4.t
	desc = bul_ACDI.4.d
	picture = GFX_special_forces
	fire_only_once = yes
	is_triggered_only = yes
	option = {
		name = bul_ACDI.4.a
		log = "[GetDateText]: [This.GetName]: bul_ACDI.4.a executed"
	}
}

country_event = {
	id = bulg.4
	title = bulg.4.t
	desc = bulg.4.d
	is_triggered_only = yes
	fire_only_once = yes
	picture = GFX_simeon_event
	option = {
		name = bulg.4.a
		log = "[GetDateText]: [This.GetName]: bulg.4.a executed"
	}

}

country_event = {
	id = bulg.5
	title = bulg.5.t
	desc = bulg.5.d
	is_triggered_only = yes
	fire_only_once = yes
	picture = GFX_communists_event
	option = {
		name = bulg.5.a
		log = "[GetDateText]: [This.GetName]: bulg.5.a executed"
	}

}

country_event = {
	id = bulg.6
	title = bulg.6.t
	desc = bulg.6.d
	is_triggered_only = yes
	fire_only_once = yes
	picture = GFX_Megali
	option = {
		name = bulg.6.a
		log = "[GetDateText]: [This.GetName]: bulg.6.a executed"
		set_cosmetic_tag = BUL_empire2
		add_political_power = 150
	}
	option = {
		name = bulg.6.b
		log = "[GetDateText]: [This.GetName]: bulg.6.b executed"
		set_cosmetic_tag = BUL_empire
		add_political_power = 150
	}
	option = {
		name = bulg.6.c
		log = "[GetDateText]: [This.GetName]: bulg.6.c executed"
		set_cosmetic_tag = BUL_empire3
		add_political_power = 150
	}

}

country_event = {
	id = bulg.7
	title = bulg.7.t
	desc = bulg.7.d
	is_triggered_only = yes
	fire_only_once = yes
	picture = GFX_bulgaria_street
	option = {
		name = bulg.7.a
		log = "[GetDateText]: [This.GetName]: bulg.7.a executed"
	}
}

country_event = {
	id = bulg.8
	title = bulg.8.t
	desc = bulg.8.d
	is_triggered_only = yes
	fire_only_once = yes
	picture = GFX_bulgaria_street
	option = {
		name = bulg.8.a
		log = "[GetDateText]: [This.GetName]: bulg.8.a executed"
		country_event = { id = bulg.9 days = 15 }
		add_political_power = -50
		add_popularity = {
			ideology = nationalist
			popularity = 0.05
		}
		add_to_variable = { party_pop_array^23 = 0.075 }
		recalculate_party = yes
	}
	option = {
		name = bulg.8.b
		log = "[GetDateText]: [This.GetName]: bulg.8.b executed"
		country_event = { id = bulg.9 days = 15 }
		add_political_power = -50
		add_popularity = {
			ideology = communism
			popularity = 0.05
		}
		add_to_variable = { party_pop_array^4 = 0.075 }
		recalculate_party = yes
	}
}

country_event = {
	id = bulg.9
	title = bulg.9.t
	desc = bulg.9.d
	is_triggered_only = yes
	fire_only_once = yes
	picture = GFX_social_democrats
	option = {
		name = bulg.9.a
		log = "[GetDateText]: [This.GetName]: bulg.9.a executed"
		country_event = { id = bulg.10 days = 30 }
		add_political_power = -50
		add_popularity = {
			ideology = nationalist
			popularity = 0.05
		}
		add_to_variable = { party_pop_array^23 = 0.075 }
		recalculate_party = yes
	}
	option = {
		name = bulg.9.b
		log = "[GetDateText]: [This.GetName]: bulg.9.b executed"
		country_event = { id = bulg.10 days = 30 }
		add_political_power = -50
		add_popularity = {
			ideology = communism
			popularity = 0.05
		}
		add_to_variable = { party_pop_array^4 = 0.075 }
		recalculate_party = yes
	}
}

country_event = {
	id = bulg.10
	title = bulg.10.t
	desc = bulg.10.d
	is_triggered_only = yes
	fire_only_once = yes
	picture = GFX_parlament_old
	option = {
		name = bulg.10.a
		log = "[GetDateText]: [This.GetName]: bulg.10.a executed"
		add_political_power = -50
		add_popularity = {
			ideology = nationalist
			popularity = -0.05
		}
		add_to_variable = { party_pop_array^23 = -0.075 }
		recalculate_party = yes
	}
	option = {
		name = bulg.10.b
		log = "[GetDateText]: [This.GetName]: bulg.10.b executed"
		add_political_power = -50
		add_popularity = {
			ideology = communism
			popularity = -0.05
		}
		add_to_variable = { party_pop_array^4 = -0.075 }
		recalculate_party = yes
	}
}

country_event = {
	id = bulg.11
	title = bulg.11.t
	desc = bulg.11.d
	is_triggered_only = yes
	fire_only_once = yes
	picture = GFX_bulgaria_street
	option = {
		name = bulg.11.a
		log = "[GetDateText]: [This.GetName]: bulg.11.a executed"
		add_opinion_modifier = {
			target = SOV
			modifier = diplomatic_support
		}
		reverse_add_opinion_modifier = {
			target = SOV
			modifier = diplomatic_support
		}
		set_temp_variable = { percent_change = 2 }
		set_temp_variable = { tag_index = SOV }
		set_temp_variable = { influence_target = BUL }
		change_influence_percentage = yes
		set_temp_variable = { receiver_nation = SOV.id }
		set_temp_variable = { sender_nation = BUL.id }
		set_improved_trade_agreement = yes
	}
	option = {
		name = bulg.11.b
		log = "[GetDateText]: [This.GetName]: bulg.11.b executed"
		add_opinion_modifier = {
			target = SOV
			modifier = drama
		}
		reverse_add_opinion_modifier = {
			target = SOV
			modifier = drama
		}
	}
}

country_event = {
	id = bulg.12
	title = bulg.12.t
	desc = bulg.12.d
	is_triggered_only = yes
	fire_only_once = yes
	picture = GFX_bulgaria_street
	option = {
		name = bulg.12.a
		log = "[GetDateText]: [This.GetName]: bulg.12.a executed"
		add_political_power = 100
	}
	option = {
		name = bulg.12.b
		log = "[GetDateText]: [This.GetName]: bulg.12.b executed"
		hidden_effect = {
			clear_array = ruling_party
			clear_array = gov_coalition_array
			add_to_array = { ruling_party = 21 }
			add_to_array = { gov_coalition_array = 2 }
			custom_effect_tooltip = ARM_com1_TT
			set_temp_variable = { current_cons_popularity = party_pop_array^21 }
			multiply_temp_variable = { current_cons_popularity = 0.5 }
			# Remove most of support, retrack it to emerg communist party
			subtract_from_variable = { party_pop_array^6 = current_cons_popularity }
			add_to_variable = { party_pop_array^21 = current_cons_popularity }
			recalculate_party = yes
			update_government_coalition_strength = yes
			update_party_name = yes
			set_coalition_drift = yes
			set_ruling_leader = yes
			set_leader = yes
		}
		create_country_leader = {
			name = "Krasimir Karakachanov"
			desc = "Virgin Gigachad"
			picture = "krasimir_karakachanov.dds"
			expire = "2050.1.1"
			ideology = Nat_populism
			traits = {
				nationalist_Nat_Populism
			}
		}
	}
	option = {
		name = bulg.12.c
		log = "[GetDateText]: [This.GetName]: bulg.12.c executed"
		hidden_effect = {
			clear_array = ruling_party
			clear_array = gov_coalition_array
			add_to_array = { ruling_party = 20 }
			add_to_array = { gov_coalition_array = 2 }
			custom_effect_tooltip = ARM_com1_TT
			set_temp_variable = { current_cons_popularity = party_pop_array^20 }
			multiply_temp_variable = { current_cons_popularity = 0.5 }
			# Remove most of support, retrack it to emerg communist party
			subtract_from_variable = { party_pop_array^6 = current_cons_popularity }
			add_to_variable = { party_pop_array^20 = current_cons_popularity }
			recalculate_party = yes
			update_government_coalition_strength = yes
			update_party_name = yes
			set_coalition_drift = yes
			set_ruling_leader = yes
			set_leader = yes
		}
	}
}

country_event = {
	id = bul_mech.7
	title = bul_mech.7.t
	desc = bul_mech.7.d
	is_triggered_only = yes
	picture = GFX_bulgaria_macedonia
	option = {
		name = bul_mech.7.a
		log = "[GetDateText]: [This.GetName]: bul_mech.7.a executed"
	}
}

country_event = {
	id = bul_mech.8
	title = bul_mech.8.t
	desc = bul_mech.8.d
	is_triggered_only = yes
	picture = GFX_bulgaria_macedonia
	option = {
		name = bul_mech.8.a
		log = "[GetDateText]: [This.GetName]: bul_mech.8.a executed"
	}
}

country_event = {
	id = bul_mech.9
	title = bul_mech.9.t
	desc = bul_mech.9.d
	is_triggered_only = yes
	picture = GFX_georg_specops
	option = {
		name = bul_mech.9.a
		log = "[GetDateText]: [This.GetName]: bul_mech.9.a executed"
	}
}

country_event = {
	id = bul_mech.10
	title = bul_mech.10.t
	desc = bul_mech.10.d
	is_triggered_only = yes
	picture = GFX_bulgaria_macedonia
	option = {
		name = bul_mech.10.a
		log = "[GetDateText]: [This.GetName]: bul_mech.10.a executed"
	}
}

country_event = {
	id = bul_mech.11
	title = bul_mech.11.t
	desc = bul_mech.11.d
	is_triggered_only = yes
	picture = GFX_czub_building
	option = {
		name = bul_mech.11.a
		log = "[GetDateText]: [This.GetName]: bul_mech.11.a executed"
		BUL = {
			country_event = bul_mech.12
		}
	}
	option = {
		name = bul_mech.11.b
		log = "[GetDateText]: [This.GetName]: bul_mech.11.b executed"
		BUL = {
			country_event = bul_mech.13
		}
	}
}

country_event = {
	id = bul_mech.12
	title = bul_mech.12.t
	desc = bul_mech.12.d
	is_triggered_only = yes
	picture = GFX_czub_building
	option = {
		name = bul_mech.12.a
		log = "[GetDateText]: [This.GetName]: bul_mech.12.a executed"
		set_temp_variable = { treasury_change = -7.5 }
		modify_treasury_effect = yes
		set_temp_variable = { int_investment_change = 5 }
		modify_international_investment_effect = yes
		FYR = {
			random_owned_state = {
				add_extra_state_shared_building_slots = 1
				add_building_construction = {
					type = industrial_complex
					level = 1
					instant_build = yes
				}
			}
			add_autonomy_score = { value = -150 }
		}
		set_temp_variable = { temp_opinion = 5 }
		change_small_medium_business_owners_opinion = yes
		set_temp_variable = { temp_opinion = 5 }
		change_international_bankers_opinion = yes
	}
	option = {
		name = bul_mech.12.b
		log = "[GetDateText]: [This.GetName]: bul_mech.12.b executed"
	}
}

country_event = {
	id = bul_mech.13
	title = bul_mech.13.t
	desc = bul_mech.13.d
	is_triggered_only = yes
	picture = GFX_czub_building
	option = {
		name = bul_mech.13.a
		log = "[GetDateText]: [This.GetName]: bul_mech.13.a executed"
	}
}

country_event = {
	id = bul_mech.14
	title = bul_mech.14.t
	desc = bul_mech.14.d
	is_triggered_only = yes
	picture = GFX_report_event_Arrest_3
	option = {
		name = bul_mech.14.a
		log = "[GetDateText]: [This.GetName]: bul_mech.14.a executed"
	}
}

country_event = {
	id = bul_mech.16
	title = bul_mech.16.t
	desc = bul_mech.16.d
	is_triggered_only = yes
	picture = GFX_report_event_Tavolo_Trattative_1
	option = {
		name = bul_mech.16.a
		log = "[GetDateText]: [This.GetName]: bul_mech.16.a executed"
		trigger = {
			tag = FYR
		}
		set_temp_variable = {
			percent_change = 2
		}
		change_domestic_influence_percentage = yes
		add_opinion_modifier = {
			target = BUL
			modifier = drama
		}
		reverse_add_opinion_modifier = {
			target = BUL
			modifier = drama
		}
	}
	option = {
		name = bul_mech.16.b
		log = "[GetDateText]: [This.GetName]: bul_mech.16.b executed"
		trigger = {
			tag = FYR
		}
		add_opinion_modifier = {
			target = BUL
			modifier = diplomatic_support
		}
		reverse_add_opinion_modifier = {
			target = BUL
			modifier = diplomatic_support
		}
	}
	option = {
		name = bul_mech.16.c
		log = "[GetDateText]: [This.GetName]: bul_mech.16.c executed"
		trigger = {
			tag = FYR
		}
		BUL = {
			country_event = bul_mech.12
		}
	}
	option = {
		name = bul_mech.16.a1
		log = "[GetDateText]: [This.GetName]: bul_mech.16.a1 executed"
		trigger = {
			tag = BUL
		}
		set_temp_variable = { percent_change = 2 }
		set_temp_variable = { tag_index = BUL }
		set_temp_variable = { influence_target = FYR }
		change_influence_percentage = yes
		add_opinion_modifier = {
			target = FYR
			modifier = drama
		}
		reverse_add_opinion_modifier = {
			target = FYR
			modifier = drama
		}
	}
	option = {
		name = bul_mech.16.b2
		log = "[GetDateText]: [This.GetName]: bul_mech.16.b2 executed"
		trigger = {
			tag = BUL
		}
		add_opinion_modifier = {
			target = FYR
			modifier = diplomatic_support
		}
		reverse_add_opinion_modifier = {
			target = FYR
			modifier = diplomatic_support
		}
	}
}

country_event = {
	id = bul_mech.17
	title = bul_mech.17.t
	desc = bul_mech.17.d
	is_triggered_only = yes
	picture = GFX_army_arm_old
	option = {
		name = bul_mech.17.a
		log = "[GetDateText]: [This.GetName]: bul_mech.17.a executed"
		trigger = {
			tag = BUL
		}
		ROOT = {
			army_experience = 10
			add_command_power = 10
		}
	}
	option = {
		name = bul_mech.17.a1
		log = "[GetDateText]: [This.GetName]: bul_mech.17.a1 executed"
		trigger = {
			tag = FYR
		}
		ROOT = {
			army_experience = 10
			add_command_power = 10
		}
	}
	option = {
		name = bul_mech.17.b
		log = "[GetDateText]: [This.GetName]: bul_mech.17.b executed"
		trigger = {
			tag = BUL
		}
		ROOT = {
			army_experience = 5
			add_command_power = 5
		}
		FYR = {
			country_event = bul_mech.18
		}
	}
	option = {
		name = bul_mech.17.b1
		log = "[GetDateText]: [This.GetName]: bul_mech.17.b1 executed"
		trigger = {
			tag = FYR
		}
		ROOT = {
			army_experience = 5
			add_command_power = 5
		}
		BUL = {
			country_event = bul_mech.18
		}
	}
}

country_event = {
	id = bul_mech.18
	title = bul_mech.18.t
	desc = bul_mech.18.d
	is_triggered_only = yes
	picture = GFX_army_arm_old
	option = {
		name = bul_mech.18.a
		log = "[GetDateText]: [This.GetName]: bul_mech.18.a executed"
		ROOT = {
			army_experience = 5
			add_command_power = 5
		}
	}
	option = {
		name = bul_mech.18.b
		log = "[GetDateText]: [This.GetName]: bul_mech.18.b executed"
	}
}

country_event = {
	id = bul_mech.19
	title = bul_mech.19.t
	desc = bul_mech.19.d
	is_triggered_only = yes
	picture = GFX_bulgaria_macedonia
	option = {
		name = bul_mech.19.a
		log = "[GetDateText]: [This.GetName]: bul_mech.19.a executed"
		set_temp_variable = { treasury_change = -7.5 }
		modify_treasury_effect = yes
		add_offsite_building = { type = industrial_complex level = 1 }
		set_temp_variable = { percent_change = 3.5 }
		set_temp_variable = { tag_index = BUL }
		set_temp_variable = { influence_target = FYR }
		change_influence_percentage = yes
		add_ideas = BUL_FYR_friendship
		add_ideas = developmentalism
		set_country_flag = bul_fyr_friendship
		FYR = {
			set_temp_variable = { treasury_change = -7.5 }
			modify_treasury_effect = yes
			add_offsite_building = { type = industrial_complex level = 1 }
			set_temp_variable = { percent_change = 3.5 }
			set_temp_variable = { tag_index = FYR }
			set_temp_variable = { influence_target = BUL }
			change_influence_percentage = yes
			add_ideas = BUL_FYR_friendship
			add_ideas = developmentalism
			set_country_flag = bul_fyr_friendship
		}
	}
	option = {
		name = bul_mech.19.b
		log = "[GetDateText]: [This.GetName]: bul_mech.19.b executed"
	}
}

country_event = {
	id = bul_mech.20
	title = bul_mech.20.t
	desc = bul_mech.20.d
	is_triggered_only = yes
	picture = GFX_bulgaria_macedonia
	option = {
		name = bul_mech.20.a
		log = "[GetDateText]: [This.GetName]: bul_mech.20.a executed"
	}
}

country_event = {
	id = bul_mech.21
	title = bul_mech.21.t
	desc = bul_mech.21.d
	is_triggered_only = yes
	picture = GFX_bulgaria_macedonia
	option = {
		name = bul_mech.21.a
		log = "[GetDateText]: [This.GetName]: bul_mech.21.a executed"
	}
}

country_event = {
	id = bulg.13
	title = bulg.13.t
	desc = bulg.13.d
	is_triggered_only = yes
	fire_only_once = yes
	picture = GFX_powerplant
	trigger = {
		original_tag = BUL
	}
	option = {
		name = bulg.13.a
		log = "[GetDateText]: [This.GetName]: bulg.13.a executed"

	}
}

country_event = {
	id = bulg.14
	title = bulg.14.t
	desc = bulg.14.d
	is_triggered_only = yes
	fire_only_once = yes
	picture = GFX_TUR_generic
	trigger = {
		original_tag = BUL
		NOT = { has_country_flag = BUL_no_turks }
		BUL = {
			has_idea = BUL_unsatisf_rurk_minority
		}
	}
	option = {
		name = bulg.14.a
		log = "[GetDateText]: [This.GetName]: bulg.13.a executed"
		trigger = {
			original_tag = BUL
			NOT = { has_country_flag = BUL_no_turks }
			BUL = {
				has_idea = BUL_unsatisf_rurk_minority
			}
		}
		add_stability = -0.15
		set_temp_variable = { party_index = 12 }
		set_temp_variable = { party_popularity_increase = 0.02 }
		add_relative_party_popularity = yes
		hidden_effect = {
			country_event = {
				id = bulg.14
				days = 100
			}
		}
	}

	option = {
		name = bulg.14.a
		log = "[GetDateText]: [This.GetName]: bulg.13.a executed"
		trigger = {
			original_tag = BUL
			has_country_flag = BUL_no_turks
		}
		add_stability = 0.15
	}
}

country_event = {
	id = bulg.15
	title = bulg.15.t
	desc = bulg.15.d
	is_triggered_only = yes
	fire_only_once = yes
	picture = GFX_TUR_generic

	option = {
		name = bulg.15.a
		log = "[GetDateText]: [This.GetName]: bulg.15.a executed"
		fuck_ai_bulgaria_passive = yes
	}
}
#Export Choose
country_event = {
	id = bulg_export.1
	title = bulg_export.1.t
	desc = bulg_export.1.d
	picture = GFX_weapons_arms1

	is_triggered_only = yes
	#PWO
	option = {
		name = bulg_export.1.a
		log = "[GetDateText]: [This.GetName]: bulg_export.1.a executed"
		add_equipment_to_stockpile = {
			type = Anti_Air_0
			amount = 40
			producer = BUL
		}
		BUL = {
				set_temp_variable = { treasury_change = 3 }
				modify_treasury_effect = yes
				add_equipment_to_stockpile = {
					type = Anti_Air_0
					amount = -40
					producer = BUL
				}
		}
			set_temp_variable = { treasury_change = -3 }
			modify_treasury_effect = yes
	}
	#SNARYAGA
	option = {
		name = bulg_export.1.b
		log = "[GetDateText]: [This.GetName]: bulg_export.1.b executed"
			add_equipment_to_stockpile = {
				type = command_control_equipment
				amount = 70
				producer = BUL
			}

		BUL = {
			set_temp_variable = { treasury_change = 4.50 }
			modify_treasury_effect = yes
			add_equipment_to_stockpile = {
				type = command_control_equipment
				amount = -70
				producer = BUL
			}
		}
			set_temp_variable = { treasury_change = -4.50 }
			modify_treasury_effect = yes
	}
	#AWTOMATS
	option = {
			name = bulg_export.1.c
			log = "[GetDateText]: [This.GetName]: bulg_export.1.c executed"
			set_temp_variable = { treasury_change = -2.25 }
			modify_treasury_effect = yes
			add_equipment_to_stockpile = {
				type = infantry_weapons1
				amount = 900
				producer = BUL
			}
			BUL = {
				add_equipment_to_stockpile = {
					type = infantry_weapons1
					amount = -900
					producer = BUL
				}
				set_temp_variable = { treasury_change = 2.25 }
				modify_treasury_effect = yes
			}
	}
	#PTRK
	option = {
		name = bulg_export.1.g
		log = "[GetDateText]: [This.GetName]: bulg_export.1.g executed"
		set_temp_variable = { treasury_change = -3.4 }
		modify_treasury_effect = yes
		add_equipment_to_stockpile = {
			type = Anti_tank_0
			amount = 50
			producer = BUL
		}
		BUL = {
			add_equipment_to_stockpile = {
				type = Anti_tank_0
				amount = -50
				producer = BUL
			}
			set_temp_variable = { treasury_change = 3.4 }
			modify_treasury_effect = yes
		}
	}
	option = {
		name = bulg_export.1.e
		log = "[GetDateText]: [This.GetName]: bulg_export.1.e executed"
	}
}
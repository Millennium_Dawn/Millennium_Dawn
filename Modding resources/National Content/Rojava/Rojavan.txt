ideas = {
	country = {
		rojava_kurds_immigrating = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea rojava_kurds_immigrating" }

			allowed = {
				always = no
			}

			modifier = {
				monthly_population = 0.25
			}
		}

		rojava_international_volunteers = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea rojava_international_volunteers" }

			allowed = {
				always = no
			}

			modifier = {
				weekly_manpower = 50
				drift_defence_factor = -0.1
			}
		}

		rojava_international_volunteers2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea rojava_international_volunteers2" }

			allowed = {
				always = no
			}

			modifier = {
				weekly_manpower = 100
				drift_defence_factor = -0.1
				democratic_drift = 0.02
				neutrality_drift = 0.02
			}
		}

		rojava_education_bonus = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea rojava_education_bonus" }

			allowed = {
				always = no
			}

			modifier = {
				custom_modifier_tooltip = 15_reduction_in_education_cost_tt
				research_speed_factor = -0.05
			}
		}

		rojava_raqqa_offensive = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea rojava_raqqa_offensive" }

			allowed = {
				always = no
			}

			modifier = {
				army_core_attack_factor = 0.2
				war_support_factor = 0.1
			}
		}

		rojava_centralized_militias = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea rojava_centralized_militias" }

			allowed = {
				always = no
			}

			modifier = {
				conscription = 0.005
			}
		}

		rojava_officer_corps = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea rojava_centralized_militias" }

			allowed = {
				always = no
			}

			modifier = {
				army_leader_start_level = 1
				army_morale_factor = 0.25
			}
		}
	}
}
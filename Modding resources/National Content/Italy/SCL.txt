characters={
	SCL_air_chief={
		portraits={
			army={
				small="gfx/leaders/SCL/small/SCL_air_chief_small.dds"
				large="gfx/leaders/SCL/SCL_air_chief.dds"
			}
		}
		advisor={
			slot = air_chief
			idea_token = SCL_air_chief
			allowed = {
				original_tag = SCL
			}
			traits = {
				air_strategic_bombing_1
			}
			cost = 100
			ai_will_do = {
				factor = 1
			}
		}
	}
}

add_namespace = eac
add_namespace = eac_vote
add_namespace = eaf


country_event = {
	id = eac.0
	title = eac.0.t
	desc = eac.0.d
	picture = GFX_EAF_event_ken_tnz_uga


	is_triggered_only = yes

	fire_only_once = no

	option = {
		name = eac.0.a
		add_political_power = 1
	}
}

news_event = {
	id = eac.1
	title = eac.1.t
	desc = eac.1.d
	picture = GFX_great_african_war

	is_triggered_only = yes

	fire_only_once = no

	option = {
		log = "[GetDateText]: [Root.GetName]: event eac.1.a"
		name = eac.1.a

	}
}

news_event = {
	id = eac.2
	title = eac.2.t
	desc = eac.2.d
	picture = GFX_great_african_war

	is_triggered_only = yes

	fire_only_once = no

	option = {
		log = "[GetDateText]: [Root.GetName]: event eac.2.a"
		name = eac.2.a

	}
}

news_event = {
	id = eac.3
	title = eac.3.t
	desc = eac.3.d
	picture = GFX_great_african_war

	is_triggered_only = yes

	fire_only_once = no

	option = {
		log = "[GetDateText]: [Root.GetName]: event eac.3.a"
		name = eac.3.a

	}
}

news_event = {
	id = eac.4
	title = eac.4.t
	desc = eac.4.d
	picture = GFX_great_african_war

	is_triggered_only = yes

	fire_only_once = no

	option = {
		log = "[GetDateText]: [Root.GetName]: event eac.4.a"
		name = eac.4.a
		add_ideas = eac_shilling
	}
}

news_event = {
	id = eac.5
	title = eac.5.t
	desc = eac.5.d
	picture = GFX_great_african_war

	is_triggered_only = yes

	fire_only_once = no

	option = {
		log = "[GetDateText]: [Root.GetName]: event eac.5.a"
		name = eac.5.a
	}
}

news_event = {
	id = eac.6
	title = eac.6.t
	desc = eac.6.d
	picture = GFX_great_african_war

	is_triggered_only = yes

	fire_only_once = no

	option = {
		log = "[GetDateText]: [Root.GetName]: event eac.6.a"
		name = eac.6.a
	}
}

news_event = {
	id = eac.7
	title = eac.7.t
	desc = eac.7.d
	picture = GFX_great_african_war

	is_triggered_only = yes

	fire_only_once = no

	option = {
		name = eac.7.a
	}
}

news_event = {
	id = eac.8
	title = eac.8.t
	desc = eac.8.d
	picture = GFX_great_african_war

	is_triggered_only = yes

	fire_only_once = no

	option = {
		name = eac.8.a
	}
}

news_event = {
	id = eac.9
	title = eac.9.t
	desc = eac.9.d
	picture = GFX_great_african_war

	is_triggered_only = yes

	fire_only_once = no

	option = {
		name = eac.9.a
	}
}

news_event = {
	id = eac.10
	title = eac.10.t
	desc = eac.10.d
	picture = GFX_great_african_war

	is_triggered_only = yes

	fire_only_once = no

	option = {
		name = eac.10.a
	}
}

news_event = {
	id = eac.11
	title = eac.11.t
	desc = eac.11.d
	picture = GFX_great_african_war

	is_triggered_only = yes

	fire_only_once = no

	major = yes

	option = {
		name = eac.11.a
	}
}

news_event = {
	id = eac.12
	title = eac.12.t
	desc = eac.12.d
	picture = GFX_great_african_war

	is_triggered_only = yes

	fire_only_once = no

	major = yes

	option = {
		name = eac.12.a
	}
}

news_event = {
	id = eac.13
	title = eac.13.t
	desc = eac.13.d
	picture = GFX_great_african_war

	is_triggered_only = yes

	fire_only_once = no

	major = yes

	option = {
		name = eac.13.a
	}
}

news_event = {
	id = eac.15
	title = eac.15.t
	desc = eac.15.d
	picture = GFX_great_african_war

	hidden = yes

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = eac.15.a
	}
}

news_event = {
	id = eac.16
	title = eac.16.t
	desc = eac.16.d
	picture = GFX_great_african_war

	hidden = yes

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = eac.16.a
	}
}

country_event = {
	id = eac.17
	title = eac.17.t
	desc = eac.17.d
	picture = GFX_great_african_war

	hidden = yes

	is_triggered_only = yes

	option = {
		name = eac.17.a
		every_country = {
			limit = {
				has_idea = idea_eac_member_state
			}
			give_military_access = ROOT
		}

	}
}

country_event = {
	id = eac.18
	title = eac.18.t
	desc = eac.18.d
	picture = GFX_great_african_war

	hidden = yes

	is_triggered_only = yes

	option = {
		name = eac.18.a
		USA = {
			set_country_flag = trade_agreement@ROOT
		}
	}
}

country_event = {
	id = eac.19
	title = eac.19.t
	desc = eac.19.d
	picture = GFX_great_african_war

	hidden = yes

	is_triggered_only = yes

	option = {
		name = eac.19.a
		CHI = {
			set_country_flag = trade_agreement@ROOT
		}
	}
}

country_event = {
	id = eac.20
	title = eac.20.t
	desc = eac.20.d
	picture = GFX_great_african_war

	hidden = yes

	is_triggered_only = yes

	option = {
		name = eac.20.a
		every_country = {
			limit = {
				has_idea = idea_eac_member_state
			}
			add_opinion_modifier = { target = ROOT modifier = mutual_trade_agreement }
			add_opinion_modifier = { target = ROOT modifier = mutual_trade_opinion }
			reverse_add_opinion_modifier = { target = ROOT modifier = mutual_trade_opinion }
			reverse_add_opinion_modifier = { target = ROOT modifier = mutual_trade_agreement }
			set_country_flag = trade_agreement@ROOT
			country_event = { id = eac.25 hours = 6 }
		}
		country_event = { id = eac.25 hours = 6 }
	}
}

country_event = {
	id = eac.21
	title = eac.21.t
	desc = eac.21.d
	picture = GFX_great_african_war

	hidden = yes

	is_triggered_only = yes

	option = {
		name = eac.21.a
		every_country = {
			limit = {
				has_idea = EU_member
			}
			add_opinion_modifier = { target = ROOT modifier = mutual_trade_agreement }
			add_opinion_modifier = { target = ROOT modifier = mutual_trade_opinion }
			reverse_add_opinion_modifier = { target = ROOT modifier = mutual_trade_opinion }
			reverse_add_opinion_modifier = { target = ROOT modifier = mutual_trade_agreement }
			set_country_flag = trade_agreement@ROOT
			country_event = { id = eac.22 hours = 6 }
		}
		country_event = { id = eac.22 hours = 6 }
	}
}

country_event = {
	id = eac.22
	title = eac.22.t
	desc = eac.22.d
	picture = GFX_africa_eu_meeting

	is_triggered_only = yes

	option = {
		name = eac.22.a
		custom_effect_tooltip = EAC_eu_trade_agreement_tt
		hidden_effect = {
			if = {
				limit = {
					has_idea = EU_member
				}
				every_country = {
					limit = {
						has_idea = idea_eac_member_state
					}
					add_opinion_modifier = { target = ROOT modifier = mutual_trade_agreement }
					add_opinion_modifier = { target = ROOT modifier = mutual_trade_opinion }
					reverse_add_opinion_modifier = { target = ROOT modifier = mutual_trade_opinion }
					reverse_add_opinion_modifier = { target = ROOT modifier = mutual_trade_agreement }
					set_country_flag = trade_agreement@ROOT
				}
			}
		}
	}
}

country_event = {
	id = eac.23
	title = eac.23.t
	desc = eac.23.d
	picture = GFX_africa_us_meeting

	is_triggered_only = yes

	option = {
		name = eac.23.a
		custom_effect_tooltip = EAC_us_trade_agreement_tt
	}
}

country_event = {
	id = eac.24
	title = eac.24.t
	desc = eac.24.d
	picture = GFX_africa_china_meeting

	is_triggered_only = yes

	option = {
		name = eac.24.a
		custom_effect_tooltip = EAC_china_trade_agreement_tt
	}
}

country_event = {
	id = eac.25
	title = eac.25.t
	desc = eac.25.d
	picture = GFX_african_meeting

	is_triggered_only = yes

	option = {
		name = eac.25.a
		custom_effect_tooltip = EAC_africa_trade_agreement_tt
		hidden_effect = {
			if = {
				limit = {
					has_idea = idea_eac_member_state
				}
				every_neighbor_country = {
					limit = {
						has_idea = AU_member
					}
					set_country_flag = trade_agreement@ROOT
				}
			}
		}
	}
}


country_event = {
	id = eac.50
	title = eac.50.t
	desc = eac.50.d
	picture = GFX_EAF_event_ken_tnz_uga


	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = eac.50.a
		add_stability = -0.03
		add_political_power = -100
	}
}





country_event = {
	id = eaf.1
	title = eaf.1.t
	desc = eaf.1.d
	picture = GFX_EAF_event_ken_tnz_uga



	is_triggered_only = yes

	option = {
		name = eaf.1.a
		set_cosmetic_tag = EAF_UNIFIED
	}
}












news_event = {
	id = eaf.2
	title = eaf.2.t
	desc = eaf.2.d
	picture = GFX_EAF_event_signing

	major = yes

	is_triggered_only = yes

	option = {
		name = eaf.2.a
		ai_chance = { factor = 100 }
		set_country_flag = eac_meeting_underway
	}
}


country_event = {
	id = eaf.3
	title = eaf.3.t
	desc = eaf.3.d
	picture = GFX_eaf_event_001
	
	fire_only_once = yes

	is_triggered_only = yes
	

		option = {
		name = eaf.3.a
		ai_chance = { factor = 94 }
		add_stability = 0.1
		add_war_support = 0.05
		set_capital = 256
	}

		option = {
		name = eaf.3.b
		ai_chance = { factor = 1 }
		add_stability = 0.1
		add_war_support = 0.05
		set_capital = 255
	}

		option = {
		name = eaf.3.c
		ai_chance = { factor = 1 }
		add_stability = 0.1
		add_war_support = 0.05
		set_capital = 242
	}

		option = {
		name = eaf.3.no
		ai_chance = { factor = 4 }
		country_event = { id = eaf.4 }
		}

	}

country_event = {
	id = eaf.4
	title = eaf.4.t
	desc = eaf.4.d
	picture = GFX_eaf_event_001

	fire_only_once = yes

	is_triggered_only = yes

		option = {
		name = eaf.4.a
		ai_chance = { factor = 25 }
		add_stability = 0.1
		add_war_support = 0.05
		set_capital = 250
	}

		option = {
		name = eaf.4.b
		ai_chance = { factor = 25 }
		add_stability = 0.1
		add_war_support = 0.05
		set_capital = 251
	}

		option = {
		name = eaf.4.c
		ai_chance = { factor = 25 }
		add_stability = 0.1
		add_war_support = 0.05
		set_capital = 257
	}

		option = {
		name = eaf.4.d
		ai_chance = { factor = 25 }
		add_stability = 0.1
		add_war_support = 0.05
		set_capital = 227
		trigger = {
			227 = { is_owned_by = TNZ }
		}

	}

}

country_event = {
	id = eaf.6
	title = eaf.6.t
	desc = eaf.6.d
	picture = GFX_EAF_event_ken_tnz_uga


	is_triggered_only = yes

	option = {
		name = eaf.6.a
		ai_chance = { factor = 100 }
		set_cosmetic_tag = EAF_UNIFIED
	}

	option = {
		name = eaf.6.b
		ai_chance = { factor = 0 }
		add_political_power = -1
	}
}

country_event = {
	id = eaf.7
	title = eaf.7.t
	desc = eaf.7.d
	picture = GFX_stock_market

	is_triggered_only = yes

	option = {
		name = eaf.7.a
		log = "[GetDateText]: [Root.GetName]: event eaf.7.a"
		if = {
			limit = {
					NOT = {
						has_idea = international_bankers
					}
					has_idea = power_vacuum_3
			}
			swap_ideas = { remove_idea = power_vacuum_3 add_idea = international_bankers }
		}
		if = {
			limit = {
					NOT = {
						has_idea = power_vacuum_3
						has_idea = international_bankers
					}
					has_idea = power_vacuum_2
			}
			swap_ideas = { remove_idea = power_vacuum_2 add_idea = international_bankers }
		}
		if = {
			limit = {
				NOT = {
					has_idea = power_vacuum_2
					has_idea = power_vacuum_3
					has_idea = international_bankers
				}
				has_idea = power_vacuum_1
			}
			swap_ideas = { remove_idea = power_vacuum_1 add_idea = international_bankers }
		}
		ai_chance = { factor = 100 }
	}

	option = {
		name = eaf.7.b
		log = "[GetDateText]: [Root.GetName]: event eaf.7.b"
		increase_centralization = yes
		add_political_power = 100
		ai_chance = { factor = 0 }
	}
}





country_event = {
	id = eaf.8
	title = eaf.8.t
	desc = eaf.8.d
	picture = GFX_EAF_event_ken_tnz_uga



	is_triggered_only = yes

	option = {
		name = eaf.8.a

		if = {
			limit = {
				has_full_control_of_state = 250
			}
			create_field_marshal = {
				name = "Paul Kagame"
				picture = "Paul_Kagame.dds"
				traits = { trickster commando jungle_rat war_hero }
				skill = 5
				attack_skill = 5
				defense_skill = 2
				planning_skill = 3
				logistics_skill = 5
			}
		}

		if = {
			limit = {
				has_full_control_of_state = 251
			}
			create_field_marshal = {
				name = "Pierre Buyoya"
				picture = "pierre_buyoya.dds"
				traits = { jungle_rat war_hero }
				skill = 4
				attack_skill = 3
				defense_skill = 4
				planning_skill = 2
				logistics_skill = 4
			}
		}

		if = {
			limit = {
				has_full_control_of_state = 256
			}
			create_field_marshal = {
				name = "David 'General Mutukula' Musuguri"
				picture = "David_Musuguri.dds"
				traits = { old_guard war_hero }
				skill = 4
				attack_skill = 3
				defense_skill = 2
				planning_skill = 5
				logistics_skill = 4
			}
		}

		random_army_leader = {
			limit = {
				skill < 3
				NOT = { has_trait = panzer_leader }
			}

			add_skill_level = 1
			add_attack = 1
			add_defense = 1
			add_planning = 1
			add_logistics = 1

			add_unit_leader_trait = panzer_leader
		}

		trigger = {
			has_completed_focus = EAF_east_african_army
		}
	}

	option = {
		name = eaf.8.b

		if = {
			limit = {
				has_full_control_of_state = 241
			}
			create_navy_leader = {
				name = "Jimson Longiro Mutai"
				picture = "Jimson_Mutai.dds"
				traits = { spotter }
				skill = 3
				coordination_skill = 3
				maneuvering_skill = 3
				defense_skill = 2
				attack_skill = 3
			}
		}

		if = {
			limit = {
				has_full_control_of_state = 257
			}
			create_navy_leader = {
				name = "Richard Mutayoba Makanzo"
				picture = "Richard_Makanzo.dds"
				traits = { seawolf }
				skill = 3
				coordination_skill = 2
				maneuvering_skill = 3
				defense_skill = 1
				attack_skill = 3
			}
		}

		random_army_leader = {
			limit = {
				skill < 3
				NOT = { has_trait = naval_invader }
			}

			add_skill_level = 1
			add_attack = 1
			add_planning = 1
			add_logistics = 1

			add_unit_leader_trait = naval_invader
		}

		trigger = {
			has_completed_focus = EAF_east_african_navy
		}
	}

	option = {
		name = eaf.8.c
		if = {
			limit = {
				has_full_control_of_state = 241
			}
			create_field_marshal = {
				name = "James Kabarebe"
				picture = "Portrait_James_Kabarebe.dds"
				traits = { trickster commando trait_reckless war_hero }
				skill = 5
				attack_skill = 5
				defense_skill = 2
				planning_skill = 2
				logistics_skill = 5
			}
		}

		if = {
			limit = {
				has_full_control_of_state = 241
			}
			create_navy_leader = {
				name = "Jimson Longiro Mutai"
				picture = "Jimson_Mutai.dds"
				traits = { air_controller }
				skill = 3
				coordination_skill = 3
				maneuvering_skill = 2
				defense_skill = 3
				attack_skill = 2
			}
		}

		random_army_leader = {
			limit = {
				skill < 3
				NOT = { has_trait = commando }
			}

			add_skill_level = 1
			add_attack = 1
			add_planning = 1
			add_logistics = 1

			add_unit_leader_trait = commando
		}


		trigger = {
			has_completed_focus = EAF_east_african_airforce
		}
	}
}

country_event = {
	id = eaf.9
	title = eaf.9.t
	desc = eaf.9.d
	picture = GFX_handgun

	is_triggered_only = yes

	option = {
		name = eaf.9.a
		log = "[GetDateText]: [Root.GetName]: event eaf.9.a"
		increase_military_spending = yes
		ai_chance = { factor = 100 }
	}
	option = {
		name = eaf.9.b
		log = "[GetDateText]: [Root.GetName]: event eaf.9.b"
		decrease_military_spending = yes
		ai_chance = { factor = 0 }
	}
	option = {
		name = eaf.9.c
		log = "[GetDateText]: [Root.GetName]: event eaf.9.c"
		add_political_power = 50
		ai_chance = { factor = 0 }
	}
	option = {
		name = eaf.9.e
		log = "[GetDateText]: [Root.GetName]: event eaf.9.e"
		if = {
			limit = {
				NOT = {
					has_idea = the_military
				}
				has_idea = power_vacuum_3
			}
			swap_ideas = { remove_idea = power_vacuum_3 add_idea = the_military }
		}
		if = {
			limit = {
				NOT = {
					has_idea = power_vacuum_3
					has_idea = the_military
				}
				has_idea = power_vacuum_2
			}
			swap_ideas = { remove_idea = power_vacuum_2 add_idea = the_military }
		}
		if = {
			limit = {
				NOT = {
					has_idea = power_vacuum_2
					has_idea = power_vacuum_3
					has_idea = the_military
				}
				has_idea = power_vacuum_1
			}
			swap_ideas = { remove_idea = power_vacuum_1 add_idea = the_military }
		}
		if = {
			limit = {
				has_idea = defence_06
			}
			swap_ideas = { remove_idea = defence_00 add_idea = defence_07 }
		}
		if = {
			limit = {
				has_idea = defence_05
			}
			swap_ideas = { remove_idea = defence_00 add_idea = defence_07 }
		}
		if = {
			limit = {
				has_idea = defence_04
			}
			swap_ideas = { remove_idea = defence_00 add_idea = defence_07 }
		}
		if = {
			limit = {
				has_idea = defence_03
			}
			swap_ideas = { remove_idea = defence_00 add_idea = defence_07 }
		}
		if = {
			limit = {
				has_idea = defence_02
			}
			swap_ideas = { remove_idea = defence_00 add_idea = defence_07 }
		}
		if = {
			limit = {
				has_idea = defence_01
			}
			swap_ideas = { remove_idea = defence_00 add_idea = defence_07 }
		}
		if = {
			limit = {
				has_idea = defence_00
			}
			swap_ideas = { remove_idea = defence_00 add_idea = defence_07 }
		}
		trigger = {
			NOT = {
				has_idea = defence_07
				has_idea = defence_08
				has_idea = defence_09
			}
		}
		add_war_support = 0.02
		add_political_power = -100
		ai_chance = { factor = 0 }
	}
}

country_event = {
	id = eaf.10
	title = eaf.10.t
	desc = eaf.10.d
	picture = GFX_stock_market

	is_triggered_only = yes

	option = {
		name = eaf.10.a
		log = "[GetDateText]: [Root.GetName]: event eaf.10.a"
		remove_ideas = farmers
		remove_ideas = maritime_industry
		remove_ideas = the_military
		remove_ideas = fossil_fuel_industry
		remove_ideas = defense_industry
		remove_ideas = landowners
		remove_ideas = intelligence_community
		remove_ideas = labour_unions
		remove_ideas = small_medium_business_owners
		remove_ideas = industrial_conglomerates
		remove_ideas = international_bankers
		add_ideas = power_vacuum_1
		add_ideas = power_vacuum_2
		add_ideas = power_vacuum_3
		ai_chance = { factor = 100 }
	}

	option = {
		name = eaf.10.b
		log = "[GetDateText]: [Root.GetName]: event eaf.10.b"
		increase_centralization = yes
		add_political_power = 300
		ai_chance = { factor = 0 }
	}
}

country_event = {
	id = eaf.12
	title = eaf.12.t
	desc = eaf.12.d
	picture = GFX_stock_market

	is_triggered_only = yes

	option = {
		name = eaf.12.a
		log = "[GetDateText]: [Root.GetName]: event eaf.12.a"
		if = {
			limit = {
				has_idea = USA_usaid
			}
			remove_ideas = USA_usaid
		}
		custom_effect_tooltip = foreign_independence_20_NIG_TT
		hidden_effect = {
			add_to_variable = { domestic_influence_amount = 20 }
			recalculate_influence = yes
		}
		add_stability = -0.01
		ai_chance = { factor = 1 }
	}

	option = {
		name = eaf.12.b
		log = "[GetDateText]: [Root.GetName]: event eaf.12.b"
		custom_effect_tooltip = TNZ_usa_gain_20_influence_tt
		usa_root_gain_20_influence = yes
		ai_chance = { factor = 0 }
	}
}

country_event = {
	id = eaf.13
	title = eaf.13.t
	desc = eaf.13.d
	picture = GFX_african_meeting2

	is_triggered_only = yes

	option = {
		name = eaf.13.a
		log = "[GetDateText]: [Root.GetName]: event eaf.13.a"
		country_event = eaf.14
		ai_chance = { factor = 1 }
	}
}

country_event = {
	id = eaf.14
	title = eaf.14.t
	desc = eaf.14.d
	picture = GFX_african_meeting3

	is_triggered_only = yes

	option = {
		name = eaf.14.a
		log = "[GetDateText]: [Root.GetName]: event eaf.14.a"
		random_list = {
			70 = {
				country_event = eaf.15
			}
			30 = {
				country_event = eaf.16
			}
		}
		if = {
			limit = {
				NOT = {
					has_idea = labour_unions
				}
				has_idea = power_vacuum_3
			}
			swap_ideas = { remove_idea = power_vacuum_3 add_idea = labour_unions }
		}
		if = {
			limit = {
				NOT = {
					has_idea = power_vacuum_3
					has_idea = labour_unions
				}
				has_idea = power_vacuum_2
			}
			swap_ideas = { remove_idea = power_vacuum_2 add_idea = labour_unions }
		}
		if = {
			limit = {
				NOT = {
					has_idea = power_vacuum_2
					has_idea = power_vacuum_3
					has_idea = labour_unions
				}
				has_idea = power_vacuum_1
			}
			swap_ideas = { remove_idea = power_vacuum_1 add_idea = labour_unions }
		}
		ai_chance = { factor = 1 }
	}

	option = {
		name = eaf.14.b
		log = "[GetDateText]: [Root.GetName]: event eaf.14.b"
		random_list = {
			70 = {
				country_event = eaf.17
			}
			30 = {
				country_event = eaf.18
			}
		}
		if = {
			limit = {
				NOT = {
					has_idea = industrial_conglomerates
				}
				has_idea = power_vacuum_3
			}
			swap_ideas = { remove_idea = power_vacuum_3 add_idea = industrial_conglomerates }
		}
		if = {
			limit = {
				NOT = {
					has_idea = power_vacuum_3
					has_idea = industrial_conglomerates
				}
				has_idea = power_vacuum_2
			}
			swap_ideas = { remove_idea = power_vacuum_2 add_idea = industrial_conglomerates }
		}
		if = {
			limit = {
				NOT = {
					has_idea = power_vacuum_2
					has_idea = power_vacuum_3
					has_idea = industrial_conglomerates
				}
				has_idea = power_vacuum_1
			}
			swap_ideas = { remove_idea = power_vacuum_1 add_idea = industrial_conglomerates }
		}
		ai_chance = { factor = 0 }
	}
	option = {
		name = eaf.14.c
		log = "[GetDateText]: [Root.GetName]: event eaf.14.c"
		custom_effect_tooltip = EAF_both_con_uni_kicked_tt
		add_political_power = -10
		random_list = {
			60 = {
				country_event = eaf.19
			}
			20 = {
				country_event = eaf.16
			}
			20 = {
				country_event = eaf.18
			}
		}
		ai_chance = { factor = 1 }
	}
	option = {
		name = eaf.14.e
		log = "[GetDateText]: [Root.GetName]: event eaf.14.e"
		custom_effect_tooltip = EAF_both_con_uni_pleased_tt
		add_political_power = -100
		swap_ideas = { remove_idea = power_vacuum_2 add_idea = labour_unions }
		swap_ideas = { remove_idea = power_vacuum_1 add_idea = industrial_conglomerates }
		country_event = eaf.19
		trigger = {
			has_idea = power_vacuum_2
		}
		ai_chance = { factor = 1 }
	}
}

country_event = {
	id = eaf.15
	title = eaf.15.t
	desc = eaf.15.d
	picture = GFX_african_meeting3

	is_triggered_only = yes

	option = {
		name = eaf.15.a
		log = "[GetDateText]: [Root.GetName]: event eaf.15.a"
		add_political_power = -10
		country_event = eaf.19
		ai_chance = { factor = 1 }
	}
}

country_event = {
	id = eaf.16
	title = eaf.16.t
	desc = eaf.16.d
	picture = GFX_african_meeting3

	is_triggered_only = yes

	option = {
		name = eaf.16.a
		log = "[GetDateText]: [Root.GetName]: event eaf.16.a"
		add_political_power = -50
		add_stability = -0.03
		country_event = eaf.19
		ai_chance = { factor = 1 }
	}
}

country_event = {
	id = eaf.17
	title = eaf.17.t
	desc = eaf.17.d
	picture = GFX_african_meeting3

	is_triggered_only = yes

	option = {
		name = eaf.17.a
		log = "[GetDateText]: [Root.GetName]: event eaf.17.a"
		add_political_power = -10
		country_event = eaf.19
		ai_chance = { factor = 1 }
	}
}

country_event = {
	id = eaf.18
	title = eaf.18.t
	desc = eaf.18.d
	picture = GFX_african_meeting3

	is_triggered_only = yes

	option = {
		name = eaf.18.a
		log = "[GetDateText]: [Root.GetName]: event eaf.18.a"
		add_political_power = -50
		add_stability = -0.03
		country_event = eaf.19
		ai_chance = { factor = 1 }
	}
}

country_event = {
	id = eaf.19
	title = eaf.19.t
	desc = eaf.19.d
	picture = GFX_african_meeting3

	is_triggered_only = yes

	option = {
		name = eaf.19.a
		log = "[GetDateText]: [Root.GetName]: event eaf.19.a"
		if = {
			limit = {
				NOT = {
					has_idea = landowners
				}
				has_idea = power_vacuum_3
			}
			swap_ideas = { remove_idea = power_vacuum_3 add_idea = landowners }
		}
		if = {
			limit = {
				NOT = {
					has_idea = power_vacuum_3
					has_idea = landowners
				}
				has_idea = power_vacuum_2
			}
			swap_ideas = { remove_idea = power_vacuum_2 add_idea = landowners }
		}
		if = {
			limit = {
				NOT = {
					has_idea = power_vacuum_2
					has_idea = power_vacuum_3
					has_idea = landowners
				}
				has_idea = power_vacuum_1
			}
			swap_ideas = { remove_idea = power_vacuum_1 add_idea = landowners }
		}
		country_event = eaf.20
		ai_chance = { factor = 1 }
	}

	option = {
		name = eaf.19.b
		log = "[GetDateText]: [Root.GetName]: event eaf.19.b"
		if = {
			limit = {
				NOT = {
					has_idea = farmers
				}
				has_idea = power_vacuum_3
			}
			swap_ideas = { remove_idea = power_vacuum_3 add_idea = farmers }
		}
		if = {
			limit = {
				NOT = {
					has_idea = power_vacuum_3
					has_idea = farmers
				}
				has_idea = power_vacuum_2
			}
			swap_ideas = { remove_idea = power_vacuum_2 add_idea = farmers }
		}
		if = {
			limit = {
				NOT = {
					has_idea = power_vacuum_2
					has_idea = power_vacuum_3
					has_idea = farmers
				}
				has_idea = power_vacuum_1
			}
			swap_ideas = { remove_idea = power_vacuum_1 add_idea = farmers }
		}
		country_event = eaf.20
		ai_chance = { factor = 0 }
	}
	option = {
		name = eaf.19.c
		log = "[GetDateText]: [Root.GetName]: event eaf.19.c"
		custom_effect_tooltip = EAF_both_chi_far_ignored_tt
		add_political_power = -300
		country_event = eaf.21
		ai_chance = { factor = 1 }
	}
	option = {
		name = eaf.19.e
		log = "[GetDateText]: [Root.GetName]: event eaf.19.e"
		custom_effect_tooltip = EAF_both_chi_far_pleased_tt
		add_political_power = -100
		if = {
			limit = {
				NOT = {
					has_idea = farmers
				}
				has_idea = power_vacuum_3
			}
			swap_ideas = { remove_idea = power_vacuum_3 add_idea = farmers }
		}
		if = {
			limit = {
				NOT = {
					has_idea = power_vacuum_3
					has_idea = farmers
				}
				has_idea = power_vacuum_2
			}
			swap_ideas = { remove_idea = power_vacuum_2 add_idea = farmers }
		}
		if = {
			limit = {
				NOT = {
					has_idea = power_vacuum_2
					has_idea = power_vacuum_3
					has_idea = farmers
				}
				has_idea = power_vacuum_1
			}
			swap_ideas = { remove_idea = power_vacuum_1 add_idea = farmers }
		}
		if = {
			limit = {
				NOT = {
					has_idea = landowners
				}
				has_idea = power_vacuum_3
			}
			swap_ideas = { remove_idea = power_vacuum_3 add_idea = landowners }
		}
		if = {
			limit = {
				NOT = {
					has_idea = power_vacuum_3
					has_idea = landowners
				}
				has_idea = power_vacuum_2
			}
			swap_ideas = { remove_idea = power_vacuum_2 add_idea = landowners }
		}
		if = {
			limit = {
				NOT = {
					has_idea = power_vacuum_2
					has_idea = power_vacuum_3
					has_idea = landowners
				}
				has_idea = power_vacuum_1
			}
			swap_ideas = { remove_idea = power_vacuum_1 add_idea = landowners }
		}
		country_event = eaf.20
		trigger = {
			has_idea = power_vacuum_2
		}
		ai_chance = { factor = 1 }
	}
}

country_event = {
	id = eaf.20
	title = eaf.20.t
	desc = eaf.20.d
	picture = GFX_african_meeting3

	is_triggered_only = yes

	option = {
		name = eaf.20.a
		log = "[GetDateText]: [Root.GetName]: event eaf.20.a"
		country_event = eaf.22
		ai_chance = { factor = 1 }
	}
}

country_event = {
	id = eaf.21
	title = eaf.21.t
	desc = eaf.21.d
	picture = GFX_african_meeting3

	is_triggered_only = yes

	option = {
		name = eaf.21.a
		log = "[GetDateText]: [Root.GetName]: event eaf.21.a"
		add_stability = -0.05
		country_event = eaf.22
		ai_chance = { factor = 1 }
	}
}

country_event = {
	id = eaf.22
	title = eaf.22.t
	desc = eaf.22.d
	picture = GFX_african_meeting3

	is_triggered_only = yes

	option = {
		name = eaf.22.a
		log = "[GetDateText]: [Root.GetName]: event eaf.22.a"
		custom_effect_tooltip = EAF_join_opec_tt
		add_ideas = EAF_opec_friend
		if = {
			limit = {
					NOT = {
						has_idea = fossil_fuel_industry
					}
					has_idea = power_vacuum_3
			}
			swap_ideas = { remove_idea = power_vacuum_3 add_idea = fossil_fuel_industry }
		}
		if = {
			limit = {
					NOT = {
						has_idea = power_vacuum_3
						has_idea = fossil_fuel_industry
					}
					has_idea = power_vacuum_2
			}
			swap_ideas = { remove_idea = power_vacuum_2 add_idea = fossil_fuel_industry }
		}
		if = {
			limit = {
				NOT = {
					has_idea = power_vacuum_2
					has_idea = power_vacuum_3
					has_idea = fossil_fuel_industry
				}
				has_idea = power_vacuum_1
			}
			swap_ideas = { remove_idea = power_vacuum_1 add_idea = fossil_fuel_industry }
		}
		country_event = eaf.23
		ai_chance = { factor = 1 }
	}

	option = {
		name = eaf.22.b
		log = "[GetDateText]: [Root.GetName]: event eaf.22.b"
		custom_effect_tooltip = EAF_oil_alone_tt
		if = {
			limit = {
					NOT = {
						has_idea = fossil_fuel_industry
					}
					has_idea = power_vacuum_3
			}
			swap_ideas = { remove_idea = power_vacuum_3 add_idea = fossil_fuel_industry }
		}
		if = {
			limit = {
					NOT = {
						has_idea = power_vacuum_3
						has_idea = fossil_fuel_industry
					}
					has_idea = power_vacuum_2
			}
			swap_ideas = { remove_idea = power_vacuum_2 add_idea = fossil_fuel_industry }
		}
		if = {
			limit = {
				NOT = {
					has_idea = power_vacuum_2
					has_idea = power_vacuum_3
					has_idea = fossil_fuel_industry
				}
				has_idea = power_vacuum_1
			}
			swap_ideas = { remove_idea = power_vacuum_1 add_idea = fossil_fuel_industry }
		}
		country_event = eaf.23
		ai_chance = { factor = 1 }
	}

	option = {
		name = eaf.22.c
		log = "[GetDateText]: [Root.GetName]: event eaf.22.c"
		custom_effect_tooltip = EAF_no_oil_tt
		add_political_power = -50
		add_country_leader_trait = enviromentalist
		country_event = eaf.23
		ai_chance = { factor = 1 }
	}
}

country_event = {
	id = eaf.23
	title = eaf.23.t
	desc = eaf.23.d
	picture = GFX_african_meeting2

	is_triggered_only = yes

	option = {
		name = eaf.23.a
		log = "[GetDateText]: [Root.GetName]: event eaf.23.a"
		add_stability = 0.005
		add_political_power = -50
		country_event = eaf.24
		trigger = {
			has_idea = power_vacuum_1
		}
		ai_chance = { factor = 1 }
	}

	option = {
		name = eaf.23.b
		log = "[GetDateText]: [Root.GetName]: event eaf.23.b"
		add_political_power = -5
		news_event = eaf.25
		ai_chance = { factor = 1 }
	}
}

country_event = {
	id = eaf.24
	title = eaf.24.t
	desc = eaf.24.d
	picture = GFX_african_meeting3

	is_triggered_only = yes

	option = {
		name = eaf.24.a
		log = "[GetDateText]: [Root.GetName]: event eaf.24.a"
		if = {
			limit = {
					NOT = {
						has_idea = maritime_industry
					}
					has_idea = power_vacuum_3
			}
			swap_ideas = { remove_idea = power_vacuum_3 add_idea = maritime_industry }
		}
		if = {
			limit = {
					NOT = {
						has_idea = power_vacuum_3
						has_idea = maritime_industry
					}
					has_idea = power_vacuum_2
			}
			swap_ideas = { remove_idea = power_vacuum_2 add_idea = maritime_industry }
		}
		if = {
			limit = {
				NOT = {
					has_idea = power_vacuum_2
					has_idea = power_vacuum_3
					has_idea = maritime_industry
				}
				has_idea = power_vacuum_1
			}
			swap_ideas = { remove_idea = power_vacuum_1 add_idea = maritime_industry }
		}
		news_event = eaf.25
		ai_chance = { factor = 1 }
	}

	option = {
		name = eaf.24.b
		log = "[GetDateText]: [Root.GetName]: event eaf.24.b"
		if = {
			limit = {
					NOT = {
						has_idea = small_medium_business_owners
					}
					has_idea = power_vacuum_3
			}
			swap_ideas = { remove_idea = power_vacuum_3 add_idea = small_medium_business_owners }
		}
		if = {
			limit = {
					NOT = {
						has_idea = power_vacuum_3
						has_idea = small_medium_business_owners
					}
					has_idea = power_vacuum_2
			}
			swap_ideas = { remove_idea = power_vacuum_2 add_idea = small_medium_business_owners }
		}
		if = {
			limit = {
				NOT = {
					has_idea = power_vacuum_2
					has_idea = power_vacuum_3
					has_idea = small_medium_business_owners
				}
				has_idea = power_vacuum_1
			}
			swap_ideas = { remove_idea = power_vacuum_1 add_idea = small_medium_business_owners }
		}
		news_event = eaf.25
		ai_chance = { factor = 1 }
	}

	option = {
		name = eaf.24.c
		log = "[GetDateText]: [Root.GetName]: event eaf.24.c"
		add_political_power = -100
		swap_ideas = { remove_idea = power_vacuum_2 add_idea = maritime_industry }
		swap_ideas = { remove_idea = power_vacuum_1 add_idea = small_medium_business_owners }
		news_event = eaf.25
		trigger = {
			has_idea = power_vacuum_2
		}
		ai_chance = { factor = 1 }
	}

	option = {
		name = eaf.24.e
		log = "[GetDateText]: [Root.GetName]: event eaf.24.e"
		add_stability = -0.005
		add_political_power = -50
		news_event = eaf.25
		ai_chance = { factor = 1 }
	}
}

news_event = {
	id = eaf.25
	title = eaf.25.t
	desc = eaf.25.d
	picture = GFX_african_meeting4

	is_triggered_only = yes

	option = {
		name = eaf.25.a
		log = "[GetDateText]: [Root.GetName]: event eaf.25.a"

		ai_chance = { factor = 1 }
	}
}

country_event = {
	id = eaf.31
	title = eaf.31.t
	desc = eaf.31.d
	picture = GFX_african_meeting

	is_triggered_only = yes

	option = {
		name = eaf.31.a
		log = "[GetDateText]: [Root.GetName]: event eaf.31.a"
		swap_ideas = { remove_idea = labour_unions add_idea = power_vacuum_1 }
		swap_ideas = { remove_idea = industrial_conglomerates add_idea = power_vacuum_2 }
		swap_ideas = { remove_idea = farmers add_idea = power_vacuum_3 }
		ai_chance = { factor = 1 }
	}
}

country_event = {
	id = eaf.32
	title = eaf.32.t
	desc = eaf.32.d
	picture = GFX_african_meeting

	is_triggered_only = yes

	option = {
		name = eaf.32.a
		log = "[GetDateText]: [Root.GetName]: event eaf.32.a"
		every_country = {
			limit = {
				has_country_flag = EAF_FORMED
			}
			set_country_flag = EAF_nuclear_assistance
			add_opinion_modifier = {
				target = SAF
				modifier = EAF_nuclear_assistance
			}
			hidden_effect = {
				country_event = eaf.33
			}
		}
		ai_chance = { factor = 1 }
	}
	option = {
		name = eaf.32.b
		log = "[GetDateText]: [Root.GetName]: event eaf.32.b"
		add_political_power = -5
		hidden_effect = {
			every_country = {
				limit = {
					has_country_flag = EAF_FORMED
				}
				country_event = eaf.34
			}
		}
		ai_chance = { factor = 0 }
	}
}

country_event = {
	id = eaf.33
	title = eaf.33.t
	desc = eaf.33.d
	picture = GFX_african_meeting

	is_triggered_only = yes

	option = {
		name = eaf.33.a
		log = "[GetDateText]: [Root.GetName]: event eaf.33.a"
		SAF = {
			add_opinion_modifier = {
				target = ROOT
				modifier = EAF_nuclear_assistance
			}
		}
		ai_chance = { factor = 1 }
	}
}

country_event = {
	id = eaf.34
	title = eaf.34.t
	desc = eaf.34.d
	picture = GFX_treaty_rejected

	is_triggered_only = yes

	option = {
		name = eaf.34.a
		log = "[GetDateText]: [Root.GetName]: event eaf.34.a"
		add_political_power = -5
		ai_chance = { factor = 1 }
	}
}
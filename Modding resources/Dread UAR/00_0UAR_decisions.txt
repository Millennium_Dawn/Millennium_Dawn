ARAB_liberal_conservative_split = {
	ARAB_remove_saudi_aid = {
		icon = generic_break_treaty
		available = { has_idea = saudi_aid }
		fire_only_once = yes
		visible = { has_idea = saudi_aid }
		cost = 50
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision ARAB_remove_saudi_aid"
			add_power_balance_value = {
				id = ARAB_liberalism_conservatism
				value = -0.025
				tooltip_side = ARAB_liberalism_side
			}
			hidden_effect = {
				set_temp_variable = { temp_modify_social_con_government = -0.025 }
				modify_social_conservatism_government = yes
			}
			SAU = { add_opinion_modifier = { target = ROOT modifier = GEN_banned_saudi_mosques } }
			add_stability = -0.05
			remove_ideas = saudi_aid
		}
	}
	ARAB_subsidise_western_culture = {
		icon = GFX_decision_ger_mefo_bills
		available = {
			if = {
				limit = { original_tag = SYR }
				NOT = {
					has_completed_focus = SYR_islamist_victory
					has_completed_focus = SYR_moderate_secularism
				}
			}
			no_jihadist_government = yes
			power_balance_value = {
				id = ARAB_liberalism_conservatism
				value > -0.9
			}
		}
		visible = {
			power_balance_value = {
				id = ARAB_liberalism_conservatism
				value > -1
			}
		}
		custom_cost_trigger = { check_variable = { treasury > 1.49 } }
		custom_cost_text = cost_1_5
		days_remove = 35
		days_re_enable = 70
		complete_effect = {
			set_temp_variable = { treasury_change = -1.5 }
			modify_treasury_effect = yes
			add_power_balance_modifier = {
				id = ARAB_liberalism_conservatism
				modifier = ARAB_SECULAR_small_weekly
			}
			log = "[GetDateText]: [Root.GetName]: Decision ARAB_subsidise_western_culture"
		}
		remove_effect = {
			remove_power_balance_modifier = {
				id = ARAB_liberalism_conservatism
				modifier = ARAB_SECULAR_small_weekly
			}
			hidden_effect = {
				set_temp_variable = { temp_modify_social_con_government = -0.025 }
				modify_social_conservatism_government = yes
			}
			add_popularity = {
				ideology = fascism
				popularity = -0.01
			}
			recalculate_party = yes
			log = "[GetDateText]: [Root.GetName]: Decision remove ARAB_subsidise_western_culture"
		}
	}
	ARAB_subsidise_salafism = {
		icon = GFX_decision_ger_mefo_bills
		available = {
			if = {
				limit = { original_tag = SYR }
				NOT = {
					has_completed_focus = SYR_social_democratic_victory
					has_completed_focus = SYR_moderate_secularism
					has_completed_focus = SYR_true_secularism
				}
			}
			power_balance_value = {
				id = ARAB_liberalism_conservatism
				value < 0.9
			}
		}
		visible = {
			power_balance_value = {
				id = ARAB_liberalism_conservatism
				value < 1
			}
		}
		custom_cost_trigger = { check_variable = { treasury > 1.49 } }
		custom_cost_text = cost_1_5
		days_remove = 35
		days_re_enable = 70
		complete_effect = {
			set_temp_variable = { treasury_change = -1.5 }
			modify_treasury_effect = yes
			add_power_balance_modifier = {
				id = ARAB_liberalism_conservatism
				modifier = ARAB_RELIGIOUS_small_weekly
			}
			log = "[GetDateText]: [Root.GetName]: Decision ARAB_subsidise_salafism"
		}
		remove_effect = {
			remove_power_balance_modifier = {
				id = ARAB_liberalism_conservatism
				modifier = ARAB_RELIGIOUS_small_weekly
			}
			hidden_effect = {
				set_temp_variable = { temp_modify_social_con_government = 0.025 }
				modify_social_conservatism_government = yes
			}
			add_popularity = {
				ideology = fascism
				popularity = 0.01
			}
			recalculate_party = yes
			log = "[GetDateText]: [Root.GetName]: Decision remove ARAB_subsidise_salafism"
		}
	}
	ARAB_encourage_female_representation = {
		icon = generic_civil_support
		available = {
			if = {
				limit = { original_tag = SYR }
				NOT = {
					has_completed_focus = SYR_islamist_victory
					has_completed_focus = SYR_moderate_secularism
				}
			}
			power_balance_value = {
				id = ARAB_liberalism_conservatism
				value > -0.6
			}
			no_jihadist_government = yes
		}
		visible = {
			if = {
				limit = { has_focus_tree = gulf }
				has_completed_focus = GCC_weaken_the_mutaween
				has_completed_focus = GCC_women_in_politics
			}
			power_balance_value = {
				id = ARAB_liberalism_conservatism
				value > -0.7
			}
		}
		cost = 25
		days_remove = 70
		days_re_enable = 140
		complete_effect = {
			add_power_balance_modifier = {
				id = ARAB_liberalism_conservatism
				modifier = ARAB_SECULAR_small_weekly
			}
			log = "[GetDateText]: [Root.GetName]: Decision ARAB_encourage_female_representation"
		}
		remove_effect = {
			remove_power_balance_modifier = {
				id = ARAB_liberalism_conservatism
				modifier = ARAB_SECULAR_small_weekly
			}
			hidden_effect = {
				set_temp_variable = { temp_modify_social_con_government = -0.05 }
				modify_social_conservatism_government = yes
			}
			log = "[GetDateText]: [Root.GetName]: Decision remove ARAB_encourage_female_representation"
		}
	}
	ARAB_discourage_female_representation = {
		icon = oppression
		available = {
			if = {
				limit = { original_tag = SYR }
				NOT = {
					has_completed_focus = SYR_true_secularism
					has_completed_focus = SYR_moderate_secularism
					has_completed_focus = SYR_social_democratic_victory
				}
			}
			power_balance_value = {
				id = ARAB_liberalism_conservatism
				value < 0.6
			}
		}
		visible = {
			if = {
				limit = { has_focus_tree = gulf }
				has_completed_focus = GCC_empower_the_mutaween
				has_completed_focus = GCC_women_in_politics
			}
			power_balance_value = {
				id = ARAB_liberalism_conservatism
				value < 0.7
			}
		}
		cost = 25
		days_remove = 70
		days_re_enable = 140
		complete_effect = {
			add_power_balance_modifier = {
				id = ARAB_liberalism_conservatism
				modifier = ARAB_RELIGIOUS_small_weekly
			}
			log = "[GetDateText]: [Root.GetName]: Decision ARAB_discourage_female_representation"
		}
		remove_effect = {
			remove_power_balance_modifier = {
				id = ARAB_liberalism_conservatism
				modifier = ARAB_RELIGIOUS_small_weekly
			}
			hidden_effect = {
				set_temp_variable = { temp_modify_social_con_government = 0.05 }
				modify_social_conservatism_government = yes
			}
			log = "[GetDateText]: [Root.GetName]: Decision remove ARAB_discourage_female_representation"
		}
	}
	ARAB_expand_female_employment = {
		icon = generic_civil_support
		available = {
			if = {
				limit = { original_tag = SYR }
				NOT = {
					has_completed_focus = SYR_moderate_secularism
					has_completed_focus = SYR_islamist_victory
				}
			}
			power_balance_value = {
				id = ARAB_liberalism_conservatism
				value > -0.8
			}
		}
		visible = {
			if = {
				limit = { has_focus_tree = gulf }
				has_completed_focus = GCC_weaken_the_mutaween
				has_completed_focus = GCC_women_in_the_economy
			}
			no_jihadist_government = yes
			power_balance_value = {
				id = ARAB_liberalism_conservatism
				value > -0.9
			}
		}
		cost = 25
		days_remove = 105
		days_re_enable = 210
		complete_effect = {
			add_power_balance_modifier = {
				id = ARAB_liberalism_conservatism
				modifier = ARAB_SECULAR_small_weekly
			}
			log = "[GetDateText]: [Root.GetName]: Decision ARAB_expand_female_employment"
		}
		remove_effect = {
			remove_power_balance_modifier = {
				id = ARAB_liberalism_conservatism
				modifier = ARAB_SECULAR_small_weekly
			}
			hidden_effect = {
				set_temp_variable = { temp_modify_social_con_government = -0.075 }
				modify_social_conservatism_government = yes
			}
			log = "[GetDateText]: [Root.GetName]: Decision remove ARAB_expand_female_employment"
		}
	}
	ARAB_limit_female_employment = {
		icon = oppression
		available = {
			if = {
				limit = { original_tag = SYR }
				NOT = {
					has_completed_focus = SYR_true_secularism
					has_completed_focus = SYR_moderate_secularism
					has_completed_focus = SYR_social_democratic_victory
				}
			}
			power_balance_value = {
				id = ARAB_liberalism_conservatism
				value < 0.8
			}
		}
		visible = {
			if = {
				limit = { has_focus_tree = gulf }
				has_completed_focus = GCC_empower_the_mutaween
				has_completed_focus = GCC_women_in_the_economy
			}
			power_balance_value = {
				id = ARAB_liberalism_conservatism
				value < 0.9
			}
		}
		cost = 25
		days_remove = 105
		days_re_enable = 210
		complete_effect = {
			add_power_balance_modifier = {
				id = ARAB_liberalism_conservatism
				modifier = ARAB_RELIGIOUS_small_weekly
			}
			log = "[GetDateText]: [Root.GetName]: Decision ARAB_limit_female_employment"
		}
		remove_effect = {
			remove_power_balance_modifier = {
				id = ARAB_liberalism_conservatism
				modifier = ARAB_RELIGIOUS_small_weekly
			}
			hidden_effect = {
				set_temp_variable = { temp_modify_social_con_government = 0.075 }
				modify_social_conservatism_government = yes
			}
			log = "[GetDateText]: [Root.GetName]: Decision remove ARAB_limit_female_employment"
		}
	}
	# LEGALISING/BANNING
	ARAB_legalise_alcohol_for_tourists = {
		icon = eng_trade_unions_support
		available = {
			if = {
				limit = { original_tag = SYR }
				NOT = {
					has_completed_focus = SYR_moderate_secularism
					has_completed_focus = SYR_islamist_victory
				}
			}
			OR = {
				if = {
					limit = { has_focus_tree = gulf }
					has_completed_focus = GCC_reduce_vice_prevention
				}
				power_balance_value = {
					id = ARAB_liberalism_conservatism
					value < 0.5
				}
			}
			no_jihadist_government = yes
		}
		visible = { NOT = { has_country_flag = limited_alcohol } }
		cost = 50
		complete_effect = {
			add_power_balance_value = {
				id = ARAB_liberalism_conservatism
				value = -0.02
				tooltip_side = ARAB_liberalism_side
			}
			hidden_effect = {
				set_temp_variable = { temp_modify_social_con_government = -0.02 }
				modify_social_conservatism_government = yes
			}
			set_country_flag = limited_alcohol
			log = "[GetDateText]: [Root.GetName]: Decision ARAB_legalise_alcohol_for_tourists"
		}
	}
	ARAB_ban_alcohol_for_tourists = {
		icon = eng_trade_unions_support
		available = {
			if = {
				limit = { original_tag = SYR }
				NOT = {
					has_completed_focus = SYR_true_secularism
					has_completed_focus = SYR_moderate_secularism
					has_completed_focus = SYR_social_democratic_victory
				}
			}
			power_balance_value = {
				id = ARAB_liberalism_conservatism
				value > 0.5
			}
		}
		visible = { has_country_flag = limited_alcohol }
		cost = 50
		complete_effect = {
			add_power_balance_value = {
				id = ARAB_liberalism_conservatism
				value = 0.02
				tooltip_side = ARAB_conservative_side
			}
			hidden_effect = {
				set_temp_variable = { temp_modify_social_con_government = 0.02 }
				modify_social_conservatism_government = yes
			}
			clr_country_flag = limited_alcohol
			log = "[GetDateText]: [Root.GetName]: Decision ARAB_ban_alcohol_for_tourists"
		}
	}
	ARAB_legalise_alcohol_for_citizens = {
		icon = eng_trade_unions_support
		available = {
			if = {
				limit = { original_tag = SYR }
				NOT = {
					has_completed_focus = SYR_moderate_secularism
					has_completed_focus = SYR_islamist_victory
				}
			}
			OR = {
				if = {
					limit = { has_focus_tree = gulf }
					has_completed_focus = GCC_westernization_policies
				}
				power_balance_value = {
					id = ARAB_liberalism_conservatism
					value < 0.3
				}
			}
			has_country_flag = limited_alcohol
			no_jihadist_government = yes
		}
		visible = { NOT = { has_country_flag = alcohol } }
		cost = 100
		complete_effect = {
			add_power_balance_value = {
				id = ARAB_liberalism_conservatism
				value = -0.05
				tooltip_side = ARAB_liberalism_side
			}
			hidden_effect = {
				set_temp_variable = { temp_modify_social_con_government = -0.05 }
				modify_social_conservatism_government = yes
			}
			set_country_flag = alcohol
			log = "[GetDateText]: [Root.GetName]: Decision ARAB_legalise_alcohol_for_citizens"
		}
	}
	ARAB_ban_alcohol_for_citizens = {
		icon = eng_trade_unions_support
		available = {
			if = {
				limit = { original_tag = SYR }
				NOT = {
					has_completed_focus = SYR_true_secularism
					has_completed_focus = SYR_moderate_secularism
					has_completed_focus = SYR_social_democratic_victory
				}
			}
			power_balance_value = {
				id = ARAB_liberalism_conservatism
				value > 0.3
			}
		}
		visible = { has_country_flag = alcohol }
		cost = 100
		complete_effect = {
			add_power_balance_value = {
				id = ARAB_liberalism_conservatism
				value = 0.05
				tooltip_side = ARAB_conservative_side
			}
			hidden_effect = {
				set_temp_variable = { temp_modify_social_con_government = 0.05 }
				modify_social_conservatism_government = yes
			}
			clr_country_flag = alcohol
			log = "[GetDateText]: [Root.GetName]: Decision ARAB_ban_alcohol_for_citizens"
		}
	}
	ARAB_relax_dress_code = {
		icon = eng_trade_unions_support
		available = {
			if = {
				limit = { original_tag = SYR }
				NOT = {
					has_completed_focus = SYR_moderate_secularism
					has_completed_focus = SYR_islamist_victory
				}
			}
			OR = {
				if = {
					limit = { has_focus_tree = gulf }
					has_completed_focus = GCC_reduce_vice_prevention
				}
				power_balance_value = {
					id = ARAB_liberalism_conservatism
					value < 0.5
				}
			}
			no_jihadist_government = yes
		}
		visible = { NOT = { has_country_flag = relaxed_dress_code } }
		cost = 50
		complete_effect = {
			add_power_balance_value = {
				id = ARAB_liberalism_conservatism
				value = -0.05
				tooltip_side = ARAB_liberalism_side
			}
			hidden_effect = {
				set_temp_variable = { temp_modify_social_con_government = -0.05 }
				modify_social_conservatism_government = yes
			}
			set_country_flag = relaxed_dress_code
			log = "[GetDateText]: [Root.GetName]: Decision ARAB_relax_dress_code"
		}
	}
	ARAB_impose_dress_code = {
		icon = eng_trade_unions_support
		available = {
			if = {
				limit = { original_tag = SYR }
				NOT = {
					has_completed_focus = SYR_true_secularism
					has_completed_focus = SYR_moderate_secularism
					has_completed_focus = SYR_social_democratic_victory
				}
			}
			power_balance_value = {
				id = ARAB_liberalism_conservatism
				value > 0.5
			}
		}
		visible = { has_country_flag = relaxed_dress_code }
		cost = 50
		complete_effect = {
			add_power_balance_value = {
				id = ARAB_liberalism_conservatism
				value = 0.05
				tooltip_side = ARAB_conservative_side
			}
			hidden_effect = {
				set_temp_variable = { temp_modify_social_con_government = 0.05 }
				modify_social_conservatism_government = yes
			}
			clr_country_flag = relaxed_dress_code
			log = "[GetDateText]: [Root.GetName]: Decision ARAB_impose_dress_code"
		}
	}
	ARAB_ban_burqa = {
		icon = eng_trade_unions_support
		available = {
			if = {
				limit = { original_tag = SYR }
				NOT = {
					has_completed_focus = SYR_moderate_secularism
					has_completed_focus = SYR_islamist_victory
				}
			}
			if = {
				limit = { has_focus_tree = gulf }
				has_completed_focus = GCC_westernization_policies
			}
			power_balance_value = {
				id = ARAB_liberalism_conservatism
				value < 0.3
			}
			no_jihadist_government = yes
		}
		visible = {
			has_country_flag = relaxed_dress_code
			NOT = { has_country_flag = banned_burqa }
		}
		cost = 100
		complete_effect = {
			add_power_balance_value = {
				id = ARAB_liberalism_conservatism
				value = -0.1
				tooltip_side = ARAB_liberalism_side
			}
			hidden_effect = {
				set_temp_variable = { temp_modify_social_con_government = -0.1 }
				modify_social_conservatism_government = yes
			}
			set_country_flag = banned_burqa
			log = "[GetDateText]: [Root.GetName]: Decision ARAB_ban_burqa"
		}
	}
	ARAB_legalise_burqa = {
		icon = eng_trade_unions_support
		available = {
			if = {
				limit = { original_tag = SYR }
				has_completed_focus = SYR_moderate_secularism
			}
			power_balance_value = {
				id = ARAB_liberalism_conservatism
				value > 0.3
			}
		}
		visible = { has_country_flag = banned_burqa }
		cost = 100
		complete_effect = {
			add_power_balance_value = {
				id = ARAB_liberalism_conservatism
				value = 0.1
				tooltip_side = ARAB_conservative_side
			}
			hidden_effect = {
				set_temp_variable = { temp_modify_social_con_government = 0.1 }
				modify_social_conservatism_government = yes
			}
			clr_country_flag = banned_burqa
			log = "[GetDateText]: [Root.GetName]: Decision ARAB_legalise_burqa"
		}
	}
	ARAB_ban_hijab = {
		icon = eng_trade_unions_support
		available = {
			if = {
				limit = { original_tag = SYR }
				NOT = {
					has_completed_focus = SYR_moderate_secularism
					has_completed_focus = SYR_islamist_victory
				}
			}
			if = {
				limit = { has_focus_tree = gulf }
				has_completed_focus = GCC_westernization_policies
			}
			power_balance_value = {
				id = ARAB_liberalism_conservatism
				value < -0.3
			}
			no_jihadist_government = yes
		}
		visible = {
			has_country_flag = banned_burqa
			NOT = { has_country_flag = banned_head_scarf }
		}
		cost = 150
		complete_effect = {
			add_power_balance_value = {
				id = ARAB_liberalism_conservatism
				value = -0.15
				tooltip_side = ARAB_liberalism_side
			}
			hidden_effect = {
				set_temp_variable = { temp_modify_social_con_government = -0.15 }
				modify_social_conservatism_government = yes
			}
			set_country_flag = banned_head_scarf
			log = "[GetDateText]: [Root.GetName]: Decision ARAB_ban_hijab"
		}
	}
	ARAB_legalise_hijab = {
		icon = eng_trade_unions_support
		available = {
			power_balance_value = {
				id = ARAB_liberalism_conservatism
				value > -0.4
			}
		}
		visible = { has_country_flag = banned_head_scarf }
		cost = 150
		complete_effect = {
			add_power_balance_value = {
				id = ARAB_liberalism_conservatism
				value = 0.15
				tooltip_side = ARAB_conservative_side
			}
			hidden_effect = {
				set_temp_variable = { temp_modify_social_con_government = 0.15 }
				modify_social_conservatism_government = yes
			}
			clr_country_flag = banned_head_scarf
			log = "[GetDateText]: [Root.GetName]: Decision ARAB_legalise_hijab"
		}
	}
	ARAB_legalise_same_sex_relations = {
		icon = GFX_decision_generic_political_discourse
		available = {
			if = {
				limit = { original_tag = SYR }
				NOT = {
					has_completed_focus = SYR_moderate_secularism
					has_completed_focus = SYR_islamist_victory
				}
			}
			power_balance_value = {
				id = ARAB_liberalism_conservatism
				value < -0.6
			}
			no_jihadist_government = yes
		}
		visible = { NOT = { has_country_flag = same_sex_decriminalized } }
		cost = 150
		complete_effect = {
			add_power_balance_value = {
				id = ARAB_liberalism_conservatism
				value = -0.15
				tooltip_side = ARAB_liberalism_side
			}
			hidden_effect = {
				set_temp_variable = { temp_modify_social_con_government = -0.15 }
				modify_social_conservatism_government = yes
			}
			set_country_flag = same_sex_decriminalized
			log = "[GetDateText]: [Root.GetName]: Decision ARAB_legalise_same_sex_relations"
		}
	}
	ARAB_ban_same_sex_relations = {
		icon = oppression
		available = {
			if = {
				limit = { original_tag = SYR }
				NOT = {
					has_completed_focus = SYR_social_democratic_victory
					has_completed_focus = SYR_moderate_secularism
					has_completed_focus = SYR_true_secularism
				}
			}
			power_balance_value = {
				id = ARAB_liberalism_conservatism
				value > -0.7
			}
		}
		visible = { has_country_flag = same_sex_decriminalized }
		cost = 150
		complete_effect = {
			add_power_balance_value = {
				id = ARAB_liberalism_conservatism
				value = 0.15
				tooltip_side = ARAB_conservative_side
			}
			hidden_effect = {
				set_temp_variable = { temp_modify_social_con_government = 0.15 }
				modify_social_conservatism_government = yes
			}
			clr_country_flag = same_sex_decriminalized
			log = "[GetDateText]: [Root.GetName]: Decision ARAB_ban_same_sex_relations"
		}
	}
	ARAB_secularise_the_state = {
		icon = GFX_decision_generic_political_discourse
		available = {
			if = {
				limit = { original_tag = SYR }
				NOT = { has_completed_focus = SYR_islamist_victory }
			}
			power_balance_value = {
				id = ARAB_liberalism_conservatism
				value < -0.9
			}
			NOT = {
				has_idea = the_ulema
				has_idea = wahabi_ulema
			}
			no_jihadist_government = yes
		}
		visible = { NOT = { has_country_flag = secularized } }
		cost = 150
		complete_effect = {
			add_power_balance_value = {
				id = ARAB_liberalism_conservatism
				value = -0.2
				tooltip_side = ARAB_liberalism_side
			}
			hidden_effect = {
				set_temp_variable = { temp_modify_social_con_government = -0.2 }
				modify_social_conservatism_government = yes
			}
			set_country_flag = secularized
			if = {
				limit = { has_idea = sunni }
				swap_ideas = {
					remove_idea = sunni
					add_idea = pluralist
				}
			}
			else_if = {
				limit = { has_idea = ibadi }
				swap_ideas = {
					remove_idea = ibadi
					add_idea = pluralist
				}
			}
			else_if = {
				limit = { has_idea = shia }
				swap_ideas = {
					remove_idea = shia
					add_idea = pluralist
				}
			}
			else_if = {
				limit = { has_idea = sufi_islam}
				swap_ideas = {
					remove_idea = sufi_islam
					add_idea = pluralist
				}
			}
			log = "[GetDateText]: [Root.GetName]: Decision ARAB_secularise_the_state"
		}
	}
	ARAB_radicalise_the_state = {
		icon = GFX_decision_generic_political_discourse
		available = {
			if = {
				limit = { original_tag = SYR }
				NOT = {
					has_completed_focus = SYR_true_secularism
					has_completed_focus = SYR_social_democratic_victory
				}
			}
			power_balance_value = {
				id = ARAB_liberalism_conservatism
				value > 0.9
			}
			jihadist_government = yes
		}
		visible = { NOT = { has_country_flag = secularized } }
		cost = 150
		complete_effect = {
			add_power_balance_value = {
				id = ARAB_liberalism_conservatism
				value = 0.2
				tooltip_side = ARAB_conservative_side
			}
			hidden_effect = {
				set_temp_variable = { temp_modify_social_con_government = 0.2 }
				modify_social_conservatism_government = yes
			}
			set_country_flag = radicalised
			if = {
				limit = { has_idea = pluralist }
				swap_ideas = {
					remove_idea = pluralist
					add_idea = sunni
				}
			}
			log = "[GetDateText]: [Root.GetName]: Decision ARAB_radicalise_the_state"
		}
	}
}
ARAB_assad_saddam_or_neither = {
	# ASSADISM
		ARAB_ASSAD_baath_message = {
			icon = eng_trade_unions_support
			available = {
				if = {
					limit = { original_tag = SYR }
					always = yes
				}
				else = {
					SYR = {
						has_opinion = {
							target = ROOT
							value > -1
						}
					}
					power_balance_value = {
						id = ARAB_assadist_saddamist
						value < 0.75
					}
				}
			}
			visible = {
				power_balance_value = {
					id = ARAB_assadist_saddamist
					value < 0.85
				}
			}
			custom_cost_trigger = { check_variable = { treasury > 1.99 } }
			custom_cost_text = cost_2_0
			days_remove = 70
			days_re_enable = 180
			complete_effect = {
				add_power_balance_modifier = {
					id = ARAB_assadist_saddamist
					modifier = ARAB_ASSADISM_small_weekly
				}
				set_temp_variable = { treasury_change = -2 }
				modify_treasury_effect = yes
				log = "[GetDateText]: [Root.GetName]: Decision ARAB_ASSAD_baath_message"
			}
			remove_effect = {
				remove_power_balance_modifier = {
					id = ARAB_assadist_saddamist
					modifier = ARAB_ASSADISM_small_weekly
				}
				hidden_effect = {
					set_temp_variable = { baathism_change = -0.07 }
					modify_baathism = yes
				}
				log = "[GetDateText]: [Root.GetName]: Decision remove ARAB_ASSAD_baath_message"
			}
		}
		ARAB_ASSAD_aid_from_damascus = {
			icon = eng_trade_unions_support
			available = {
				SYR = {
					has_opinion = {
						target = ROOT
						value > 24
					}
				}
				power_balance_value = {
					id = ARAB_assadist_saddamist
					value < -0.25
				}
			}
			visible = {
				NOT = { original_tag = SYR }
				power_balance_value = {
					id = ARAB_assadist_saddamist
					value < -0.05
				}
			}
			cost = 100
			days_re_enable = 360
			complete_effect = {
				SYR = { country_event = { id = UAR.1 } }
				log = "[GetDateText]: [Root.GetName]: Decision ARAB_ASSAD_aid_from_damascus"
			}
		}
		ARAB_ASSAD_corrective_movement = {
			icon = eng_trade_unions_support
			available = {
				power_balance_value = {
					id = ARAB_assadist_saddamist
					value < 0.25
				}
			}
			visible = {
				power_balance_value = {
					id = ARAB_assadist_saddamist
					value < 0.35
				}
			}
			cost = 50
			days_remove = 70
			days_re_enable = 360
			complete_effect = {
				add_power_balance_modifier = {
					id = ARAB_assadist_saddamist
					modifier = ARAB_ASSADISM_medium_weekly
				}
				log = "[GetDateText]: [Root.GetName]: Decision ARAB_ASSAD_corrective_movement"
			}
			remove_effect = {
				remove_power_balance_modifier = {
					id = ARAB_assadist_saddamist
					modifier = ARAB_ASSADISM_medium_weekly
				}
				hidden_effect = {
					set_temp_variable = { baathism_change = -0.2 }
					modify_baathism = yes
				}
				log = "[GetDateText]: [Root.GetName]: Decision remove ARAB_ASSAD_corrective_movement"
			}
		}
		ARAB_ASSAD_nepotism = {
			icon = eng_trade_unions_support
			available = {
				power_balance_value = {
					id = ARAB_assadist_saddamist
					value < -0.2
				}
			}
			visible = {
				check_variable = { ARAB_nepotism < 3 }
				power_balance_value = {
					id = ARAB_assadist_saddamist
					value < -0.1
				}
			}
			cost = 0
			days_remove = 90
			days_re_enable = 180
			complete_effect = {
				add_timed_idea = {
					idea = ARAB_nepotism
					days = 180
				}
				log = "[GetDateText]: [Root.GetName]: Decision ARAB_ASSAD_nepotism"
			}
			remove_effect = {
				add_power_balance_value = {
					id = ARAB_assadist_saddamist
					value = -0.1
					tooltip_side = ARAB_assadism_side
				}
				hidden_effect = {
					set_temp_variable = { baathism_change = -0.1 }
					modify_baathism = yes
					add_to_variable = { ARAB_nepotism = 1 }
				}
				log = "[GetDateText]: [Root.GetName]: Decision remove ARAB_ASSAD_nepotism"
			}
		}
		ARAB_ASSAD_leader_knows_best = {
			icon = eng_trade_unions_support
			available = {
				power_balance_value = {
					id = ARAB_assadist_saddamist
					value < -0.5
				}
			}
			visible = {
				any_character = {
					NOT = { has_trait = ARAB_knows_best_ASSADISM }
					NOT = { has_trait = ARAB_personality_cult_ASSADISM }
				}
				power_balance_value = {
					id = ARAB_assadist_saddamist
					value < -0.35
				}
			}
			cost = 200
			days_remove = 35
			complete_effect = {
				add_country_leader_trait = ARAB_knows_best_ASSADISM
				add_power_balance_modifier = {
					id = ARAB_assadist_saddamist
					modifier = ARAB_ASSADISM_small_weekly
				}
				log = "[GetDateText]: [Root.GetName]: Decision ARAB_ASSAD_leader_knows_best"
			}
			remove_effect = {
				remove_power_balance_modifier = {
					id = ARAB_assadist_saddamist
					modifier = ARAB_ASSADISM_small_weekly
				}
				hidden_effect = {
					set_temp_variable = { baathism_change = -0.025 }
					modify_baathism = yes
				}
				log = "[GetDateText]: [Root.GetName]: Decision remove ARAB_ASSAD_leader_knows_best"
			}
		}
		ARAB_ASSAD_socialist_cult = {
			icon = eng_trade_unions_support
			available = {
				power_balance_value = {
					id = ARAB_assadist_saddamist
					value < -0.75
				}
			}
			visible = {
				any_character = {
					has_trait = ARAB_knows_best_ASSADISM
					NOT = { has_trait = ARAB_personality_cult_ASSADISM }
				}
				power_balance_value = {
					id = ARAB_assadist_saddamist
					value < -0.6
				}
			}
			days_remove = 70
			custom_cost_trigger = { check_variable = { treasury > 4.99 } }
			custom_cost_text = cost_5_0
			complete_effect = {
				if = {
					limit = {
						any_character = {
							has_trait = ARAB_knows_best_ASSADISM
						}
					}
					remove_country_leader_trait = ARAB_knows_best_ASSADISM
				}
				add_country_leader_trait = ARAB_personality_cult_ASSADISM
				add_power_balance_modifier = {
					id = ARAB_assadist_saddamist
					modifier = ARAB_ASSADISM_small_weekly
				}
				log = "[GetDateText]: [Root.GetName]: Decision ARAB_ASSAD_socialist_cult"
			}
			remove_effect = {
				remove_power_balance_modifier = {
					id = ARAB_assadist_saddamist
					modifier = ARAB_ASSADISM_small_weekly
				}
				hidden_effect = {
					set_temp_variable = { baathism_change = -0.1 }
					modify_baathism = yes
				}
				log = "[GetDateText]: [Root.GetName]: Decision remove ARAB_ASSAD_socialist_cult"
			}
		}
		ARAB_ASSAD_official_alignment = {
			icon = eng_trade_unions_support
			available = {
				power_balance_value = {
					id = ARAB_assadist_saddamist
					value < -0.9
				}
			}
			visible = {
				NOT = { has_idea = ARAB_syrian_baathism }
			}
			cost = 250
			complete_effect = {
				add_ideas = ARAB_syrian_baathism
				add_power_balance_value = {
					id = ARAB_assadist_saddamist
					value = -0.15
					tooltip_side = ARAB_assadism_side
				}
				hidden_effect = {
					set_temp_variable = { baathism_change = -0.15 }
					modify_baathism = yes
				}
				log = "[GetDateText]: [Root.GetName]: Decision ARAB_ASSAD_official_alignment"
			}
		}
	# NASSERISM
		# ARAB_NASSER_secularisation = {
		# 	icon = eng_trade_unions_support
		# 	available = {}
		# 	visible = {}
		# 	cost = 50
		# 	complete_effect = {
		# 		add_power_balance_value = {
		# 			id = ARAB_liberalism_conservatism
		# 			value = -0.1
		# 			tooltip_side = ARAB_liberalism_side
		# 		}
		# 		hidden_effect = {
		# 			set_temp_variable = { temp_modify_social_con_government = -0.1 }
		# 			modify_social_conservatism_government = yes
		# 		}
		# 		log = "[GetDateText]: [Root.GetName]: Decision ARAB_ASSAD_baath_message"
		# 	}
		# }
		# ARAB_NASSER_al_arabi = {
		# 	icon = eng_trade_unions_support
		# 	available = {}
		# 	visible = {}
		# 	cost = 50
		# 	complete_effect = {
		# 		add_power_balance_value = {
		# 			id = ARAB_liberalism_conservatism
		# 			value = -0.1
		# 			tooltip_side = ARAB_liberalism_side
		# 		}
		# 		hidden_effect = {
		# 			set_temp_variable = { temp_modify_social_con_government = -0.1 }
		# 			modify_social_conservatism_government = yes
		# 		}
		# 		log = "[GetDateText]: [Root.GetName]: Decision ARAB_ASSAD_baath_message"
		# 	}
		# }
		# ARAB_NASSER_settle_differences = {
		# 	icon = eng_trade_unions_support
		# 	available = {}
		# 	visible = {}
		# 	cost = 50
		# 	complete_effect = {
		# 		add_power_balance_value = {
		# 			id = ARAB_liberalism_conservatism
		# 			value = -0.1
		# 			tooltip_side = ARAB_liberalism_side
		# 		}
		# 		hidden_effect = {
		# 			set_temp_variable = { temp_modify_social_con_government = -0.1 }
		# 			modify_social_conservatism_government = yes
		# 		}
		# 		log = "[GetDateText]: [Root.GetName]: Decision ARAB_ASSAD_baath_message"
		# 	}
		# }
		# ARAB_NASSER_democratic_values = {
		# 	icon = eng_trade_unions_support
		# 	available = {}
		# 	visible = {}
		# 	cost = 50
		# 	complete_effect = {
		# 		add_power_balance_value = {
		# 			id = ARAB_liberalism_conservatism
		# 			value = -0.1
		# 			tooltip_side = ARAB_liberalism_side
		# 		}
		# 		hidden_effect = {
		# 			set_temp_variable = { temp_modify_social_con_government = -0.1 }
		# 			modify_social_conservatism_government = yes
		# 		}
		# 		log = "[GetDateText]: [Root.GetName]: Decision ARAB_ASSAD_baath_message"
		# 	}
		# }
		# ARAB_NASSER_arab_determination = {
		# 	icon = eng_trade_unions_support
		# 	available = {}
		# 	visible = {}
		# 	cost = 50
		# 	complete_effect = {
		# 		add_power_balance_value = {
		# 			id = ARAB_liberalism_conservatism
		# 			value = -0.1
		# 			tooltip_side = ARAB_liberalism_side
		# 		}
		# 		hidden_effect = {
		# 			set_temp_variable = { temp_modify_social_con_government = -0.1 }
		# 			modify_social_conservatism_government = yes
		# 		}
		# 		log = "[GetDateText]: [Root.GetName]: Decision ARAB_ASSAD_baath_message"
		# 	}
		# }
		# ARAB_NASSER_interarab_cooperation = {
		# 	icon = eng_trade_unions_support
		# 	available = {}
		# 	visible = {}
		# 	cost = 50
		# 	complete_effect = {
		# 		add_power_balance_value = {
		# 			id = ARAB_liberalism_conservatism
		# 			value = -0.1
		# 			tooltip_side = ARAB_liberalism_side
		# 		}
		# 		hidden_effect = {
		# 			set_temp_variable = { temp_modify_social_con_government = -0.1 }
		# 			modify_social_conservatism_government = yes
		# 		}
		# 		log = "[GetDateText]: [Root.GetName]: Decision ARAB_ASSAD_baath_message"
		# 	}
		# }
		# ARAB_NASSER_neutrality_promises = {
		# 	icon = eng_trade_unions_support
		# 	available = {}
		# 	visible = {}
		# 	cost = 50
		# 	complete_effect = {
		# 		add_power_balance_value = {
		# 			id = ARAB_liberalism_conservatism
		# 			value = -0.1
		# 			tooltip_side = ARAB_liberalism_side
		# 		}
		# 		hidden_effect = {
		# 			set_temp_variable = { temp_modify_social_con_government = -0.1 }
		# 			modify_social_conservatism_government = yes
		# 		}
		# 		log = "[GetDateText]: [Root.GetName]: Decision ARAB_ASSAD_baath_message"
		# 	}
		# }
		# ARAB_NASSER_republican_promises = {
		# 	icon = eng_trade_unions_support
		# 	available = {}
		# 	visible = {}
		# 	cost = 50
		# 	complete_effect = {
		# 		add_power_balance_value = {
		# 			id = ARAB_liberalism_conservatism
		# 			value = -0.1
		# 			tooltip_side = ARAB_liberalism_side
		# 		}
		# 		hidden_effect = {
		# 			set_temp_variable = { temp_modify_social_con_government = -0.1 }
		# 			modify_social_conservatism_government = yes
		# 		}
		# 		log = "[GetDateText]: [Root.GetName]: Decision ARAB_ASSAD_baath_message"
		# 	}
		# }
	# SADDAMISM
		ARAB_SADDAM_babel = {
			icon = eng_trade_unions_support
			available = {
				if = {
					limit = { original_tag = IRQ }
					always = yes
				}
				else = {
					IRQ = {
						has_opinion = {
							target = ROOT
							value > -1
						}
					}
					power_balance_value = {
						id = ARAB_assadist_saddamist
						value > -0.75
					}
				}
			}
			visible = {}
			custom_cost_trigger = { check_variable = { treasury > 1.99 } }
			custom_cost_text = cost_2_0
			days_remove = 70
			days_re_enable = 180
			complete_effect = {
				add_power_balance_modifier = {
					id = ARAB_assadist_saddamist
					modifier = ARAB_SADDAMISM_small_weekly
				}
				set_temp_variable = { treasury_change = -2 }
				modify_treasury_effect = yes
				log = "[GetDateText]: [Root.GetName]: Decision ARAB_SADDAM_babel"
			}
			remove_effect = {
				remove_power_balance_modifier = {
					id = ARAB_assadist_saddamist
					modifier = ARAB_SADDAMISM_small_weekly
				}
				hidden_effect = {
					set_temp_variable = { baathism_change = 0.07 }
					modify_baathism = yes
				}
				log = "[GetDateText]: [Root.GetName]: Decision remove ARAB_SADDAM_babel"
			}
		}
		ARAB_SADDAM_aid_from_baghdad = {
			icon = eng_trade_unions_support
			available = {
				IRQ = {
					has_opinion = {
						target = ROOT
						value > 24
					}
				}
				power_balance_value = {
					id = ARAB_assadist_saddamist
					value > 0.25
				}
			}
			visible = {
				NOT = { original_tag = IRQ }
			}
			cost = 100
			days_re_enable = 360
			complete_effect = {
				IRQ = { country_event = { id = UAR.2 } }
				log = "[GetDateText]: [Root.GetName]: Decision ARAB_SADDAM_aid_from_baghdad"
			}
		}
		ARAB_SADDAM_reject_nasserism = {
			icon = eng_trade_unions_support
			available = {
				power_balance_value = {
					id = ARAB_assadist_saddamist
					value > -0.25
				}
			}
			visible = {}
			cost = 50
			days_remove = 70
			days_re_enable = 360
			complete_effect = {
				add_power_balance_modifier = {
					id = ARAB_assadist_saddamist
					modifier = ARAB_SADDAMISM_medium_weekly
				}
				log = "[GetDateText]: [Root.GetName]: Decision ARAB_ASSAD_corrective_movement"
			}
			remove_effect = {
				remove_power_balance_modifier = {
					id = ARAB_assadist_saddamist
					modifier = ARAB_SADDAMISM_medium_weekly
				}
				hidden_effect = {
					set_temp_variable = { baathism_change = 0.2 }
					modify_baathism = yes
				}
				log = "[GetDateText]: [Root.GetName]: Decision remove ARAB_ASSAD_corrective_movement"
			}
		}
		ARAB_SADDAM_militarism = {
			icon = eng_trade_unions_support
			available = {
				power_balance_value = {
					id = ARAB_assadist_saddamist
					value > 0.2
				}
			}
			visible = {
				check_variable = { ARAB_military_parade < 3 }
			}
			cost = 0
			days_remove = 90
			days_re_enable = 180
			complete_effect = {
				add_timed_idea = {
					idea = ARAB_militarist
					days = 180
				}
				log = "[GetDateText]: [Root.GetName]: Decision ARAB_SADDAM_militarism"
			}
			remove_effect = {
				add_power_balance_value = {
					id = ARAB_assadist_saddamist
					value = 0.1
					tooltip_side = ARAB_saddamism_side
				}
				hidden_effect = {
					set_temp_variable = { baathism_change = 0.1 }
					modify_baathism = yes
					add_to_variable = { ARAB_military_parade = 1 }
				}
				log = "[GetDateText]: [Root.GetName]: Decision remove ARAB_SADDAM_militarism"
			}
		}
		ARAB_SADDAM_rewrite_history = {
			icon = eng_trade_unions_support
			available = {
				power_balance_value = {
					id = ARAB_assadist_saddamist
					value > 0.5
				}
			}
			visible = {
				any_character = {
					NOT = { has_trait = ARAB_patriotic_educator_SADDAMISM }
					NOT = { has_trait = ARAB_anti_marxist_SADDAMISM }
				}
			}
			cost = 200
			days_remove = 35
			complete_effect = {
				add_country_leader_trait = ARAB_patriotic_educator_SADDAMISM
				add_power_balance_modifier = {
					id = ARAB_assadist_saddamist
					modifier = ARAB_SADDAMISM_small_weekly
				}
				log = "[GetDateText]: [Root.GetName]: Decision ARAB_SADDAM_rewrite_history"
			}
			remove_effect = {
				remove_power_balance_modifier = {
					id = ARAB_assadist_saddamist
					modifier = ARAB_SADDAMISM_small_weekly
				}
				hidden_effect = {
					set_temp_variable = { baathism_change = 0.025 }
					modify_baathism = yes
				}
				log = "[GetDateText]: [Root.GetName]: Decision remove ARAB_SADDAM_rewrite_history"
			}
		}
		ARAB_SADDAM_oppose_marxism = {
			icon = eng_trade_unions_support
			available = {
				power_balance_value = {
					id = ARAB_assadist_saddamist
					value > 0.75
				}
			}
			visible = {
				any_character = {
					has_trait = ARAB_patriotic_educator_SADDAMISM
					NOT = { has_trait = ARAB_anti_marxist_SADDAMISM }
				}
			}
			days_remove = 70
			custom_cost_trigger = { check_variable = { treasury > 4.99 } }
			custom_cost_text = cost_5_0
			complete_effect = {
				if = {
					limit = {
						any_character = {
							has_trait = ARAB_patriotic_educator_SADDAMISM
						}
					}
					remove_country_leader_trait = ARAB_patriotic_educator_SADDAMISM
				}
				add_country_leader_trait = ARAB_anti_marxist_SADDAMISM
				add_power_balance_modifier = {
					id = ARAB_assadist_saddamist
					modifier = ARAB_SADDAMISM_small_weekly
				}
				log = "[GetDateText]: [Root.GetName]: Decision ARAB_SADDAM_oppose_marxism"
			}
			remove_effect = {
				remove_power_balance_modifier = {
					id = ARAB_assadist_saddamist
					modifier = ARAB_SADDAMISM_small_weekly
				}
				hidden_effect = {
					set_temp_variable = { baathism_change = 0.1 }
					modify_baathism = yes
				}
				log = "[GetDateText]: [Root.GetName]: Decision remove ARAB_SADDAM_oppose_marxism"
			}
		}
		ARAB_SADDAM_official_alignment = {
			icon = eng_trade_unions_support
			available = {
				power_balance_value = {
					id = ARAB_assadist_saddamist
					value > 0.9
				}
			}
			visible = {
				NOT = { has_idea = ARAB_iraqi_baathism }
			}
			cost = 250
			complete_effect = {
				add_ideas = ARAB_iraqi_baathism
				add_power_balance_value = {
					id = ARAB_assadist_saddamist
					value = 0.15
					tooltip_side = ARAB_saddamism_side
				}
				hidden_effect = {
					set_temp_variable = { baathism_change = 0.15 }
					modify_baathism = yes
				}
				log = "[GetDateText]: [Root.GetName]: Decision ARAB_SADDAM_official_alignment"
			}
		}
}
AFG	Afghanistan			1 - 1000
ALB	Albania		          	1001 - 2000
ALG	Algeria		         	2001 - 3000
ANG	Angola		         	3001 - 4000
SCO	Antigua&Barbuda	         	4001 - 5000
ARG	Argentina			5001 - 6000
ARM	Armenia		         	6001 - 7000
AST	Australia			7001 - 8000
AUS	Austria		         	8001 - 9000
AZB	Azerbaijan			9001 - 10000
BRU	Bahamas		         	10001 - 11000
CAL	Bangladesh			11001 - 12000
DFR	Barbados			12001 - 13000
BLR	Belarus		         	13001 - 14000
BEL	Belgium		         	14001 - 15000
CSA	Belize                          15001 - 16000
BEN	Benin		         	16001 - 17000
BHU	Tamil Eelam               	17001 - 18000
BOL	Bolivia		         	18001 - 19000
BOS	Bosnia & Herzegovina      	19001 - 20000
U07	Botswana			20001 - 21000
BRA	Brazil		                21001 - 22000
BUL	Bulgaria			22001 - 23000
U08	Burkina Faso			23001 - 24000
BUR	Burma		         	24001 - 25000
CMB	Cambodia			25001 - 26000
CAM	Cameroon			26001 - 27000
CAN	Canada		         	27001 - 28000
AXI	Cap Verde			28001 - 29000
U10	Centralafrican Republic    	29001 - 30000
OTT	Chad		         	15501 - 16000
CHI	Chechnya			16001 - 16500
CHL	Chile		         	16501 - 17000
CHC	China		         	17001 - 17500
COL	Colombia			17501 - 18000
COS	Costa Rica			18001 - 18500
CRO	Croatia		         	18501 - 19000
CUB	Cuba		         	19001 - 19500
CYP	Cyprus		         	19501 - 20000
CZE	Czech Republic	         	20001 - 20500
U11	Democratic Republic of Congo	20501 - 21000
DEN	Denmark		         	21001 - 21500
DDR	Djibouti			21501 - 22000
DOM	Dominican Republic		22001 - 22500
TEX	East Timor			22501 - 23000
ECU	Ecuador		         	23001 - 23500
EGY	Egypt		         	23501 - 24000
SAL	El Salvador			24001 - 24500
EQA	Equatorial Guinea		24501 - 25000
U13	Eritrea		         	25001 - 25500
EST	Estonia		         	25501 - 26000
ETH	Ethiopia			26001 - 26500
U06	FARC			        26501 - 27000
TEX	Federation of Micronesia	27001 - 27500
FLA	Fiji			        27501 - 28000
FIN	Finland		         	28001 - 28500
FRA	France		         	28501 - 29000
SCA	FYROM		         	29001 - 29500
GAB	Gabon		         	29501 - 30000
         	        30001 - 30500
GEO	Georgia		         	30501 - 31000
GER	Germany		         	31001 - 31500
GLD	Ghana		         	31501 - 32000
GRE	Greece		         	32001 - 32500
ALS	Grenada		         	32501 - 33000
GUA	Guatemala			33001 - 33500
GUI	Guinea		         	33501 - 34000
IDC	Guinea-Bissau	         	34001 - 34500
GUY	Guyana		         	34501 - 35000
HAI	Haiti		         	35001 - 35500
HON	Honduras			35501 - 36000
HUN	Hungary		         	36001 - 36500
ICL	Iceland		         	36501 - 37000
IND	India		         	37001 - 37500
INO	Indonesia			37501 - 38000
PER	Iran		         	38001 - 38500
IRQ	Iraq		         	38501 - 39000
IRE	Ireland		         	39001 - 39500
ISR	Israel		         	39501 - 40000
ITA	Italy		         	40001 - 40500
U12	Ivory Coast			40501 - 41000
CSX	Jamaica		         	41001 - 41500
JAP	Japan		         	41501 - 42000
JOR	Jordan		         	42001 - 42500
KAZ	Kazakhstan			42501 - 43000
U15	Kenya		         	43001 - 43500
EAF	Kiribati			43501 - 44000
KUR	Kurdistan			44001 - 44500
U16	Kuwait		         	44501 - 45000
KYG	Kyrgyzstan			45001 - 45500
LAO	Laos		         	45501 - 46000
EUS	Lappland			46001 - 46500
LAT	Latvia		         	46501 - 47000
LEB	Lebanon		         	47001 - 47500
LIB	Liberia		         	47501 - 48000
LBY	Libya		         	48001 - 48500
LIT	Lithuania			48501 - 49000
LUX	Luxembourg			49001 - 49500
MAD	Madagascar			49501 - 50000
U05	Malawi		         	50001 - 50500
MLY	Malaysia			50501 - 51000
MAL	Mali		         	51001 - 51500
MAN	Malta		         	51501 - 52000
PRI	Marshall Islands		52001 - 52500
MEN	Mauritania			52501 - 53000
U02	Mauritius			53001 - 53500
MEX	Mexico		         	53501 - 54000
U17	Moldova		         	54001 - 54500
MON	Mongolia			54501 - 55000
MTN	Montenegro			55001 - 55500
MOR	Morocco		         	55501 - 56000
MOZ	Mozambique			56001 - 56500
NAM	Namibia		         	56501 - 57000
YUG	Somaliland		        57001 - 57500
NEP	Nepal		         	57501 - 58000
HOL	Netherlands			58001 - 58500
NZL	New Zealand	         	58501 - 59000
NIC	Nicaragua			59001 - 59500
VIC	Niger		         	59501 - 60000
NIG	Nigeria		         	60001 - 60500
U09	North Korea			60501 - 61000
NOR	Norway		         	61001 - 61500
CGX	Novorossiya	         	61501 - 62000
OMN	Oman		         	62001 - 62500
PAK	Pakistan			62501 - 63000
U03	Transnistria		        63001 - 63500
PAL	Palestine			63501 - 64000
PAN	Panama		         	64001 - 64500
PRK	Papua-New Guinea		64501 - 65000
PAR	Paraguay			65001 - 65500
PRU	Peru		         	65501 - 66000
PHI	Philippines			66001 - 66500
POL	Poland		         	66501 - 67000
POR	Portugal			67001 - 67500
QUE	Qatar		         	67501 - 68000
CON	Republic of Congo		68001 - 68500
ROM	Romania		         	68501 - 69000
RUS	Russia		         	69001 - 69500
RSI	Rwanda		          	69501 - 70000
SAU	Saudi-Arabia	         	70001 - 70500
SIB	Senegal		         	70501 - 71000
SER	Serbia                  	71001 - 71500
U01	Seychelles			71501 - 72000
SIE	Sierra Leone			72001 - 72500
SIK	Singapore			72501 - 73000
SLO	Slovakia			73001 - 73500
SLV	Slovenia			73501 - 74000
ARA	Northern Cyprus	         	74001 - 74500
SOM	Somalia		         	74501 - 75000
SAF	South Africa			75001 - 75500
KOR	South Korea			75501 - 76000
SPA	Spain		         	76001 - 76500
SAR	Sri Lanka			76501 - 77000
CYN	St. Lucia			77001 - 77500
SUD	Sudan		         	77501 - 78000
SPR	Suriname			78001 - 78500
U00	Swaziland			78501 - 79000
SWE	Sweden		         	79001 - 79500
SCH	Switzerland			79501 - 80000
SYR	Syria		         	80001 - 80500
TRA	Taiwan		         	80501 - 81000
TAJ	Tajikistan			81001 - 81500
TAN	Tanzania			81501 - 82000
SIA	Thailand			82001 - 82500
TIB	Tibet		         	82501 - 83000
RHO	Togo		         	83001 - 83500
CXB	Trinidad & Tobago		83501 - 84000
TUN	Tunisia		         	84001 - 84500
TUR	Turkey		         	84501 - 85000
TRK	Turkmenistan	         	85001 - 85500
U18	Uganda		         	85501 - 86000
UKR	Ukraine		         	86001 - 86500
U14	United Arab Emirates      	86501 - 87000
ENG	United Kingdom	         	87001 - 87500
USA	United States	         	87501 - 88000
URU	Uruguay		         	88001 - 88500
UZB	Uzbekistan			88501 - 89000
VIE	Scotland	         	89001 - 89500
VEN	Venezuela			89501 - 90000
WLL	Vietnam		         	90001 - 90500
U04	Republika Srpska	        90501 - 91000
YEM	Yemen		         	91001 - 91500
U19	Zambia		         	91501 - 92000
ALI	Zimbabwe			92001 - 92500			
SOV     Quebec                          92501 - 93000
MTN     Montenegro                      93001 - 93500
CYN     Kosovo                          93501 - 94000
ALS     Abkhazia                        94001 - 94500
PRI     South Ossetia                   94501 - 95000
    

NATO events                             100001 - 100500
Shanghai Pact events                    100501 - 101000
European Union events                   101001 - 101500
Surrender events                        101501 - 102000
Flavor events                           150000 - 160000
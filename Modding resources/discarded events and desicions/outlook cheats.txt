	open_cheat_decisions_outlook = {

		icon = civil_support

		visible = {
			IF = {
				limit = {
					has_country_flag = game_rule_allow_toggling_cheat_decisions
				}
				has_country_flag = enable_all_decisions
			}
			has_country_flag = cheat_decisions_open
			has_global_flag = game_rule_allow_cheat_decisions
		}

		#cost = 0

		#days_remove = 1
		#days_re_enable = 1

		complete_effect = {
			if = {
				limit = { NOT = { has_country_flag = cheat_decisions_open_outlook } }
				set_country_flag = cheat_decisions_open_outlook
			}
			else = {
				clr_country_flag = cheat_decisions_open_outlook
			}
		}


	}

	cheat_decisions_outlook_nationalist = {

		visible = {
			IF = {
				limit = {
					has_country_flag = game_rule_allow_toggling_cheat_decisions
				}
				has_country_flag = enable_all_decisions
			}
			has_country_flag = cheat_decisions_open
			has_global_flag = game_rule_allow_cheat_decisions
			has_country_flag = cheat_decisions_open_outlook
		}

		#cost = 0

		#days_remove = 1
		#days_re_enable = 1

		complete_effect = {
			IF = {
				limit = {
					NOT = {
						has_government = nationalist
					}
				}
				clear_array = ruling_party
				clear_array = gov_coalition_array
				set_politics = {
					ruling_party = nationalist
				}
				meta_effect = {
					text = {
						set_country_flag = [META_SET_RULING_PARTY]
					}
					META_SET_RULING_PARTY = "[meta_set_ruling_leader]"
				}
				set_leader = yes
			}
			add_popularity = {
				ideology = nationalist
				popularity = 0.5
			}
			recalculate_party = yes
		}


	}

	cheat_decisions_outlook_neutrality = {

		visible = {
			IF = {
				limit = {
					has_country_flag = game_rule_allow_toggling_cheat_decisions
				}
				has_country_flag = enable_all_decisions
			}
			has_country_flag = cheat_decisions_open
			has_global_flag = game_rule_allow_cheat_decisions
			has_country_flag = cheat_decisions_open_outlook
		}

		#cost = 0

		#days_remove = 1
		#days_re_enable = 1

		complete_effect = {
			IF = {
				limit = {
					NOT = {
						has_government = neutrality
					}
				}
				set_politics = {
					ruling_party = neutrality
				}
				meta_effect = {
					text = {
						set_country_flag = [META_SET_RULING_PARTY]
					}
					META_SET_RULING_PARTY = "[meta_set_ruling_leader]"
				}
				set_leader = yes
			}
			add_popularity = {
				ideology = neutrality
				popularity = 0.5
			}
			recalculate_party = yes
		}


	}

	cheat_decisions_outlook_fascism = {

		visible = {
			IF = {
				limit = {
					has_country_flag = game_rule_allow_toggling_cheat_decisions
				}
				has_country_flag = enable_all_decisions
			}
			has_country_flag = cheat_decisions_open
			has_global_flag = game_rule_allow_cheat_decisions
			has_country_flag = cheat_decisions_open_outlook
		}

		#cost = 0

		#days_remove = 1
		#days_re_enable = 1

		complete_effect = {
			IF = {
				limit = {
					NOT = {
						has_government = fascism
					}
				}
				set_politics = {
					ruling_party = fascism
				}
				meta_effect = {
					text = {
						set_country_flag = [META_SET_RULING_PARTY]
					}
					META_SET_RULING_PARTY = "[meta_set_ruling_leader]"
				}
				set_leader = yes
			}
			add_popularity = {
				ideology = fascism
				popularity = 0.5
			}
			recalculate_party = yes
		}


	}

	cheat_decisions_outlook_democratic = {

		visible = {
			IF = {
				limit = {
					has_country_flag = game_rule_allow_toggling_cheat_decisions
				}
				has_country_flag = enable_all_decisions
			}
			has_country_flag = cheat_decisions_open
			has_global_flag = game_rule_allow_cheat_decisions
			has_country_flag = cheat_decisions_open_outlook
		}

		#cost = 0

		#days_remove = 1
		#days_re_enable = 1

		complete_effect = {
			IF = {
				limit = {
					NOT = {
						has_government = democratic
					}
				}
				set_politics = {
					ruling_party = democratic
				}
				meta_effect = {
					text = {
						set_country_flag = [META_SET_RULING_PARTY]
					}
					META_SET_RULING_PARTY = "[meta_set_ruling_leader]"
				}
				set_leader = yes
			}
			add_popularity = {
				ideology = democratic
				popularity = 0.5
			}
			recalculate_party = yes
		}


	}

	cheat_decisions_outlook_communism = {

		visible = {
			IF = {
				limit = {
					has_country_flag = game_rule_allow_toggling_cheat_decisions
				}
				has_country_flag = enable_all_decisions
			}
			has_country_flag = cheat_decisions_open
			has_global_flag = game_rule_allow_cheat_decisions
			has_country_flag = cheat_decisions_open_outlook
		}

		#cost = 0

		#days_remove = 1
		#days_re_enable = 1

		complete_effect = {
			IF = {
				limit = {
					NOT = {
						has_government = communism
					}
				}
				set_politics = {
					ruling_party = communism
				}
				meta_effect = {
					text = {
						set_country_flag = [META_SET_RULING_PARTY]
					}
					META_SET_RULING_PARTY = "[meta_set_ruling_leader]"
				}
				set_leader = yes
			}
			add_popularity = {
				ideology = communism
				popularity = 0.5
			}
			recalculate_party = yes
		}


	}
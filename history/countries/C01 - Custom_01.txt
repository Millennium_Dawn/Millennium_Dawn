2000.1.1 = {
	set_cosmetic_tag = C01

	set_technology = {
		util_vehicle_0 = 1
		util_vehicle_1 = 1
		util_vehicle_2 = 1
		util_vehicle_3 = 1
		util_vehicle_4 = 1
		util_vehicle_5 = 1
		util_vehicle_6 = 1
		util_vehicle_7 = 1
	}
	create_equipment_variant = {
		name = "Cougar MRAP"
		type = util_vehicle_4
		upgrades = {
		}
		mark_older_equipment_obsolete = yes
		icon = "gfx/interface/technologies/USA/LAND/U5_alt1.dds"
	}
	set_popularities = {
		democratic = 100.0
		communism = 0.0
		fascism = 0.0
		neutrality = 0.0
		nationalist = 0.0
	}
	set_politics = {
		ruling_party = democratic
		last_election = "1998.9.13"
		election_frequency = 48
		elections_allowed = yes
	}

	start_politics_input = yes

	set_variable = { party_pop_array^0 = 0.096 } #Western_Autocracy
	set_variable = { party_pop_array^1 = 0.052 } #conservatism
	set_variable = { party_pop_array^2 = 0.214 } #liberalism
	set_variable = { party_pop_array^3 = 0.076 } #socialism
	set_variable = { party_pop_array^4 = 0.001 } #Communist-State
	set_variable = { party_pop_array^5 = 0.023 } #anarchist_communism
	set_variable = { party_pop_array^6 = 0 } #Conservative
	set_variable = { party_pop_array^7 = 0 } #Autocracy
	set_variable = { party_pop_array^8 = 0 } #Mod_Vilayat_e_Faqih
	set_variable = { party_pop_array^9 = 0 } #Vilayat_e_Faqih
	set_variable = { party_pop_array^10 = 0 } #Kingdom
	set_variable = { party_pop_array^11 = 0 } #Caliphate
	set_variable = { party_pop_array^12 = 0 } #Neutral_Muslim_Brotherhood
	set_variable = { party_pop_array^13 = 0 } #Neutral_Autocracy
	set_variable = { party_pop_array^14 = 0.155 } #Neutral_conservatism
	set_variable = { party_pop_array^16 = 0 } #Neutral_Libertarian
	set_variable = { party_pop_array^17 = 0 } #Neutral_green
	set_variable = { party_pop_array^18 = 0 } #neutral_Social
	set_variable = { party_pop_array^19 = 0.01 } #Neutral_Communism
	set_variable = { party_pop_array^20 = 0.212 } #Nat_Populism
	set_variable = { party_pop_array^21 = 0.17 } #Nat_Fascism
	set_variable = { party_pop_array^22 = 0 } #Nat_Autocracy
	set_variable = { party_pop_array^23 = 0 } #Monarchist

	add_to_array = { ruling_party = 6 } #Conservative

	set_variable = { party_pop_elect_array^20 = 0.334 } #Conservative

	startup_politics = yes
}

2017.1.1 = {
	set_cosmetic_tag = C01

	set_technology = {
		util_vehicle_0 = 1
		util_vehicle_1 = 1
		util_vehicle_2 = 1
		util_vehicle_3 = 1
		util_vehicle_4 = 1
		util_vehicle_5 = 1
		util_vehicle_6 = 1
		util_vehicle_7 = 1
	}
	set_popularities = {
		democratic = 100.0
		communism = 0.0
		fascism = 0.0
		neutrality = 0.0
		nationalist = 0.0
	}
	set_politics = {
		ruling_party = democratic
		last_election = "1998.9.13"
		election_frequency = 48
		elections_allowed = yes
	}

	start_politics_input = yes

	set_variable = { party_pop_array^0 = 0.016 } #Western_Autocracy
	set_variable = { party_pop_array^1 = 0.026 } #conservatism
	set_variable = { party_pop_array^2 = 0.501 } #liberalism
	set_variable = { party_pop_array^3 = 0.10 } #socialism
	set_variable = { party_pop_array^4 = 0.007 } #Communist-State
	set_variable = { party_pop_array^5 = 0.016 } #anarchist_communism
	set_variable = { party_pop_array^6 = 0 } #Conservative
	set_variable = { party_pop_array^7 = 0.016 } #Autocracy
	set_variable = { party_pop_array^8 = 0 } #Mod_Vilayat_e_Faqih
	set_variable = { party_pop_array^9 = 0 } #Vilayat_e_Faqih
	set_variable = { party_pop_array^10 = 0 } #Kingdom
	set_variable = { party_pop_array^11 = 0 } #Caliphate
	set_variable = { party_pop_array^12 = 0 } #Neutral_Muslim_Brotherhood
	set_variable = { party_pop_array^13 = 0 } #Neutral_Autocracy
	set_variable = { party_pop_array^14 = 0.033 } #Neutral_conservatism
	set_variable = { party_pop_array^16 = 0 } #Neutral_Libertarian
	set_variable = { party_pop_array^17 = 0.014 } #Neutral_green
	set_variable = { party_pop_array^18 = 0.109 } #neutral_Social
	set_variable = { party_pop_array^19 = 0 } #Neutral_Communism
	set_variable = { party_pop_array^20 = 0.039 } #Nat_Populism
	set_variable = { party_pop_array^21 = 0.116 } #Nat_Fascism
	set_variable = { party_pop_array^22 = 0 } #Nat_Autocracy
	set_variable = { party_pop_array^23 = 0.007 } #Monarchist

	add_to_array = { ruling_party = 2 } #liberalism

	startup_politics = yes

	create_country_leader = {
		name = "Milorad Dodik"
		picture = "milorad_dodik.dds"
		ideology = neutral_Social
		traits = {
			western_liberalism
		}
	}
}
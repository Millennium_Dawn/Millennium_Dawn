instant_effect = {
	add_equipment_to_stockpile = {
		type = MR_Fighter1		 #F-5
		amount = 33
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter1		 #Mirage F1
		amount = 29
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = transport_plane1		 #C-130
		amount = 11
		producer = USA
	}
}
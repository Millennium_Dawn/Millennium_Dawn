units = {
	fleet = {
		name = "An tSeirbhis Chabhlaigh"
		naval_base = 7394
		task_force = {
			name = "An tSeirbhis Chabhlaigh"
			location = 7394
			ship = { name = "LE Roisin" definition = corvette start_experience_factor = 0.40 equipment = { corvette_hull_3 = { amount = 1 owner = IRE creator = ENG version_name = "Roisin Class" } } }
			ship = { name = "LE Niamh" definition = corvette start_experience_factor = 0.40 equipment = { corvette_hull_3 = { amount = 1 owner = IRE creator = ENG version_name = "Roisin Class" } } }
			ship = { name = "LE Ciara" definition = corvette start_experience_factor = 0.40 equipment = { corvette_hull_2 = { amount = 1 owner = IRE creator = ENG version_name = "Peacock Class" } } }
			ship = { name = "LE Orla" definition = corvette start_experience_factor = 0.40 equipment = { corvette_hull_2 = { amount = 1 owner = IRE creator = ENG version_name = "Peacock Class" } } }
		}
	}
}
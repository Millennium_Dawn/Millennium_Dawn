﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = transport_plane2 #C-212 Aviocar
		amount = 1
		producer = SPR
	}
	add_equipment_to_stockpile = {
		type = transport_plane1 #Aeritalia G.222
		amount = 2
		producer = ITA
	}
	add_equipment_to_stockpile = {
		type = Strike_fighter1 #IAI Nesher
		amount = 23
		producer = ISR
	}
	add_equipment_to_stockpile = {
		type = Strike_fighter1 #Dassault Mirage 5
		amount = 6
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = Strike_fighter2 #A-4AR Fightinghawk
		amount = 36
		producer = ARG
	}
	add_equipment_to_stockpile = {
		type = AS_Fighter2 #Dassault Mirage III
		amount = 14
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = L_Strike_fighter1 #IA 58 Pucará
		amount = 30
		producer = ARG
	}
	add_equipment_to_stockpile = {
		type = transport_plane1 #C-130 Hercules
		amount = 17
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = L_Strike_fighter1 #A-37 Dragonfly
		amount = 30
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = L_Strike_fighter2 #Embraer EMB-312 Tucano
		amount = 27
		producer = BRA
	}
	add_equipment_to_stockpile = {
		type = L_Strike_fighter2 #IA 63 Pampa
		amount = 14
		producer = ARG
	}

	#helis
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #AgustaWestland AW109
		amount = 6
		producer = ITA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Eurocopter AS332 Super Puma
		amount = 3
		producer = FRA
		#version_name = "Eurocopter AS332 Super Puma"
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Eurocopter AS332 Super Puma
		amount = 3
		producer = FRA
		#version_name = "Eurocopter AS332 Super Puma"
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Bell UH-1 Iroquois
		amount = 36
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Aérospatiale SA-330 Puma
		amount = 1
		producer = FRA
	}
}
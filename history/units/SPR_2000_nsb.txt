﻿division_template = {
	name = "Mechanized Brigade"

	division_names_group = SPR_ARMY_DIVISIONS

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		Arm_Inf_Bat = { x = 0 y = 1 }
		armor_Bat = { x = 1 y = 0 }
		Mech_Inf_Bat = { x = 2 y = 0 }
		SP_Arty_Bat = { x = 3 y = 0 }
	}
	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		armor_Recce_Comp = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Recon Brigade"

	division_names_group = SPR_ARMY_BRIGADES

	regiments = {
		Arm_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 1 y = 0 }
		Mech_Inf_Bat = { x = 1 y = 1 }
		Mot_Inf_Bat = { x = 2 y = 0 }
		Arty_Bat = { x = 3 y = 0 }
	}
	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		Mot_Recce_Comp = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Parachute Brigade"

	division_names_group = SPR_PARATROOPERS_BRIGADES

	regiments = {
		L_Air_Inf_Bat = { x = 0 y = 0 }
		Mech_Air_Inf_Bat = { x = 1 y = 0 }
		Mech_Air_Inf_Bat = { x = 1 y = 1 }
	}
	support = {
		Arty_Battery = { x = 0 y = 0 }
		Mot_Recce_Comp = { x = 0 y = 1 }
		armor_Recce_Comp = { x = 0 y = 2 }
	}
}
division_template = {
	name = "Mechanized Brigade 2"

	division_names_group = SPR_ARMY_BRIGADES

	regiments = {
		L_Air_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 1 y = 0 }
		Mech_Inf_Bat = { x = 1 y = 1 }
		Mot_Inf_Bat = { x = 2 y = 0 }
		Arty_Bat = { x = 3 y = 0 }
	}
	support = {
		Mot_Recce_Comp = { x = 0 y = 0 }
		armor_Recce_Comp = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Cavalry Regiment"

	division_names_group = SPR_CAVALRY_REGIMENT

	regiments = {
		Arm_Inf_Bat = { x = 0 y = 0 }
	}
	support = {
		armor_Comp = { x = 0 y = 0 }
	}
}
division_template = {
	name = "Cavalry Regiment 2"

	division_names_group = SPR_CAVALRY_REGIMENT

	regiments = {
		Arm_Inf_Bat = { x = 0 y = 0 }
		Arm_Inf_Bat = { x = 0 y = 1 }
	}
	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		armor_Comp = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Special Operations Group"

	division_names_group = SPR_SPEC_FORCES_BRIGADES

	regiments = {
		Special_Forces = { x = 0 y = 0 }
	}

	priority = 2
}
division_template = {
	name = "Light Infantry Brigade"

	division_names_group = SPR_ARMY_BRIGADES

	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
		L_Inf_Bat = { x = 0 y = 2 }
		L_Inf_Bat = { x = 0 y = 3 }
	}
	support = {
		L_Engi_Comp = { x = 0 y = 0 }
		Arty_Battery = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Marine Infantry Brigade"

	division_names_group = SPR_MAR_BRIGADES

	regiments = {
		L_Marine_Bat = { x = 0 y = 0 }
		L_Marine_Bat = { x = 0 y = 1 }
		Mech_Marine_Bat = { x = 1 y = 0 }
	}
	support = {
		Mot_Recce_Comp = { x = 0 y = 0 }
		armor_Comp = { x = 0 y = 1 }
		Arty_Battery = { x = 0 y = 2 }
		SP_Arty_Battery = { x = 0 y = 3 }
		SP_AA_Battery = { x = 0 y = 4 }
	}
}

units = {
	#Division "San Marcial"
	division = {
		name = "I Brigade Aragón"
		location = 3816
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
	division = {
		name = "X Brigade Guzmán el Bueno"
		location = 875
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
	division = {
		name = "XI Brigade Extremadura"
		location = 6902
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
	division = {
		name = "XII Brigade Guadarrama"
		location = 9767
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}

	#Division Castillejos
	division = {
		name = "VI Parachute Brigade Almogávares"
		location = 3938
		division_template = "Parachute Brigade"
		start_experience_factor = 0.55
		start_equipment_factor = 0.01
	}
	division = {
		name = "VII Brigade Galicia"
		location = 6734
		division_template = "Mechanized Brigade 2"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
	division = {
		name = "VIII Brigade Galicia"
		location = 6734
		division_template = "Mechanized Brigade 2"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
	division = {
		name = "Mountain Infantry Brigade"
		location = 6734
		division_template = "Light Infantry Brigade"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
	division = {
		name = "Light Infantry Brigade"
		location = 6734
		division_template = "Light Infantry Brigade"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
	division = {
		name = "Light Infantry Brigade"
		location = 6734
		division_template = "Light Infantry Brigade"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
	division = {
		name = "Light Infantry Brigade"
		location = 6734
		division_template = "Light Infantry Brigade"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
	division = {
		name = "Light Infantry Brigade"
		location = 6734
		division_template = "Light Infantry Brigade"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}

	#Balearic General Command

	#Ceuta General Command
	division = {
		name = "3rd Cavalry Regiment Montesa"
		location = 3799
		division_template = "Cavalry Regiment"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	#Melilla General Command
	division = {
		name = "10th Cavalry Regiment Alcántara"
		location = 9877
		division_template = "Cavalry Regiment"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		name = "11th Cavalry Regiment Alcántara"
		location = 9877
		division_template = "Cavalry Regiment"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	#Special Operations Command
	division = {
		name = "2nd Special Operations Group Granada"
		location = 1176
		division_template = "Special Operations Group"
		start_experience_factor = 0.9
		start_equipment_factor = 0.01
	}
	division = {
		name = "3rd Special Operations Group Valencia"
		location = 6906
		division_template = "Special Operations Group"
		start_experience_factor = 0.9
		start_equipment_factor = 0.01
	}
	division = {
		name = "4th Special Operations Group Tercio del Ampurdán"
		location = 3938
		division_template = "Special Operations Group"
		start_experience_factor = 0.9
		start_equipment_factor = 0.01
	}
	division = {
		name = "XVI Brigade Canarias"
		location = 962
		division_template = "Marine Infantry Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	#2 Hem&n Cortes (US Newport) 1ST, capcity: 400 rps, 500t vehides, 3 LCVPs, 1LCPL
	#2 Galicia LPD, capadty 620 rps, 6 LCVP Plus 13 craft: 3 LCT, 2 LCU, 8 LCM
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = infantry_weapons2
		amount = 15000
		producer = SPR
	}
	add_equipment_to_stockpile = {
		type = infantry_weapons2
		amount = 1000
		producer = SPR
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_1
		amount = 500
	}
	add_equipment_to_stockpile = {
		type = command_control_equipment2
		amount = 1800
	}
	add_equipment_to_stockpile = {
		type = Anti_tank_1 #C90-CR
		amount = 750
	}
	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_1 #TOW-2
		amount = 200
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = Anti_Air_1
		amount = 525
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = mbt_hull_0
		variant_name = "M48A5"
		amount = 164
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = mbt_hull_0
		variant_name = "M60A3"
		amount = 184
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = mbt_hull_1
		variant_name = "Leopard 2A4"
		amount = 108
		producer = GER
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_2
		amount = 80 #added for balance
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_2
		amount = 340
		producer = SPR
		variant_name = "Pegaso VEC-M1"
	}
	add_equipment_to_stockpile = {
		type = apc_hull_1
		variant_name = "Pegaso BMR"
		amount = 195
	}
	add_equipment_to_stockpile = {
		type = apc_hull_1
		variant_name = "M113"
		amount = 1011
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = apc_hull_1
		variant_name = "M113A2"
		amount = 300
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = artillery_0 #OTO-Melara Mod 56
		amount = 158 #+12 from marines
		producer = ITA
	}
	add_equipment_to_stockpile = {
		type = artillery_1 #L118
		amount = 56
		producer = ENG
	}
	add_equipment_to_stockpile = {
		type = artillery_0 #M114
		amount = 20
		producer = USA
		#version_name = "M114"
	}
	add_equipment_to_stockpile = {
		type = spart_hull_1
		variant_name = "M108"
		amount = 48
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = spart_hull_1
		variant_name = "M109"
		amount = 90
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "M110A2"
		amount = 64
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = spaa_hull_1
		amount = 120
		producer = FRA
		variant_name = "Mistral ATLAS"
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter2 #Eurocopter AS532 Cougar
		amount = 23
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Bell UH-1 Iroquois
		amount = 93
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Sea King HU5
		amount = 2
		producer = USA
		#version_name = "Sea King HU5"
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Bell UH-1 Iroquois
		amount = 10
		producer = USA
	}
	#11SH-3D (8 -H ASW,
	add_equipment_to_stockpile = {
		type = transport_helicopter2 #Sikorsky UH-60 Black Hawk
		amount = 6
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = light_tank_hull_1
		amount = 17
		producer = ENG
		variant_name = "FV101 Scorpion"
	}
	add_equipment_to_stockpile = {
		type = artillery_0 #OTO-Melara Mod 56
		amount = 12 #+12 from marines
		producer = ITA
	}
	add_equipment_to_stockpile = {
		type = spart_hull_1
		variant_name = "M109"
		amount = 6
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Eurocopter AS332 Super Puma
		amount = 78
		producer = FRA
	}
}
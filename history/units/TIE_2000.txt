﻿division_template = {
	name = "Liwa Mushat"

	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
		L_Inf_Bat = { x = 0 y = 2 }
	}
}

division_template = {
	name = "Milishia Qabalia"

	regiments = {
		Militia_Bat = { x = 0 y = 0 }
		Militia_Bat = { x = 0 y = 1 }
	}
}

units = {
	division = {
		name = "Huraas al-Dardih"
		location = 14397
		division_template = "Milishia Qabalia"
		start_experience_factor = 0.2
		start_equipment_factor = 0.80
	}
	division = {
		name = "Rijal Qabayil Tayda"
		location = 4902
		division_template = "Milishia Qabalia"
		start_experience_factor = 0.2
		start_equipment_factor = 0.80
	}
	division = {
		name = "Rijal Qabayil Daza"
		location = 4897
		division_template = "Milishia Qabalia"
		start_experience_factor = 0.2
		start_equipment_factor = 0.80
	}
}
﻿units = {
	fleet = {
		name = "Recommissioned Battleships"
		naval_base = 9671
		task_force = {
			name = "Recommissioned Battleships Force"
			location = 9671
			ship = { name = "USS New Jersey (BBG-62)" definition = battleship equipment = { battleship_hull_1 = { amount = 1 owner = USA version_name = "Iowa Class 80s refit" } } }
			ship = { name = "USS Missouri (BBG-63)" definition = battleship equipment = { battleship_hull_1 = { amount = 1 owner = USA version_name = "Iowa Class 80s refit" } } }
		}
	}
}
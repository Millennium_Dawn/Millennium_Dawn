﻿instant_effect = {
	add_equipment_to_stockpile = {
		variant_name = "A-4N Ayit"
		type = cv_small_plane_strike_airframe_1
		amount = 25
	}
	add_equipment_to_stockpile = {
		variant_name = "F-4E Phantom II"
		type = medium_plane_airframe_1
		amount = 20
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "F-4E Kurnass 2000"
		type = medium_plane_airframe_1
		amount = 50
	}
	#add_equipment_to_stockpile = {
		#type = medium_plane_fighter_airframe_2
		#amount = 46
		#variant_name = "F-15A Baz"
	#}
	add_equipment_to_stockpile = {
		type = medium_plane_fighter_airframe_2
		amount = 27
		variant_name = "F-15C Baz Meshupar"
	}
	#add_equipment_to_stockpile = {
		#type = medium_plane_airframe_2
		#amount = all deployed
		#variant_name = "F-15I Ra'am"
	#}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_2
		amount = 16 #100 deployed
		variant_name = "F-16A Netz"
	}
	#add_equipment_to_stockpile = {
		#type = small_plane_strike_airframe_2
		#amount = all deployed
		#variant_name = "F-16C Barak II"
	#}
	add_equipment_to_stockpile = {
		variant_name = "C-130 Hercules"
		type = large_plane_air_transport_airframe_1
		amount = 25
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "C-130J Super Hercules"		#To cover C-47 and Boeing 707's we dont have variants for
		type = large_plane_air_transport_airframe_1
		amount = 17
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "Boeing EL/M-2075 Phalcon"
		type = large_plane_awacs_airframe_2
		amount = 6
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "IAI Sea Scan"
		type = medium_plane_maritime_patrol_airframe_1
		amount = 3
		producer = ISR
	}
	# Missile OOB
	if = {
		limit = {
			has_dlc = "Gotterdammerung"
		}
		add_equipment_to_stockpile = {
			type = sam_missile_equipment_2
			amount = 307
		}
		add_equipment_to_stockpile = {
			type = sam_missile_equipment_3
			amount = 72
		}
		add_equipment_to_stockpile = {
			type = sam_missile_equipment_4
			amount = 4
		}
		add_equipment_to_stockpile = {
			type = guided_missile_equipment_1
			amount = 185
		}
		add_equipment_to_stockpile = {
			type = ballistic_missile_equipment_2
			amount = 60
		}
		add_equipment_to_stockpile = {
			type = nuclear_ballistic_missile_equipment_3
			amount = 30
		}
	}

}
air_wings = {
	205 = { #Galilea Ramat David AB
		small_plane_strike_airframe_2 = { owner = "ISR" amount = 50 version_name = "F-16C Barak II" }
		name = "101 'First Fighter' Squadron / 105 'Scorpion' Squadron"
		start_experience_factor = 0.5
	}	
	204 = { #Judea Hatzor AB
		small_plane_strike_airframe_2 = { owner = "ISR" amount = 50 version_name = "F-16A Netz" }
		name = "144 'Phoenix' Squadron / 119 'Bat' Squadron"
		start_experience_factor = 0.5
		#
		medium_plane_fighter_airframe_2 = { owner = "ISR" amount = 46 version_name = "F-15A Baz" }
		name = "106 'Spearhead' Squadron / 133 'Knight of the Twin Tail' Squadron"
		start_experience_factor = 0.5
		#
	}
	206 = { #Negev Hatzerim AB
		medium_plane_airframe_2 = { owner = "ISR" amount = 25 version_name = "F-15I Ra'am" }
		name = "69 'Hammers' Squadron"
		start_experience_factor = 0.5
		#
		small_plane_strike_airframe_2 = { owner = "ISR" amount = 50 version_name = "F-16A Netz" }
		name = "107 'Knights of the Orange Tail' Squadron / 115 'Flying Dragon' Squadron"
		start_experience_factor = 0.5
		#
		small_plane_strike_airframe_2 = { owner = "ISR" amount = 50 version_name = "F-16C Barak II" }
		name = "201 'The One' Squadron / 253 'Negev' Squadron"
		start_experience_factor = 0.5
		#
		small_plane_strike_airframe_2 = { owner = "ISR" amount = 21 version_name = "F-16C Barak II" }
		name = "116 'Lions of the South' Squadron / 117 'First Jet' Squadron"
		start_experience_factor = 0.5
	}
}
instant_effect = {
	
	add_equipment_to_stockpile = {
	type = large_plane_air_transport_airframe_1
	amount = 1
	variant_name = "An-26"
	producer = SOV
	}
	
	add_equipment_to_stockpile = {
	type = large_plane_air_transport_airframe_1
	amount = 5
	variant_name = "An-2"
	producer = SOV
	}
}
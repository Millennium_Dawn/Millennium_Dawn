﻿division_template = {
	name = "Mechanized Brigade"
	division_names_group = FRA_MECH_01

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
		Mech_Inf_Bat = { x = 0 y = 2 }
		Arm_Inf_Bat = { x = 1 y = 0 }
		L_arm_Bat = { x = 1 y = 1 }
		SP_Arty_Bat = { x = 1 y = 2 }
		attack_helo_bat = { x = 1 y = 3 }
	}
	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		SP_Arty_Battery = { x = 0 y = 1 }
		Mech_Recce_Comp = { x = 0 y = 2 }
		combat_service_support_company = { x = 0 y = 3 }
	}
}
division_template = {
	name = "Heavy Mechanized Brigade"
	division_names_group = FRA_MECH_01
	
	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
		Arm_Inf_Bat = { x = 1 y = 0 }
		Arm_Inf_Bat = { x = 1 y = 1 }
		L_arm_Bat = { x = 2 y = 0 }
		SP_Arty_Bat = { x = 2 y = 1 }
		SP_Arty_Bat = { x = 2 y = 2 }
		attack_helo_bat = { x = 2 y = 3 }
		Mot_Marine_Bat = { x = 3 y = 0 }
	}
	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		SP_AA_Battery = { x = 0 y = 1 }
		Mech_Recce_Comp = { x = 0 y = 2 }
		combat_service_support_company = { x = 0 y = 3 }
	}
}
division_template = {
	name = "Light Armored Brigade"
	division_names_group = FRA_ARM_03

	regiments = {
		Arm_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
		SP_Arty_Bat = { x = 0 y = 2 }
		Mech_Inf_Bat = { x = 1 y = 0 }
		Mech_Inf_Bat = { x = 1 y = 1 }
		Mech_Marine_Bat = { x = 1 y = 2 }
		Mech_Marine_Bat = { x = 1 y = 3 }
		SP_Arty_Bat = { x = 1 y = 4 }
		Mot_Inf_Bat = { x = 2 y = 0 }
		Mot_Inf_Bat = { x = 2 y = 1 }
		L_arm_Bat = { x = 3 y = 0 }
		attack_helo_bat = { x = 3 y = 1 }
	}
	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		SP_AA_Battery = { x = 0 y = 1 }
		armor_Recce_Comp = { x = 0 y = 2 }
		combat_service_support_company = { x = 0 y = 3 }
	}
}
division_template = {
	name = "Armored Brigade"
	division_names_group = FRA_ARM_01

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		armor_Bat = { x = 0 y = 1 }
		Mech_Inf_Bat = { x = 0 y = 2 }
		Mech_Inf_Bat = { x = 0 y = 3 }
		
		armor_Bat = { x = 1 y = 0 }
		armor_Bat = { x = 1 y = 1 }
		Mech_Inf_Bat = { x = 1 y = 2 }
		Mech_Inf_Bat = { x = 1 y = 3 }
		
		armor_Bat = { x = 2 y = 0 }
		Arm_Inf_Bat = { x = 2 y = 0 }
		Arm_Inf_Bat = { x = 2 y = 1 }
		
		SP_Arty_Bat = { x = 3 y = 0 }
		SP_Arty_Bat = { x = 3 y = 1 }
		attack_helo_bat = { x = 3 y = 2 }
	}
	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		SP_AA_Battery = { x = 0 y = 1 }
		Arm_Recce_Comp = { x = 0 y = 2 }
		combat_service_support_company = { x = 0 y = 3 }
	}
}
division_template = {
	name = "Marine Brigade"
	division_names_group = FRA_MAR_01

	regiments = {
		Mech_Marine_Bat = { x = 0 y = 0 }
		Mech_Marine_Bat = { x = 0 y = 1 }
		
		Special_Forces = { x = 1 y = 0 }
	}
	support = {
		L_Engi_Comp = { x = 0 y = 0 }
		L_Recce_Comp = { x = 0 y = 1 }
		combat_service_support_company = { x = 0 y = 2 }
		attack_helo_comp = { x = 0 y = 3 }
	}
}
division_template = {
	name = "Light Armored Marine Brigade"
	division_names_group = FRA_MAR_02

	regiments = {
		Mech_Marine_Bat = { x = 0 y = 0 }
		Mech_Marine_Bat = { x = 0 y = 1 }
		Mech_Marine_Bat = { x = 0 y = 2 }
		Arm_Marine_Bat = { x = 0 y = 3 }
		SP_Arty_Bat = { x = 0 y = 4 }
		
		Mot_Marine_Bat = { x = 1 y = 0 }
		Mot_Marine_Bat = { x = 1 y = 1 }
		
		L_arm_Bat = { x = 2 y = 0 }
		L_arm_Bat = { x = 2 y = 1 }
		SP_Arty_Bat = { x = 2 y = 2 }
		
		Mech_Inf_Bat = { x = 3 y = 0 }
		Mech_Inf_Bat = { x = 3 y = 1 }
	}
	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		SP_AA_Battery = { x = 0 y = 1 }
		armor_Recce_Comp = { x = 0 y = 2 }
		combat_service_support_company = { x = 0 y = 3 }
		attack_helo_comp = { x = 0 y = 4 }
	}
}

division_template = {
	name = "Mountain Infantry Brigade" #Changed from mechanized to light infantry so that the unit can actually fight in mountain.
	division_names_group = FRA_MNT_01

	regiments = {
		Special_Forces = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
		L_Inf_Bat = { x = 0 y = 2 }
		Arty_Bat = { x = 0 y = 3 }
		Special_Forces = { x = 1 y = 0 }
		L_Inf_Bat = { x = 1 y = 1 }
		L_Inf_Bat = { x = 1 y = 2 }
		Arty_Bat = { x = 1 y = 3 }
		L_arm_Bat = { x = 2 y = 0 }
		Mot_Inf_Bat = { x = 3 y = 0 }
	}
	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		SP_AA_Battery = { x = 0 y = 1 }
		armor_Recce_Comp = { x = 0 y = 2 }
		helicopter_combat_service_support = { x = 0 y = 3 }
		attack_helo_comp = { x = 0 y = 4 }
	}
}

division_template = {
	name = "Parachute Brigade"
	division_names_group = FRA_PAR_01

	regiments = {
		Mech_Air_Inf_Bat = { x = 0 y = 0 }
		Mech_Air_Inf_Bat = { x = 0 y = 1 }
		
		Mot_Air_Inf_Bat = { x = 1 y = 0 }
		Mot_Air_Inf_Bat = { x = 1 y = 1 }
	}
	support = {
		L_Engi_Comp = { x = 0 y = 0 }
		armor_Recce_Comp = { x = 0 y = 1 }
		combat_service_support_company = { x = 0 y = 2 }
		Arty_Battery = { x = 0 y = 3 }
		attack_helo_comp = { x = 0 y = 4 }
	}
	priority = 2
}

division_template = {
	name = "Airmobile Brigade"
	division_names_group = FRA_AIR_01

	regiments = {
		L_Air_assault_Bat = { x = 0 y = 0 }
		L_Air_assault_Bat = { x = 0 y = 1 }
		L_Air_assault_Bat = { x = 0 y = 2 }
		L_Air_assault_Bat = { x = 0 y = 3 }
	}

	support = {
		L_Engi_Comp = { x = 0 y = 0 }
		helicopter_combat_service_support = { x = 0 y = 1 }
		Arty_Battery = { x = 0 y = 2 }
		attack_helo_comp = { x = 0 y = 3 }
		Mot_Recce_Comp = { x = 0 y = 4 }
	}
}

division_template = {
	name = "Special Forces Battalion"
	division_names_group = FRA_SOF_01

	regiments = {
		Special_Forces = { x = 0 y = 0 }
	}
	support = {
		helicopter_combat_service_support = { x = 0 y = 0 }
		Mot_Recce_Comp = { x = 0 y = 1 }
	}

	priority = 2
}
division_template = {
	name = "Special Forces Regiment"
	division_names_group = FRA_SOF_01

	regiments = {
		Special_Forces = { x = 0 y = 0 }
		Special_Forces = { x = 0 y = 1 }
	}
	support = {
		helicopter_combat_service_support = { x = 0 y = 0 }
		Mot_Recce_Comp = { x = 0 y = 1 }
		attack_helo_comp = { x = 0 y = 2 }
	}
	priority = 2
}

division_template = {
	name = "Foreign Legion"
	division_names_group = FRA_FRENCH_LEGION_NAME

	regiments = {
		Special_Forces = { x = 0 y = 0 }
	}
	support = {
		L_Engi_Comp = { x = 0 y = 0 }
		combat_service_support_company = { x = 0 y = 1 }
		Mot_Recce_Comp = { x = 0 y = 1 }
		attack_helo_comp = { x = 0 y = 2 }
	}
	priority = 2
}

division_template = {
	name = "Franco-German Brigade"

	regiments = {
		L_arm_Bat = { x = 0 y = 0 }
		L_arm_Bat = { x = 0 y = 1 }
		L_arm_Bat = { x = 0 y = 2 }
		Mech_Inf_Bat = { x = 0 y = 3 }
		Mech_Inf_Bat = { x = 0 y = 4 }
		Mech_Inf_Bat = { x = 1 y = 0 }
		Mech_Inf_Bat = { x = 1 y = 1 }
		Mech_Inf_Bat = { x = 1 y = 2 }
	}
	support = {
		L_Engi_Comp = { x = 0 y = 0 }
		SP_AA_Battery = { x = 0 y = 1 }
		Mech_Recce_Comp = { x = 0 y = 2 }
		combat_service_support_company = { x = 0 y = 3 }
	}
}

units = {
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}
		location = 6545 #Chalons
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.55
		start_equipment_factor = 0.01
	}
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 2
		}
		location = 521 #Illkirch
		division_template = "Armored Brigade"
		start_experience_factor = 0.55
		start_equipment_factor = 0.01
	}
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 3
		}
		location = 454 #Clermont-Ferrand
		division_template = "Heavy Mechanized Brigade"
		start_experience_factor = 0.55
		start_equipment_factor = 0.01
	}
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 4
		}
		location = 11516 #Essey-les-Nancy
		division_template = "Airmobile Brigade"
		start_experience_factor = 0.55
		start_equipment_factor = 0.01
	}
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 6
		}
		location = 958 #Nîmes
		division_template = "Light Armored Brigade"
		start_experience_factor = 0.55
		start_equipment_factor = 0.01
	}
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 7
		}
		location = 6698 #Besançon
		division_template = "Armored Brigade"
		start_experience_factor = 0.55
		start_equipment_factor = 0.01
	}
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 8
		}
		location = 3626 #Poitiers
		division_template = "Light Armored Marine Brigade"
		start_experience_factor = 0.55
		start_equipment_factor = 0.01
		force_equipment_variants = { attack_helicopter_hull_1 = { amount = 6 owner = FRA version_name = "AS565 Panther" } }
	}
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 11
		}
		location = 6766 #Balma
		division_template = "Parachute Brigade"
		start_experience_factor = 0.55
		start_equipment_factor = 0.01
	}
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 52
		}
		location = 6766 #Balma
		division_template = "Parachute Brigade"
		start_experience_factor = 0.55
		start_equipment_factor = 0.01
	}
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 101
		}
		location = 12885 #guyane
		division_template = "Marine Brigade"
		start_experience_factor = 0.65
		start_equipment_factor = 0.01
		force_equipment_variants = { attack_helicopter_hull_1 = { amount = 6 owner = FRA version_name = "AS565 Panther" } }
	}
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 27
		}
		location = 3768 #Varces
		division_template = "Mountain Infantry Brigade"
		start_experience_factor = 0.6
		start_equipment_factor = 0.01
	}
	# Brigade franco-allemande
	division = {
		name = "Brigade franco-allemande"
		location = 3679 # Immendingen
		division_template = "Franco-German Brigade"
		start_experience_factor = 0.55
		start_equipment_factor = 0.01
	}
	# Special forces
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}
		location = 743 #Bayonne
		division_template = "Special Forces Regiment"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
		force_equipment_variants = { infantry_weapons2 = { amount = 592 owner = USA version_name = "Colt Commando" } }
	}
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 13
		}
		location = 651 #Souge
		division_template = "Special Forces Regiment"
		start_equipment_factor = 0.01
		start_experience_factor = 0.75
		force_equipment_variants = { transport_helicopter2 = { amount = 15 owner = FRA version_name = "AS555 Fennec" } }
	}
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 10
		}
		location = 3506 #Orléans airbase
		division_template = "Special Forces Battalion"
		start_equipment_factor = 0.01
		start_experience_factor = 0.75
		force_equipment_variants = { transport_helicopter2 = { amount = 15 owner = FRA version_name = "AS555 Fennec" } }
	}
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 100
		}
		location = 11506 #Paris
		division_template = "Special Forces Battalion"
		start_experience_factor = 0.8
		start_equipment_factor = 0.01
		force_equipment_variants = { transport_helicopter2 = { amount = 15 owner = FRA version_name = "AS555 Fennec" } }
		force_equipment_variants = { infantry_weapons3 = { amount = 342 owner = SWI } }
		force_equipment_variants = { Anti_Air_1 = { amount = 11 owner = USA } }
		force_equipment_variants = { Anti_tank_2 = { amount = 16 owner = USA } }
	}
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 101
		}
		location = 911 #Toulon
		division_template = "Special Forces Battalion"
		start_experience_factor = 0.8
		start_equipment_factor = 0.01
		force_equipment_variants = { transport_helicopter2 = { amount = 15 owner = FRA version_name = "AS555 Fennec" } }
		force_equipment_variants = { infantry_weapons3 = { amount = 342 owner = SWI } }
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = infantry_weapons #old MAS49 stocks
		amount = 10000
	}
	add_equipment_to_stockpile = {
		type = infantry_weapons1 #Famas F1
		amount = 40000
	}
	add_equipment_to_stockpile = {
		type = command_control_equipment2 #C4ISTAR
		amount = 3200 #+1k since overseas units are gone
	}
	add_equipment_to_stockpile = {
		type = Anti_Air_1 #Mistral
		amount = 600 #+300 since overseas units are gone
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_2
		amount = 337
		producer = FRA
		variant_name = "AMX-10 RC"
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_1
		amount = 192
		producer = FRA
		variant_name = "Panhard ERC 90"
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_1
		amount = 2500
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_2
		amount = 550
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_3 #VBL M-ll
		amount = 899
	}
	add_equipment_to_stockpile = {
		type = artillery_1 #TRF1
		amount = 105
	}
	add_equipment_to_stockpile = {
		type = spart_hull_1
		variant_name = "M270"
		amount = 61
		producer = USA
	}
	#26 HAWK, 98 Roland I/II, 331 Mistral

	### Helicopters
	add_equipment_to_stockpile = {
		type = attack_helicopter_hull_0
		amount = 154
		producer = FRA
		variant_name = "SA-341 Gazelle"
	}
	add_equipment_to_stockpile = {
		type = attack_helicopter_hull_0
		amount = 185
		producer = FRA
		variant_name = "SA-342 Gazelle"
	}
	add_equipment_to_stockpile = {
		type = attack_helicopter_hull_1
		amount = 1 #12 deployed
		producer = FRA
		variant_name = "AS565 Panther"
	}
	
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Puma French Army
		amount = 128
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter2 #Cougar French Army
		amount = 31
		variant_name = "AS532 Cougar"
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Puma French Airforce
		amount = 29
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter2 #Cougar French Airforce
		amount = 10
		variant_name = "AS532 Cougar"
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter2 #Fennec French Airforce
		amount = 2 #45 deployed
		variant_name = "AS555 Fennec"
		producer = FRA
	}

##GROUND PRODUCTION##

	add_equipment_production = {
		equipment = {
			type = Anti_tank_1
			creator = "FRA"
			version_name = "Milan 3"
		}
		requested_factories = 1
	}
	add_equipment_production = {
		equipment = {
			type = Anti_Air_1
			creator = "FRA"
		}
		requested_factories = 1
	}
	add_equipment_production = {
		equipment = {
			type = util_vehicle_3
			creator = "FRA"
		}
		requested_factories = 1
	}
	add_equipment_production = {
		equipment = {
			type = transport_helicopter2
			creator = "FRA"
			version_name = "AS532 Cougar"
		}
		requested_factories = 1
		industrial_manufacturer = mio:FRA_airbus_helicopters_tank_manufacturer
	}
	add_equipment_production = {
		equipment = {
			type = mbt_hull_2
			creator = "FRA"
			version_name = "Leclerc S2"
		}
		requested_factories = 2
		industrial_manufacturer = mio:FRA_nexter_tank_manufacturer
	}
#AIR PRODUCTION##
	add_equipment_production = {
		equipment = {
			type = cv_medium_plane_airframe_2
			creator = "FRA"
			version_name = "Rafale M"
		}
		requested_factories = 1
		industrial_manufacturer = mio:FRA_dassault_aviation_aircraft_manufacturer
	}
	add_equipment_production = {
		equipment = {
			type = medium_plane_fighter_airframe_2
			creator = "FRA"
			version_name = "Rafale C"
		}
		requested_factories = 1
		industrial_manufacturer = mio:FRA_dassault_aviation_aircraft_manufacturer
	}
	add_equipment_production = {
		equipment = {
			type = medium_plane_airframe_2
			creator = "FRA"
			version_name = "Rafale B"
		}
		requested_factories = 1
		industrial_manufacturer = mio:FRA_dassault_aviation_aircraft_manufacturer
	}
	add_equipment_production = {
		equipment = {
			type = small_plane_cas_airframe_2
			creator = "FRA"
			version_name = "Mirage 2000D"
		}
		requested_factories = 1
		industrial_manufacturer = mio:FRA_dassault_aviation_aircraft_manufacturer
	}
	#NSB SHIP PRODUCTION#
if = {
	limit = { has_dlc = "Man the Guns" }
		add_equipment_production = {
			equipment = {
				type = missile_submarine_hull_3
				creator = "FRA"
				version_name = "Triomphant Class"
			}
			requested_factories = 1
			progress = 0.30
			efficiency = 100
			amount = 1
			industrial_manufacturer = mio:FRA_naval_group_naval_manufacturer_2
		}
		add_equipment_production = {
			equipment = {
				type = missile_submarine_hull_3
				creator = "FRA"
				version_name = "Triomphant Class"
			}
			requested_factories = 1
			progress = 0.10
			efficiency = 80
			amount = 1
			industrial_manufacturer = mio:FRA_naval_group_naval_manufacturer_2
		}
		add_equipment_production = {
			equipment = {
				type = destroyer_hull_3
				creator = "FRA"
				version_name = "Horizon Class"
			}
			requested_factories = 1
			progress = 0
			efficiency = 35
			amount = 1
			industrial_manufacturer = mio:FRA_naval_group_naval_manufacturer
		}
		add_equipment_production = {
			equipment = {
				type = destroyer_hull_3
				creator = "FRA"
				version_name = "Horizon Class"
			}
			requested_factories = 1
			progress = 0
			efficiency = 30
			amount = 1
			industrial_manufacturer = mio:FRA_naval_group_naval_manufacturer
		}
	}
}
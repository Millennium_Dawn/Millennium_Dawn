﻿units = {

	### Naval OOB ###
	fleet = {
	   name = "East Sea Fleet"
	   naval_base = 3189
		task_force = {
			name = "1st East Sea Surface Flotilla"
			location = 3189
			ship = { name = "531" definition = frigate start_experience_factor = 0.50 equipment = { frigate_hull_1 = { amount = 1 owner = NKO creator = NKO version_name = "Najin Class" } } }
			ship = { name = "321" definition = corvette start_experience_factor = 0.50 equipment = { corvette_hull_1 = { amount = 1 owner = NKO creator = NKO version_name = "Sariwon Class" } } } #Fictional Name
			ship = { name = "452" definition = corvette start_experience_factor = 0.50 equipment = { corvette_hull_1 = { amount = 1 owner = NKO creator = NKO version_name = "Sariwon Class" } } } #Fictional Name
		}
		task_force = {
			name = "1st East Sea Submarine Flotilla"
			location = 3189
			ship = { name = "SS-01" definition = attack_submarine start_experience_factor = 0.50 equipment = { attack_submarine_hull_1 = { amount = 1 owner = NKO creator = SOV version_name = "Whiskey Class" } } } #Fictional Name
			ship = { name = "SS-02" definition = attack_submarine start_experience_factor = 0.50 equipment = { attack_submarine_hull_1 = { amount = 1 owner = NKO creator = SOV version_name = "Whiskey Class" } } } #Fictional Name
			ship = { name = "SS-03" definition = attack_submarine start_experience_factor = 0.50 equipment = { attack_submarine_hull_1 = { amount = 1 owner = NKO creator = SOV version_name = "Whiskey Class" } } } #Fictional Name
			ship = { name = "SS-04" definition = attack_submarine start_experience_factor = 0.50 equipment = { attack_submarine_hull_1 = { amount = 1 owner = NKO creator = SOV version_name = "Whiskey Class" } } } #Fictional Name
			ship = { name = "SS-23" definition = attack_submarine start_experience_factor = 0.50 equipment = { attack_submarine_hull_1 = { amount = 1 owner = NKO creator = SOV version_name = "Romeo Class" } } } #Fictional Name
			ship = { name = "SS-09" definition = attack_submarine start_experience_factor = 0.50 equipment = { attack_submarine_hull_1 = { amount = 1 owner = NKO creator = SOV version_name = "Romeo Class" } } } #Fictional Name
			ship = { name = "SS-12" definition = attack_submarine start_experience_factor = 0.50 equipment = { attack_submarine_hull_1 = { amount = 1 owner = NKO creator = SOV version_name = "Romeo Class" } } } #Fictional Name
			ship = { name = "SS-05" definition = attack_submarine start_experience_factor = 0.50 equipment = { attack_submarine_hull_1 = { amount = 1 owner = NKO creator = SOV version_name = "Romeo Class" } } } #Fictional Name
			ship = { name = "SS-35" definition = attack_submarine start_experience_factor = 0.50 equipment = { attack_submarine_hull_1 = { amount = 1 owner = NKO creator = NKO version_name = "Sang-O Class" } } } #Fictional Name
			ship = { name = "SS-29" definition = attack_submarine start_experience_factor = 0.50 equipment = { attack_submarine_hull_1 = { amount = 1 owner = NKO creator = NKO version_name = "Sang-O Class" } } } #Fictional Name
			ship = { name = "SS-34" definition = attack_submarine start_experience_factor = 0.50 equipment = { attack_submarine_hull_1 = { amount = 1 owner = NKO creator = NKO version_name = "Sang-O Class" } } } #Fictional Name
			ship = { name = "SS-27" definition = attack_submarine start_experience_factor = 0.50 equipment = { attack_submarine_hull_1 = { amount = 1 owner = NKO creator = NKO version_name = "Sang-O Class" } } } #Fictional Name
		}
	}
	fleet = {
	   name = "West Sea Fleet"
	   naval_base = 3189
		task_force = {
			name = "1st West Sea Surface Flotilla"
			location = 848
			ship = { name = "631" definition = frigate start_experience_factor = 0.50 equipment = { frigate_hull_1 = { amount = 1 owner = NKO version_name = "Najin Class" } } }
			ship = { name = "409" definition = frigate start_experience_factor = 0.50 equipment = { frigate_hull_2 = { amount = 1 owner = NKO version_name = "Soho Class" } } } #Fictional Name
			ship = { name = "750" definition = corvette start_experience_factor = 0.50 equipment = { corvette_hull_1 = { amount = 1 owner = NKO creator = NKO version_name = "Sariwon Class" } } } #Fictional Name
			ship = { name = "523" definition = corvette start_experience_factor = 0.50 equipment = { corvette_hull_1 = { amount = 1 owner = NKO creator = NKO version_name = "Sariwon Class" } } } #Fictional Name
			ship = { name = "896" definition = corvette start_experience_factor = 0.50 equipment = { corvette_hull_1 = { amount = 1 owner = NKO creator = NKO version_name = "Sariwon Class" } } } #Fictional Name
		}
		task_force = {
			name = "1st West Sea Submarine Flotilla"
			location = 848
			ship = { name = "SS-08" definition = attack_submarine start_experience_factor = 0.50 equipment = { attack_submarine_hull_1 = { amount = 1 owner = NKO creator = SOV version_name = "Romeo Class" } } } #Fictional Name
			ship = { name = "SS-17" definition = attack_submarine start_experience_factor = 0.50 equipment = { attack_submarine_hull_1 = { amount = 1 owner = NKO creator = SOV version_name = "Romeo Class" } } } #Fictional Name
			ship = { name = "SS-14" definition = attack_submarine start_experience_factor = 0.50 equipment = { attack_submarine_hull_1 = { amount = 1 owner = NKO creator = SOV version_name = "Romeo Class" } } } #Fictional Name
			ship = { name = "SS-15" definition = attack_submarine start_experience_factor = 0.50 equipment = { attack_submarine_hull_1 = { amount = 1 owner = NKO creator = SOV version_name = "Romeo Class" } } } #Fictional Name
			ship = { name = "SS-11" definition = attack_submarine start_experience_factor = 0.50 equipment = { attack_submarine_hull_1 = { amount = 1 owner = NKO creator = SOV version_name = "Romeo Class" } } } #Fictional Name
			ship = { name = "SS-21" definition = attack_submarine start_experience_factor = 0.50 equipment = { attack_submarine_hull_1 = { amount = 1 owner = NKO creator = SOV version_name = "Romeo Class" } } } #Fictional Name
			ship = { name = "SS-10" definition = attack_submarine start_experience_factor = 0.50 equipment = { attack_submarine_hull_1 = { amount = 1 owner = NKO creator = SOV version_name = "Romeo Class" } } } #Fictional Name
			ship = { name = "SS-38" definition = attack_submarine start_experience_factor = 0.50 equipment = { attack_submarine_hull_1 = { amount = 1 owner = NKO creator = SOV version_name = "Romeo Class" } } } #Fictional Name
			ship = { name = "SS-13" definition = attack_submarine start_experience_factor = 0.50 equipment = { attack_submarine_hull_1 = { amount = 1 owner = NKO creator = SOV version_name = "Romeo Class" } } } #Fictional Name
			ship = { name = "SS-31" definition = attack_submarine start_experience_factor = 0.50 equipment = { attack_submarine_hull_1 = { amount = 1 owner = NKO creator = NKO version_name = "Sang-O Class" } } } #Fictional Name
			ship = { name = "SS-26" definition = attack_submarine start_experience_factor = 0.50 equipment = { attack_submarine_hull_1 = { amount = 1 owner = NKO creator = NKO version_name = "Sang-O Class" } } } #Fictional Name
			ship = { name = "SS-24" definition = attack_submarine start_experience_factor = 0.50 equipment = { attack_submarine_hull_1 = { amount = 1 owner = NKO creator = NKO version_name = "Sang-O Class" } } } #Fictional Name
			ship = { name = "SS-33" definition = attack_submarine start_experience_factor = 0.50 equipment = { attack_submarine_hull_1 = { amount = 1 owner = NKO creator = NKO version_name = "Sang-O Class" } } } #Fictional Name
		}
		task_force = {
			name = "2st West Sea Submarine Flotilla"
			location = 6944
			ship = { name = "SS-22" definition = attack_submarine start_experience_factor = 0.50 equipment = { attack_submarine_hull_1 = { amount = 1 owner = NKO creator = SOV version_name = "Romeo Class" } } } #Fictional Name
			ship = { name = "SS-37" definition = attack_submarine start_experience_factor = 0.50 equipment = { attack_submarine_hull_1 = { amount = 1 owner = NKO creator = SOV version_name = "Romeo Class" } } } #Fictional Name
			ship = { name = "SS-07" definition = attack_submarine start_experience_factor = 0.50 equipment = { attack_submarine_hull_1 = { amount = 1 owner = NKO creator = SOV version_name = "Romeo Class" } } } #Fictional Name
			ship = { name = "SS-39" definition = attack_submarine start_experience_factor = 0.50 equipment = { attack_submarine_hull_1 = { amount = 1 owner = NKO creator = SOV version_name = "Romeo Class" } } } #Fictional Name
			ship = { name = "SS-16" definition = attack_submarine start_experience_factor = 0.50 equipment = { attack_submarine_hull_1 = { amount = 1 owner = NKO creator = SOV version_name = "Romeo Class" } } } #Fictional Name
			ship = { name = "SS-06" definition = attack_submarine start_experience_factor = 0.50 equipment = { attack_submarine_hull_1 = { amount = 1 owner = NKO creator = SOV version_name = "Romeo Class" } } } #Fictional Name
			ship = { name = "SS-18" definition = attack_submarine start_experience_factor = 0.50 equipment = { attack_submarine_hull_1 = { amount = 1 owner = NKO creator = SOV version_name = "Romeo Class" } } } #Fictional Name
			ship = { name = "SS-20" definition = attack_submarine start_experience_factor = 0.50 equipment = { attack_submarine_hull_1 = { amount = 1 owner = NKO creator = SOV version_name = "Romeo Class" } } } #Fictional Name
			ship = { name = "SS-19" definition = attack_submarine start_experience_factor = 0.50 equipment = { attack_submarine_hull_1 = { amount = 1 owner = NKO creator = SOV version_name = "Romeo Class" } } } #Fictional Name
			ship = { name = "SS-32" definition = attack_submarine start_experience_factor = 0.50 equipment = { attack_submarine_hull_1 = { amount = 1 owner = NKO creator = NKO version_name = "Sang-O Class" } } } #Fictional Name
			ship = { name = "SS-30" definition = attack_submarine start_experience_factor = 0.50 equipment = { attack_submarine_hull_1 = { amount = 1 owner = NKO creator = NKO version_name = "Sang-O Class" } } } #Fictional Name
			ship = { name = "SS-36" definition = attack_submarine start_experience_factor = 0.50 equipment = { attack_submarine_hull_1 = { amount = 1 owner = NKO creator = NKO version_name = "Sang-O Class" } } } #Fictional Name
			ship = { name = "SS-28" definition = attack_submarine start_experience_factor = 0.50 equipment = { attack_submarine_hull_1 = { amount = 1 owner = NKO creator = NKO version_name = "Sang-O Class" } } } #Fictional Name
		}
	}
}
﻿instant_effect = {
	#110 Phantom KWS, all deployed

	add_equipment_to_stockpile = {
		variant_name = "Tornado IDS/ECR"
		type = medium_plane_cas_airframe_1
		amount = 30 #250 deployed, 30 old stocks from MFG 1 to reinforce luftwaffe
		producer = GER
	}
	add_equipment_to_stockpile = {
		variant_name = "Tornado IDS MFG"
		type = medium_plane_maritime_patrol_airframe_1
		amount = 12 #50 deployed in MFG 2
		producer = GER
	}
	#add_equipment_to_stockpile = {
	#	variant_name = "Atlantique 2"
	#	type = large_plane_maritime_patrol_airframe_1
	#	amount = 16
	#	producer = FRA
	#}
	add_equipment_to_stockpile = {
		variant_name = "F-4F Phantom II"
		type = medium_plane_airframe_1
		amount = 35
		producer = GER
	}
	#add_equipment_to_stockpile = { #2003
	#	variant_name = "Typhoon Tranche 1"
	#	type = medium_plane_airframe_2
	#	amount = 8
	#	producer = GER
	#}
	add_equipment_to_stockpile = {
		variant_name = "MiG-21 Bis"
		type = small_plane_strike_airframe_1
		amount = 1
		producer = SOV
	}
	add_equipment_to_stockpile = {
		variant_name = "MiG-23 ML/MLD"
		type = medium_plane_airframe_1
		amount = 3
		producer = SOV
	}
	#add_equipment_to_stockpile = { #moved to JGS71
	#	variant_name = "MiG-29 Fulcrum"
	#	type = small_plane_airframe_2
	#	amount = 23
	#	producer = SOV
	#}
	add_equipment_to_stockpile = {
		variant_name = "Su-17M4"
		type = small_plane_cas_airframe_1
		amount = 1
		producer = SOV
		#version_name = "Su-22 Fitter"
	}
	add_equipment_to_stockpile = {
		variant_name = "Dassault Alpha Jet"
		type = small_plane_strike_airframe_1
		amount = 32
		producer = FRA
	}
	add_equipment_to_stockpile = {
		variant_name = "Transall C-160"
		type = large_plane_air_transport_airframe_1
		amount = 9
		producer = GER
	}
	# Missile OOB
	if = {
		limit = {
			has_dlc = "Gotterdammerung"
		}
		add_equipment_to_stockpile = {
			type = sam_missile_equipment_1
			amount = 50
		}
		add_equipment_to_stockpile = {
			type = sam_missile_equipment_2
			amount = 250
		}
		add_equipment_to_stockpile = {
			variant_name = "MIM-104B PAC-1"
			type = sam_missile_equipment_3
			amount = 150
		}
		add_equipment_to_stockpile = {
			type = sam_missile_equipment_4
			variant_name = "MIM-104F PAC-3"
			amount = 125
		}
		#
		#add_equipment_to_stockpile = {
		#	type = guided_missile_equipment_2
		#	amount = 40
		#}
		#add_equipment_to_stockpile = {
		#	type = guided_missile_equipment_4
		#	amount = 40
		#}
	}
}
air_wings = {
	1079 = { #Holzdorf Air Base
		large_plane_air_transport_airframe_1 = { owner = "GER" amount = 25 version_name = "Transall C-160" }
		name = "LTG 62 " #Flugplatz Ahlhorst
		start_experience_factor = 0.5
	}
	966 = { #Nörvenich Air Base
		medium_plane_cas_airframe_1 = { owner = "GER" amount = 50 version_name = "Tornado IDS/ECR" }
		name = "JaBoG 31 'Boelcke'"
		start_experience_factor = 0.57
	}
	42 = { #Memmingen Airbase
		medium_plane_cas_airframe_1 = { owner = "GER" amount = 50 version_name = "Tornado IDS/ECR" }
		name = "JaBoG 34 'Allgäu'" #Memmingen Air Base
		start_experience_factor = 0.5
	}
	40 = { #Büchel Air Base
		medium_plane_cas_airframe_1 = { owner = "GER" amount = 50 version_name = "Tornado IDS/ECR" }
		name = "JaBoG 33"
		start_experience_factor = 0.5
	}
	37 = { #Airbase
		medium_plane_cas_airframe_1 = { owner = "GER" amount = 50 version_name = "Tornado IDS/ECR ASSTA 1" }
		name = "AG 51 'Immelmann'" #Jagel/Schleswig Air Base
		start_experience_factor = 0.5
		#
		medium_plane_maritime_patrol_airframe_1 = { owner = "GER" amount = 45 version_name = "Tornado IDS MFG" }
		name = "MFG 2 'Die Luft über See gehört dem MFG'" #Eggebek Air Base
		start_experience_factor = 0.5
		#
		large_plane_air_transport_airframe_1 = { owner = "GER" amount = 25 version_name = "Transall C-160" }
		name = "LTG 63 " #Fliegerhorst Hohn
		start_experience_factor = 0.5
	}
	38 = { #Wittmundhafen Air Base
		medium_plane_airframe_1 = { owner = "GER" amount = 50 version_name = "F-4F KWS Phantom II" }
		name = "JG 71 'Richthofen'"
		start_experience_factor = 0.5
		#
		medium_plane_cas_airframe_1 = { owner = "GER" amount = 40 version_name = "Tornado IDS/ECR" }
		name = "JaBoG 38 'Friesland'" #Jever Air Base
		start_experience_factor = 0.5
		#
		large_plane_maritime_patrol_airframe_1 = { owner = "GER" creator = "FRA" amount = 16 version_name = "Atlantique 2" }
		name = "MFG 3 'Graf Zeppelin'" #Nordholz
		start_experience_factor = 0.5
	}
	39 = { #Hoopsten Airbase
		medium_plane_airframe_1 = { owner = "GER" amount = 50 version_name = "F-4F KWS Phantom II" }
		name = "JG 72 'Westfalen'" #Hoopsten Airbase
		start_experience_factor = 0.5
	}
	539 = { #Rostock-Laage Airport
		small_plane_airframe_2 = { owner = "GER" creator = "SOV" amount = 25 version_name = "MiG-29 Fulcrum" }
		name = "JG 73 'Steinhoff'"
		start_experience_factor = 0.5
	}
	43 = { #Neuburg Air Base
		medium_plane_airframe_1 = { owner = "GER" amount = 50 version_name = "F-4F KWS Phantom II" }
		name = "JG 74 'Mölders'"
		start_experience_factor = 0.5
		#
		large_plane_air_transport_airframe_1 = { owner = "GER" amount = 25 version_name = "Transall C-160" }
		name = "LTG 61 " #Lansberg am Lech
		start_experience_factor = 0.5
		#
		medium_plane_cas_airframe_1 = { owner = "GER" amount = 10 version_name = "Tornado IDS/ECR" }
		name = "JaBoG 32" #Lagerlechfeld Base 43
		start_experience_factor = 0.5
	}
}
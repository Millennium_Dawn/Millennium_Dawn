﻿division_template = {
	name = "SLF Brigade"

	regiments = {
		Militia_Bat = { x = 0 y = 0 }
		Militia_Bat = { x = 0 y = 1 }
		Militia_Bat = { x = 0 y = 2 }
	}
}

division_template = {
	name = "Ethiopian Volunteers"

	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
		L_Inf_Bat = { x = 0 y = 2 }
	}
}

units = {
	division = {
		name = "Damazin Brigade"
		location = 4918
		division_template = "SLF Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.75
	}
	division = {
		name = "Sudanese Workers' Brigade"
		location = 4918
		division_template = "Ethiopian Volunteers"
		start_experience_factor = 0.3
		start_equipment_factor = 0.50
	}
	division = {
		name = "Rock of Shangul"
		location = 11042
		division_template = "SLF Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.75
	}
	division = {
		name = "Roseires Dam Protection Brigade"
		location = 11042
		division_template = "SLF Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.75
	}
}
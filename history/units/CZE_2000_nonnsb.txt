﻿division_template = {
	name = "Mechanizovaná Brigáda"

	division_names_group = CZE_ARM_01

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
		Arm_Inf_Bat = { x = 0 y = 2 }
		Arm_Inf_Bat = { x = 0 y = 3 }

		armor_Bat = { x = 1 y = 0 }
		Mech_Inf_Bat = { x = 1 y = 1 }
		SP_AA_Bat = { x = 1 y = 2 }
		SP_Arty_Bat = { x = 1 y = 3 }
	}
}

division_template = {
	name = "Výsadkový Pluk"

	division_names_group = CZE_ARM_02

	regiments = {
		L_Air_Inf_Bat = { x = 0 y = 0 }
		L_Air_Inf_Bat = { x = 0 y = 1 }
	}

	support = {
		L_Recce_Comp = { x = 0 y = 0 }
		L_Engi_Comp = { x = 0 y = 1 }
		combat_service_support_company = { x = 0 y = 2 }
	}
}

division_template = {
	name = "Dělostřelecký Pluk"

	division_names_group = CZE_ARM_03

	regiments = {
		Arty_Bat = { x = 0 y = 0 }
		SP_Arty_Bat = { x = 0 y = 1 }
		SP_Arty_Bat = { x = 0 y = 2 }
		Arty_Bat = { x = 1 y = 0 }
		SP_Arty_Bat = { x = 1 y = 1 }
	}

	support = {
		Arty_Battery = { x = 0 y = 0 }
		SP_Arty_Battery = { x = 0 y = 1 }
		L_Engi_Comp = { x = 0 y = 2 }
	}
}

units = {
	division = {
		name = "4. Brigáda Rychlého Nasazení"
		location = 3418 		#zatec
		division_template = "Mechanizovaná Brigáda"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "7. Mechanizovaná Brigáda"
		location = 6590 		#Hranice
		division_template = "Mechanizovaná Brigáda"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "2. Mechanizovaná Brigáda"
		location = 9541 	#Písek
		division_template = "Mechanizovaná Brigáda"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "6. Mobilizační Základna"
		location = 11556 		#Jihlava
		division_template = "Mechanizovaná Brigáda"
		start_experience_factor = 0.10
		start_equipment_factor = 0.01
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = MBT_2 					#T-72M
		amount = 541
	}

	add_equipment_to_stockpile = {
		type = Rec_tank_0				#BDRM
		producer = UKR
		amount = 182
	}

	add_equipment_to_stockpile = {
		type = IFV_3 					#BVP-2
		amount = 174
	}
	add_equipment_to_stockpile = {
		type = IFV_2 					#BVP-1K
		amount = 15
	}
	add_equipment_to_stockpile = {
		type = IFV_1 					#BVP-1
		amount = 612
	}
	add_equipment_to_stockpile = {
		type = APC_2 					#OT-90
		amount = 403
	}
	add_equipment_to_stockpile = {
		type = APC_1 					#OT-64
		amount = 577
	}

	add_equipment_to_stockpile = {
		type = SP_arty_0				#SpM 85
		amount = 8
	}

	add_equipment_to_stockpile = {
		type = SP_arty_1 				#SpGH DANA
		amount = 273
	}

	add_equipment_to_stockpile = {
		type = artillery_1 			#D-30
		amount = 148
	}
	add_equipment_to_stockpile = {
		type = SP_R_arty_1 				#RM-70
		amount = 135
	}
	add_equipment_to_stockpile = {
		type = Anti_tank_1		#Metis
		amount = 250
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_1 		#Konkurs
		producer = SOV
		amount = 21
	}
	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_0 		#Malyutka
		producer = SOV
		amount = 521
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_0 			#Tatra T813
		amount = 882
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_1 			#Tatra T815
		amount = 434
	}
	add_equipment_to_stockpile = {
		type = SP_Anti_Air_1 			#Strela-10
		 #version_name = "9K35 Strela-10"
		producer = SOV
		amount = 140
	}
	add_equipment_to_stockpile = {
		type = Anti_Air_0 				#SA-7
		producer = SOV
		amount = 600
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons1 		#vz. 58
		amount = 9000
	}

	add_equipment_to_stockpile = {
		type = command_control_equipment
		amount = 800
	}

	add_equipment_to_stockpile = {
		type = attack_helicopter1 		#Mil Mi-24
		producer = SOV
		amount = 34
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 	#Mil Mi-8
		producer = SOV
		amount = 14
	}

	add_equipment_to_stockpile = {
		type = transport_helicopter2	#Mil Mi-17
		amount = 42
	}

	add_equipment_to_stockpile = {
		type = transport_helicopter1 	#W-3 Sokół
		producer = POL
		amount = 11
	}

	add_equipment_production = {
		equipment = {
			type = infantry_weapons3
			creator = "CZE"
		}
		requested_factories = 1
		progress = 0.49
		efficiency = 60
	}

	add_equipment_production = {
		equipment = {
			type = command_control_equipment
			creator = "CZE"
		}
		requested_factories = 1
		progress = 0.80
		efficiency = 50
	}
}
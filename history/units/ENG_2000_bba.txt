﻿instant_effect = {
	##################
	#### Aircraft ####
	##################

	add_equipment_to_stockpile = {
		variant_name = "C-130 Hercules"
		type = large_plane_air_transport_airframe_1
		amount = 51
		producer = USA
	}

	add_equipment_to_stockpile = {
		variant_name = "C-17 Globemaster III"
		type = large_plane_air_transport_airframe_2
		amount = 25
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = cv_small_plane_strike_airframe_2
		variant_name = "Sea Harrier F(A).2"
		amount = 48
		producer = ENG
	}
	add_equipment_to_stockpile = {
		variant_name = "Jaguar GR"
		type = small_plane_cas_airframe_1
		amount = 53
		producer = ENG
	}
	add_equipment_to_stockpile = {
		variant_name = "BAE Hawk T1"
		type = small_plane_strike_airframe_1
		amount = 125
		producer = ENG
	}
	add_equipment_to_stockpile = {
		type = cv_small_plane_strike_airframe_2
		variant_name = "BAE Harrier GR7"
		amount = 63
		producer = ENG
	}
	add_equipment_to_stockpile = {
		variant_name = "Tornado GR1"
		type = medium_plane_cas_airframe_1
		amount = 58
		producer = ENG
	}
	add_equipment_to_stockpile = {
		variant_name = "Tornado GR4"
		type = medium_plane_cas_airframe_1
		amount = 63
		producer = ENG
	}
	add_equipment_to_stockpile = {
		variant_name = "Tornado F3"
		type = medium_plane_fighter_airframe_1
		amount = 63
		producer = ENG
	}
	add_equipment_to_stockpile = {
		variant_name = "Tornado F3 CSP"
		type = medium_plane_fighter_airframe_1
		amount = 30
		producer = ENG
	}
	add_equipment_to_stockpile = {
		variant_name = "Embraer EMB-312 Tucano"
		type = small_plane_strike_airframe_1
		amount = 84
		producer = BRA
	}
	add_equipment_to_stockpile = {
		variant_name = "English Electric Canberra"
		type = large_plane_airframe_1
		amount = 7
		producer = ENG
	}
	add_equipment_to_stockpile = {
		variant_name = "Hawker Siddeley Nimrod"
		type = large_plane_maritime_patrol_airframe_1
		amount = 26
		producer = ENG
	}
	add_equipment_to_stockpile = {
		type = medium_plane_maritime_patrol_airframe_1
		variant_name = "BN-2 Defender"
		amount = 9
		producer = ENG
	}
	add_equipment_to_stockpile = {
		variant_name = "Lockheed TriStar"
		type = large_plane_air_transport_airframe_1
		amount = 9
		producer = ENG
	}
	add_equipment_to_stockpile = {
		variant_name = "British Aerospace Jetstream"
		type = large_plane_air_transport_airframe_1
		amount = 11
		producer = ENG
	}
	add_equipment_to_stockpile = {
		variant_name = "E-3 Sentry"
		type = large_plane_awacs_airframe_2
		amount = 7
		producer = USA
	}
	# Missile OOB
	if = {
		limit = {
			has_dlc = "Gotterdammerung"
		}
		add_equipment_to_stockpile = {
			type = sam_missile_equipment_4
			amount = 30
		}
		add_equipment_to_stockpile = {
			type = guided_missile_equipment_4 #65 tomahawk sold by US in 1995. Fired from sub but added as ground for now
			amount = 65
			producer = USA
		}
	}
}
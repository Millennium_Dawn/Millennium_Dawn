﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = AS_Fighter1			#MiG-23
		amount = 20
	}

	add_equipment_to_stockpile = {
		type = MR_Fighter1		#MiG-21
		amount = 80
	}

	add_equipment_to_stockpile = {
		type = MR_Fighter2		#MiG-29
		amount = 6
	}

	add_equipment_to_stockpile = {
		type = transport_helicopter1			#Mi-8
		amount = 25
	}

	add_equipment_to_stockpile = {
		type = transport_helicopter1			#Mi-17
		#version_name = "Mil Mi-17"
		amount = 20
	}
}
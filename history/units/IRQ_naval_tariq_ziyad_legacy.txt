units = {
	fleet = {
		name = "Gulf Fleet"
		naval_base = 9045
		task_force = {
			name = "Arabian Gulf Fleet"
			location = 9045
			ship = { name = "Tariq Ibn Ziyad" definition = corvette start_experience_factor = 0.30 equipment = { missile_corvette_1 = { amount = 1 owner = IRQ version_name = "Assad Class" } } }
		}
	}
}
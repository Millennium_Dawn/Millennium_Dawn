﻿units = {

	#https://www.naval-history.net/xGW-RNOrganisation1947-2013.htm
	###Ship Index###

	#destroyer_hull_4 = Type 45 class

	#missile_frigate_1 = Type 23 class
	#attack_submarine_4 = Astute-class
	#attack_submarine_3 = Trafalgar-class
	#missile_submarine_3 = Vanguard-class submarine

	### Naval OOB ###
	fleet = {
		name = "Royal Navy Submarine Service"
		naval_base = 6395
		task_force = {
			name = "1st RN Submarine Squadron - HMNB Clyde"
			location = 6395

			ship = { name = "HMS Vanguard (S28)" definition = missile_submarine start_experience_factor = 0.65 equipment = { missile_submarine_hull_3 = { amount = 1 owner = ENG version_name = "Vanguard Class" } } }
			ship = { name = "HMS Victorious (S29)" definition = missile_submarine start_experience_factor = 0.65 equipment = { missile_submarine_hull_3 = { amount = 1 owner = ENG version_name = "Vanguard Class" } } }
			ship = { name = "HMS Vigilant (S30)" definition = missile_submarine start_experience_factor = 0.65 equipment = { missile_submarine_hull_3 = { amount = 1 owner = ENG version_name = "Vanguard Class" } } }
			ship = { name = "HMS Vengeance (S31)" definition = missile_submarine start_experience_factor = 0.65 equipment = { missile_submarine_hull_3 = { amount = 1 owner = ENG version_name = "Vanguard Class" } } }

			ship = { name = "HMS Sceptre (S104)" definition = attack_submarine start_experience_factor = 0.65 equipment = { attack_submarine_hull_2 = { amount = 1 owner = ENG version_name = "Swiftsure Class" } } }
			ship = { name = "HMS Spartan (S105)" definition = attack_submarine start_experience_factor = 0.65 equipment = { attack_submarine_hull_2 = { amount = 1 owner = ENG version_name = "Swiftsure Class" } } }
			ship = { name = "HMS Sovereign (S108)" definition = attack_submarine start_experience_factor = 0.65 equipment = { attack_submarine_hull_2 = { amount = 1 owner = ENG version_name = "Swiftsure Class" } } }
			ship = { name = "HMS Superb (S109)" definition = attack_submarine start_experience_factor = 0.65 equipment = { attack_submarine_hull_2 = { amount = 1 owner = ENG version_name = "Swiftsure Class" } } }
		}
		task_force = {
			name = "2nd RN Submarine Squadron - HMNB Devonport"
			location = 540

			ship = { name = "HMS Trafalgar (S107)" definition = attack_submarine start_experience_factor = 0.65 equipment = { attack_submarine_hull_2 = { amount = 1 owner = ENG version_name = "Trafalgar Class" } } }
			ship = { name = "HMS Turbulent (S87)" definition = attack_submarine start_experience_factor = 0.65 equipment = { attack_submarine_hull_2 = { amount = 1 owner = ENG version_name = "Trafalgar Class" } } }
			ship = { name = "HMS Tireless (S88)" definition = attack_submarine start_experience_factor = 0.65 equipment = { attack_submarine_hull_2 = { amount = 1 owner = ENG version_name = "Trafalgar Class" } } }
			ship = { name = "HMS Torbay (S90)" definition = attack_submarine start_experience_factor = 0.65 equipment = { attack_submarine_hull_2 = { amount = 1 owner = ENG version_name = "Trafalgar Class" } } }
			ship = { name = "HMS Trenchant (S91)" definition = attack_submarine start_experience_factor = 0.65 equipment = { attack_submarine_hull_2 = { amount = 1 owner = ENG version_name = "Trafalgar Class" } } }
			ship = { name = "HMS Talent (S92)" definition = attack_submarine start_experience_factor = 0.65 equipment = { attack_submarine_hull_2 = { amount = 1 owner = ENG version_name = "Trafalgar Class" } } }
			ship = { name = "HMS Triumph (S93)" definition = attack_submarine start_experience_factor = 0.65 equipment = { attack_submarine_hull_2 = { amount = 1 owner = ENG version_name = "Trafalgar Class" } } }
		}
	}
	fleet = {
		name = "Royal Navy Surface Fleet"
		naval_base = 9458
		task_force = {
			name = "RN Surface Fleet - HMNB Portsmouth"
			location = 9458

			ship = { name = "HMS Invincible (R05)" definition = helicopter_operator start_experience_factor = 0.65 equipment = { helicopter_operator_hull_1 = { amount = 1 owner = ENG version_name = "Invincible Class" } } }
			ship = { name = "HMS Illustrious (R06)" definition = helicopter_operator start_experience_factor = 0.65 equipment = { helicopter_operator_hull_1 = { amount = 1 owner = ENG  version_name = "Invincible Class" } } }

			ship = { name = "HMS Fearless (L10)" definition = helicopter_operator start_experience_factor = 0.65 equipment = { helicopter_operator_hull_1 = { amount = 1 owner = ENG version_name = "Fearless Class" } } }

			ship = { name = "HMS Ledbury (M30)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = ENG version_name = "Hunt Class" } } }
			ship = { name = "HMS Brocklesby (M33)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = ENG version_name = "Hunt Class" } } }
			ship = { name = "HMS Chiddingfold (M37)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = ENG version_name = "Hunt Class" } } }

			ship = { name = "HMS Cromer (M103)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_3 = { amount = 1 owner = ENG version_name = "Sandown Class" } } }
			ship = { name = "HMS Walney (M104)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_3 = { amount = 1 owner = ENG version_name = "Sandown Class" } } }

			ship = { name = "HMS Blazer (P279)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = ENG version_name = "Archer Class" } } }
			ship = { name = "HMS Ranger (P293)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = ENG version_name = "Archer Class" } } }
			ship = { name = "HMS Exploit (P167)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = ENG version_name = "Archer Class" } } }
			ship = { name = "HMS Archer (P264)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = ENG version_name = "Archer Class" } } }
		}
		task_force = {
			name = "2nd Frigate Squadron - HMNB Devonport"
			location = 540

			ship = { name = "HMS Cornwall (F99)" definition = frigate start_experience_factor = 0.65 equipment = { frigate_hull_2 = { amount = 1 owner = ENG version_name = "Type 22 Class - Batch 3" } } }
			ship = { name = "HMS Campbeltown (F86)" definition = frigate start_experience_factor = 0.65 equipment = { frigate_hull_2 = { amount = 1 owner = ENG version_name = "Type 22 Class - Batch 3" } } }
			ship = { name = "HMS Chatham (F87)" definition = frigate start_experience_factor = 0.65 equipment = { frigate_hull_2 = { amount = 1 owner = ENG version_name = "Type 22 Class - Batch 3" } } }
			ship = { name = "HMS Coventry (F98)" definition = frigate start_experience_factor = 0.65 equipment = { frigate_hull_2 = { amount = 1 owner = ENG version_name = "Type 22 Class - Broadsword Class" } } }
			ship = { name = "HMS Sheffield (F96)" definition = frigate start_experience_factor = 0.65 equipment = { frigate_hull_2 = { amount = 1 owner = ENG version_name = "Type 22 Class - Broadsword Class" } } }
		}
		task_force = {
			name = "3rd Destroyer Squadron - HMNB Portsmouth"
			location = 9458

			ship = { name = "HMS Glasgow (D88)" definition = destroyer start_experience_factor = 0.65 equipment = { destroyer_hull_2 = { amount = 1 owner = ENG version_name = "Type 42 Class" } } }
			ship = { name = "HMS Nottingham (D91)" definition = destroyer start_experience_factor = 0.65 equipment = { destroyer_hull_2 = { amount = 1 owner = ENG version_name = "Type 42 Class" } } }
			ship = { name = "HMS York (D98)" definition = destroyer start_experience_factor = 0.65 equipment = { destroyer_hull_2 = { amount = 1 owner = ENG version_name = "Type 42 Class" } } }
			ship = { name = "HMS Edinburgh (D97)" definition = destroyer start_experience_factor = 0.65 equipment = { destroyer_hull_2 = { amount = 1 owner = ENG version_name = "Type 42 Class" } } }
		}
		task_force = {
			name = "4th Frigate Squadron - HMNB Portsmouth"
			location = 9458

			ship = { name = "HMS Marlborough(F233)" definition = frigate start_experience_factor = 0.65 equipment = { frigate_hull_3 = { amount = 1 owner = ENG version_name = "Type 23 Class" } } }
			ship = { name = "HMS Lancaster (F229)" definition = frigate start_experience_factor = 0.65 equipment = { frigate_hull_3 = { amount = 1 owner = ENG version_name = "Type 23 Class" } } }
			ship = { name = "HMS Iron Duke (F234)" definition = frigate start_experience_factor = 0.65 equipment = { frigate_hull_3 = { amount = 1 owner = ENG version_name = "Type 23 Class" } } }
			ship = { name = "HMS Westminster (F237)" definition = frigate start_experience_factor = 0.65 equipment = { frigate_hull_3 = { amount = 1 owner = ENG version_name = "Type 23 Class" } } }
			ship = { name = "HMS Richmond (F239)" definition = frigate start_experience_factor = 0.65 equipment = { frigate_hull_3 = { amount = 1 owner = ENG version_name = "Type 23 Class" } } }
			ship = { name = "HMS Grafton (F80)" definition = frigate start_experience_factor = 0.65 equipment = { frigate_hull_3 = { amount = 1 owner = ENG version_name = "Type 23 Class" } } }
			
		}
		task_force = {
			name = "5th Destroyer Squadron - HMNB Portsmouth"
			location = 9458

			ship = { name = "HMS Cardiff (D108)" definition = destroyer start_experience_factor = 0.65 equipment = { destroyer_hull_2 = { amount = 1 owner = ENG version_name = "Type 42 Class" } } }
			ship = { name = "HMS Newcastle (D87)" definition = destroyer start_experience_factor = 0.65 equipment = { destroyer_hull_2 = { amount = 1 owner = ENG version_name = "Type 42 Class" } } }
			ship = { name = "HMS Exeter (D89)" definition = destroyer start_experience_factor = 0.65 equipment = { destroyer_hull_2 = { amount = 1 owner = ENG version_name = "Type 42 Class" } } }
			
			ship = { name = "HMS Manchester (D95)" definition = destroyer start_experience_factor = 0.65 equipment = { destroyer_hull_2 = { amount = 1 owner = ENG version_name = "Type 42 Class" } } }
			ship = { name = "HMS Gloucester (D96)" definition = destroyer start_experience_factor = 0.65 equipment = { destroyer_hull_2 = { amount = 1 owner = ENG version_name = "Type 42 Class" } } }
		}
		task_force = {
			name = "6th Frigate Squadron - HMNB Portsmouth"
			location = 9458

			ship = { name = "HMS Montrose (F236)" definition = frigate start_experience_factor = 0.65 equipment = { frigate_hull_3 = { amount = 1 owner = ENG version_name = "Type 23 Class" } } }
			ship = { name = "HMS Norfolk (F230)" definition = frigate start_experience_factor = 0.65 equipment = { frigate_hull_3 = { amount = 1 owner = ENG version_name = "Type 23 Class" } } }
			ship = { name = "HMS Monmouth (F235)" definition = frigate start_experience_factor = 0.65 equipment = { frigate_hull_3 = { amount = 1 owner = ENG version_name = "Type 23 Class" } } }
			ship = { name = "HMS Sutherland (F81)" definition = frigate start_experience_factor = 0.65 equipment = { frigate_hull_3 = { amount = 1 owner = ENG version_name = "Type 23 Class" } } }
		}
	}
	fleet = {
		name = "Standing Royal Navy Deployments"
		naval_base = 9458
		task_force = {
			name = "RN Commander United Kingdom Carrier Strike Group - HMNB Portsmouth"
			location = 9458

			ship = { name = "HMS Ark Royal (R07)" definition = helicopter_operator start_experience_factor = 0.65 equipment = { helicopter_operator_hull_1 = { amount = 1 owner = ENG version_name = "Invincible Class" } } }
			ship = { name = "HMS Ocean (L12)" definition = helicopter_operator start_experience_factor = 0.65 equipment = { helicopter_operator_hull_2 = { amount = 1 owner = ENG version_name = "Ocean Class" } } }

			ship = { name = "HMS Southampton (D90)" definition = destroyer start_experience_factor = 0.65 equipment = { destroyer_hull_2 = { amount = 1 owner = ENG version_name = "Type 42 Class" } } }
			ship = { name = "HMS Liverpool (D92)" definition = destroyer start_experience_factor = 0.65 equipment = { destroyer_hull_2 = { amount = 1 owner = ENG version_name = "Type 42 Class" } } }
			ship = { name = "HMS Cumberland (F85)" definition = frigate start_experience_factor = 0.65 equipment = { frigate_hull_2 = { amount = 1 owner = ENG version_name = "Type 22 Class - Batch 3" } } }
			ship = { name = "HMS Northumberland (F238)" definition = frigate start_experience_factor = 0.65 equipment = { frigate_hull_3 = { amount = 1 owner = ENG version_name = "Type 23 Class" } } }
			ship = { name = "HMS Somerset (F82)" definition = frigate start_experience_factor = 0.65 equipment = { frigate_hull_3 = { amount = 1 owner = ENG version_name = "Type 23 Class" } } }

			ship = { name = "HMS Splendid (S106)" definition = attack_submarine start_experience_factor = 0.65 equipment = { attack_submarine_hull_2 = { amount = 1 owner = ENG version_name = "Swiftsure Class" } } }		
		}
		task_force = {
			name = "RN Commander Littoral Strike Group - HMNB Gibraltar"
			location = 4135

			ship = { name = "HMS Pembroke (M107)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_3 = { amount = 1 owner = ENG version_name = "Sandown Class" } } }

			ship = { name = "HMS Raider (P275)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = ENG version_name = "Archer Class" } } }
			ship = { name = "HMS Dasher (P280)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = ENG version_name = "Archer Class" } } }
		}
		task_force = {
			name = "RN Commander Littoral Strike Group - Mare Harbour"
			location = 14578

			ship = { name = "HMS Leeds Castle (P258)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = ENG version_name = "Castle Class" } } }

			ship = { name = "HMS Puncher (P291)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = ENG version_name = "Archer Class" } } }
		}
		task_force = {
			name = "RN Commander Littoral Strike Group - UK National Support Element Bahrain"
			location = 2691

			ship = { name = "HMS Cattistock (M31)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = ENG version_name = "Hunt Class" } } }
			ship = { name = "HMS Hurworth (M39)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = ENG version_name = "Hunt Class" } } }
			ship = { name = "HMS Bangor (M109)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_3 = { amount = 1 owner = ENG version_name = "Sandown Class" } } }

			ship = { name = "HMS Biter (P270)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = ENG version_name = "Archer Class" } } }
			ship = { name = "HMS Pursuer (P273)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = ENG version_name = "Archer Class" } } }
		}
		task_force = {
			name = "RN Commander Littoral Strike Group - UK Joint Logistics Base Oman"
			location = 10760

			ship = { name = "HMS Brecon (M29)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = ENG version_name = "Hunt Class" } } }
			ship = { name = "HMS Middleton (M34)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = ENG version_name = "Hunt Class" } } }
			ship = { name = "HMS Atherstone (M38)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = ENG version_name = "Hunt Class" } } }

			ship = { name = "HMS Example (P165)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = ENG version_name = "Archer Class" } } }
		}
		task_force = {
			name = "RN Commander Littoral Strike Group - British Defence Singapore Support Unit"
			location = 12299

			ship = { name = "HMS Argyll (F231)" definition = frigate start_experience_factor = 0.65 equipment = { frigate_hull_3 = { amount = 1 owner = ENG version_name = "Type 23 Class" } } }

			ship = { name = "HMS Trumpeter (P294)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = ENG version_name = "Archer Class" } } }
			ship = { name = "HMS Tracker (P274)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = ENG version_name = "Archer Class" } } }
		}
		task_force = {
			name = "RN Fishery Protection Squadron HMNB Clyde"
			location = 6395

			ship = { name = "HMS Dumbarton Castle (P265)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = ENG version_name = "Castle Class" } } }
			ship = { name = "HMS Penzance (M106)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_3 = { amount = 1 owner = ENG version_name = "Sandown Class" } } }

			ship = { name = "HMS Smiter (P272)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = ENG version_name = "Archer Class" } } }
			ship = { name = "HMS Explorer (P164)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = ENG version_name = "Archer Class" } } }
			ship = { name = "HMS Express (P163)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = ENG version_name = "Archer Class" } } }
			ship = { name = "HMS Charger (P292)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = ENG version_name = "Archer Class" } } }
		}
	}
	#Newly Built Ships - Awaiting Commissioning
	fleet = {
		name = "Delivered Naval Ships (Non-Commissioned) - Undergoing Work Ups & Sea Trials"
		naval_base = 9392
		task_force = {
			name = "Delivered Surface Naval Ships - Rosyth Dockyard"
			location = 9392

			ship = { name = "HMS Kent (F78)" definition = frigate start_experience_factor = 0.35 equipment = { frigate_hull_3 = { amount = 1 owner = ENG version_name = "Type 23 Class" } } }
			ship = { name = "HMS Portland (F79)" definition = frigate start_experience_factor = 0.1 equipment = { frigate_hull_3 = { amount = 1 owner = ENG version_name = "Type 23 Class" } } }

			ship = { name = "HMS Ramsey (M110)" definition = corvette start_experience_factor = 0.25 equipment = { corvette_hull_3 = { amount = 1 owner = ENG version_name = "Sandown Class" } } }
			ship = { name = "HMS Blyth (M111)" definition = corvette start_experience_factor = 0.05 equipment = { corvette_hull_3 = { amount = 1 owner = ENG version_name = "Sandown Class" } } }

		}
	}

}
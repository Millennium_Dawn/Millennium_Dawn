﻿division_template = {
	name = "Congolese Volunteer Brigade"

	regiments = {
		Militia_Bat = { x = 0 y = 0 }
		Militia_Bat = { x = 0 y = 1 }
		Militia_Bat = { x = 0 y = 2 }
	}
}

units = {
	division = {
		name = "Yambio Tigers"
		location = 1953
		division_template = "Congolese Volunteer Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.5
	}
	division = {
		name = "Yambio Leopards"
		location = 1953
		division_template = "Congolese Volunteer Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.5
	}
	division = {
		name = "Yei Lions"
		location = 7996
		division_template = "Congolese Volunteer Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.5
	}
	division = {
		name = "Yei Rhinos"
		location = 7996
		division_template = "Congolese Volunteer Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.5
	}
}
﻿units = {

	### Naval OOB ###
	fleet = {
		name = "South African Navy"
		naval_base = 12589
		task_force = {
			name = "South African Navy"
			location = 12589
			ship = { name = "SAS Jan Smuts" definition = corvette start_experience_factor = 0.50 equipment = { corvette_hull_1 = { amount = 1 owner = SAF creator = SAF version_name = "Warrior Class" } }  }
			ship = { name = "SAS Shaka" definition = corvette start_experience_factor = 0.50 equipment = { corvette_hull_1 = { amount = 1 owner = SAF creator = SAF version_name = "Warrior Class" } }  }
			ship = { name = "SAS Adam Kok" definition = corvette start_experience_factor = 0.50 equipment = { corvette_hull_1 = { amount = 1 owner = SAF creator = SAF version_name = "Warrior Class" } }  }
			ship = { name = "SAS Sekhukhuni" definition = corvette start_experience_factor = 0.50 equipment = { corvette_hull_1 = { amount = 1 owner = SAF creator = SAF version_name = "Warrior Class" } }  }
			ship = { name = "SAS Isaac Dyobha" definition = corvette start_experience_factor = 0.50 equipment = { corvette_hull_1 = { amount = 1 owner = SAF creator = SAF version_name = "Warrior Class" } }  }
			ship = { name = "SAS René Sethren" definition = corvette start_experience_factor = 0.50 equipment = { corvette_hull_1 = { amount = 1 owner = SAF creator = SAF version_name = "Warrior Class" } }  }
			ship = { name = "SAS Galeshewe" definition = corvette start_experience_factor = 0.50 equipment = { corvette_hull_1 = { amount = 1 owner = SAF creator = SAF version_name = "Warrior Class" } }  }
			ship = { name = "SAS Job Masego" definition = corvette start_experience_factor = 0.50 equipment = { corvette_hull_1 = { amount = 1 owner = SAF creator = SAF version_name = "Warrior Class" } }  }
			ship = { name = "SAS Makhanda" definition = corvette start_experience_factor = 0.50 equipment = { corvette_hull_1 = { amount = 1 owner = SAF creator = SAF version_name = "Warrior Class" } }  }
		}
		task_force = {
			name = "South African Underwater Navy"
			location = 12589
			ship = { name = "SAS Spear" definition = attack_submarine start_experience_factor = 0.50 equipment = { attack_submarine_hull_1 = { amount = 1 owner = SAF creator = FRA version_name = "Daphné Class" } }  }
			ship = { name = "SAS Umkhonto" definition = attack_submarine start_experience_factor = 0.50 equipment = { attack_submarine_hull_1 = { amount = 1 owner = SAF creator = FRA version_name = "Daphné Class" } }  }
			ship = { name = "SAS Assegaai" definition = attack_submarine start_experience_factor = 0.50 equipment = { attack_submarine_hull_1 = { amount = 1 owner = SAF creator = FRA version_name = "Daphné Class" } }  }
		}
	}
}

﻿division_template = {
	name = "DLF Brigade"

	regiments = {
		Militia_Bat = { x = 0 y = 0 }
		Militia_Bat = { x = 0 y = 1 }
		Militia_Bat = { x = 1 y = 0 }
		Militia_Bat = { x = 1 y = 1 }
	}
}

division_template = {
	name = "JEM Brigade"

	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
		L_Inf_Bat = { x = 1 y = 0 }
		L_Inf_Bat = { x = 1 y = 1 }
	}
}

units = {
	division = {
		name = "5th DLF Brigade"
		location = 10739
		division_template = "DLF Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.75
	}

	division = {
		name = "3rd JEM Brigade"
		location = 10739
		division_template = "JEM Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.75
	}

	division = {
		name = "4th JEM Brigade"
		location = 10739
		division_template = "JEM Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.75
	}
}
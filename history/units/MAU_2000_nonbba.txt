instant_effect = {

	add_equipment_to_stockpile = {
		#variant_name = "Piper PA-31T Cheyenne"
		type = L_Strike_fighter1
		amount = 2
		producer = USA
	}
	add_equipment_to_stockpile = {
		#variant_name = "C-20 Gulfstream"
		type = transport_plane1
		amount = 1
		producer = USA
	}
	add_equipment_to_stockpile = {
		#variant_name = "Harbin Y-12"
		type = transport_plane1
		amount = 2
		producer = CHI
	}
	add_equipment_to_stockpile = {
		type = naval_plane1
		#variant_name = "BN-2 Defender"
		amount = 1
		producer = ENG
	}

}
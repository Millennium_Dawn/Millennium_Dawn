﻿division_template = {
	name = "Special Warfare Brigade"

	regiments = {
		Special_Forces = { x = 0 y = 0 }
		Special_Forces = { x = 0 y = 1 }
	}
	priority = 2
}

division_template = {
	name = "Air Cavalry Brigade"

	regiments = {
		L_Air_assault_Bat = { x = 0 y = 0 }
		L_Air_assault_Bat = { x = 0 y = 1 }
		L_Air_assault_Bat = { x = 0 y = 2 }
		L_Air_assault_Bat = { x = 0 y = 3 }
	}
	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		armor_Recce_Comp = { x = 0 y = 1 }
		combat_service_support_company = { x = 0 y = 2 }
	}
}

division_template = {
	name = "Armored Brigade"

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		armor_Bat = { x = 0 y = 1 }
		armor_Bat = { x = 0 y = 2 }
		Mech_Inf_Bat = { x = 0 y = 3 }
		
		SP_Arty_Bat = { x = 1 y = 0 }
	}
	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		armor_Recce_Comp = { x = 0 y = 1 }
		combat_service_support_company = { x = 0 y = 2 }
	}
}

division_template = {
	name = "Mechanized Division"

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
		Mech_Inf_Bat = { x = 0 y = 2 }
		armor_Bat = { x = 0 y = 3 }
		
		SP_Arty_Bat = { x = 1 y = 0 }
	}
	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		armor_Recce_Comp = { x = 0 y = 1 }
		combat_service_support_company = { x = 0 y = 2 }
	}
}

division_template = {
	name = "Infantry Division"

	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
		L_Inf_Bat = { x = 0 y = 2 }
		Arty_Bat = { x = 0 y = 3 }
		
		L_Inf_Bat = { x = 1 y = 0 }
		L_Inf_Bat = { x = 1 y = 1 }
		L_Inf_Bat = { x = 1 y = 2 }
		Arty_Bat = { x = 1 y = 3 }
		
		L_Inf_Bat = { x = 2 y = 0 }
		L_Inf_Bat = { x = 2 y = 1 }
		L_Inf_Bat = { x = 2 y = 2 }
		Arty_Bat = { x = 2 y = 3 }
	}
	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		armor_Recce_Comp = { x = 0 y = 1 }
		combat_service_support_company = { x = 0 y = 2 }
	}
}

division_template = {
	name = "Marine Division"

	regiments = {
		L_Marine_Bat = { x = 0 y = 0 }
		L_Marine_Bat = { x = 0 y = 1 }
		L_Marine_Bat = { x = 0 y = 2 }

		L_Marine_Bat = { x = 1 y = 0 }
		L_Marine_Bat = { x = 1 y = 1 }
		L_Marine_Bat = { x = 1 y = 2 }

		L_Marine_Bat = { x = 2 y = 0 }
		L_Marine_Bat = { x = 2 y = 1 }
		L_Marine_Bat = { x = 2 y = 2 }

		Arty_Bat = { x = 3 y = 0 }
		Arty_Bat = { x = 3 y = 1 }
		Arty_Bat = { x = 3 y = 2 }
	}
	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		armor_Recce_Comp = { x = 0 y = 1 }
		combat_service_support_company = { x = 0 y = 2 }
	}
}

#divs 66th and 99th
#new marine corps:
# 66th bde,  77th bde, 99 bde

units = {
	### ROC Marine Corps ###
	division = {
		name = "66th Marine Division"
		location = 13274
		division_template = "Marine Division"
		start_experience_factor = 0.55
		start_equipment_factor = 0.01
	}
	division = {
		name = "99th Marine Division"
		location = 7186
		division_template = "Marine Division"
		start_experience_factor = 0.55
		start_equipment_factor = 0.01
	}

	### Aviation and Special Forces Command ###

	#862 Special Warfare Brigade
	division = {
		name = "862nd Special Warfare Brigade"
		location = 7186
		division_template = "Special Warfare Brigade"
		start_experience_factor = 0.7
		start_equipment_factor = 0.01
	}

	#60th Air Cavalry Division
	division = {
		name = "601st Air Cavalry Brigade"
		location = 7214
		division_template = "Air Cavalry Brigade"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}

	#602 Air Cavalry Brigade
	division = {
		name = "603rd Air Cavalry Brigade"
		location = 7214
		division_template = "Air Cavalry Brigade"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}

	### 6th Army Corps ### Northern Taiwan###
	#div
	#16, 26, 49, 51

	division = {
		name = "16th Infantry Division"
		location = 7186
		division_template = "Infantry Division"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
	division = {
		name = "26th Infantry Division"
		location = 7186
		division_template = "Infantry Division"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
	division = {
		name = "49th Infantry Division"
		location = 11959
		division_template = "Infantry Division"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
	division = {
		name = "51st Infantry Division"
		location = 7214
		division_template = "Infantry Division"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
	division = {
		name = "2nd Armor Brigade"
		location = 7214
		division_template = "Armored Brigade"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
	division = {
		name = "4th Armor Brigade"
		location = 7186
		division_template = "Armored Brigade"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
	### 8th Army Corps ### Southern Taiwan
	#2 divs

	division = {
		name = "5th Infantry Division" #fake name
		location = 13275
		division_template = "Infantry Division"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
	division = {
		name = "6th Infantry Division" #fake name
		location = 14608
		division_template = "Infantry Division"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
	division = {
		name = "18th Infantry Division" #fake name
		location = 12068
		division_template = "Infantry Division"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
	division = {
		name = "5th Armor Brigade"
		location = 12068
		division_template = "Armored Brigade"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}


	### 10th Army Corps ### Central Taiwan
	#2divs
	#3rd mech, 8th mech div
	#6th ind arm bde

	division = {
		name = "3rd Mechanized Division"
		location = 4096
		division_template = "Mechanized Division"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
	division = {
		name = "8th Mechanized Division"
		location = 11959
		division_template = "Mechanized Division"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
	division = {
		name = "6th Armor Brigade"
		location = 11959
		division_template = "Armored Brigade"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = infantry_weapons1 #"Steyr AUG A1"
		amount = 2500
		producer = AUS
	}
	add_equipment_to_stockpile = {
		type = infantry_weapons1 #M16A1
		amount = 20000
		producer = TAI
	}
	add_equipment_to_stockpile = {
		type = infantry_weapons2 #T65
		amount = 60000
		producer = TAI
	}

	add_equipment_to_stockpile = {
		type = command_control_equipment1
		amount = 4500
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = Anti_tank_1
		amount = 1400
		producer = SWE
	}
	add_equipment_to_stockpile = {
		type = Anti_Air_0
		amount = 700
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = util_vehicle_1 #"M561 Gama Goat"
		amount = 600
		producer = USA
	}

	####Vehicles##

	add_equipment_to_stockpile = {
		type = mbt_hull_0
		variant_name = "M60A3"
		amount = 189
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = mbt_hull_0
		variant_name = "CM-12"
		amount = 100
		producer = TAI
	}
	add_equipment_to_stockpile = {
		type = mbt_hull_0
		variant_name = "CM-11"
		amount = 450
		producer = TAI
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_0
		amount = 675
		producer = TAI
		variant_name = "M41D"
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_0
		amount = 300
		producer = USA
		variant_name = "V150 Cadillac Commando"
	}
	add_equipment_to_stockpile = {
		type = apc_hull_1
		variant_name = "CM-21"
		amount = 100
		producer = TAI
	}
	add_equipment_to_stockpile = {
		type = apc_hull_1
		variant_name = "M113"
		amount = 650
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_2 #Humvee
		amount = 3000
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = artillery_0
		variant_name = "M101"
		amount = 650
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = artillery_0
		variant_name = "M114"
		amount = 250
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = artillery_0
		variant_name = "M1"
		amount = 90
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = artillery_0
		variant_name = "M115"
		amount = 70
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = spart_hull_0 #M108
		variant_name = "M108"
		amount = 100
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "M110"
		amount = 60
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = spart_hull_1
		variant_name = "M109A3"
		amount = 110
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "Kung Feng"
		amount = 120
	}

	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_0 # USA_Heavy_Anti_tank_0:0 "BGM-71 TOW" 1,000
		amount = 1000
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = Anti_Air_1 #USA_Anti_Air_1:0 "FIM-92 Stinger"
		amount = 480
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = spaa_hull_1
		amount = 100
		producer = USA
		variant_name = "AN/TWQ-1 Avenger"
	}

	add_equipment_to_stockpile = {
		type = spaa_hull_0 	# "MIM-72 Chaparral" & "MIM-104 Patriot"
		amount = 27
		producer = USA
		variant_name = "MIM-72 Chaparral"
	}

	#Helicopters
	add_equipment_to_stockpile = {
		type = transport_helicopter1	#USA_transport_helicopter1:0 "Bell UH-1 Iroquois"
		amount = 110
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 	#USA_transport_helicopter1:0 "Bell 206 aka TH-67"
		amount = 30
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #USA_transport_helicopter1:0 "Bell 206 aka TH-67" 30
		amount = 30
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = attack_helicopter_hull_0
		amount = 26
		producer = USA
		variant_name = "OH-58D Kiowa Warrior"
	}
	add_equipment_to_stockpile = {
		type = attack_helicopter_hull_0
		amount = 53
		producer = USA
		variant_name = "AH-1 Cobra"
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Boeing CH-47 Chinook
		amount = 8
		producer = USA
	}
}
﻿instant_effect = {
	add_equipment_to_stockpile = {
		variant_name = "P-3 Orion"
		type = large_plane_maritime_patrol_airframe_1
		amount = 6
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "C-130 Hercules"
		type = large_plane_air_transport_airframe_1
		amount = 7
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "A-4 Skyhawk"
		type = cv_small_plane_strike_airframe_1
		amount = 19
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "Aermacchi MB-339"
		type = small_plane_strike_airframe_1
		amount = 17
		producer = ITA
	}
}